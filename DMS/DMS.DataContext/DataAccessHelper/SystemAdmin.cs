﻿using AutoMapper;
using DMS.Models.DTO;
using DMS.Models.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.DataContext.DataAccessHelper
{
    public class SystemAdmin
    {
        #region Users
        public List<UserDTO> GetAllUsers()
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<UserDTO> lst = new List<UserDTO>();
                //List<User> sourceList = context.Users.ToList();
                //lst = AutoMapperGenericsHelper<User, UserDTO>.ConvertToEntityList(sourceList);

                //return lst;

                lst = (from us in context.Users
                       join r in context.Roles on us.RoleId equals r.RoleId into rl
                       from r in rl.DefaultIfEmpty()
                       join s in context.SiteInfoes on us.SiteID equals s.ID into si
                       from s in si.DefaultIfEmpty()
                       select new UserDTO
                       {
                           UID = us.UID,
                           SiteID = s.ID,
                           SiteName = s.SiteID,
                           UserId = us.UserId,
                           Password = us.Password,
                           RoleId = us.RoleId,
                           Role = r.Description,
                           Name = us.Name,
                           Email = us.Email,
                           Mobile = us.Mobile,
                           Blocked = us.Blocked,
                           Active = us.Active,
                           CurrentAccessed = us.CurrentAccessed,
                           LastAccessed = us.LastAccessed,
                           NoOfAttempt = us.NoOfAttempt,
                           CreatedBy = us.CreatedBy,
                           CreationDate = us.CreationDate,
                           ModifiedBy = us.ModifiedBy,
                           ModificationDate = us.ModificationDate,
                           LastPasswordChanged = us.LastPasswordChanged
                       }).ToList();

                return lst;
            }
        }

        public List<UserDTO> GetAllUsersBySite(int siteId)
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<UserDTO> lst = new List<UserDTO>();
                //List<User> sourceList = context.Users.ToList();
                //lst = AutoMapperGenericsHelper<User, UserDTO>.ConvertToEntityList(sourceList);

                //return lst;

                lst = (from us in context.Users
                       join r in context.Roles on us.RoleId equals r.RoleId into rl
                       from r in rl.DefaultIfEmpty()
                       join s in context.SiteInfoes on us.SiteID equals s.ID into si
                       from s in si.DefaultIfEmpty()                       
                       select new UserDTO
                       {
                           UID = us.UID,
                           SiteID = s.ID,
                           SiteName = s.SiteID,
                           UserId = us.UserId,
                           Password = us.Password,
                           RoleId = us.RoleId,
                           Role = r.Description,
                           Name = us.Name,
                           Email = us.Email,
                           Mobile = us.Mobile,
                           Blocked = us.Blocked,
                           Active = us.Active,
                           CurrentAccessed = us.CurrentAccessed,
                           LastAccessed = us.LastAccessed,
                           NoOfAttempt = us.NoOfAttempt,
                           CreatedBy = us.CreatedBy,
                           CreationDate = us.CreationDate,
                           ModifiedBy = us.ModifiedBy,
                           ModificationDate = us.ModificationDate,
                           LastPasswordChanged = us.LastPasswordChanged
                       }).Where(r => r.RoleId == (int)UserRoles.OPERATIONAL_MANAGER && r.SiteID == siteId).ToList();

                return lst;
            }
        }

        

        public UserStatus CheckUserStatusByUserId(string userId, string password)
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<UserDTO> lst = new List<UserDTO>();
                User user = (context.Users
                                    .Where(b => b.UserId == userId && b.Password == password)
                                   ).ToList().FirstOrDefault();

                if (user == null)
                {
                    return UserStatus.NOT_EXIST;
                }
                else if (user != null && user.UserId == userId)
                {
                    if (user.Blocked == true)
                        return UserStatus.BLOCKED;
                    else
                        return UserStatus.Active;
                }
                else
                    return UserStatus.USER_INCORRECT;
            }
        }

        public UserStatus CheckUserStatusByEmail(string emailId)
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<UserDTO> lst = new List<UserDTO>();
                User user = (context.Users
                                    .Where(b => b.Email == emailId)
                                   ).ToList().FirstOrDefault();

                if (user == null)
                {
                    return UserStatus.NOT_EXIST;
                }
                else if (user != null && user.Email == emailId)
                {
                    if (user.Blocked == true)
                        return UserStatus.BLOCKED;
                    else
                        return UserStatus.Active;
                }
                else
                    return UserStatus.USER_INCORRECT;
            }
        }

        public UserDTO GetUserById(int uId)
        {
            UserDTO userDto = new UserDTO();
            using (DMSEntities context = new DMSEntities())
            {
                User user = (context.Users                             
                                     .Where(b => b.UID == uId)
                                    ).ToList().FirstOrDefault();

                if (user != null)
                {
                    userDto = AutoMapperGenericsHelper<User, UserDTO>.ConvertToEntity(user);
                }
            }

             
            return userDto;
        }
 
        public UserDTO GetUserByUserName(string userName)
        {
            UserDTO userDto = new UserDTO();
            using (DMSEntities context = new DMSEntities())
            {
                User user = (context.Users
                                     .Where(b => b.UserId == userName)
                                    ).ToList().FirstOrDefault();

                if (user != null)
                {
                    userDto = AutoMapperGenericsHelper<User, UserDTO>.ConvertToEntity(user);
                }
            }


            return userDto;
        }

        public List<RoleDTO> GetAllRoles()
        {
            List<RoleDTO> lst = new List<RoleDTO>();
            using (DMSEntities context = new DMSEntities())
            {
                List<Role> sourceList = context.Roles.ToList();
                lst = AutoMapperGenericsHelper<Role, RoleDTO>.ConvertToEntityList(sourceList);

                return lst;
            }
        }

        public List<RoleDTO> GetAllRolesForDistribute()
        {
            List<RoleDTO> lst = new List<RoleDTO>();
            using (DMSEntities context = new DMSEntities())
            {
                List<int> allUserRoles = (from d in context.Users
                                      select (int)d.RoleId).Distinct().ToList();

                List<Role> sourceList = (from r in context.Roles
                                         where allUserRoles.Contains(r.RoleId)
                                         select r).ToList();

                lst = AutoMapperGenericsHelper<Role, RoleDTO>.ConvertToEntityList(sourceList);

                lst = lst.Where(r => r.RoleId != (int)UserRoles.SUPER_ADMINISTRATION
                                && r.RoleId != (int)UserRoles.SITE_USER
                                && r.RoleId != (int)UserRoles.SECRETARY
                                && r.RoleId != (int)UserRoles.PROJECT_MANAGER).ToList();
                return lst;
            }
        }


        public List<DocumentDistributeToDTO> GetAllRolesForDistributeWithFlags(int documentId)
        {
            List<DocumentDistributeToDTO> lst = new List<DocumentDistributeToDTO>();
            using (DMSEntities context = new DMSEntities())
            {

                List<DistributeList_Result> checkedList = context.DistributeList(documentId).ToList();

                lst = AutoMapperGenericsHelper<DistributeList_Result, DocumentDistributeToDTO>.ConvertToEntityList(checkedList);

                return lst;
            }
        }

        public int CreateUser(UserDTO userDto)
        {
            User user = AutoMapperGenericsHelper<UserDTO, User>.ConvertToEntity(userDto);

            using (DMSEntities context = new DMSEntities())
            {
                user.CreationDate = DateTime.Now;
                context.Users.Add(user);
                context.SaveChanges();

                return user.UID;
            }
        } 

        public int UpdateUser(UserDTO userDto)
        {
            using (DMSEntities context = new DMSEntities())
            {
                User user = context.Users.Where(b => b.UID == userDto.UID).FirstOrDefault();

                user.UserId = userDto.UserId;
                user.Name = userDto.Name;
                user.Password = userDto.Password;
                user.RoleId = userDto.RoleId;
                user.SiteID = userDto.SiteID;
                user.Email = userDto.Email;
                user.Mobile = userDto.Mobile;
                user.ModificationDate = DateTime.Now;
                user.ModifiedBy = userDto.ModifiedBy;
                user.Active = userDto.Active;

                context.SaveChanges();

                return user.UID;
            }
        }

        #endregion

        #region User Priviledges

        public List<string> GetPrivilege(string userId)
        {
            List<string> privilegeCode = new List<string>();

            using (DMSEntities context = new DMSEntities())
            {
                if (userId == "admin")
                {
                    privilegeCode = (from b in context.Privileges
                                     select b.PrivilegeCode
                                    ).ToList();
                }
                else
                {
                    privilegeCode = (from b in context.User_Privilege
                                     where b.UserId == userId
                                     select b.PrivilegeCode
                                    ).ToList();
                }
                return privilegeCode;
            }
        }

        #endregion

        #region Project Sites

        public List<SiteDTO> GetAllProjectSites()
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<SiteDTO> lst = new List<SiteDTO>();
                List<SiteInfo> sourceList = context.SiteInfoes.ToList();

                lst = AutoMapperGenericsHelper<SiteInfo, SiteDTO>.ConvertToEntityList(sourceList);

                return lst;
            }
        } 

        public SiteDTO GetProjectSiteById(int siteId)
        {
            SiteDTO siteDto = new SiteDTO();
            using (DMSEntities context = new DMSEntities())
            {
                SiteInfo site = (context.SiteInfoes
                                     .Where(b => b.ID == siteId)
                                    ).ToList().FirstOrDefault();

                if (site != null)
                {
                    siteDto = AutoMapperGenericsHelper<SiteInfo, SiteDTO>.ConvertToEntity(site);
                }
            }
             
            return siteDto;
        } 

        public int CreateProjectSite(SiteDTO siteDto)
        {
            //SiteDTO siteDto = new SiteDTO();
            SiteInfo site = AutoMapperGenericsHelper<SiteDTO, SiteInfo>.ConvertToEntity(siteDto);

            using (DMSEntities context = new DMSEntities())
            {
                site.CreatedOn = DateTime.Now;
                context.SiteInfoes.Add(site);
                context.SaveChanges();

                return site.ID;
            }
        }

        public int UpdateProjectSite(SiteDTO siteDto)
        { 
            using (DMSEntities context = new DMSEntities())
            {
                SiteInfo site = context.SiteInfoes.Where(b => b.ID == siteDto.ID).FirstOrDefault();

                site.SiteID = siteDto.SiteID;
                site.SiteName = siteDto.SiteName;
                site.Consultant = siteDto.Consultant;
                site.Contractor = siteDto.Contractor;
                site.Client = siteDto.Client;
                site.ProjectManager = siteDto.ProjectManager;
                site.AddressLine1 = siteDto.AddressLine1;
                site.AddressLine2 = siteDto.AddressLine2;
                site.POBoxNo = siteDto.POBoxNo;
                site.ModifiedOn = DateTime.Now;
                site.ModifiedBy = siteDto.ModifiedBy;
                site.Active = siteDto.Active;
                site.IsHeadOffice = siteDto.IsHeadOffice;

                context.SaveChanges();

                return site.ID;
            }
        }

        #endregion
    }
}
