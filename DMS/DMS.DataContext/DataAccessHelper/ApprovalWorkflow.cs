﻿using DMS.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.DataContext.DataAccessHelper
{
    public class ApprovalWorkflow
    {
        public List<WorkflowHeaderDTO> FillModule()
        {
            List<WorkflowHeaderDTO> lst = new List<WorkflowHeaderDTO>();
            try
            {
                using (DMSEntities context = new DMSEntities())
                {
                    
                    List<WorkflowHeader> sourceList = context.WorkflowHeaders.ToList();

                    lst = AutoMapperGenericsHelper<WorkflowHeader, WorkflowHeaderDTO>.ConvertToEntityList(sourceList);

                    return lst;
                }
            }
            catch (Exception ex)
            {
                return lst;
            }
        }

        public List<WorkflowStageDTO> FillStages(int workflowId)
        {
            List<WorkflowStageDTO> lst = new List<WorkflowStageDTO>();

            try
            {
                using (DMSEntities context = new DMSEntities())
                {
                    
                    List<WorkflowStage> sourceList = context.WorkflowStages.Where(t => t.WorkflowId == workflowId).ToList();

                    lst = AutoMapperGenericsHelper<WorkflowStage, WorkflowStageDTO>.ConvertToEntityList(sourceList);

                    return lst;
                }
            }
            catch (Exception ex)
            {
                return lst;
            }
        }

        public List<TempWorkflowLevelDTO> LoadLevel(int stageId, string sessionId)
        {
            List<TempWorkflowLevelDTO> lst = new List<TempWorkflowLevelDTO>();

            try
            {
                using (DMSEntities context = new DMSEntities())
                {
                    List<WorkflowLevel1> sourceList = context.WorkflowLevel1
                                                        .Where(t => t.StageId == stageId && t.SessionId == sessionId && t.LevelNo > 1).ToList();

                    lst = AutoMapperGenericsHelper<WorkflowLevel1, TempWorkflowLevelDTO>.ConvertToEntityList(sourceList);

                    return lst;
                }
            }
            catch (Exception ex)
            {
                return lst;
            }
        }

        public List<TempWorkflowActionDTO> LoadLevelUserDetail(int levelId, string sessionId)
        {
            List<TempWorkflowActionDTO> lst = new List<TempWorkflowActionDTO>();


            using (DMSEntities context = new DMSEntities())
            {
                try
                {
                    lst = (from wa in context.WorkflowAction1
                           join u in context.Users on wa.UserId equals u.UserId into j1
                           from u in j1.DefaultIfEmpty()
                           join r in context.Roles on u.RoleId equals r.RoleId into j2
                           from r in j2.DefaultIfEmpty()
                           where wa.SessionId == sessionId && wa.LevelId == levelId
                           select new TempWorkflowActionDTO
                           {
                               ActionId = wa.ActionId,
                               SessionId = wa.SessionId,
                               ModuleCode = wa.ModuleCode,
                               StageId = wa.StageId,
                               LevelId = wa.LevelId,
                               RoleId = wa.RoleId,
                               IsCreate = wa.IsCreate,
                               IsApprove = wa.IsApprove,
                               IsEdit = wa.IsEdit,
                               IsPost = wa.IsPost,
                               ActionStatus = wa.ActionStatus,
                               Designation = r.Description,
                               UserName = u.Name,
                               UserId = wa.UserId
                           }).ToList();

                    return lst;
                }
                catch (Exception ex)
                {
                    return lst;
                }
            }

        }

        public int SaveLevelData(int levelId, string levelTitle, string actionData)
        { 
            using (DMSEntities context = new DMSEntities())
            {

                if (context.Database.Connection.State != System.Data.ConnectionState.Open)
                    context.Database.Connection.Open();

                using (var trans = context.Database.Connection.BeginTransaction())
                {
                    try
                    {
                        context.Database.UseTransaction(trans);
                        int result = context.Workflow_Level_Save(levelId, levelTitle, actionData);

                        trans.Commit();
                        return 0;
                         
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        return 1;
                    }
                }
            }
        }

        public int AddNewUser(int levelId, string sessionId, string userId)
        {
            using (DMSEntities context = new DMSEntities())
            {

                if (context.Database.Connection.State != System.Data.ConnectionState.Open)
                    context.Database.Connection.Open();

                using (var trans = context.Database.Connection.BeginTransaction())
                {
                    try
                    {
                        context.Database.UseTransaction(trans);
                        context.Workflow_Level_User_Add(userId, sessionId, levelId);

                        trans.Commit();
                        return 0;

                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        return 1;
                    }
                }
            }
        }

        public int RemoveUser(int actionId)
        {
            using (DMSEntities context = new DMSEntities())
            {

                if (context.Database.Connection.State != System.Data.ConnectionState.Open)
                    context.Database.Connection.Open();

                using (var trans = context.Database.Connection.BeginTransaction())
                {
                    try
                    {
                        context.Database.UseTransaction(trans);
                        var action = context.WorkflowAction1.Where(t => t.ActionId == actionId).FirstOrDefault();

                        context.WorkflowAction1.Remove(action);
                        context.SaveChanges();

                        trans.Commit();
                        return 1;

                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        return 0;
                    }
                }
            }
        }
         
        public int AddLevel(int stageId, string sessionId)
        {
            using (DMSEntities context = new DMSEntities())
            {

                if (context.Database.Connection.State != System.Data.ConnectionState.Open)
                    context.Database.Connection.Open();

                using (var trans = context.Database.Connection.BeginTransaction())
                {
                    try
                    {
                        context.Database.UseTransaction(trans);
                        int result = context.Workflow_Level_Add(stageId, sessionId);

                        trans.Commit();
                        return 0;

                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        return 1;
                    }
                }
            }
        }
         
        public int RemoveLevel(int levelId)
        {
            using (DMSEntities context = new DMSEntities())
            {

                if (context.Database.Connection.State != System.Data.ConnectionState.Open)
                    context.Database.Connection.Open();

                using (var trans = context.Database.Connection.BeginTransaction())
                {
                    try
                    {
                        context.Database.UseTransaction(trans);

                        var action = context.WorkflowAction1.Where(t => t.LevelId == levelId).FirstOrDefault();
                        context.WorkflowAction1.Remove(action);
                        context.SaveChanges();

                        var level = context.WorkflowLevel1.Where(t => t.LevelId == levelId).FirstOrDefault();
                        context.WorkflowLevel1.Remove(level);
                        context.SaveChanges();

                        trans.Commit();
                        return 1;

                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        return 0;
                    }
                }
            }
        }

        public List<TempWorkflowLevelDTO> LoadStage(int stageId, string sessionId)
        {
            List<TempWorkflowLevelDTO> lst = new List<TempWorkflowLevelDTO>();

            using (DMSEntities context = new DMSEntities())
            {

                if (context.Database.Connection.State != System.Data.ConnectionState.Open)
                    context.Database.Connection.Open();

                using (var trans = context.Database.Connection.BeginTransaction())
                {
                    try
                    {
                        context.Database.UseTransaction(trans);
                        List<Workflow_Initialize_Result> result = context.Workflow_Initialize(sessionId, stageId).ToList();
                        
                        lst = AutoMapperGenericsHelper<Workflow_Initialize_Result, TempWorkflowLevelDTO>.ConvertToEntityList(result);

                        trans.Commit();
                        return lst;

                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        return lst;
                    }
                }
            }
        }


        public int SaveWorkflow(int stageId, string sessionId)
        {
            using (DMSEntities context = new DMSEntities())
            {

                if (context.Database.Connection.State != System.Data.ConnectionState.Open)
                    context.Database.Connection.Open();

                using (var trans = context.Database.Connection.BeginTransaction())
                {
                    try
                    {
                        context.Database.UseTransaction(trans);
                        int result = context.Workflow_Save(sessionId, stageId);

                        trans.Commit();
                        return 0;

                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        return 1;
                    }
                }
            }
        }
    }
}
