﻿using DMS.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.DataContext.DataAccessHelper
{
    public class Masters
    {
        #region Senders

        public List<DocumentMovementDTO> GetAllSenders()
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<DocumentMovementDTO> lst = new List<DocumentMovementDTO>();
                List<DocumentMovement> sourceList = context.DocumentMovements.Where(a => a.Active == true).ToList();

                lst = AutoMapperGenericsHelper<DocumentMovement, DocumentMovementDTO>.ConvertToEntityList(sourceList);

                return lst;
            }
        }

        public List<DocumentMovementDTO> GetAllSendersBySite(int siteId, bool isAdmin = false)
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<DocumentMovementDTO> lst = new List<DocumentMovementDTO>();
                if (isAdmin)
                {
                    List<DocumentMovement> sourceList = context.DocumentMovements.Where(a => a.Active == true).ToList();
                    lst = AutoMapperGenericsHelper<DocumentMovement, DocumentMovementDTO>.ConvertToEntityList(sourceList);
                }
                else
                {
                    List<DocumentMovement> sourceList = context.DocumentMovements.Where(a => a.Active == true && a.SiteID == siteId).ToList();
                    lst = AutoMapperGenericsHelper<DocumentMovement, DocumentMovementDTO>.ConvertToEntityList(sourceList);
                }                

                return lst;
            }
        }

        public bool ValidateSender(DocumentMovementDTO senderDto)
        {
            using (DMSEntities context = new DMSEntities())
            {
                DocumentMovement sender = context.DocumentMovements
                    .Where(b => b.MovementCode == senderDto.MovementCode 
                            && b.MovementName == senderDto.MovementName
                            && b.MovementId != senderDto.MovementId
                           ).FirstOrDefault();

                if (sender != null && sender.MovementId > 0)                
                    return true;                
                else
                    return false;

            }
        }

        public int CreateSender(DocumentMovementDTO senderDto)
        {
            DocumentMovement sender = AutoMapperGenericsHelper<DocumentMovementDTO, DocumentMovement>.ConvertToEntity(senderDto);

            using (DMSEntities context = new DMSEntities())
            {
                sender.CreatedOn = DateTime.Now;
                context.DocumentMovements.Add(sender);
                context.SaveChanges();

                return sender.MovementId;
            }
        }

        public int UpdateSender(DocumentMovementDTO senderDto)
        {
            using (DMSEntities context = new DMSEntities())
            {
                DocumentMovement sender = context.DocumentMovements.Where(b => b.MovementId == senderDto.MovementId).FirstOrDefault();

                sender.MovementCode = senderDto.MovementCode;
                sender.MovementName = senderDto.MovementName;
                sender.SiteID = senderDto.SiteID;
                sender.ModifiedOn = DateTime.Now;
                sender.ModifiedBy = senderDto.ModifiedBy;

                context.SaveChanges();

                return sender.MovementId;
            }
        }

        public bool DeactivateSender(int MovementId, string modifyBy)
        {
            using (DMSEntities context = new DMSEntities())
            {
                DocumentMovement sender = context.DocumentMovements.Where(b => b.MovementId == MovementId).FirstOrDefault();
                                
                sender.Active = false;
                sender.ModifiedOn = DateTime.Now;
                sender.ModifiedBy = modifyBy;

                try
                {
                    context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }                
            }
        }

        #endregion

        #region Document Purpose

        public List<PurposeDTO> GetAllPurpose()
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<PurposeDTO> lst = new List<PurposeDTO>();
                List<Purpose> sourceList = context.Purposes.Where(a => a.Active == true).ToList();

                lst = AutoMapperGenericsHelper<Purpose, PurposeDTO>.ConvertToEntityList(sourceList);

                return lst;
            }
        }

        #endregion
    }
}
