﻿using DMS.Models.DTO;
using DMS.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using DMS.Models.Common;

namespace DMS.DataContext.DataAccessHelper
{
    public class Documents
    {
        #region Documents
        
        public List<DocumentsDTO> GetAllDocumentsByRole(int roleId, int siteId)
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<DocumentsDTO> lst = new List<DocumentsDTO>();
                List<short> roleIds = (from r in context.Roles
                                       where r.RoleId == (int)UserRoles.SUPER_ADMINISTRATION || r.RoleId == (int)UserRoles.SITE_USER || r.RoleId == (int)UserRoles.SECRETARY
                                       select r.RoleId).ToList();

                lst = (from d in context.DocumentDatas
                       join s1 in context.DocumentMovements on d.SenderId equals s1.MovementId into r1
                       from s1 in r1.DefaultIfEmpty()

                       join s2 in context.DocumentMovements on d.ReceiverId equals s2.MovementId into r2
                       from s2 in r2.DefaultIfEmpty()

                       join si in context.SiteInfoes on d.SiteID equals si.ID into s
                       from si in s.DefaultIfEmpty()

                       join st in context.StatusMasters on d.StatusId equals st.StatusId into r3
                       from st in r3.DefaultIfEmpty()

                       join sr in context.StatusMasters on d.RequiredActionId equals sr.StatusId into r4
                       from sr in r4.DefaultIfEmpty()

                       join p in context.Purposes on d.DocumentPurposeId equals p.DocumentPurposeId into p1
                       from p in p1.DefaultIfEmpty()

                       select new DocumentsDTO
                       {
                           Id = d.Id,
                           SiteID = d.SiteID,
                           Site = si.SiteID,
                           IsHeadOfficeSite = si.IsHeadOffice,
                           DocumentCode = d.DocumentCode,
                           CategoryId = d.CategoryId,
                           Category = (d.CategoryId != null && d.CategoryId == 1) ? "InComing" : "OutGoing",
                           DocumentPurposeId = d.DocumentPurposeId,
                           DocumentPurpose = p.DocumentPurpose,
                           SenderId = d.SenderId,
                           //Sender = s1.MovementCode + " - " + s1.MovementName,
                           Sender = s1.MovementName,
                           ReceiverId = d.ReceiverId,
                           //Receiver = s2.MovementCode + " - " + s2.MovementName,
                           Receiver = s2.MovementName,
                           ReferenceNo = d.ReferenceNo,
                           ReplyRefNo = d.ReplyRefNo,
                           DateOfLetter = d.DateOfLetter,
                           DateReceived = d.DateReceived,
                           SubjectTitle = d.SubjectTitle,
                           SubjectDescription = d.SubjectDescription,
                           WorkflowStatus = d.WorkflowStatus,
                           WorkflowStatusDescription = d.WorkflowStatus == "N" ? "Draft" : (d.WorkflowStatus == "W" ? "Approval Waiting" : "Approved"),
                           StatusId = d.StatusId,
                           StatusDesc = st.StatusDescription,
                           RequiredActionId = d.RequiredActionId,
                           RequiredActionDesc = sr.StatusDescription,
                           ActionTakenBy = d.ActionTakenBy,
                           ActionTakenDesc = d.ActionTakenDesc,
                           ActionTakenOn = d.ActionTakenOn,
                           Remarks = d.Remarks,
                           Priority = d.Priority,
                           Originator = d.Originator,
                           OrganizationAddressTo = d.OrganizationAddressTo,
                           SubmittedTo = d.SubmittedTo,
                           SubmittedBy = d.SubmittedBy,
                           SubmittedOn = d.SubmittedOn,
                           ApprovedBy = d.ApprovedBy,
                           ApprovedOn = d.ApprovedOn,
                           IsReponded = ((from rm in context.Remarks
                                          join d1 in context.DocumentDatas on rm.DocumentId equals d1.Id into s1
                                          from d1 in s1.DefaultIfEmpty()

                                          join u in context.Users on rm.DoneBy equals u.UserId into s2
                                          from u in s2.DefaultIfEmpty()

                                          join rl in context.Roles on u.RoleId equals rl.RoleId into s3
                                          from rl in s3.DefaultIfEmpty()

                                          where !roleIds.Contains(rl.RoleId) && rm.DocumentId == d.Id
                                          select rm.Id).Distinct().ToList()).Count() > 0,
                       }).ToList(); 

                if (lst != null && lst.Count > 0)
                {                    
                    if (roleId == (int)UserRoles.SITE_USER || roleId == (int)UserRoles.SECRETARY || roleId == (int)UserRoles.PROJECT_MANAGER)
                    {
                        lst = lst.Where(l => l.SiteID == siteId).ToList();
                    }
                    else if (roleId != (int)UserRoles.SUPER_ADMINISTRATION)
                    {
                        List<int> distToHO = (from d in context.DistributeToes
                                              where d.RoleId == roleId
                                              select d.DocumentId).Distinct().ToList();

                        List<int> distTo = (from d in context.DistributeToes
                                            where d.RoleId == roleId && (d.IsForAction == true || d.IsForInfo == true)
                                            select d.DocumentId).Distinct().ToList();

                        List<int> approvedDoc = (from d in context.DocumentDatas
                                                 where distTo.Contains(d.Id) //&& d.WorkflowStatus == "A"
                                                 select d.Id).Distinct().ToList();


                        //lst = (from i in lst
                        //       where distTo.Contains(i.Id) //&& i.WorkflowStatus == "A"
                        //       select i).ToList();

                        var site = context.SiteInfoes.Where(s => s.ID == siteId).FirstOrDefault();
                        if (site != null)
                        {
                            bool? isHeadOffice = site.IsHeadOffice;
                            if ((bool)isHeadOffice)
                            {
                                lst = (from i in lst
                                       where distToHO.Contains(i.Id) //&& i.WorkflowStatus == "A"
                                       select i).ToList(); 
                            }
                            else
                            {
                                lst = (from i in lst
                                       where distTo.Contains(i.Id) && i.SiteID == siteId
                                       select i).ToList();
                            }
                        }                        
                        
                    }
                    else if (roleId == (int)UserRoles.SUPER_ADMINISTRATION)
                    {
                        var site = context.SiteInfoes.Where(s => s.ID == siteId).FirstOrDefault();
                        if (site != null)
                        {
                            bool? isHeadOffice = site.IsHeadOffice;
                            if ((bool)isHeadOffice || roleId == (int)UserRoles.SUPER_ADMINISTRATION)
                            {
                                lst = lst.ToList();
                            }
                            else
                                lst = lst.Where(l => l.SiteID == siteId).ToList();
                        }
                    }
                }
                return lst.OrderByDescending(l=>l.Id).ToList();
            }
        }

        public List<DocumentsDTO> GetPendingDocuments(int roleId, int siteId, string userName)
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<DocumentsDTO> lst = new List<DocumentsDTO>();

                lst = (from d in context.DocumentDatas
                       join s1 in context.DocumentMovements on d.SenderId equals s1.MovementId into r1
                       from s1 in r1.DefaultIfEmpty()

                       join s2 in context.DocumentMovements on d.ReceiverId equals s2.MovementId into r2
                       from s2 in r2.DefaultIfEmpty()

                       join si in context.SiteInfoes on d.SiteID equals si.ID into s
                       from si in s.DefaultIfEmpty()

                       join st in context.StatusMasters on d.StatusId equals st.StatusId into r3
                       from st in r3.DefaultIfEmpty()

                       join sr in context.StatusMasters on d.RequiredActionId equals sr.StatusId into r4
                       from sr in r4.DefaultIfEmpty()

                       join p in context.Purposes on d.DocumentPurposeId equals p.DocumentPurposeId into p1
                       from p in p1.DefaultIfEmpty()

                       select new DocumentsDTO
                       {
                           Id = d.Id,
                           SiteID = d.SiteID,
                           Site = si.SiteID,
                           IsHeadOfficeSite = si.IsHeadOffice,
                           DocumentCode = d.DocumentCode,
                           CategoryId = d.CategoryId,
                           Category = (d.CategoryId != null && d.CategoryId == 1) ? "InComing" : "OutGoing",
                           DocumentPurposeId = d.DocumentPurposeId,
                           DocumentPurpose = p.DocumentPurpose,
                           SenderId = d.SenderId,
                           //Sender = s1.MovementCode + " - " + s1.MovementName,
                           Sender = s1.MovementName,
                           ReceiverId = d.ReceiverId,
                           //Receiver = s2.MovementCode + " - " + s2.MovementName,
                           Receiver = s2.MovementName,
                           ReferenceNo = d.ReferenceNo,
                           ReplyRefNo = d.ReplyRefNo,
                           DateOfLetter = d.DateOfLetter,
                           DateReceived = d.DateReceived,
                           SubjectTitle = d.SubjectTitle,
                           SubjectDescription = d.SubjectDescription,
                           WorkflowStatus = d.WorkflowStatus,
                           WorkflowStatusDescription = d.WorkflowStatus == "N" ? "Draft" : (d.WorkflowStatus == "W" ? "Approval Waiting" : "Approved"),
                           StatusId = d.StatusId,
                           StatusDesc = st.StatusDescription,
                           RequiredActionId = d.RequiredActionId,
                           RequiredActionDesc = sr.StatusDescription,
                           ActionTakenBy = d.ActionTakenBy,
                           ActionTakenDesc = d.ActionTakenDesc,
                           ActionTakenOn = d.ActionTakenOn,
                           Remarks = d.Remarks,
                           Priority = d.Priority,
                           Originator = d.Originator,
                           OrganizationAddressTo = d.OrganizationAddressTo,
                           SubmittedTo = d.SubmittedTo,
                           SubmittedBy = d.SubmittedBy,
                           SubmittedOn = d.SubmittedOn,
                           ApprovedBy = d.ApprovedBy,
                           ApprovedOn = d.ApprovedOn,
                       }).ToList();

                if (lst != null && lst.Count > 0)
                {
                    if (roleId == (int)UserRoles.PROJECT_MANAGER || roleId == (int)UserRoles.SECRETARY)
                        lst = lst.Where(l => l.SiteID == siteId && l.WorkflowStatus == "W").ToList();
                    else if (roleId != (int)UserRoles.SUPER_ADMINISTRATION)
                    {

                        List<int> distToHO = (from d in context.DistributeToes
                                            where d.RoleId == roleId 
                                            select d.DocumentId).Distinct().ToList();

                        List<int> distTo = (from d in context.DistributeToes
                                            where d.RoleId == roleId && (d.IsForAction == true || d.IsForInfo == true)
                                            select d.DocumentId).Distinct().ToList();

                        List<int> approvedDoc = (from d in context.DocumentDatas
                                                 where distTo.Contains(d.Id) //&& d.WorkflowStatus == "A"
                                                 select d.Id).Distinct().ToList();

                        List<int> skipDoc = (from d in context.Remarks
                                             where approvedDoc.Contains((int)d.DocumentId) && d.DoneBy == userName
                                             select (int)d.DocumentId).Distinct().ToList();

                        //lst = (from i in lst
                        //       where distTo.Contains(i.Id) //&& i.WorkflowStatus == "A"
                        //       select i).ToList();

                        //List<int> remarksDoc = (from r in context.Remarks
                        //                        where distTo.Contains((int)r.DocumentId)
                        //                         && r.DoneBy != userName
                        //                         && !skipDoc.Contains((int)r.DocumentId)
                        //                        select (int)r.DocumentId).Distinct().ToList();

                        List<int> remarksDoc = (from r in context.Remarks
                                                where distTo.Contains((int)r.DocumentId)
                                                 && r.DoneBy == userName 
                                                select (int)r.DocumentId).Distinct().ToList();


                        var site = context.SiteInfoes.Where(s => s.ID == siteId).FirstOrDefault();
                        if (site != null)
                        {
                            bool? isHeadOffice = site.IsHeadOffice;
                            if ((bool)isHeadOffice || roleId == (int)UserRoles.SUPER_ADMINISTRATION)
                            {
                                lst = (from i in lst
                                       where distToHO.Contains(i.Id) //&& i.WorkflowStatus == "A"
                                       select i).ToList();

                                if (remarksDoc.Count() > 0)
                                {
                                    lst = (from l in lst
                                           where !remarksDoc.Contains(l.Id)
                                           select l).ToList();
                                }
                                else
                                {
                                    lst = lst.ToList();
                                }
                            }
                            else
                            {
                                lst = (from i in lst
                                       where distTo.Contains(i.Id) //&& i.WorkflowStatus == "A"
                                       select i).ToList(); 

                                if (remarksDoc.Count() > 0)
                                {
                                    lst = (from l in lst
                                           where l.SiteID == siteId &&
                                           !remarksDoc.Contains(l.Id)
                                           select l).ToList();
                                }
                                else
                                {
                                    lst = lst.Where(l => l.SiteID == siteId //&& l.WorkflowStatus == "A"
                                    ).ToList();
                                }
                            }
                        }
                        
                    }
                    else
                    {
                        var site = context.SiteInfoes.Where(s => s.ID == siteId).FirstOrDefault();
                        if (site != null)
                        {
                            bool? isHeadOffice = site.IsHeadOffice;
                            if ((bool)isHeadOffice || roleId == (int)UserRoles.SUPER_ADMINISTRATION)
                            {
                                //lst = lst.Where(l => l.WorkflowStatus == "A").ToList();
                                lst = lst.ToList();
                            }
                            else
                                //lst = lst.Where(l => l.SiteID == siteId && l.WorkflowStatus == "A").ToList();
                                lst = lst.Where(l => l.SiteID == siteId).ToList();
                        }
                    }
                            
                }
                return lst.OrderByDescending(l => l.Id).ToList();
            }
        }

        public int CreateDocumentData(DocumentsDTO docDto, List<FileDTO> lstFiles, List<DocumentDistributeToDTO> lstDistribute)
        {
            //SiteDTO siteDto = new SiteDTO();
            DocumentData doc = AutoMapperGenericsHelper<DocumentsDTO, DocumentData>.ConvertToEntity(docDto);

            using (DMSEntities context = new DMSEntities())
            {
                if (context.Database.Connection.State != System.Data.ConnectionState.Open)
                    context.Database.Connection.Open();

                using (var trans = context.Database.Connection.BeginTransaction())
                {
                    try
                    {
                        context.Database.UseTransaction(trans);

                        doc.CreatedOn = DateTime.Now;
                        ObjectParameter codeOut = new ObjectParameter("code", "nvarchar(50)");
                        context.DocumentCode_YYMM_Series_Generate(docDto.Site, "[Document].[DocumentData]", "DocumentCode", 4, (DateTime)doc.CreatedOn, codeOut);
                        var docCode = codeOut.Value.ToString();

                        doc.DocumentCode = docCode;
                        doc.DocumentPurposeId = 4;
                        context.DocumentDatas.Add(doc);
                        context.SaveChanges();

                        var existingFiles = context.FilesLinks.Where(b => b.DocumentId == doc.Id);
                        if (existingFiles != null && existingFiles.Count() > 0)
                        {
                            context.FilesLinks.RemoveRange(existingFiles);
                            context.SaveChanges();
                        }

                        foreach (var item in lstFiles)
                        {
                            item.TemporaryFileName = item.FileName + "" + DateTime.Now.ToString("dd-MM-yyyy-hh:mm:ss").Replace("-", "").Replace(":", "");

                            File newFile = new File();
                            newFile.FileName = item.FileName;
                            newFile.TemporaryFileName = item.TemporaryFileName;
                            newFile.FilePath = item.FilePath;
                            newFile.UploadedBy = item.UploadedBy;
                            newFile.UploadedOn = item.UploadedOn;

                            context.Files.Add(newFile);
                            context.SaveChanges();

                            var tempFile = context.Files.Where(b => b.TemporaryFileName == item.TemporaryFileName).FirstOrDefault();

                            int fileId = tempFile.FileId;

                            context.FilesLinks.Add(new FilesLink { DocumentId = doc.Id, FileId = fileId });
                            context.SaveChanges();
                        }

                        if (lstDistribute != null && lstDistribute.Count() > 0)
                        {
                            var old = context.DistributeToes.Where(d => d.DocumentId == doc.Id).ToList();
                            context.DistributeToes.RemoveRange(old);

                            foreach (var item in lstDistribute)
                            {
                                DistributeTo obj = new DistributeTo();
                                obj.DocumentId = doc.Id;
                                obj.RoleId = item.RoleId;
                                obj.IsForAction = item.IsForAction;
                                obj.IsForInfo = item.IsForInfo;

                                context.DistributeToes.Add(obj);
                            }
                            context.SaveChanges();
                        }

                        trans.Commit();

                        return doc.Id;
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        return 0;
                    }
                }
            }
        }

        public int UpdateDocumentData(DocumentsDTO docDto, List<FileDTO> lstFiles, List<DocumentDistributeToDTO> lstDistribute)
        {
            using (DMSEntities context = new DMSEntities())
            {
                if (context.Database.Connection.State != System.Data.ConnectionState.Open)
                    context.Database.Connection.Open();

                using (var transaction = context.Database.Connection.BeginTransaction())
                {
                    try
                    {
                        context.Database.UseTransaction(transaction);
                        DocumentData doc = context.DocumentDatas.Where(b => b.Id == docDto.Id).FirstOrDefault();

                        doc.SiteID = docDto.SiteID;
                        doc.SenderId = docDto.SenderId;
                        doc.ReceiverId = docDto.ReceiverId;
                        doc.CategoryId = docDto.CategoryId;
                        doc.ReferenceNo = docDto.ReferenceNo;
                        doc.DateOfLetter = docDto.DateOfLetter;
                        doc.DateReceived = docDto.DateReceived;
                        //doc.DocumentPurposeId = docDto.DocumentPurposeId;
                        doc.ModifiedOn = DateTime.Now;
                        doc.ModifiedBy = docDto.ModifiedBy;
                        doc.SubjectTitle = docDto.SubjectTitle;
                        doc.SubjectDescription = docDto.SubjectDescription;
                        doc.ReplyRefNo = docDto.ReplyRefNo;
                        doc.Remarks = docDto.Remarks;
                        doc.Priority = docDto.Priority;
                        doc.CrossReferences = docDto.CrossReferences;
                        doc.Originator = docDto.Originator;
                        doc.OrganizationAddressTo = docDto.OrganizationAddressTo;
                        
                        doc.StatusId = docDto.StatusId;
                        doc.RequiredActionId = docDto.RequiredActionId;
                        
                        if (docDto.WorkflowStatus == "N")
                        {
                            doc.SubmittedOn = null;
                            doc.SubmittedBy = null;
                            doc.WorkflowStatus = "N";
                        }
                        if (docDto.WorkflowStatus == "W")
                        {
                            doc.SubmittedOn = docDto.SubmittedOn;
                            doc.SubmittedBy = docDto.SubmittedBy;
                            doc.SubmittedTo = docDto.SubmittedTo;
                            doc.WorkflowStatus = "W";
                        }
                        if (docDto.WorkflowStatus == "A")
                        {
                            doc.ApprovedBy = docDto.ApprovedBy;
                            doc.ApprovedOn = docDto.ApprovedOn;
                            doc.ActionTakenBy = docDto.ActionTakenBy;
                            doc.ActionTakenDesc = docDto.ActionTakenDesc;
                            doc.ActionTakenOn = docDto.ActionTakenOn;
                            doc.WorkflowStatus = "A";
                        }
                        context.SaveChanges();

                        var existingFiles = context.FilesLinks.Where(b => b.DocumentId == doc.Id);
                        if (existingFiles != null && existingFiles.Count() > 0)
                        {
                            foreach (var item in existingFiles)
                            {
                                var fl = context.Files.Where(b => b.FileId == item.FileId).FirstOrDefault();
                                if (fl != null)
                                    context.Files.Remove(fl);
                            }                            
                            context.SaveChanges();
                        }

                        var existingFileLinks = context.FilesLinks.Where(b => b.DocumentId == doc.Id);
                        if (existingFileLinks != null && existingFileLinks.Count() > 0)
                        {
                            context.FilesLinks.RemoveRange(existingFileLinks);
                            context.SaveChanges();
                        }

                        foreach (var item in lstFiles)
                        {                            
                            item.TemporaryFileName = item.FileName + "" + DateTime.Now.ToString("dd-MM-yyyy-hh:mm:ss").Replace("-", "").Replace(":", "");

                            File newFile = new File();
                            newFile.FileName = item.FileName;
                            newFile.TemporaryFileName = item.TemporaryFileName;
                            newFile.FilePath = item.FilePath;
                            newFile.UploadedBy = item.UploadedBy;
                            newFile.UploadedOn = item.UploadedOn;

                            context.Files.Add(newFile);
                            context.SaveChanges();

                            var tempFile = context.Files.Where(b => b.TemporaryFileName == item.TemporaryFileName).FirstOrDefault();

                            int fileId = tempFile.FileId;

                            context.FilesLinks.Add(new FilesLink { DocumentId = doc.Id, FileId = fileId });
                            context.SaveChanges();
                        }

                        if (lstDistribute != null && lstDistribute.Count() > 0)
                        {
                            var old = context.DistributeToes.Where(d => d.DocumentId == doc.Id).ToList();
                            context.DistributeToes.RemoveRange(old);

                            foreach (var item in lstDistribute)
                            {
                                DistributeTo obj = new DistributeTo();
                                obj.DocumentId = doc.Id;
                                obj.RoleId = item.RoleId;
                                obj.IsForAction = item.IsForAction;
                                obj.IsForInfo = item.IsForInfo;

                                context.DistributeToes.Add(obj);
                            }
                            context.SaveChanges();
                        }

                        transaction.Commit();

                        return doc.Id;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return 0;
                    }
                }
            }
        }

        public DocumentsDTO GetDocumentsById(int docId)
        {
            using (DMSEntities context = new DMSEntities())
            {
                DocumentsDTO docDto = new DocumentsDTO();

                DocumentData doc = context.DocumentDatas.Where(d => d.Id == docId).FirstOrDefault();
                docDto = AutoMapperGenericsHelper<DocumentData, DocumentsDTO>.ConvertToEntity(doc);
                                 
                return docDto;               
            }
        }
               
        public List<DocumentMovementDTO> GetAllSender()
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<DocumentMovementDTO> lst = new List<DocumentMovementDTO>();

                List<DocumentMovement> sourceList = context.DocumentMovements.Where(b => b.Active == true).ToList();
                lst = AutoMapperGenericsHelper<DocumentMovement, DocumentMovementDTO>.ConvertToEntityList(sourceList);

                foreach (var item in lst)
                {
                    item.Description = item.MovementCode + " - " + item.MovementName;
                }
                return lst; 
            }
        }

        public List<DocumentMovementDTO> GetSenderBySite(int siteId)
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<DocumentMovementDTO> lst = new List<DocumentMovementDTO>();

                List<DocumentMovement> sourceList = context.DocumentMovements.Where(b => b.Active == true && b.SiteID == siteId).ToList();
                lst = AutoMapperGenericsHelper<DocumentMovement, DocumentMovementDTO>.ConvertToEntityList(sourceList);

                foreach (var item in lst)
                {
                    item.Description = item.MovementName;
                }
                return lst;
            }
        }
       
        public List<StatusMasterDTO> GetStatus(string module)
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<StatusMasterDTO> lst = new List<StatusMasterDTO>();

                List<StatusMaster> sourceList = context.StatusMasters.Where(b => b.ModuleCode == module).ToList();
                lst = AutoMapperGenericsHelper<StatusMaster, StatusMasterDTO>.ConvertToEntityList(sourceList);

                foreach (var item in lst)
                {
                    item.StatusDescription = item.StatusCode + " - " + item.StatusDescription;
                }

                return lst;
            }
        }
         
        #endregion

        #region Document Links

        public List<FileDTO> GetDocumentAttachments(int documentId)
        {

            using (DMSEntities context = new DMSEntities())
            {
                List<FileDTO> fileList = (from f in context.Files
                                          join d in context.FilesLinks on f.FileId equals d.FileId
                                          where d.DocumentId == documentId
                                          select new FileDTO()
                                          {
                                              FileId = f.FileId,
                                              DocumentId = d.DocumentId,
                                              FileName = f.FileName,
                                              Text = f.FileName,
                                              FilePath = f.FilePath,
                                              UploadedBy = f.UploadedBy,
                                              UploadedOn = f.UploadedOn
                                          }).ToList();

                return fileList;
            }
        }
  
        #endregion

        #region Document Remarks

        public List<RemarksDTO> GetDocumentRemarks(int documentId)
        {
            using (DMSEntities context = new DMSEntities())
            {
                List<RemarksDTO> lst = (from f in context.Remarks
                                                 join d in context.DocumentDatas on f.DocumentId equals d.Id
                                                 where d.Id == documentId
                                                 select new RemarksDTO()
                                                 {
                                                     Id = f.Id,
                                                     DocumentId = d.Id,
                                                     Remarks = f.Remarks,
                                                     RemarksOn = f.RemarksOn,
                                                     DoneBy = f.DoneBy
                                                 }).ToList();

                return lst;
            }
        }

        public bool CreateRemarks(List<RemarksDTO> remarksLst)
        {

            List<Remark> lst = AutoMapperGenericsHelper<RemarksDTO, Remark>.ConvertToEntityList(remarksLst);
            using (DMSEntities context = new DMSEntities())
            {
                if (lst.Count > 0)
                {
                    var docuId = lst[0].DocumentId;
                    var oldRemarks = context.Remarks.Where(t => t.DocumentId == docuId).ToList();
                    context.Remarks.RemoveRange(oldRemarks);
                    context.SaveChanges();

                    context.Remarks.AddRange(lst);
                    context.SaveChanges();
                }
            }
            return true;
        }
        
        public bool RemoveAllRemarks(int docId)
        { 
            using (DMSEntities context = new DMSEntities())
            {
                var oldRemarks = context.Remarks.Where(t => t.DocumentId == docId).ToList();
                context.Remarks.RemoveRange(oldRemarks);

            }
            return true;
        }

        #endregion
    }
}
