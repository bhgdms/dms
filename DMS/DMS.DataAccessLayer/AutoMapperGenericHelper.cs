﻿using AutoMapper;  
using System.Collections.Generic; 

namespace DMS.DataAccessLayer
{
    public class AutoMapperGenericsHelper<TSource, TDestination>
    { 
        public static TDestination ConvertToEntity(TSource model)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TSource, TDestination>();
                cfg.AllowNullCollections = true;
            });

            IMapper iMapper = config.CreateMapper();
            return iMapper.Map<TSource, TDestination>(model);
        }

        public static List<TDestination> ConvertToEntityList(List<TSource> model)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TSource, TDestination>();
                cfg.AllowNullCollections = true;
            });

            IMapper iMapper = config.CreateMapper();
            return iMapper.Map<List<TSource>, List<TDestination>>(model);
        }
    }
}
