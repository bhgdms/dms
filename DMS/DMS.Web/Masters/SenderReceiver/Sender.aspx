﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"  CodeBehind="Sender.aspx.vb" Inherits="DMS.Web.Sender" %>


<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.3.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" Namespace="DevExpress.Web" TagPrefix="dx" %>
 
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <div class="pageHeader">
                <div style="width: 100%;">
                    <div class="w3-padding inlineblock moveMiddle" style="width: 40%">
                        <div class="Title">
                            Sender / Receiver Master
                        </div>
                        <div class="TitleDescription">
                            Add / Modify Sender - Receiver detail
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <dx:ASPxGridView ID="grdList" ClientInstanceName="grdList" runat="server" EnableViewState="false" width="100%"
                    AutoGenerateColumns="False" KeyFieldName="MovementId" DataSourceID="odsList" EnableTheming="True" Theme="Glass">
                    <Toolbars>
                        <dx:GridViewToolbar EnableAdaptivity="true">
                            <Items>
                                <dx:GridViewToolbarItem Name="CustomExportToPDF" Text="Export to PDF" Image-IconID="export_exporttopdf_16x16" />
                                <dx:GridViewToolbarItem Name="CustomExportToXLSX" Text="Export to XLSX" Image-IconID="export_exporttoxlsx_16x16office2013">
                                </dx:GridViewToolbarItem> 
                            </Items>
                        </dx:GridViewToolbar>
                    </Toolbars>
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Sr." ReadOnly="true" VisibleIndex="0" Width="50px">
                            <EditItemTemplate><%#If(Container.ItemIndex < 0, grdList.VisibleRowCount, Container.ItemIndex) + 1 %></EditItemTemplate>
                            <DataItemTemplate>
                                <%#Container.ItemIndex + 1 %>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewCommandColumn ShowNewButtonInHeader="true" ShowDeleteButton="true" ShowEditButton="true" VisibleIndex="0" Width="120px">
                        </dx:GridViewCommandColumn> 
                        <dx:GridViewDataComboBoxColumn Settings-FilterMode="DisplayText" FieldName="SiteID" Width="100px" Caption="Site" CellStyle-Wrap="False" Visible="false">
                            <PropertiesComboBox AllowNull="false" TextField="SiteID" ValueField="ID" EnableSynchronization="true"
                                IncrementalFilteringMode="Contains" DataSourceID="odsSites">
                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnSiteChanged(s); }" />
                                <ValidationSettings SetFocusOnError="true" Display="Dynamic">
                                    <RequiredField IsRequired="true" ErrorText="Site is required !!!" />
                                </ValidationSettings>
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn> 
                        <dx:GridViewDataTextColumn FieldName="MovementCode" Caption="Code"  Width="150px" >
                            <CellStyle CssClass="UpperCaseStyle"/>
                            <PropertiesTextEdit MaxLength="200">
                                <ValidationSettings Display="Dynamic">
                                    <RequiredField IsRequired="true" ErrorText="Code is required" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="MovementName" Caption="Name"  Width="550px" >
                            <PropertiesTextEdit MaxLength="200">
                                <ValidationSettings Display="Dynamic">
                                    <RequiredField IsRequired="true" ErrorText="Name is required" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn> 
                    </Columns> 
                    <Settings ShowHeaderFilterButton="true" HorizontalScrollBarMode="Visible" ShowFilterBar="Visible" ShowFilterRow="true" ShowFilterRowMenu="true" ShowFooter="true" ShowGroupPanel="true" ShowGroupFooter="VisibleIfExpanded" ShowPreview="true" />
                    <SettingsPager>
                        <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                    </SettingsPager>
                    <SettingsEditing Mode="Inline"></SettingsEditing>
                    <SettingsBehavior />
                    <SettingsResizing  ColumnResizeMode="Control" Visualization="Live" />
            
                    <ClientSideEvents ToolbarItemClick="function (s, e) { onToolbarClick(s, e); }" BeginCallback="function (s, e) { onBeginCallBack(s, e); }" EndCallback ="function (s, e) { onEndCallBack(s, e); }" />
                    <SettingsExport EnableClientSideExportAPI="true" Landscape="true" LeftMargin="0" RightMargin="0" TopMargin="0" BottomMargin="0" ExcelExportMode="WYSIWYG" />
                    <SettingsSearchPanel Visible="True" />
                </dx:ASPxGridView>
                <asp:ObjectDataSource ID="odsList" runat="server" DataObjectTypeName="DMS.DataContext.DataAccessHelper.Masters"
                    SelectMethod="GetAllSendersBySite" TypeName="DMS.DataContext.DataAccessHelper.Masters">
                    <SelectParameters>
                        <asp:Parameter Name="siteId" DefaultValue="" />
                        <asp:Parameter Name="isAdmin" DefaultValue="" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsSites" runat="server" DataObjectTypeName="DMS.DataContext.DataAccessHelper.SystemAdmin"
                    SelectMethod="GetAllProjectSites" TypeName="DMS.DataContext.DataAccessHelper.SystemAdmin">

                </asp:ObjectDataSource>
            </div> 
        </ContentTemplate>
    </asp:UpdatePanel>
   <%-- <%If False Then%>
    <script type="text/javascript" src="../../../Include/Common.js"></script>
    <script type="text/javascript" src="../../../Include/jquery-vsdoc.js"></script>
    <%End If%>--%>
    <script type="text/javascript">
        function onToolbarClick(s, e) {
            e.processOnServer = true; e.usePostBack = true;
        }

        function onBeginCallBack(s, e) {
            delete (s.cp_scriptToRun);
        }
        function onEndCallBack(s, e) {
            if (s.cp_scriptToRun) {
                var fn = new Function(s.cp_scriptToRun);
                fn();
                delete (s.cp_scriptToRun);
            }
        }

        init_page_events_and_controls();

        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
            init_page_events_and_controls();
        });

        function init_page_events_and_controls() {
            
        }

    </script>
</asp:Content>