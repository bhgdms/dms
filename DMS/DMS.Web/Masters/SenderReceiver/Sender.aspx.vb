﻿Imports DevExpress.Web
Imports DevExpress.Web.Data
Imports DMS.DataContext.DataAccessHelper
Imports DMS.Models.DTO

Public Class Sender
    Inherits BasePage

    Dim caller As Masters
    Private Property SiteId As Integer
        Get
            Return ViewState("SITEID")
        End Get
        Set(value As Integer)
            ViewState("SITEID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            caller = New Masters()
            If Not IsPostBack Then
                If IsSuperAdmin Then
                    grdList.Columns(2).Visible = True
                End If
            End If
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub grdList_RowInserting(sender As Object, e As ASPxDataInsertingEventArgs) Handles grdList.RowInserting
        Try
            Dim dto As DocumentMovementDTO = New DocumentMovementDTO()
            dto.MovementCode = e.NewValues("MovementCode")
            dto.MovementName = e.NewValues("MovementName")
            dto.SiteID = IIf(IsSuperAdmin, e.NewValues("SiteID"), UserProfile.SiteID)
            dto.Active = True
            dto.CreatedBy = UserProfile.UserID
            dto.CreatedOn = DateTime.Now

            Dim isExist As Boolean = False
            isExist = caller.ValidateSender(dto)

            If Not isExist Then
                Dim result As Integer = caller.CreateSender(dto)
                If result > 0 Then
                    grdList.JSProperties("cp_scriptToRun") = "$.showMessageAttention('Data is added successfully.!!', 'Attention');"
                End If
            Else
                grdList.JSProperties("cp_scriptToRun") = "$.showMessageError('Data is already exist in the system.!!', 'Attention');"
            End If

            grdList.CancelEdit()
            e.Cancel = True
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub grdList_RowUpdating(sender As Object, e As ASPxDataUpdatingEventArgs) Handles grdList.RowUpdating
        Try
            Dim dto As DocumentMovementDTO = New DocumentMovementDTO()
            dto.MovementId = e.Keys("MovementId")
            dto.MovementCode = e.NewValues("MovementCode")
            dto.MovementName = e.NewValues("MovementName")
            dto.SiteID = IIf(IsSuperAdmin, e.NewValues("SiteID"), UserProfile.SiteID)
            dto.Active = e.NewValues("Active")
            dto.CreatedBy = UserProfile.UserID
            dto.CreatedOn = DateTime.Now

            Dim result As Integer = caller.UpdateSender(dto)
            If result > 0 Then
                grdList.JSProperties("cp_scriptToRun") = "$.showMessageAttention('Data is updated successfully.!!', 'Attention');"
            End If
            grdList.CancelEdit()
            e.Cancel = True

        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub grdList_RowDeleting(sender As Object, e As ASPxDataDeletingEventArgs) Handles grdList.RowDeleting
        Try
            Dim id As Integer = e.Keys("MovementId")
            Dim result As Boolean = caller.DeactivateSender(id, UserProfile.UserID)

            If result Then
                grdList.JSProperties("cp_scriptToRun") = "$.showMessageAttention('Data is deactivated successfully.!!', 'Attention');"
            Else
                grdList.JSProperties("cp_scriptToRun") = "$.showMessageError('Some error occured, no data updated.!!', 'Attention');"
            End If
            grdList.CancelEdit()
            e.Cancel = True
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub grdList_CellEditorInitialize(sender As Object, e As ASPxGridViewEditorEventArgs) Handles grdList.CellEditorInitialize
        If e.Column.FieldName.Equals("MovementCode") Then
            Dim tb As ASPxTextBox = TryCast(e.Editor, ASPxTextBox)
            tb.Text = tb.Text.ToUpper()
        End If
    End Sub

    Private Sub odsList_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs) Handles odsList.Selecting
        e.InputParameters("siteId") = UserProfile.SiteID
        If IsSuperAdmin Then
            e.InputParameters("isAdmin") = True
        End If
    End Sub
End Class