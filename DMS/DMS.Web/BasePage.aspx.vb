﻿Imports System.IO

Public Class BasePage
    Inherits System.Web.UI.Page

    Private __current_update_panel As UpdatePanel

    Protected Property RunningUpdatePanel As UpdatePanel
        Private Get

            If Me.__current_update_panel Is Nothing Then
                Me.__current_update_panel = CType(Me.GetUpdatePanel(Me.Page), UpdatePanel)
            End If

            Return Me.__current_update_panel
        End Get
        Set(ByVal value As UpdatePanel)
            Me.__current_update_panel = value
        End Set
    End Property

    Private newPropertyValue As Boolean
    Public ReadOnly Property IsSuperAdmin As Boolean
        Get
            If UserProfile.RoleID IsNot Nothing Then
                Return IIf(UserProfile.RoleID = DMS.Models.Common.UserRoles.SUPER_ADMINISTRATION, True, False)
            Else
                Return False
            End If

        End Get
    End Property

    Private Function GetUpdatePanel(ByVal parentControl As Control) As Control
        Dim control As Control

        For Each control1 As Control In parentControl.Controls

            If Not (TypeOf control1 Is UpdatePanel) Then
                Continue For
            End If

            control = control1
            Return control
        Next

        Dim enumerator As IEnumerator = parentControl.Controls.GetEnumerator()

        Try

            While enumerator.MoveNext()
                Dim current As Control = CType(enumerator.Current, Control)

                If Not current.HasControls() Then
                    Continue While
                End If

                Dim updatePanel As Control = Me.GetUpdatePanel(current)

                If updatePanel Is Nothing Then
                    Continue While
                End If

                control = updatePanel
                Return control
            End While

            Return Nothing
        Finally
            Dim disposable As IDisposable = TryCast(enumerator, IDisposable)

            If disposable IsNot Nothing Then
                disposable.Dispose()
            End If
        End Try

        Return control
    End Function

    Protected ReadOnly Property GetRootPath As String
        Get
            Dim applicationPath As String = MyBase.Request.ApplicationPath
            Dim stringBuilder As StringBuilder = New StringBuilder(applicationPath)

            If Not applicationPath.EndsWith("/") Then
                stringBuilder.Append("/")
            End If

            Return stringBuilder.ToString()
        End Get
    End Property

    Protected Sub RunScript(ByVal script As String)
        If Me.RunningUpdatePanel IsNot Nothing Then
            Dim runningUpdatePanel As UpdatePanel = Me.RunningUpdatePanel
            Dim type As Type = Me.RunningUpdatePanel.[GetType]()
            Dim guid As Guid = New Guid()
            ScriptManager.RegisterStartupScript(runningUpdatePanel, type, guid.ToString(), script, True)
            Return
        End If

        Dim clientScript As ClientScriptManager = Me.Page.ClientScript
        Dim type1 As Type = MyBase.[GetType]()
        Dim guid1 As Guid = New Guid()
        clientScript.RegisterStartupScript(type1, guid1.ToString(), script, True)
    End Sub

    Protected WriteOnly Property PageMessage As String
        Set(ByVal value As String)
            Me.RunScript(String.Concat("$.showMessage('", value, "');"))
        End Set
    End Property

    Protected ReadOnly Property PHYSICAL_TEMP_FOLDER As String
        Get
            Return HttpContext.Current.Server.MapPath(String.Concat("~/", ConfigurationManager.AppSettings("TEMP_FOLDER")))
        End Get
    End Property

    Protected ReadOnly Property VIRTUAL_TEMP_FOLDER As String
        Get
            Return String.Concat("~/", ConfigurationManager.AppSettings("TEMP_FOLDER"))
        End Get
    End Property

    Public Sub LogException(ex As Exception)
        Me.Lodge(ex.ToString, ex.Source)
        Response.Redirect("~/errorPage.aspx", False)
    End Sub

    Private Sub Lodge(ByVal errorMessage As String, ByVal category As String)
        Dim textWriterTraceListener As TextWriterTraceListener = Nothing
        Dim stringBuilder As StringBuilder = Nothing
        Dim page As Page = Nothing

        Try

            Try
                stringBuilder = New StringBuilder()
                page = New Page()
                stringBuilder.Append(page.Server.MapPath("~/Log"))
                stringBuilder.Append("\")
                Dim year As Integer = DateTime.Now.Year
                stringBuilder.Append(year.ToString())
                stringBuilder.Append("\")
                Dim month As Integer = DateTime.Now.Month
                stringBuilder.Append(month.ToString())
                stringBuilder.Append("\")
                Dim day As Integer = DateTime.Now.Day
                stringBuilder.Append(day.ToString())

                If Not Directory.Exists(stringBuilder.ToString()) Then
                    Directory.CreateDirectory(stringBuilder.ToString())
                End If

                stringBuilder.Append("\Error.log")
                textWriterTraceListener = New TextWriterTraceListener(stringBuilder.ToString())
                stringBuilder = New StringBuilder()
                stringBuilder.Append(vbCrLf)
                stringBuilder.Append("Project : ")
                stringBuilder.Append(category)
                stringBuilder.Append(vbCrLf)
                stringBuilder.Append("Date : ")
                stringBuilder.Append(DateTime.Now.ToString())
                stringBuilder.Append(vbCrLf)
                stringBuilder.Append("Error : ")
                stringBuilder.Append(errorMessage)
                stringBuilder.Append(vbCrLf)
                textWriterTraceListener.WriteLine(stringBuilder.ToString())
            Catch
            End Try

        Finally

            If textWriterTraceListener IsNot Nothing Then
                textWriterTraceListener.Close()
                textWriterTraceListener.Dispose()
            End If

            stringBuilder = Nothing
        End Try
    End Sub

    Protected Sub ShowMessage(ByVal message As String, ByVal Optional title As String = "DMS")
        Me.RunScript(String.Concat("$.showMessage('", message, "');"))
    End Sub

    Protected Sub ShowMessageAndRedirect(ByVal message As String, ByVal urlToRedirect As String)
        Dim strArrays As String() = New String() {"$.showMessageAndRedirect('", message, "','Alert from system','", urlToRedirect, "');"}
        Me.RunScript(String.Concat(strArrays))
    End Sub
End Class