﻿
Imports DMS.DataContext.DataAccessHelper

Public Class UserProfile
    Inherits DMS.Generic.UserProfile

    Public Shared Function HasPrivilege(ParamArray accessID() As String) As Boolean
        Try
            Dim privilegeData As List(Of String)
            Dim sessionData = HttpContext.Current.Session("USER_PRIVILEGE_DATA")

            If sessionData Is Nothing OrElse DirectCast(sessionData, List(Of String)).Count() = 0 Then
                Dim caller As New SystemAdmin
                Dim pData As List(Of String) = New List(Of String)
                pData = caller.GetPrivilege(UserProfile.UserID)

                Try
                    privilegeData = pData
                    HttpContext.Current.Session("USER_PRIVILEGE_DATA") = privilegeData
                Catch
                    Return False

                End Try

            Else
                privilegeData = DirectCast(HttpContext.Current.Session("USER_PRIVILEGE_DATA"), List(Of String))
            End If

            For Each id As String In accessID
                If privilegeData.Contains(id) Then Return True
            Next
            Return False
        Catch
            Return False
        End Try
    End Function
End Class
