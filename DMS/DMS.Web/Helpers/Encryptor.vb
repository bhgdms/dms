﻿Imports System.Security.Cryptography

Public Class Encryptor
    Public Const TAMPER_PROOF_KEY As String = "ajlkasjflksfl3425"

    Public Sub New()
    End Sub

    Public Shared Function DecryptQueryString(ByVal encryptedQueryString As String) As String
        Dim str As String

        Try
            str = Encryptor.TamperProofStringDecode(encryptedQueryString, "ajlkasjflksfl3425")
        Catch
            str = Nothing
        End Try

        Return str
    End Function

    Public Shared Function EncryptQueryString(ByVal queryString As String) As String
        Dim str As String

        Try
            str = HttpUtility.UrlEncode(Encryptor.TamperProofStringEncode(queryString, "ajlkasjflksfl3425"))
        Catch
            str = Nothing
        End Try

        Return str
    End Function

    Private Shared Function TamperProofStringDecode(ByVal value As String, ByVal key As String) As String
        Dim str As String = ""
        Dim mACTripleDE As MACTripleDES = New MACTripleDES()
        Dim mD5CryptoServiceProvider As MD5CryptoServiceProvider = New MD5CryptoServiceProvider()
        mACTripleDE.Key = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(key))

        Try
            Dim uTF8 As Encoding = System.Text.Encoding.UTF8
            Dim chrArray As Char() = New Char() {"-"c}
            str = uTF8.GetString(Convert.FromBase64String(value.Split(chrArray)(0)))
            Dim encoding As Encoding = Encoding.UTF8
            Dim chrArray1 As Char() = New Char() {"-"c}

            If encoding.GetString(Convert.FromBase64String(value.Split(chrArray1)(1))) <> Encoding.UTF8.GetString(mACTripleDE.ComputeHash(Encoding.UTF8.GetBytes(str))) Then
                Throw New ArgumentException("Hash value does not match")
            End If

        Catch
            Throw New ArgumentException("Invalid TamperProofString")
        End Try

        Return str
    End Function

    Private Shared Function TamperProofStringEncode(ByVal value As String, ByVal key As String) As String
        Dim mACTripleDE As MACTripleDES = New MACTripleDES()
        Dim mD5CryptoServiceProvider As MD5CryptoServiceProvider = New MD5CryptoServiceProvider()
        mACTripleDE.Key = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(key))
        Return String.Concat(Convert.ToBase64String(Encoding.UTF8.GetBytes(value)), "-"c, Convert.ToBase64String(mACTripleDE.ComputeHash(Encoding.UTF8.GetBytes(value))))
    End Function
End Class
