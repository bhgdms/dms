﻿Public Class QueryString
    Private QueryList As ListItemCollection
    Private key As String

    Public ReadOnly Property Items As ListItemCollection
        Get
            Return Me.QueryList
        End Get
    End Property

    Public Sub New()
        Me.QueryList = New ListItemCollection()
        Me.key = String.Empty
    End Sub

    Public Sub New(ByVal key As String)
        Me.QueryList = New ListItemCollection()
        Me.key = key
    End Sub

    Public Function Add(ByVal key As String, ByVal value As String) As ListItem
        Dim listItem As ListItem = New ListItem(HttpUtility.HtmlEncode(value), key)
        Me.QueryList.Add(listItem)
        Return listItem
    End Function

    Public Function Decompose(ByVal Optional composedQueryString As String = "") As ListItemCollection
        Me.QueryList = New ListItemCollection()
        Dim strArrays As String() = Encryptor.DecryptQueryString((If(composedQueryString = "", HttpContext.Current.Request.QueryString(Me.key).ToString(), composedQueryString))).Split("&".ToCharArray())
        Dim strArrays1 As String() = strArrays

        For i As Integer = 0 To CInt(strArrays1.Length) - 1
            Dim str As String = strArrays1(i)
            Me.QueryList.Add(New ListItem(HttpUtility.HtmlDecode(str.Split("=".ToCharArray())(1)), str.Split("=".ToCharArray())(0)))
        Next

        Return Me.QueryList
    End Function

    Public Shared Function Exists(ByVal key As String) As Boolean
        Return HttpContext.Current.Request.QueryString(key) IsNot Nothing
    End Function

    Public Function Generate(ByVal Optional key As String = "") As String
        Dim empty As String = String.Empty

        For Each queryList As ListItem In Me.QueryList
            Dim str As String = empty
            Dim value As String() = New String() {str, queryList.Value, "=", queryList.Text, "&"}
            empty = String.Concat(value)
        Next

        empty = empty.Substring(0, empty.Length - 1)
        Return String.Concat((If(key = "", Me.key, key)), "=", Encryptor.EncryptQueryString(empty))
    End Function

    Public Shared Function [Get](ByVal key As String) As String
        If Not QueryString.Exists(key) Then
            Return String.Empty
        End If

        Return Encryptor.DecryptQueryString(HttpContext.Current.Request.QueryString(key).ToString())
    End Function

    Public Shared Function [Set](ByVal key As String) As String
        Return Encryptor.EncryptQueryString(key)
    End Function
End Class

