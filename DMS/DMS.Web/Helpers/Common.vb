#Region " References "
Imports System.Security.Cryptography
Imports System.Security.Principal
Imports System.Text
Imports System.IO
Imports System.Data
Imports System.Net.Mail
Imports System.Reflection
Imports System.Web
'Imports SevenHeavenFramework.Web.Tools
#End Region

Public Class ActivityModule
    Public Const BHC As String = "BHC"
End Class

Module SystemCommonFunctions

    Public Sub SetControlAttribute(controls As ControlCollection, controlType As Type, dict As Dictionary(Of String, Object))
        Try
            For Each ct As Control In controls
                If ct.GetType Is controlType Then
                    Dim thisControlType As Type = ct.GetType
                    For Each value As KeyValuePair(Of String, Object) In dict
                        Dim propInfo As PropertyInfo = thisControlType.GetProperty(value.Key)
                        If propInfo IsNot Nothing Then propInfo.SetValue(ct, value.Value, Nothing)
                    Next
                End If
                If ct.HasControls Then
                    SetControlAttribute(ct.Controls, controlType, dict)
                End If
            Next
        Catch
            Throw
        End Try
    End Sub

    Public Function ConvertToZeroIfNothing(sourceObject As Object) As Decimal
        If Not IsDBNull(sourceObject) AndAlso
            sourceObject IsNot Nothing AndAlso
            sourceObject.ToString().Trim <> String.Empty AndAlso
            IsNumeric(sourceObject) Then
            Return sourceObject
        Else
            Return 0
        End If
    End Function

    Public Function ConvertDateToDBDate(sourceDate As DateTime?) As String
        If Not IsDBNull(sourceDate) AndAlso Not sourceDate Is Nothing Then
            Return CDate(sourceDate).ToString("dd-MMM-yyyy hh:mm ttt")
        End If
        Return String.Empty
    End Function

    Public Function SetFileNameWithDateTime(fileName As String) As String
        Return fileName + "-" + DateTime.Now.ToString("dd.MM.yyyy HH-mm")
    End Function

End Module

Namespace Common

    Public Class Messages

        Public Const ACCOUNTACTIVATECONFIRMATION As String = "Are you sure you want to activate this account?."
        Public Const ACCOUNTALREADYBOOKED As String = "The User Id is already registered. Please choose another."
        Public Const ACCOUNTBLOCKCONFIRMATION As String = "Are you sure you want to block this account?."
        Public Const ACCOUNTCREATED As String = "Account created successfully."
        Public Const ACCOUNTCREATEDMAILED As String = "The User Id and Password has been mailed to user."
        Public Const ACCOUNTCREATEDNOTMAILED As String = "Account created successfully but there was a problem while sending mail to user."
        Public Const ACCOUNTNOTMAILED As String = "There was a problem while sending the mail."
        Public Const ACCOUNTDEACTIVATECONFIRMATION As String = "Are you sure you want to deactivate this account?."
        Public Const ACCOUNTMODIFIED As String = "Account modified successfully."
        Public Const ACCOUNTMODIFIEDMAILED As String = "The notification has been mailed to user."
        Public Const ACCOUNTMODIFIEDNOTMAILED As String = "Account modified successfully but there was an error while sending mail to user."
        Public Const ACCOUNTUNBLOCKCONFIRMATION As String = "Are you sure you want to unblock this account?."
        Public Const ERRORWHILESAVING As String = "Error occured while saving data."
        Public Const PASSWORDNOTRESET As String = "Error while reseting password."
        Public Const PASSWORDRESET As String = "Password reset successfully."
        Public Const PASSWORDRESETCONFIRMATION As String = "Are you sure you want to reset the password?."
        Public Const PASSWORDRESETMAIL As String = "Password reset successfully. New password has been mailed to user."
        Public Const PASSWORDRESETNOTMAIL As String = "Password reset successfully but there was an error while sending mail to user."
        Public Const SELECTROLE As String = "Select Role."
        Public Const SERVERPROBLEM As String = "Unable to process your request currently!!!\r\nPossibly the application is doing some update. Try again later. If the problem still persist, contact the support coordinator."
        Public Const USERACTIVATED As String = "User account is now activated."
        Public Const USERALREADYLOGEDIN As String = "You have already logged in from another place."
        Public Const USERBLOCKED As String = "User account is blocked successfully."
        Public Const USERDEACTIVATED As String = "User account is now deactivated."
        Public Const USERINUSE As String = "User Id is not available for usage. Please use another User Id."
        Public Const USERNOTACTIVATED As String = "Error while activating user account."
        Public Const USERNOTBLOCKED As String = "Error while blocking user account."
        Public Const USERNOTDEACTIVATED As String = "Error while deactivating user account."
        Public Const USERNOTEXIST As String = "User does not exist."
        Public Const USERNOTUNBLOCKED As String = "Error while unblocking user account."
        Public Const USERUNBLOCKED As String = "User account is unblocked successfully."

        Public Shared Function ShowMessage(message As String, Optional Title As String = "CONGRATULATION") As String
            Try
                Return String.Format("<div class=""w3-center""><div class=""w3-xlarge w3-text-green"">{0}</div><div>{1}</div></div>", Title, message)
            Catch
                Throw
            End Try
        End Function
        Public Shared Function ShowFailureMessage(message As String, Optional Title As String = "ERROR") As String
            Try
                Return String.Format("<div class=""w3-center""><div class=""w3-xlarge w3-text-red"">{0}</div><div>{1}</div></div>", Title, message)
            Catch
                Throw
            End Try
        End Function
        Public Shared Function ShowInformationMessage(message As String, Optional Title As String = "ATTENTION") As String
            Try
                Return String.Format("<div class=""w3-center""><div class='w3-xlarge w3-text-yellow'>{0}</div><div>{1}</div></div>", Title, message)
            Catch
                Throw
            End Try
        End Function

    End Class

    Public Class Alert
        Public Const PASSWORDCHANGE As String = "DMS Alert: Password Changed"
        Public Const ACCOUNTCREATED As String = "DMS Alert: Account Created"
        Public Const ACCOUNTMODIFIED As String = "DMS Alert: Account Modified"
        Public Const ACCOUNTBLOCKED As String = "DMS Alert: Account Blocked"
        Public Const ACCOUNTUNBLOCKED As String = "DMS Alert: Account Unblocked"
        Public Const ACCOUNTACTIVATED As String = "DMS Alert: Account Activated"
        Public Const ACCOUNTDEACTIVATED As String = "DMS Alert: Account Deactivated"
    End Class

    Public Class EmailMessages
        Public Const PASSWORDRESET As String = "Your password has been reset.<br><br>Your temporary password is <b>{0}</b><br><br>It is recommended that you should change your password after login."
        Public Const FORGOT_PASSWORD As String = "Your credential to access the portal is as follow:<br><br>Your login Id is <b>{0}</b><br><br>Your password is <b>{1}</b><br><br>It is recommended that you should change your password after login."
        Public Const PORTAL_ACCESS As String = "Your credential to access the portal is as follow:<br><br>Your login Id is <b>{0}</b><br><br>Your password is <b>{1}</b><br><br>It is recommended that you should change your password after login."
        Public Const ACCOUNTUNBLOCKED As String = "Your account has been unblocked."
        Public Const ACCOUNTBLOCKED As String = "Your account has been blocked.<br>For more detail, please contact administrator."
        Public Const ACCOUNTACTIVATED As String = "Your account is activated now."
        Public Const ACCOUNTDEACTIVATED As String = "Your account is deactived.<br>For more detail, please contact administrator."
        Public Const ACCOUNTCREATED As String = "Dear {0},<br>Your account has been created.<br><br>Your login details are as follow:<br><b>User Id:</b> {1}<br><b>Password:</b> {2}<br><br>Account created by: {3}<br>Date &amp; Time: {4}<br><br>Click the following url for logging in to DMS<br><a href='{5}'>{5}</a>"
        Public Const ACCOUNTMODIFIED As String = "Dear {0},<br>Your account information has been modified.<br><br>Account modified by: {1}<br>Date &amp; Time: {2}<br><br>For further detail, please contact administrator."

    End Class

End Namespace

Public Class MessageCenter
    Public Shared Function SuccessMessage(message As String) As String
        Return message
        'Return String.Format("<div class=""w3-center""><div class=""w3-padding w3-bold w3-text-green w3-xlarge"">" +
        '                            "SUCCESS !!!</div><div class=""w3-padding"">{0}</div></div>", message)
    End Function
    Public Shared Function AlertMessage(message As String) As String
        Return message

        'Return String.Format("<div class=""w3-center""><div class=""w3-padding w3-bold w3-text-orange w3-xlarge"">" +
        '                            "ATTENTION !!!</div><div class=""w3-padding"">{0}</div></div>", message)
    End Function
    Public Shared Function FailureMessage(message As String) As String
        Return message
        'Return String.Format("<div class=""w3-center""><div class=""w3-padding w3-bold w3-text-red w3-xlarge"">" +
        '                            "FAILED !!!</div><div class=""w3-padding"">{0}</div></div>", message)
    End Function
    Public Shared Function ErrorMessage(message As String) As String
        Return message
        'Return String.Format("<div class=""w3-center""><div class=""w3-padding w3-bold w3-text-red w3-xlarge"">" +
        '                            "OOPSS !!!</div><div class=""w3-padding"">{0}</div></div>", message)
    End Function
    Public Shared Function SystemErrorMessage() As String
        Return "<div class=""w3-center""><div class=""w3-padding w3-text-red w3-xlarge"">" +
                                    "OOPSS !!!</div><div class=""w3-padding"">There seems to be some issue connecting to job data. Our team has been notified. While our team is fixing the issue, you can continue different task.</div></div>"
    End Function
End Class
Public Class PostBackToClient
    Public Shared Sub RunScript(script As String, page As System.Web.UI.Page)
        page.ClientScript.RegisterStartupScript(page.GetType, Guid.NewGuid.ToString, script, True)
    End Sub
    Public Shared Sub SuccessMessage(title As String, message As String, page As System.Web.UI.Page)
        Dim script As String = "$.showMessageSuccess('" + title + "', '" + message + "');"
        page.ClientScript.RegisterStartupScript(page.GetType, Guid.NewGuid.ToString, script, True)
    End Sub
    Public Shared Sub AlertMessage(title As String, message As String, page As System.Web.UI.Page)
        Dim script As String = "$.showMessageAttention('" + title + "', '" + message + "');"
        page.ClientScript.RegisterStartupScript(page.GetType, Guid.NewGuid.ToString, script, True)
    End Sub
    Public Shared Sub FailureMessage(title As String, message As String, page As System.Web.UI.Page)
        Dim script As String = "$.showMessageFailure('" + title + "', '" + message + "');"
        page.ClientScript.RegisterStartupScript(page.GetType, Guid.NewGuid.ToString, script, True)
    End Sub
    Public Shared Sub ErrorMessage(title As String, message As String, page As System.Web.UI.Page)
        Dim script As String = "$.showMessageError('" + title + "', '" + message + "');"
        page.ClientScript.RegisterStartupScript(page.GetType, Guid.NewGuid.ToString, script, True)
    End Sub
    Public Shared Sub SystemErrorMessage(title As String, message As String, page As System.Web.UI.Page)
        Dim script As String = "$.showMessageSystemError('" + title + "', '" + message + "');"
        page.ClientScript.RegisterStartupScript(page.GetType, Guid.NewGuid.ToString, script, True)
    End Sub
End Class

Public Class TemporaryPasswordGenerator

    Private Shared DEFAULT_PASSWORD_LENGTH As Integer = 8
    Private Shared PASSWORD_CHARS_UCASE As String = "ABCDEFGHJKLMNPQRSTWXYZ"
    Private Shared PASSWORD_CHARS_NUMERIC As String = "123456789"
    Public Shared Function Generate() As String
        Generate = Generate(DEFAULT_PASSWORD_LENGTH, _
                            DEFAULT_PASSWORD_LENGTH)
    End Function
    Private Shared Function Generate(ByVal minLength As Integer, _
    ByVal maxLength As Integer) _
        As String

        ' Make sure that input parameters are valid.
        If (minLength <= 0 Or maxLength <= 0 Or minLength > maxLength) Then
            Generate = Nothing
        End If

        ' Create a local array containing supported password characters
        ' grouped by types. You can remove character groups from this
        ' array, but doing so will weaken the password strength.
        Dim charGroups As Char()() = New Char()() _
        { _
            PASSWORD_CHARS_UCASE.ToCharArray(), _
            PASSWORD_CHARS_NUMERIC.ToCharArray() _
        }

        ' Use this array to track the number of unused characters in each
        ' character group.
        Dim charsLeftInGroup As Integer() = New Integer(charGroups.Length - 1) {}

        ' Initially, all characters in each group are not used.
        Dim I As Integer
        For I = 0 To charsLeftInGroup.Length - 1
            charsLeftInGroup(I) = charGroups(I).Length
        Next

        ' Use this array to track (iterate through) unused character groups.
        Dim leftGroupsOrder As Integer() = New Integer(charGroups.Length - 1) {}

        ' Initially, all character groups are not used.
        For I = 0 To leftGroupsOrder.Length - 1
            leftGroupsOrder(I) = I
        Next

        ' Because we cannot use the default randomizer, which is based on the
        ' current time (it will produce the same "random" number within a
        ' second), we will use a random number generator to seed the
        ' randomizer.

        ' Use a 4-byte array to fill it with random bytes and convert it then
        ' to an integer value.
        Dim randomBytes As Byte() = New Byte(3) {}

        ' Generate 4 random bytes.
        Dim rng As RNGCryptoServiceProvider = New RNGCryptoServiceProvider()

        rng.GetBytes(randomBytes)

        ' Convert 4 bytes into a 32-bit integer value.
        Dim seed As Integer = ((randomBytes(0) And &H7F) << 24 Or _
                                randomBytes(1) << 16 Or _
                                randomBytes(2) << 8 Or _
                                randomBytes(3))

        ' Now, this is real randomization.
        Dim random As Random = New Random(seed)

        ' This array will hold password characters.
        Dim password As Char() = Nothing

        ' Allocate appropriate memory for the password.
        If (minLength < maxLength) Then
            password = New Char(random.Next(minLength - 1, maxLength)) {}
        Else
            password = New Char(minLength - 1) {}
        End If

        ' Index of the next character to be added to password.
        Dim nextCharIdx As Integer

        ' Index of the next character group to be processed.
        Dim nextGroupIdx As Integer

        ' Index which will be used to track not processed character groups.
        Dim nextLeftGroupsOrderIdx As Integer

        ' Index of the last non-processed character in a group.
        Dim lastCharIdx As Integer

        ' Index of the last non-processed group.
        Dim lastLeftGroupsOrderIdx As Integer = leftGroupsOrder.Length - 1

        ' Generate password characters one at a time.
        For I = 0 To password.Length - 1

            ' If only one character group remained unprocessed, process it;
            ' otherwise, pick a random character group from the unprocessed
            ' group list. To allow a special character to appear in the
            ' first position, increment the second parameter of the Next
            ' function call by one, i.e. lastLeftGroupsOrderIdx + 1.
            If (lastLeftGroupsOrderIdx = 0) Then
                nextLeftGroupsOrderIdx = 0
            Else
                nextLeftGroupsOrderIdx = random.Next(0, lastLeftGroupsOrderIdx)
            End If

            ' Get the actual index of the character group, from which we will
            ' pick the next character.
            nextGroupIdx = leftGroupsOrder(nextLeftGroupsOrderIdx)

            ' Get the index of the last unprocessed characters in this group.
            lastCharIdx = charsLeftInGroup(nextGroupIdx) - 1

            ' If only one unprocessed character is left, pick it; otherwise,
            ' get a random character from the unused character list.
            If (lastCharIdx = 0) Then
                nextCharIdx = 0
            Else
                nextCharIdx = random.Next(0, lastCharIdx + 1)
            End If

            ' Add this character to the password.
            password(I) = charGroups(nextGroupIdx)(nextCharIdx)

            ' If we processed the last character in this group, start over.
            If (lastCharIdx = 0) Then
                charsLeftInGroup(nextGroupIdx) = _
                                charGroups(nextGroupIdx).Length
                ' There are more unprocessed characters left.
            Else
                ' Swap processed character with the last unprocessed character
                ' so that we don't pick it until we process all characters in
                ' this group.
                If (lastCharIdx <> nextCharIdx) Then
                    Dim temp As Char = charGroups(nextGroupIdx)(lastCharIdx)
                    charGroups(nextGroupIdx)(lastCharIdx) = _
                                charGroups(nextGroupIdx)(nextCharIdx)
                    charGroups(nextGroupIdx)(nextCharIdx) = temp
                End If

                ' Decrement the number of unprocessed characters in
                ' this group.
                charsLeftInGroup(nextGroupIdx) = _
                           charsLeftInGroup(nextGroupIdx) - 1
            End If

            ' If we processed the last group, start all over.
            If (lastLeftGroupsOrderIdx = 0) Then
                lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1
                ' There are more unprocessed groups left.
            Else
                ' Swap processed group with the last unprocessed group
                ' so that we don't pick it until we process all groups.
                If (lastLeftGroupsOrderIdx <> nextLeftGroupsOrderIdx) Then
                    Dim temp As Integer = _
                                leftGroupsOrder(lastLeftGroupsOrderIdx)
                    leftGroupsOrder(lastLeftGroupsOrderIdx) = _
                                leftGroupsOrder(nextLeftGroupsOrderIdx)
                    leftGroupsOrder(nextLeftGroupsOrderIdx) = temp
                End If

                ' Decrement the number of unprocessed groups.
                lastLeftGroupsOrderIdx = lastLeftGroupsOrderIdx - 1
            End If
        Next

        ' Convert password characters into a string and return the result.
        Generate = New String(password)
    End Function

End Class

Public Structure TimeElapse
    Public Property Year As Integer
    Public Property Month As Integer
    Public Property Day As Integer
    Public Sub New(defaultYear%, defaultMonth%, defaultDay%)
        Year = defaultYear : Month = defaultMonth : Day = defaultDay
    End Sub
End Structure

Public Class DateFunctions
    Public Shared Function Difference(firstDate As DateTime, secondDate As DateTime) As TimeElapse
        Dim tE As New TimeElapse(0, 0, 0)
        While firstDate.AddYears(1) <= secondDate
            tE.Year += 1
            firstDate = firstDate.AddYears(1)
        End While
        While firstDate.AddMonths(1) <= secondDate
            tE.Month += 1
            firstDate = firstDate.AddMonths(1)
        End While
        While firstDate.AddDays(1) <= secondDate
            tE.Day += 1
            firstDate = firstDate.AddDays(1)
        End While

        Return tE
    End Function
End Class

Public Class DocumentCodePrefix
    Public Const PURCHASE_REQUISITION_STOCK As String = "PRT"
    Public Const PURCHASE_REQUISITION_NON_STOCK As String = "PRN"
    Public Const PURCHASE_REQUISITION_FIXED_ASSET As String = "PRF"
End Class