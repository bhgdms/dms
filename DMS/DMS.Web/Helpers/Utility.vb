#Region " References Used "
Imports System.Web.Configuration
Imports System.Net.Mail
Imports System.IO
Imports System.Data
Imports System.Xml
Imports System.Web.HttpRequest
Imports System.Text
Imports System.Web
Imports System.Xml.Xsl
Imports System.Runtime.InteropServices.Marshal
Imports System.Reflection
#End Region

Imports Microsoft.VisualBasic

Public Class Utility

    Shared objRandom As New System.Random( _
  CType(System.DateTime.Now.Ticks Mod System.Int32.MaxValue, Integer))
    Public Shared Function CountDays(ByVal StartDate As Date, ByVal EndDate As Date) As String
        If Not (StartDate = Nothing Or EndDate = Nothing) Then
            Dim ts As TimeSpan = EndDate.Subtract(StartDate)
            Return ts.TotalDays
        Else
            Return ""
        End If

    End Function
    Public Shared Function Random(Optional ByVal Low As Integer = 1, _
        Optional ByVal High As Integer = 100) As String
        ' Returns a random number,
        ' between the optional Low and High parameters
        Return objRandom.Next(Low, High + 1).ToString
    End Function
    ''' <summary>
    ''' Runs client-side script
    ''' </summary>
    Public Shared Sub RunScript(ByVal Script As String, ByVal ThisPage As System.Web.UI.Page, Optional ByVal ScriptName As String = "ClientScript")
        Script = "<script language='javascript'>" + Script + "</script>"
        ThisPage.ClientScript.RegisterClientScriptBlock(ThisPage.GetType, ScriptName, Script)
    End Sub
    Public Shared Sub ShowMessageAndRedirect(ByVal message As String, ByVal url As String, ByVal ThisPage As System.Web.UI.Page, Optional ByVal ScriptName As String = "ClientScript")
        Dim Script$ = "<script language='javascript'>alert('" + message + "'); window.location.href = '" + url + "'; </script>"
        ThisPage.ClientScript.RegisterClientScriptBlock(ThisPage.GetType, ScriptName, Script)
    End Sub
    ''' <summary>
    ''' Common function to display message
    ''' </summary>
    Public Shared Sub MessageBox(ByVal Message As String, ByVal ThisPage As System.Web.UI.Page, Optional ByVal ScriptName As String = "ClientScript")
        Dim Script As String = "<script language='javascript'>alert('" + Message + "');</script>"
        ThisPage.ClientScript.RegisterStartupScript(ThisPage.GetType, ScriptName, Script)
    End Sub
    ''' <summary>
    ''' Removes the null from the variable
    ''' </summary>
    Public Shared Function RemoveNull(ByVal objVal As Object) As String
        If IsDBNull(objVal) Then
            RemoveNull = ""
        Else
            If objVal.ToString = "" Then
                RemoveNull = ""
            Else
                RemoveNull = objVal
            End If
        End If
    End Function

    Public Shared Function GetDate(ByVal DBDate As Object) As String
        Try
            If Not DBDate Is DBNull.Value Then
                Return CDate(DBDate).ToString("dd/MM/yyyy")
            End If
            Return "&nbsp;"
        Catch
            Return "&nbsp;"
        End Try
    End Function
    Public Shared Function RemoveDateNull(ByVal objDate As Object) As String
        Try
            If IsDBNull(objDate) Or objDate.ToString = "" Then
                RemoveDateNull = Nothing
            Else
                RemoveDateNull = Format(Convert.ToDateTime(Date.ParseExact(objDate, "dd/MM/yyyy", Nothing)), "MM/dd/yyyy").Trim
            End If
        Catch
            RemoveDateNull = Nothing
        End Try
    End Function
    Public Shared Function GetBinaryData(ByVal fileName As String) As Byte()
        Try
            If File.Exists(fileName) Then Return File.ReadAllBytes(fileName)
            Return Nothing
        Catch
            Throw
        End Try
    End Function
    Public Shared Function GetTemporaryFileName(ByVal folderName As String, ByVal extension As String) As String
        Dim temporaryFile As String = System.IO.Path.GetFileName(System.IO.Path.ChangeExtension(System.IO.Path.GetTempFileName, extension))
        While File.Exists(folderName + "\" + temporaryFile)
            temporaryFile = System.IO.Path.GetFileName(System.IO.Path.ChangeExtension(System.IO.Path.GetTempFileName, extension))
        End While
        Return temporaryFile
    End Function
    Public Shared Sub BindDesignation(lControl As ListControl, Optional addSelect As Boolean = True)
        lControl.Items.Clear()
        If addSelect Then lControl.Items.Add(New ListItem("-- Select --", ""))
        lControl.Items.Add(New ListItem("Security Manager", "S"))
        lControl.Items.Add(New ListItem("Security Guard", "G"))
        lControl.Items.Add(New ListItem("Supervisor", "V"))
        lControl.Items.Add(New ListItem("Cleaner", "C"))
        lControl.Items.Add(New ListItem("Maintenance", "M"))
    End Sub
    Public Shared Function CombinedSelectedTextIntoSingleString(ctrl As ListControl, Optional seperator As String = "^", Optional hasSelect As Boolean = False) As String
        Dim combinedString As String = String.Empty
        For i As Integer = If(hasSelect, 1, 0) To ctrl.Items.Count - 1
            If ctrl.Items(i).Selected Then combinedString += ctrl.Items(i).Text.Trim + seperator
        Next
        Return combinedString
    End Function
    Public Shared Function CombinedSelectedValueIntoSingleString(ctrl As ListControl, Optional seperator As String = "^", Optional hasSelect As Boolean = False) As String
        Dim combinedString As String = String.Empty
        For i As Integer = If(hasSelect, 1, 0) To ctrl.Items.Count - 1
            If ctrl.Items(i).Selected Then combinedString += ctrl.Items(i).Value.Trim + seperator
        Next
        Return combinedString
    End Function
    Public Shared Sub BindControlWithSelectedItem(ctrl As ListControl, items As String, Optional seperator As String = "^")
        Dim itemArray() As String = items.Split(seperator.ToCharArray)
        For j As Integer = 0 To ctrl.Items.Count - 1
            ctrl.Items(j).Selected = False
            For i As Integer = 0 To itemArray.Count - 1
                If ctrl.Items(j).Value = itemArray(i) Then
                    ctrl.Items(j).Selected = True
                    Exit For
                End If
            Next
        Next
    End Sub
    Public Shared Function AmountInWords(ByVal nAmount As String, Optional ByVal wAmount _
                     As String = vbNullString, Optional ByVal nSet As Object = Nothing) As String
        'Let's make sure entered value is numeric
        If Not IsNumeric(nAmount) Then Return "Please enter numeric values only."

        Dim tempDecValue As String = String.Empty : If InStr(nAmount, ".") Then _
            tempDecValue = nAmount.Substring(nAmount.IndexOf("."))
        nAmount = Replace(nAmount, tempDecValue, String.Empty)

        Try
            Dim intAmount As Long = nAmount
            If intAmount > 0 Then
                nSet = IIf((intAmount.ToString.Trim.Length / 3) _
                 > (CLng(intAmount.ToString.Trim.Length / 3)), _
                  CLng(intAmount.ToString.Trim.Length / 3) + 1, _
                   CLng(intAmount.ToString.Trim.Length / 3))
                Dim eAmount As Long = Microsoft.VisualBasic.Left(intAmount.ToString.Trim, _
                  (intAmount.ToString.Trim.Length - ((nSet - 1) * 3)))
                Dim multiplier As Long = 10 ^ (((nSet - 1) * 3))

                Dim Ones() As String = _
                {"", "One", "Two", "Three", _
                  "Four", "Five", _
                  "Six", "Seven", "Eight", "Nine"}
                Dim Teens() As String = {"", _
                "Eleven", "Twelve", "Thirteen", _
                  "Fourteen", "Fifteen", _
                  "Sixteen", "Seventeen", "Eighteen", "Nineteen"}
                Dim Tens() As String = {"", "Ten", _
                "Twenty", "Thirty", _
                  "Forty", "Fifty", "Sixty", _
                  "Seventy", "Eighty", "Ninety"}
                Dim HMBT() As String = {"", "", _
                "Thousand", "Million", _
                  "Billion", "Trillion", _
                  "Quadrillion", "Quintillion"}

                intAmount = eAmount

                Dim nHundred As Integer = intAmount \ 100 : intAmount = intAmount Mod 100
                Dim nTen As Integer = intAmount \ 10 : intAmount = intAmount Mod 10
                Dim nOne As Integer = intAmount \ 1

                If nHundred > 0 Then wAmount = wAmount & _
                Ones(nHundred) & " Hundred " 'This is for hundreds                
                If nTen > 0 Then 'This is for tens and teens
                    If nTen = 1 And nOne > 0 Then 'This is for teens 
                        wAmount = wAmount & Teens(nOne) & " "
                    Else 'This is for tens, 10 to 90
                        wAmount = wAmount & Tens(nTen) & IIf(nOne > 0, "-", " ")
                        If nOne > 0 Then wAmount = wAmount & Ones(nOne) & " "
                    End If
                Else 'This is for ones, 1 to 9
                    If nOne > 0 Then wAmount = wAmount & Ones(nOne) & " "
                End If
                wAmount = wAmount & HMBT(nSet) & " "
                wAmount = AmountInWords(CStr(CLng(nAmount) - _
                  (eAmount * multiplier)).Trim & tempDecValue, wAmount, nSet - 1)
            Else
                If Val(nAmount) = 0 Then nAmount = nAmount & _
                tempDecValue : tempDecValue = String.Empty
                If (Math.Round(Val(nAmount), 2) * 100) > 0 Then wAmount = _
                  Trim(AmountInWords(CStr(Math.Round(Val(nAmount), 2) * 100), _
                  wAmount.Trim & " Dirham And ", 1)) & " Fils"
            End If
        Catch
            Throw
        End Try

        'Trap null values
        If IsNothing(wAmount) = True Then wAmount = String.Empty Else wAmount = _
          IIf(InStr(wAmount.Trim, "Dirham"), _
          wAmount.Trim, wAmount.Trim & " Dirham")

        'Display the result
        Return wAmount
    End Function
    Public Shared Function SplitWord(amount As String) As String

        If Len(amount) > 35 Then
            Dim index As Integer = amount.IndexOf(" ", 35)
            If index >= 0 Then
                Dim firststring As String = amount.Substring(0, index)
                If (index < Len(amount) - 1) Then firststring += vbCrLf & vbCrLf & amount.Substring(index + 1)
                amount = firststring
            End If
        End If

        Return amount
    End Function
End Class

Public Class SourceType
    Public Const TENANCY_CONTRACT As String = "TC"
    Public Const RESERVATION As String = "RS"
    Public Const VOUCHER As String = "VC"
    Public Const PURCHASE_INQUIRY As String = "PQ"
    Public Const PURCHASE_ORDER As String = "PO"
    Public Const PURCHASE_INVOICE As String = "PI"
    Public Const POST_DATED_CHEQUE As String = "PD"
    Public Const WORK_ORDER_STAFF As String = "WS"
    Public Const WORK_ORDER_DOCUMENT As String = "WD"
End Class