﻿Imports System.Runtime.CompilerServices
Public Module DMSExtension
    <Extension()>
    Public Function ToStandardDate(ByVal sourceDateString$) As String
        Try
            If IsDBNull(sourceDateString) OrElse sourceDateString Is Nothing Then Return Nothing
            Dim dateArray() As String = sourceDateString.Split("/")
            If dateArray.Length = 3 Then
                Return New DateTime(dateArray(2), dateArray(1), dateArray(0)).ToString("dd-MMM-yyyy")
            End If
            Return Nothing
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <Extension()>
    Public Function [In](Of T)(source As T, ParamArray arrays() As T) As Boolean
        Try
            If IsDBNull(arrays) OrElse arrays Is Nothing Then Return False

            Return arrays.Contains(source)
        Catch ex As Exception
            Return False
        End Try
    End Function

    <Extension()>
    Public Function ToMoneyString(value As Decimal) As String
        If value = 0 Then
            Return "0"
        Else
            Return value.ToString("#,##0.##")
        End If
    End Function
    <Extension()>
    Public Sub ResetToNothing(Of T)(source As T)
        source = Nothing
    End Sub

    <Extension()>
    Public Function HasValue(Of T)(source As T) As Boolean
        Try
            Return Not IsDBNull(source) AndAlso Not IsNothing(source) AndAlso source.ToString.Trim <> String.Empty
        Catch ex As Exception
            Return False
        End Try
    End Function

    <Extension()>
    Public Sub Reset(source As TextBox)
        source.Text = String.Empty
    End Sub

    <Extension()>
    Public Function ReplaceEx(sourceString As String, findString As String, replaceString As String, Optional IgnoreCase As Boolean = False) As String
        If Not IgnoreCase Then
            Return sourceString.Replace(findString, replaceString)
        Else
            Dim tempString$ = sourceString.ToLower
            Dim finalString$ = String.Empty
            findString = findString.ToLower
            Dim index% = tempString.IndexOf(findString)
            If index >= 0 Then
                While index >= 0
                    finalString += sourceString.Substring(0, index) + replaceString
                    sourceString = sourceString.Substring(index + findString.Length)
                    tempString = tempString.Substring(index + findString.Length)
                    index% = tempString.IndexOf(findString)
                End While

                If sourceString.Length > 0 Then finalString += sourceString
            Else
                finalString = sourceString
            End If
            Return finalString
        End If
    End Function

    <Extension()>
    Public Function Coalesce(Of T)(sourceObject As T, ParamArray objectCollection() As T) As String
        If Not IsDBNull(sourceObject) AndAlso sourceObject IsNot Nothing Then
            Return sourceObject.ToString
        Else
            For i As Integer = 0 To objectCollection.Length - 1
                If Not IsDBNull(objectCollection(i)) AndAlso objectCollection(i) IsNot Nothing Then Return objectCollection(i).ToString
            Next
        End If
        Return String.Empty
    End Function

    <Extension()>
    Public Function ConvertToZeroIfEmpty(sourceObject As String) As String
        If Not IsDBNull(sourceObject) AndAlso sourceObject IsNot Nothing AndAlso sourceObject.ToString().Trim <> String.Empty Then
            Return sourceObject.ToString.Trim.Replace(",", "")
        Else
            Return "0"
        End If
    End Function

    <Extension()>
    Public Function [IsBetween](source As Decimal, startFrom As Decimal, endTo As Decimal) As Boolean
        Try
            Return source >= startFrom And source <= endTo
        Catch ex As Exception
            Return False
        End Try
    End Function
    <Extension()>
    Public Function [IsBetween](source As Integer, startFrom As Integer, endTo As Integer) As Boolean
        Try
            Return source >= startFrom And source <= endTo
        Catch ex As Exception
            Return False
        End Try
    End Function

    <Extension()>
    Public Sub ClientVisible(source As WebControl, isVisible As Boolean)
        source.Style("Display") = If(isVisible, "", "none")
    End Sub

    <Extension()>
    Public Function ClientVisible(source As WebControl) As Boolean
        Return source.Style("Display") <> "none"
    End Function

    <Extension()>
    Public Sub ClientVisible(source As HtmlControl, isVisible As Boolean)
        source.Style("Display") = If(isVisible, "", "none")
    End Sub

    <Extension()>
    Public Function ClientVisible(source As HtmlControl) As Boolean
        Return source.Style("Display") <> "none"
    End Function

End Module