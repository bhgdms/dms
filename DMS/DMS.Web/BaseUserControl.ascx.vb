﻿Public Class BaseUserControl
    Inherits System.Web.UI.UserControl

    Private __current_update_panel As UpdatePanel

    Private Function GetUpdatePanel(ByVal parentControl As Control) As Control
        For Each subControl As Control In parentControl.Controls

            If TypeOf (subControl) Is UpdatePanel Then
                Return subControl
            End If
        Next

        For Each subControl As Control In parentControl.Controls

            If subControl.HasControls() Then
                Dim thisControl As Control = GetUpdatePanel(subControl)
                If (thisControl IsNot Nothing) Then Return thisControl
            End If
        Next

        Return Nothing
    End Function

    Protected WriteOnly Property PageMessage As String
        Set(ByVal value As String)
            RunScript("alert('" & value & "');")
        End Set
    End Property

    Protected Property RunningUpdatePanel As UpdatePanel
        Private Get

            If __current_update_panel Is Nothing Then
                __current_update_panel = CType(GetUpdatePanel(Me.Page), UpdatePanel)
            End If

            Return __current_update_panel
        End Get
        Set(ByVal value As UpdatePanel)
            __current_update_panel = value
        End Set
    End Property

    Protected Sub RunScript(ByVal script As String)
        If RunningUpdatePanel IsNot Nothing Then
            ScriptManager.RegisterStartupScript(RunningUpdatePanel, RunningUpdatePanel.[GetType](), (New Guid()).ToString(), script, True)
        Else
            Me.Page.ClientScript.RegisterStartupScript(Me.[GetType](), (New Guid()).ToString(), script, True)
        End If
    End Sub

    Protected Sub ShowMessage(ByVal message As String, ByVal Optional title As String = "DMS")
        RunScript(String.Format("$.showMessage('{0}','{1}');", message, title))
    End Sub

    Protected Sub ShowMessageAndRedirect(ByVal message As String, ByVal urlToRedirect As String, ByVal Optional title As String = "DMS")
        RunScript(String.Format("$.showMessageAndRedirect('{0}','{1}','{2}');", message, title, urlToRedirect))
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

End Class