﻿Imports System.IO
Imports DMS.DataContext.DataAccessHelper
Imports DMS.Models.DTO

Public Class ucDocUploader
    Inherits System.Web.UI.UserControl

    Dim fileDto As List(Of FileDTO)
    Dim caller As Documents

    Public Property DocId As Integer
        Get
            Return ViewState("DOCUMENT_ID")
        End Get
        Set(value As Integer)
            ViewState("DOCUMENT_ID") = value
        End Set
    End Property

    Public Property Status As String
        Get
            Return ViewState("STATUS")
        End Get
        Set(value As String)
            ViewState("STATUS") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        fileDto = New List(Of FileDTO)
        caller = New Documents()
        If Not IsPostBack Then
            BindDocuments()
            Initialize()
        End If

    End Sub

    Private Sub Initialize()
        If Request.QueryString("PARAM") IsNot Nothing Then
            Dim queryEnc As New QueryString("PARAM")
            Dim items As ListItemCollection = queryEnc.Decompose()

            If items.Count > 0 AndAlso items.FindByValue("DID") IsNot Nothing Then
                DocId = CInt(items.FindByValue("DID").Text)
            End If

            If Status.In("N", "W") Then
                divFlUpload.Visible = True
            Else
                divFlUpload.Visible = False
            End If

        Else
            If Session("isDataUnchanged") Then
                Session("FileList") = Nothing
            End If
        End If
    End Sub

    Public Sub BindDocuments()
        caller = New Documents()
        Dim files As List(Of ListItem) = New List(Of ListItem)
        Dim isDataUnchanged As Boolean = True

        If Session("isDataUnchanged") IsNot Nothing Then
            isDataUnchanged = DirectCast(Session("isDataUnchanged"), Boolean)
        End If


        If Session("FileList") IsNot Nothing Then
            fileDto = DirectCast(Session("FileList"), List(Of FileDTO))
            fileDto = fileDto.Where(Function(f) f.DocumentId = DocId).ToList()

            If fileDto.Count() = 0 And isDataUnchanged Then
                fileDto = caller.GetDocumentAttachments(DocId)
                Session("FileList") = fileDto
            End If

            For Each item In fileDto
                files.Add(New ListItem(Path.GetFileName(item.FilePath), item.FilePath))
            Next

        Else
            fileDto = caller.GetDocumentAttachments(DocId)
            Session("FileList") = fileDto
            For Each item In fileDto
                files.Add(New ListItem(Path.GetFileName(item.FilePath), item.FilePath))
            Next

        End If

        gvFiles.DataSource = files
        gvFiles.DataBind()

    End Sub

    Private Sub gvFiles_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFiles.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim deleteLink As LinkButton = DirectCast(e.Row.FindControl("lnkDelete"), LinkButton)
            Dim separator As Label = DirectCast(e.Row.FindControl("lblSeparator"), Label)

            If Status = "A" Then
                deleteLink.Visible = False
                separator.Visible = False
            End If
        End If
    End Sub
    Protected Sub DownloadFile(ByVal sender As Object, ByVal e As EventArgs)
        Dim filePath As String = CType(sender, LinkButton).CommandArgument
        filePath = Server.MapPath("~\") + filePath
        'Response.ContentType = ContentType
        Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(filePath)))
        Response.WriteFile(filePath)
        Response.End()
    End Sub

    Protected Sub DeleteFile(ByVal sender As Object, ByVal e As EventArgs)
        Dim filePath As String = CType(sender, LinkButton).CommandArgument
        Dim fileName As String = Path.GetFileName(filePath)

        If Session("FileList") IsNot Nothing Then

            fileDto = DirectCast(Session("FileList"), List(Of FileDTO))
            Dim file As FileDTO = fileDto.Where(Function(f) f.FileName = fileName).FirstOrDefault()

            fileDto.Remove(file)
            Session("FileList") = fileDto
            Session("isDataUnchanged") = False

            gvFiles.DataSource = fileDto
            gvFiles.DataBind()
        End If

        Response.Redirect(Request.Url.AbsoluteUri)
    End Sub

    Public Function AddFiles() As Boolean
        If FileUpload1.HasFile Then
            Dim fileName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)
            FileUpload1.PostedFile.SaveAs((Server.MapPath("~/data/") + fileName))

            Dim files As List(Of FileDTO) = New List(Of FileDTO)
            Dim file As FileDTO = New FileDTO()
            file.FileName = fileName
            file.DocumentId = DocId
            file.FilePath = Path.Combine("data\") + fileName
            file.UploadedBy = UserProfile.UserID
            file.UploadedOn = DateTime.Now

            If Session("FileList") IsNot Nothing Then
                files = DirectCast(Session("FileList"), List(Of FileDTO))
                files.Add(file)
                Session("FileList") = files
            Else
                files.Add(file)
                Session("FileList") = files
            End If
            Session("isDataUnchanged") = False
            BindDocuments()
        End If

        Return True
    End Function

    Private Sub btnAddFiles_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click
        Try
            AddFiles()
        Catch ex As Exception

        End Try
    End Sub
End Class