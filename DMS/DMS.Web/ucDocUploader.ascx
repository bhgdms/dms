﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucDocUploader.ascx.vb" Inherits="DMS.Web.ucDocUploader" %>

<div id="divFlUpload" runat="server" class="inlineblock moveMiddle" style="width: 473px; padding-left: 150px">
    <div class="w3-row">
        <asp:FileUpload ID="FileUpload1" runat="server" />
        <span style="margin-left: 4px;">
            <asp:Button ID="btnUpload" runat="server" CssClass="btn-upload" Text="Upload" /></span>
    </div>
</div>

<div class="inlineblock moveMiddle" style="width: 425px">
    <asp:GridView ID="gvFiles" runat="server" AutoGenerateColumns="False" Width="100%">
        <RowStyle CssClass="DataGridItem" />
        <HeaderStyle CssClass="DataGridHeader" />
        <AlternatingRowStyle CssClass="DataGridAlternateItem" />
        <EmptyDataTemplate>
            <div class="w3-center w3-card w3-padding w3-pale-yellow emptyList">
                <div class="w3-padding-4">
                    <<< NO FILES UPLOADED >>>
                </div>
            </div>
        </EmptyDataTemplate>
        <Columns>
            <asp:TemplateField HeaderText="No." ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <span style="width: 20px;"><%#Container.DataItemIndex + 1 %>.</span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Text" HeaderText="File" ItemStyle-Width="50%" />
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkDownload" Text="Download" CommandArgument='<%# Eval("Value") %>'
                        runat="server" OnClick="DownloadFile"></asp:LinkButton>
                    <asp:Label ID="lblSeparator" runat="server"> | </asp:Label>
                    <asp:LinkButton ID="lnkDelete" Text="Delete" CommandArgument='<%# Eval("Value") %>'
                        runat="server" OnClick="DeleteFile" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" Width="35%" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>
<script type="text/javascript">

    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
                <%=Me.ClientID%>_init_page_events_and_controls();
            });


    function <%=Me.ClientID%>_init_page_events_and_controls() {

        $('.btn-upload').bind('click', function () {
            var zone = $(this).closest('div');
            var e = event || window.event;
            if (e.keyCode == 13) {
                if ($.trim($(this).val()).length == 0) {
                    $.showMessage('No remarks added !!!');
                }
                else {
                    window.location = $('.btn-upload', zone).prop('href');
                }
                return false;
            }
        });

    }
</script>
