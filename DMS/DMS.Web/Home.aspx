﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Home.aspx.vb" Inherits="DMS.Web.Home" %>

<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.3.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pageHeader">
        <div style="width: 100%;">
            <div class="w3-padding inlineblock moveMiddle w3-left" style="width: 400px">
                <div class="Title">
                    Pending Documents
                </div>
                <div class="TitleDescription">
                    All Pending Documents for remarks 
                </div>
            </div>

        </div>
    </div>
    <div>
        <dx:ASPxGridView ID="gridDocuments" ClientInstanceName="gridDocuments" runat="server" EnableRowsCache="false"
            Width="100%" KeyFieldName="Id" Font-Size="18pt" AutoGenerateColumns="False" DataSourceID="odsDocuments" EnableTheming="True" Theme="Glass">
            <Toolbars>
                <dx:GridViewToolbar EnableAdaptivity="true">
                    <Items>
                        <dx:GridViewToolbarItem Name="CustomExportToPDF" Text="Export to PDF" Image-IconID="export_exporttopdf_16x16" />
                        <dx:GridViewToolbarItem Name="CustomExportToXLSX" Text="Export to XLSX" Image-IconID="export_exporttoxlsx_16x16office2013">
                        </dx:GridViewToolbarItem> 
                    </Items>
                </dx:GridViewToolbar>
            </Toolbars>
            <Columns>
                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                    <DataItemTemplate>
                        <%#Container.ItemIndex + 1 %>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Width="40px" CellStyle-Wrap="false" VisibleIndex="1" CellStyle-HorizontalAlign="Center">
                    <DataItemTemplate>
                        <a href='Documents/ViewDocument.aspx?<%#GetDetailUrl(Eval("Id")) %>' title="View Detail">
                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Priority" Caption="Priority" Width="80px" />
                <dx:GridViewDataDateColumn FieldName="DateReceived" Caption="Received Date" Width="120px">
                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataDateColumn> 
                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Document Code" Width="140px" >
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn> 
                <dx:GridViewDataTextColumn FieldName="Site" Caption="Site" Width="60px" >
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn> 
                <dx:GridViewDataTextColumn FieldName="Category" Caption="Category" Width="100px" >
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="DocumentPurpose" Caption="Purpose" Width="130px" Visible="false">
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Sender" Caption="Sender" Width="150px" >
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Receiver" Caption="Receiver" Width="150px" >
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SubjectTitle" Caption="Subject" Width="350px" >
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn> 
                <dx:GridViewDataTextColumn FieldName="ReferenceNo" Caption="Reference No" Width="200px">
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn> 
                <dx:GridViewDataTextColumn FieldName="ReplyRefNo" Caption="Reply Reference No" Width="200px">
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn> 
                <dx:GridViewDataTextColumn FieldName="SubmittedBy" Caption="Submitted By" Width="120px" Visible="false" >
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ApprovedBy" Caption="Approved By" Width="120px" Visible="false" >
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="WorkflowStatusDescription" Caption="Status" Width="120px" Visible="false" />  

            </Columns> 
            <Settings ShowHeaderFilterButton="true" HorizontalScrollBarMode="Visible" ShowFilterBar="Visible" ShowFilterRow="true" ShowFilterRowMenu="true" ShowFooter="true" ShowGroupPanel="true" ShowGroupFooter="VisibleIfExpanded" ShowPreview="true" />
            <SettingsPager PageSize="10">
                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
            </SettingsPager>
            <SettingsBehavior />
            <FormatConditions>
                <dx:GridViewFormatConditionHighlight ShowInColumn="WorkflowStatusDescription" Expression="[WorkflowStatus] = 'N'" Format="Custom" CellStyle-CssClass="w3-grey" />
                <dx:GridViewFormatConditionHighlight ShowInColumn="WorkflowStatusDescription" Expression="[WorkflowStatus] = 'W'" Format="Custom" CellStyle-CssClass="w3-purple" />
                <dx:GridViewFormatConditionHighlight ShowInColumn="WorkflowStatusDescription" Expression="[WorkflowStatus] = 'A'" Format="Custom" CellStyle-CssClass="w3-teal" />
                <dx:GridViewFormatConditionHighlight ShowInColumn="Priority" Expression="[Priority] = 'High'" Format="Custom" CellStyle-CssClass="w3-red" />
                <dx:GridViewFormatConditionHighlight ShowInColumn="Priority" Expression="[Priority] = 'Low'" Format="Custom" CellStyle-CssClass="w3-green" />
                <dx:GridViewFormatConditionHighlight ShowInColumn="Priority" Expression="[Priority] = 'Normal'" Format="Custom" CellStyle-CssClass="w3-yellow" />
            </FormatConditions>
            <SettingsResizing  ColumnResizeMode="Control" Visualization="Live" />
            <ClientSideEvents ToolbarItemClick="function (s, e) { onToolbarClick(s, e); }" />
            <SettingsExport EnableClientSideExportAPI="true" Landscape="true" LeftMargin="0" RightMargin="0" TopMargin="0" BottomMargin="0" ExcelExportMode="WYSIWYG" />
            <SettingsSearchPanel Visible="True" />
        </dx:ASPxGridView>
        <asp:ObjectDataSource ID="odsDocuments" runat="server" DataObjectTypeName="DMS.DataContext.DataAccessHelper.Documents"
            SelectMethod="GetPendingDocuments" TypeName="DMS.DataContext.DataAccessHelper.Documents">
            <SelectParameters>
                 <asp:Parameter Name="roleId" DefaultValue="" />
                 <asp:Parameter Name="siteId" DefaultValue="" />
                 <%--<asp:Parameter Name="userName" DefaultValue="" />--%>
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
    <script type="text/javascript">    
        function onToolbarClick(s, e) {
            if (e.item.name == 'AddNew') {
                window.location = 'ManageDocument.aspx';
                return false;
            }
            e.processOnServer = true; e.usePostBack = true;
        }
       
    </script>
</asp:Content>
