﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports DMS.Models.Common
Imports DMS.Models.DTO
Imports DMS.DataContext.DataAccessHelper
Imports System.Net.Mail

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
<System.Web.Script.Services.ScriptService()>
Public Class Authentication
    Inherits System.Web.Services.WebService

    <WebMethod(EnableSession:=True)>
    Public Function VerifyLogin(userName$, password$) As String
        Dim message As String = "FAILURE#Due to technical issue, cannot login to system"
        Try
            Dim userContext As New SystemAdmin

            Dim status As UserStatus = UserStatus.Active

            status = userContext.CheckUserStatusByUserId(userName, password)

            Select Case status
                Case UserStatus.Active
                    message = "SUCCESS#" + userName
                    SetUserProfile(userName)
                Case UserStatus.BLOCKED
                    message = "FAILURE#This account is blocked. Kindly contact the administrator."
                Case UserStatus.USER_INCORRECT
                    message = "FAILURE#Incorrect Username or Password"
                Case UserStatus.NOT_EXIST
                    message = "FAILURE#Username or Password does not match"
            End Select
        Catch ex As Exception
            message = "FAILURE#" + ex.ToString().Replace("#", "_")
        Finally
        End Try
        Return message
    End Function

    <WebMethod()>
    Public Function EmailPassword(emailId$) As String
        Dim message As String = "FAILURE#Due to technical issue, cannot email the password !!!"

        Dim userContext As New SystemAdmin
        Dim status As UserStatus = UserStatus.Active
        status = userContext.CheckUserStatusByEmail(emailId)
        Select Case status
            Case UserStatus.NOT_EXIST
                message = "FAILURE#User does not exists. Kindly contact the system administrator for assistance."
            Case UserStatus.BLOCKED
                message = "FAILURE#The account is blocked. Kindly contact the system administrator for assistance."
            Case UserStatus.USER_INCORRECT
                message = "FAILURE#Email address does not exists. Kindly contact the system administrator for assistance."
            Case Else
                Dim subject$ = "Message from Document Management System"
                Dim body$ = String.Format(EmailMessages.FORGOT_PASSWORD)

                Dim smtp As New SmtpClient
                Dim msg As New MailMessage()
                smtp.UseDefaultCredentials = False
                smtp.Credentials = New Net.NetworkCredential("a@gmail.com", "password")
                smtp.Port = 587
                smtp.EnableSsl = True
                smtp.Host = "smtp.gmail.com"

                msg.To.Add(emailId)
                msg.Body = body
                msg.Subject = subject

                message = "FAILURE#Sorry!!! the password cannot be emailed."
                Try
                    smtp.Send(msg)
                    message = "SUCCESS#Your password has been emailed to your email address"
                Catch
                    message = "FAILURE#Email failed to sent a message"
                End Try
        End Select
        Return message
    End Function

    Private Sub SetUserProfile(userName As String)
        Dim userContext As New SystemAdmin
        Dim user As UserDTO = userContext.GetUserByUserName(userName)
        Dim up As New UserProfile

        up.SetProfile(user.UserId, user.Name, user.Password, user.RoleId, user.Role, user.Blocked, user.SiteID)

    End Sub
End Class