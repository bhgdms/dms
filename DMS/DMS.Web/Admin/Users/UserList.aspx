﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="UserList.aspx.vb" Inherits="DMS.Web.UserList" %>


<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.3.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pageHeader">
        <div style="width: 100%;">
            <div class="w3-padding inlineblock moveMiddle w3-left" style="width: 400px">
                <div class="Title">
                    User List
                </div>
                <div class="TitleDescription">
                    All Site's User list
                </div>
            </div>

        </div>
    </div>
    <div>
        <dx:ASPxGridView ID="gridUsers" ClientInstanceName="gridUsers" runat="server" EnableRowsCache="false"
            Width="100%" KeyFieldName="UID" Font-Size="18pt" AutoGenerateColumns="False" DataSourceID="odsUser" EnableTheming="True" Theme="Glass">
            <Toolbars>
                <dx:GridViewToolbar EnableAdaptivity="true">
                    <Items>
                        <dx:GridViewToolbarItem Name="CustomExportToPDF" Text="Export to PDF" Image-IconID="export_exporttopdf_16x16" />
                        <dx:GridViewToolbarItem Name="CustomExportToXLSX" Text="Export to XLSX" Image-IconID="export_exporttoxlsx_16x16office2013">
                        </dx:GridViewToolbarItem>
                        <dx:GridViewToolbarItem Name="AddNew" Text="New User" Image-IconID="actions_newproducts_16x16devav">
                        </dx:GridViewToolbarItem>
                    </Items>
                </dx:GridViewToolbar>
            </Toolbars>
            <Columns>
                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                    <DataItemTemplate>
                        <%#Container.ItemIndex + 1 %>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Width="40px" CellStyle-Wrap="false" VisibleIndex="1" CellStyle-HorizontalAlign="Center">
                    <DataItemTemplate>
                        <a href='ManageUser.aspx?<%#GetDetailUrl(Eval("UID")) %>' title="View Detail" >
                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="UserId" Caption="UserId" VisibleIndex="2" Width="100px" >
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Name" Caption="User Name" VisibleIndex="3" Width="200px">
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SiteName" Caption="Site" VisibleIndex="3" Width="200px">
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Role" Caption="Role" VisibleIndex="3" Width="200px">
                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn>

            </Columns>
            <%--<Styles Cell-CssClass="w3-tiny" Footer-CssClass="w3-tiny" Header-CssClass="w3-tiny"></Styles>--%>
            <Settings ShowHeaderFilterButton="true" HorizontalScrollBarMode="Visible" ShowFilterBar="Visible" ShowFilterRow="true" ShowFilterRowMenu="true" ShowFooter="true" ShowGroupPanel="true" ShowGroupFooter="VisibleIfExpanded" ShowPreview="true" />
            <SettingsPager PageSize="10">
                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
            </SettingsPager>
            <SettingsBehavior />
            <SettingsResizing  ColumnResizeMode="Control" Visualization="Live" />
            <ClientSideEvents ToolbarItemClick="function (s, e) { onToolbarClick(s, e); }" />
            <SettingsExport EnableClientSideExportAPI="true" Landscape="true" LeftMargin="0" RightMargin="0" TopMargin="0" BottomMargin="0" ExcelExportMode="WYSIWYG" />
            <SettingsSearchPanel Visible="True" />
        </dx:ASPxGridView>
        <asp:ObjectDataSource ID="odsUser" runat="server" DataObjectTypeName="DMS.DataContext.DataAccessHelper.SystemAdmin"
            SelectMethod="GetAllUsers" TypeName="DMS.DataContext.DataAccessHelper.SystemAdmin">

        </asp:ObjectDataSource>
    </div>
    <script type="text/javascript">  
        function onToolbarClick(s, e) {
            if (e.item.name == 'AddNew') {
                window.location = 'ManageUser.aspx';
                return false;
            }
            e.processOnServer = true; e.usePostBack = true;
        } 
    </script>
</asp:Content>
