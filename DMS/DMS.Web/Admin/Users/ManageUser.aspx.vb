﻿Imports DMS.DataContext.DataAccessHelper
Imports DMS.Models.DTO

Public Class ManageUser
    Inherits BasePage

    Dim caller As SystemAdmin

    Private Property UID As Integer
        Get
            Return ViewState("USER_ID")
        End Get
        Set(value As Integer)
            ViewState("USER_ID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            caller = New SystemAdmin()
            If Not IsPostBack Then
                FillRoles()
                FillProjectSites()
                Initialize()
            End If
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub FillRoles()
        Dim roles As List(Of RoleDTO) = New List(Of RoleDTO)
        roles = caller.GetAllRoles()

        If roles.Count > 0 Then
            ddlRole.DataSource = roles
            ddlRole.DataValueField = "RoleId"
            ddlRole.DataTextField = "Description"
            ddlRole.DataBind()

            ddlRole.Items.Insert(0, New ListItem("-- Select -- ", ""))
        End If
    End Sub

    Private Sub FillProjectSites()
        Dim sites As List(Of SiteDTO) = New List(Of SiteDTO)
        sites = caller.GetAllProjectSites()

        If sites.Count > 0 Then
            ddlSites.DataSource = sites
            ddlSites.DataValueField = "ID"
            ddlSites.DataTextField = "SiteID"
            ddlSites.DataBind()

            ddlSites.Items.Insert(0, New ListItem("-- Select -- ", ""))
        End If
    End Sub

    Private Sub Initialize()

        If Request.QueryString("PARAM") IsNot Nothing Then
            Dim queryEnc As New QueryString("PARAM")
            Dim items As ListItemCollection = queryEnc.Decompose()

            If items.Count > 0 AndAlso items.FindByValue("UID") IsNot Nothing Then
                UID = CInt(items.FindByValue("UID").Text)
                LoadUser()
            End If
        Else
            UID = 0
        End If

    End Sub

    Private Sub LoadUser()
        Try
            Dim user As UserDTO = New UserDTO()
            user = caller.GetUserById(UID)

            If user IsNot Nothing AndAlso user.UID > 0 Then
                txtUserID.Text = user.UserId
                txtUserID.Enabled = False
                'rowPassoword.Visible = False
                txtUserName.Text = user.Name
                txtPassword.Text = user.Password
                txtEmail.Text = user.Email
                txtMobile.Text = user.Mobile
                ddlSites.SelectedValue = user.SiteID
                ddlRole.SelectedValue = user.RoleId
                chkActive.Checked = user.Active
            End If

        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles lnkSave.Click

        Try
            Dim id As Integer = 0
            Dim obj As UserDTO = New UserDTO

            obj.UserId = txtUserID.Text
            obj.Name = txtUserName.Text
            obj.Password = txtPassword.Text
            obj.Email = txtEmail.Text
            obj.Mobile = txtMobile.Text
            obj.SiteID = ddlSites.SelectedValue
            obj.RoleId = ddlRole.SelectedValue
            obj.Active = chkActive.Checked
            obj.Blocked = False

            If UID = 0 Then
                obj.UID = 0
                obj.CreatedBy = UserProfile.UserID
                id = caller.CreateUser(obj)
            Else
                obj.UID = UID
                obj.ModifiedBy = UserProfile.UserID
                id = caller.UpdateUser(obj)
            End If


            If id > 0 Then
                ShowMessageAndRedirect("<div class=""w3-center""><div class=""w3-padding w3-text-red w3-xlarge"">" +
                                        "SAVED !!!</div><div class=""w3-padding"">User information has been saved...</div></div>", "UserList.aspx")
            Else

            End If

        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

End Class