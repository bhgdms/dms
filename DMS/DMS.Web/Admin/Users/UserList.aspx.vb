﻿Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Data

Public Class UserList
    Inherits BasePage

    Protected ReadOnly Property GetDetailUrl(UID$) As String
        Get
            Dim query As New QueryString("PARAM")
            query.Add("UID", UID)
            Return query.Generate
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub grd_ToolbarItemClick(source As Object, e As ASPxGridViewToolbarItemClickEventArgs) Handles gridUsers.ToolbarItemClick
        Try
            Dim grid As ASPxGridView = DirectCast(source, ASPxGridView)
            Dim tempFolder As String = PHYSICAL_TEMP_FOLDER + "\TEMP" + "\" + Session.SessionID
            If Not System.IO.Directory.Exists(tempFolder) Then System.IO.Directory.CreateDirectory(tempFolder)

            Select Case e.Item.Name
                Case "CustomExportToXLSX"
                    e.Handled = True
                    Dim fileName As String = "Users" + ".xlsx"
                    Dim memStream As New MemoryStream
                    Try
                        grid.ExportToXlsx(memStream, New DevExpress.XtraPrinting.XlsxExportOptionsEx() With {.ExportType = .ExportType.WYSIWYG})

                        Using fs As New FileStream(Server.MapPath("~/TEMP/" + Session.SessionID + "/" + fileName), FileMode.Create)
                            memStream.WriteTo(fs)
                            fs.Close()
                        End Using

                        RunScript("window.open('../../../Temp/" + Session.SessionID + "/" + fileName + "','_blank');")
                    Catch ex As Exception
                    Finally
                        If (memStream IsNot Nothing) Then
                            memStream.Close()
                            memStream.Dispose()
                            memStream = Nothing
                        End If
                    End Try

                Case "CustomExportToPDF"
                    e.Handled = True
                    Dim fileName As String = "Users" + ".pdf"
                    Dim memStream As New MemoryStream
                    Try
                        grid.ExportToPdf(memStream)

                        Using fs As New FileStream(Server.MapPath("~/TEMP/" + Session.SessionID + "/" + fileName), FileMode.Create)
                            memStream.WriteTo(fs)
                            fs.Close()
                        End Using

                        RunScript("window.open('../../../Temp/" + Session.SessionID + "/" + fileName + "','_blank');")
                    Catch ex As Exception
                    Finally
                        If (memStream IsNot Nothing) Then
                            memStream.Close()
                            memStream.Dispose()
                            memStream = Nothing
                        End If
                    End Try

            End Select
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

End Class