﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ManageSite.aspx.vb" Inherits="DMS.Web.ManageSite" %>


<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.3.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="scriptPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cssPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <div class="pageHeader">
                <div style="width: 100%;">
                    <div class="w3-padding inlineblock moveMiddle w3-left" style="width: 400px">
                        <div class="Title">
                            Site Detail
                        </div>
                        <div class="TitleDescription">
                            Add/Modify site details
                        </div>
                    </div>

                </div>
            </div>
            <div>
                <div class="w3-row">
                    <div class="w3-row w3-padding">
                        <div class="w3-card w3-padding w3-white">
                            <div class="w3-row w3-bottombar w3-padding-4 w3-border-red dialogTitle w3-medium bold w3-text-red">
                                Site's Information
                            </div>
                            <div class="w3-row w3-padding-4"></div>

                            <div class="w3-row w3-padding-4">
                                <div class="inlineblock moveMiddle" style="width: 150px">Site ID : </div>
                                <div class="inlineblock moveMiddle" style="width: 320px">
                                    <asp:TextBox ID="txtSiteID" runat="server" Width="180px" MaxLength="200"></asp:TextBox>
                                </div>

                                <div class="inlineblock moveMiddle" style="width: 140px">Site Name : </div>
                                <div class="inlineblock moveMiddle" style="width: 180px">
                                    <asp:TextBox ID="txtSiteName" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                                </div>
                            </div>
                            <div class="w3-row w3-padding-4">
                            </div>
                            <div class="w3-row w3-padding-4">
                                <div class="inlineblock moveMiddle" style="width: 150px">Main Contractor : </div>
                                <div class="inlineblock moveMiddle" style="width: 320px">
                                    <asp:TextBox ID="txtContractor" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                                </div>

                                <div class="inlineblock moveMiddle" style="width: 140px">Consultant : </div>
                                <div class="inlineblock moveMiddle" style="width: 180px">
                                    <asp:TextBox ID="txtConsultant" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                                </div>
                            </div>

                            <div class="w3-row w3-padding-4">
                            </div>
                            <div class="w3-row w3-padding-4">
                                <div class="inlineblock moveMiddle" style="width: 150px">Client : </div>
                                <div class="inlineblock moveMiddle" style="width: 320px">
                                    <asp:TextBox ID="txtClient" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                                </div>

                                <div class="inlineblock moveMiddle" style="width: 140px">Project Manager : </div>
                                <div class="inlineblock moveMiddle" style="width: 180px">
                                    <asp:TextBox ID="txtProjectManager" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                                </div>
                            </div>
                            <div class="w3-row w3-padding-4">
                            </div>
                            <div class="w3-row w3-padding-4">
                                <div class="inlineblock moveMiddle" style="width: 150px">Address Line 1 : </div>
                                <div class="inlineblock moveMiddle" style="width: 320px">
                                    <asp:TextBox ID="txtAddressLine1" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                                </div>

                                <div class="inlineblock moveMiddle" style="width: 140px">Address Line 2 : </div>
                                <div class="inlineblock moveMiddle" style="width: 180px">
                                    <asp:TextBox ID="txtAddressLine2" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                                </div>
                            </div>
                            <div class="w3-row w3-padding-4">
                            </div>
                            <div class="w3-row w3-padding-4">
                                <div class="inlineblock moveMiddle" style="width: 150px">PO Box No : </div>
                                <div class="inlineblock moveMiddle" style="width: 320px">
                                    <asp:TextBox ID="txtPOBoxNo" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                                </div>

                                <div class="inlineblock moveMiddle" style="width: 140px">Active : </div>
                                <div class="inlineblock moveMiddle" style="width: 50px">
                                    <asp:CheckBox ID="chkActive" runat="server" Width="80px" Checked="true" />
                                </div>
                                <div class="inlineblock moveMiddle" style="width: 120px">Is Head Office : </div>
                                <div class="inlineblock moveMiddle" style="width: 50px">
                                    <asp:CheckBox ID="chkIsHeadOffice" runat="server" Width="80px" Checked="true" />
                                </div>
                            </div>
                            <div class="w3-row w3-padding-4">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w3-row">
                    <div class="inlineblock w3-padding-12 w3-padding-right w3-right">
                        <div>
                            <asp:LinkButton ToolTip="Save and Go Back to List" runat="server" ID="lnkSave" CssClass="w3-btn w3-blue"><span class="adminuicon adminuicon-write19 moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Save</span></asp:LinkButton>
                            <asp:LinkButton ToolTip="Back to List" runat="server" ID="lnkBackToList" OnClientClick="window.location.href='SitesList.aspx'; return false;" CssClass="w3-btn w3-grey"><span class="adminuicon adminuicon-verify8 moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Back to List</span></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <%If False Then%>
            <script language="javascript" type="text/javascript" src="/script/common.js">
            </script>
            <script language="javascript" type="text/javascript" src="/script/jquery-vsdoc.js">
            </script>
            <%End If%>


            <script type="text/javascript">
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
                    init_page_events_and_controls();
                });

                function init_page_events_and_controls() {
                    $('#<%=lnkSave.ClientID%>').on('click', function () {
                        Validation.init();

                        <%--Validation.addRequired('<%=txtSiteName.ClientID%>', 'Enter Site Name');
                        Validation.addRequired('<%=txtContractor.ClientID%>', 'Enter Contractor Name');
                        Validation.addRequired('<%=txtConsultant.ClientID%>', 'Enter Consultant Name');
                        Validation.addRequired('<%=txtClient.ClientID%>', 'Enter Client Name');
                        Validation.addRequired('<%=txtProjectManager.ClientID%>', 'Enter Project Manager Name');--%>
                        

                        if (Validation.execute()) {
                            return $.confirmMessage('<div class="w3-center"><div class="w3-padding w3-text-red w3-xlarge">ATTENTION !!!</div><div class="w3-padding">Do you want to Save this detail?</div></div>', this);
                        }
                        else {
                            return false;
                        }
                    });
                }
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
