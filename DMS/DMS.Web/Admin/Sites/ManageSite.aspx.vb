﻿Imports DMS.DataContext.DataAccessHelper
Imports DMS.Models.DTO

Public Class ManageSite
    Inherits BasePage
    Dim caller As SystemAdmin

    Private Property SiteId As Integer
        Get
            Return ViewState("SITE_ID")
        End Get
        Set(value As Integer)
            ViewState("SITE_ID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            caller = New SystemAdmin()
            If Not IsPostBack Then
                Initialize()
            End If
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub Initialize()

        If Request.QueryString("PARAM") IsNot Nothing Then
            Dim queryEnc As New QueryString("PARAM")
            Dim items As ListItemCollection = queryEnc.Decompose()

            If items.Count > 0 AndAlso items.FindByValue("SID") IsNot Nothing Then
                SiteId = CInt(items.FindByValue("SID").Text)
                LoadRequest()
            End If
        Else
            SiteId = 0
        End If

    End Sub

    Private Sub LoadRequest()
        Try
            Dim site As SiteDTO = New SiteDTO()
            site = caller.GetProjectSiteById(SiteId)

            If site IsNot Nothing AndAlso site.ID > 0 Then
                txtSiteID.Text = site.SiteID
                txtSiteID.Enabled = False
                txtSiteName.Text = site.SiteName
                txtConsultant.Text = site.Consultant
                txtContractor.Text = site.Contractor
                txtClient.Text = site.Client
                txtProjectManager.Text = site.ProjectManager
                txtAddressLine1.Text = site.AddressLine1
                txtAddressLine2.Text = site.AddressLine2
                txtPOBoxNo.Text = site.POBoxNo
                chkActive.Checked = site.Active
                chkIsHeadOffice.Checked = site.IsHeadOffice
            End If

        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles lnkSave.Click

        Try
            Dim id As Integer = 0
            Dim objSite As SiteDTO

            objSite = New SiteDTO

            objSite.SiteID = txtSiteID.Text
            objSite.SiteName = txtSiteName.Text
            objSite.Contractor = txtContractor.Text
            objSite.Consultant = txtConsultant.Text
            objSite.Client = txtClient.Text
            objSite.ProjectManager = txtProjectManager.Text
            objSite.AddressLine1 = txtAddressLine1.Text
            objSite.AddressLine2 = txtAddressLine2.Text
            objSite.POBoxNo = txtPOBoxNo.Text
            objSite.Active = chkActive.Checked
            objSite.IsHeadOffice = chkIsHeadOffice.Checked

            If SiteId = 0 Then
                objSite.ID = 0
                objSite.CreatedBy = UserProfile.UserID
                id = caller.CreateProjectSite(objSite)
            Else
                objSite.ID = SiteId
                objSite.ModifiedBy = UserProfile.UserID
                id = caller.UpdateProjectSite(objSite)
            End If


            If ID > 0 Then
                ShowMessageAndRedirect("<div class=""w3-center""><div class=""w3-padding w3-text-red w3-xlarge"">" +
                                        "SAVED !!!</div><div class=""w3-padding"">The request information has been saved...</div></div>", "SitesList.aspx")
            Else

            End If

        Catch ex As Exception
            Me.LogException(ex)

        End Try
    End Sub

End Class