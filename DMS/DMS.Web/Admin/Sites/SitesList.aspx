﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"  CodeBehind="SitesList.aspx.vb" Inherits="DMS.Web.SitesList" %>

<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.3.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="scriptPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cssPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pageHeader">
        <div style="width: 100%;">
            <div class="w3-padding inlineblock moveMiddle w3-left" style="width: 400px">
                <div class="Title">
                    Project Site's List
                </div>
                <div class="TitleDescription">
                    All Project Site's
                </div>
            </div>

        </div>
    </div>
    <div>
        <dx:ASPxGridView ID="gridSites" ClientInstanceName="gridSites" runat="server" EnableRowsCache="false"
            Width="100%" KeyFieldName="ID" Font-Size="18pt" AutoGenerateColumns="False" DataSourceID="odsSites" EnableTheming="True" Theme="Glass">
            <Toolbars>
                <dx:GridViewToolbar EnableAdaptivity="true">
                    <Items>
                        <dx:GridViewToolbarItem Name="CustomExportToPDF" Text="Export to PDF" Image-IconID="export_exporttopdf_16x16" />
                        <dx:GridViewToolbarItem Name="CustomExportToXLSX" Text="Export to XLSX" Image-IconID="export_exporttoxlsx_16x16office2013">
                        </dx:GridViewToolbarItem>
                        <dx:GridViewToolbarItem Name="AddNew" Text="New Site" Image-IconID="actions_newproducts_16x16devav">
                        </dx:GridViewToolbarItem>
                    </Items>
                </dx:GridViewToolbar>
            </Toolbars>
            <Columns>
                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                    <DataItemTemplate>
                        <%#Container.ItemIndex + 1 %>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Width="40px" CellStyle-Wrap="false" VisibleIndex="1" CellStyle-HorizontalAlign="Center">
                    <DataItemTemplate>
                        <a href='ManageSite.aspx?<%#GetDetailUrl(Eval("ID")) %>' title="View Detail" >
                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn> 
                <dx:GridViewDataTextColumn FieldName="SiteID" Caption="Site ID" Width="100px" />                
                <dx:GridViewDataTextColumn FieldName="SiteName" Caption="Site Name" Width="270px" />                
                <dx:GridViewDataTextColumn FieldName="Contractor" Caption="Contractor" Width="150px" />
                <dx:GridViewDataTextColumn FieldName="Consultant" Caption="Consultant" Width="150px" />
                <dx:GridViewDataTextColumn FieldName="Client" Caption="Client" Width="150px" />
                <dx:GridViewDataTextColumn FieldName="ProjectManager" Caption="ProjectManager" Width="150px" />
                <dx:GridViewDataTextColumn FieldName="AddressLine1" Caption="Address Line1" Width="150px" />
                <dx:GridViewDataTextColumn FieldName="AddressLine2" Caption="Address Line2" Width="150px" />
                <dx:GridViewDataTextColumn FieldName="POBoxNo" Caption="PO Box" Width="80px" />
                <dx:GridViewDataCheckColumn FieldName="Active" Caption="Active" Width="80px"></dx:GridViewDataCheckColumn> 
                <dx:GridViewDataCheckColumn FieldName="IsHeadOffice" Caption="Head Office" Width="80px"></dx:GridViewDataCheckColumn> 

            </Columns>
            <%--<Styles Cell-CssClass="w3-tiny" Footer-CssClass="w3-tiny" Header-CssClass="w3-tiny"></Styles>--%>
            <Settings ShowHeaderFilterButton="true" HorizontalScrollBarMode="Visible" ShowFilterBar="Visible" ShowFilterRow="true" ShowFilterRowMenu="true" ShowFooter="true" ShowGroupPanel="true" ShowGroupFooter="VisibleIfExpanded" ShowPreview="true" />
            <SettingsPager PageSize="10">
                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
            </SettingsPager>
            <SettingsBehavior />
            <SettingsResizing  ColumnResizeMode="Control" Visualization="Live" />
            
            <ClientSideEvents ToolbarItemClick="function (s, e) { onToolbarClick(s, e); }" />
            <SettingsExport EnableClientSideExportAPI="true" Landscape="true" LeftMargin="0" RightMargin="0" TopMargin="0" BottomMargin="0" ExcelExportMode="WYSIWYG" />
            <SettingsSearchPanel Visible="True" />
        </dx:ASPxGridView>
        <asp:ObjectDataSource ID="odsSites" runat="server" DataObjectTypeName="DMS.DataContext.DataAccessHelper.SystemAdmin"
            SelectMethod="GetAllProjectSites" TypeName="DMS.DataContext.DataAccessHelper.SystemAdmin"></asp:ObjectDataSource>
    </div>
    <script type="text/javascript">  
        function onToolbarClick(s, e) {
            if (e.item.name == 'AddNew') {
                window.location = 'ManageSite.aspx';
                return false;
            }
            e.processOnServer = true; e.usePostBack = true;
        } 
    </script>
</asp:Content>
