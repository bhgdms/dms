﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ApprovalMatrix
    
    '''<summary>
    '''up control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents up As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''hdnScriptToRun control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnScriptToRun As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''hdnTabID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnTabID As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''ddlModule control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlModule As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddlProcess control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlProcess As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lnkShowApproval control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkShowApproval As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lnkResetApproval control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkResetApproval As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lnkUpdateApproval control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkUpdateApproval As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''divLevelContainer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divLevelContainer As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''liApprovalSetup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents liApprovalSetup As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''tabApprovalSetup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tabApprovalSetup As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''lnkAddLevel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkAddLevel As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''rptLevel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptLevel As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''lnkDummy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkDummy As Global.System.Web.UI.WebControls.LinkButton
End Class
