﻿Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Data
'Imports SevenHeavenFramework.Web.Tools

Public Class ApprovalWorkflow
    Inherits BasePage
    Private Property ReceiptNo As String
    Private Property StageCode As String
        Get
            Return ViewState("STAGECODE")
        End Get
        Set(value As String)
            ViewState("STAGECODE") = value
            If value.Length = 4 Then
                Dim prefix$ = Left(value, 2)

                Dim tab As DevExpress.Web.TabPage = approvalTabs.TabPages.FindByName(prefix)

                approvalTabs.ActiveTabIndex = tab.Index

                Dim postFix$ = Right(value, 2)

                Dim tabPage As DevExpress.Web.ASPxPageControl = tab.FindControl(prefix + "Tab")
                tabPage.ActiveTabIndex = tabPage.TabPages.FindByName(postFix).Index
            End If
        End Set
    End Property

    Protected ReadOnly Property EncryptedQueryString(code$, qvalue$) As String
        Get
            Dim query As New QueryString("PARAM")
            query.Add(code, qvalue)
            Return query.Generate
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'If Not UserProfile.HasPrivilege(SystemAdminPrivilege.THE_USER_CAN_APPROVE_ALL_INCOMING_REQUEST_IN_APPROVAL_SCREEN) Then
            '    GoToNoAccessPage()
            '    Return
            'End If
            LoadWorkflowHeader()
        End If
    End Sub

    Private Function ShowHideTab(tabPage As DevExpress.Web.ASPxPageControl, tabName$, gridId$) As Integer
        Dim subTab As DevExpress.Web.TabPage = tabPage.TabPages.FindByName(tabName)
        Dim grid As DevExpress.Web.ASPxGridView = subTab.FindControl(gridId)
        If grid IsNot Nothing Then
            subTab.Visible = grid.VisibleRowCount > 0
            If subTab.Visible Then
                If subTab.Text.IndexOf("(") > -1 Then subTab.Text = Left(subTab.Text, subTab.Text.IndexOf("(") - 1)
                subTab.Text = subTab.Text + " (" + grid.VisibleRowCount.ToString + ")"
            End If
            Return grid.VisibleRowCount
        Else
            Return 0
        End If
    End Function

    Private Sub LoadWorkflowHeader()

        If Request.QueryString("PARAM") IsNot Nothing Then
            Dim query As New QueryString("PARAM")
            Dim queryList As ListItemCollection = Nothing
            Try
                queryList = query.Decompose()
                If queryList(0).Value <> "SC" Then Throw New ApplicationException("Browser URL Error")
                StageCode = queryList(0).Text
            Catch ex As Exception
                ShowMessageAndRedirect("Browser URL has been compromised. Browser will return back to login page.", "../../../login.aspx")
                Return
            End Try
        End If

        'rptWorkflowHeader.DataSource = (New PropertyManagement.DataAccessLayer.ApprovalWorkflow()).GetHeaderByUser
        'rptWorkflowHeader.DataBind()
    End Sub

    Private Sub rptWorkflowHeader_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptWorkflowHeader.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or
                    e.Item.ItemType = ListItemType.Item Then
                Dim dl As DataList = DirectCast(e.Item.FindControl("dlModule"), DataList)
                Dim row As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                'dl.DataSource = (New PropertyManagement.DataAccessLayer.ApprovalWorkflow()).GetModuleDetail(row("WorkflowId"))
                'dl.DataBind()

            End If
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Protected Sub dlModule_ItemDataBound(sender As Object, e As DataListItemEventArgs)
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or
                   e.Item.ItemType = ListItemType.Item Then
                Dim row As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
                Dim lnkStageNo As LinkButton = DirectCast(e.Item.FindControl("lnkStageNo"), LinkButton)

                Dim total As Integer = CInt(row("Total"))
                lnkStageNo.Enabled = total > 0
                If Not lnkStageNo.Enabled Then lnkStageNo.ForeColor = Drawing.Color.LightGray
            End If
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub ApprovalWorkflow_PreRenderComplete(sender As Object, e As EventArgs) Handles Me.PreRenderComplete
        Try
            Dim tab As DevExpress.Web.TabPage = approvalTabs.TabPages.FindByName("LA")
            If tab Is Nothing Then Return
            Dim tabPage As DevExpress.Web.ASPxPageControl = tab.FindControl("LATab")
            If tabPage Is Nothing Then Return

            Dim tabCount As Integer = 0
            tabCount += ShowHideTab(tabPage, "RS", "grdReservationList")
            tabCount += ShowHideTab(tabPage, "PL", "grdNewProposal")
            tabCount += ShowHideTab(tabPage, "DS", "grdContractDiscount")
            tabCount += ShowHideTab(tabPage, "NC", "grdNewContract")
            tabCount += ShowHideTab(tabPage, "CD", "grdContractDiscard")
            tabCount += ShowHideTab(tabPage, "CR", "grdContractRenewal")
            tabCount += ShowHideTab(tabPage, "TR", "grdTerminationRequest")
            tabCount += ShowHideTab(tabPage, "TC", "grdTerminationConfirmation")
            tabCount += ShowHideTab(tabPage, "CS", "grdSettlement")
            tabCount += ShowHideTab(tabPage, "PD", "grdPDConHold")
            tabCount += ShowHideTab(tabPage, "CE", "grdContractExpense")

            tab.Visible = tabCount > 0
            If tabCount > 0 Then
                If tab.Text.IndexOf("(") > -1 Then tab.Text = Left(tab.Text, tab.Text.IndexOf("(") - 1)
                tab.Text = tab.Text + " (" + tabCount.ToString + ")"
            End If

            tab = approvalTabs.TabPages.FindByName("FA")
            If tab Is Nothing Then Return
            tabPage = tab.FindControl("FATab")
            If tabPage Is Nothing Then Return
            tabCount = 0
            tabCount += ShowHideTab(tabPage, "TS", "grdList")
            tabCount += ShowHideTab(tabPage, "VH", "grdVoucherList")
            tabCount += ShowHideTab(tabPage, "PE", "grdPrepaidExpense")
            tabCount += ShowHideTab(tabPage, "PC", "grdPettyCash")
            tabCount += ShowHideTab(tabPage, "GR", "grdGoodReceipt")
            tabCount += ShowHideTab(tabPage, "GC", "grdGoodReceiptCosting")
            tabCount += ShowHideTab(tabPage, "AD", "grdFixedAsset")
            tabCount += ShowHideTab(tabPage, "SI", "grdSupplierInvoice")
            tabCount += ShowHideTab(tabPage, "IS", "grdProformaInvoice")
            tabCount += ShowHideTab(tabPage, "IR", "grdTAXInvoice")
            tabCount += ShowHideTab(tabPage, "SM", "grdSubMeterInvoice")
            tabCount += ShowHideTab(tabPage, "CE", "grdFAContractExpense")
            tabCount += ShowHideTab(tabPage, "BE", "grdFAPropertyExpense")
            tabCount += ShowHideTab(tabPage, "UE", "grdMeterInvoice")
            tabCount += ShowHideTab(tabPage, "DC", "grdDebitCreditNote")

            tab.Visible = tabCount > 0
            If tabCount > 0 Then
                If tab.Text.IndexOf("(") > -1 Then tab.Text = Left(tab.Text, tab.Text.IndexOf("(") - 1)
                tab.Text = tab.Text + " (" + tabCount.ToString + ")"
            End If

            tab = approvalTabs.TabPages.FindByName("PA")
            If tab Is Nothing Then Return
            tabPage = tab.FindControl("PATab")
            If tabPage Is Nothing Then Return
            tabCount = 0
            tabCount += ShowHideTab(tabPage, "PR", "grdPurchaseRequisitionList")
            tabCount += ShowHideTab(tabPage, "PO", "grdPurchaseOrder")
            tabCount += ShowHideTab(tabPage, "IQ", "grdPurchaseInquiry")
            'tabCount += ShowHideTab(tabPage, "PC", "grdPettyCash")

            tab.Visible = tabCount > 0
            If tabCount > 0 Then
                If tab.Text.IndexOf("(") > -1 Then tab.Text = Left(tab.Text, tab.Text.IndexOf("(") - 1)
                tab.Text = tab.Text + " (" + tabCount.ToString + ")"
            End If

            tab = approvalTabs.TabPages.FindByName("IA")
            If tab Is Nothing Then Return
            tabPage = tab.FindControl("IATab")
            If tabPage Is Nothing Then Return
            tabCount = 0
            tabCount += ShowHideTab(tabPage, "MR", "grdInventoryRequisitionList")
            tabCount += ShowHideTab(tabPage, "MA", "grdInventoryRequisitionConfirmationList")

            tab.Visible = tabCount > 0
            If tabCount > 0 Then
                If tab.Text.IndexOf("(") > -1 Then tab.Text = Left(tab.Text, tab.Text.IndexOf("(") - 1)
                tab.Text = tab.Text + " (" + tabCount.ToString + ")"
            End If

            tab = approvalTabs.TabPages.FindByName("MA")
            If tab Is Nothing Then Return
            tabPage = tab.FindControl("MATab")
            If tabPage Is Nothing Then Return
            tabCount = 0
            tabCount += ShowHideTab(tabPage, "CE", "grdMAContractExpense")

            tab.Visible = tabCount > 0
            If tabCount > 0 Then
                If tab.Text.IndexOf("(") > -1 Then tab.Text = Left(tab.Text, tab.Text.IndexOf("(") - 1)
                tab.Text = tab.Text + " (" + tabCount.ToString + ")"
            End If
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Protected Sub grd_ToolbarItemClick(source As Object, e As ASPxGridViewToolbarItemClickEventArgs) Handles grdPurchaseRequisitionList.ToolbarItemClick, grdPurchaseInquiry.ToolbarItemClick
        Try
            Dim grid As ASPxGridView = DirectCast(source, ASPxGridView)
            Select Case e.Item.Name
                Case "CustomExportToXLSX"
                    e.Handled = True
                    Dim fileName As String = Path.GetFileNameWithoutExtension(Path.GetTempFileName()) + ".xlsx"
                    Dim memStream As New MemoryStream
                    Try
                        grid.ExportToXlsx(memStream, New DevExpress.XtraPrinting.XlsxExportOptionsEx() With {.ExportType = .ExportType.WYSIWYG})

                        Using fs As New FileStream(Server.MapPath("~/TEMP/" + fileName), FileMode.Create)
                            memStream.WriteTo(fs)
                            fs.Close()
                        End Using

                        RunScript("window.open('../../../Temp/" + fileName + "','_blank');")
                    Catch ex As Exception
                    Finally
                        If (memStream IsNot Nothing) Then
                            memStream.Close()
                            memStream.Dispose()
                            memStream = Nothing
                        End If
                    End Try

                Case "CustomExportToPDF"
                    e.Handled = True
                    Dim fileName As String = Path.GetFileNameWithoutExtension(Path.GetTempFileName()) + ".pdf"
                    Dim memStream As New MemoryStream
                    Try
                        grid.ExportToPdf(memStream)

                        Using fs As New FileStream(Server.MapPath("~/TEMP/" + fileName), FileMode.Create)
                            memStream.WriteTo(fs)
                            fs.Close()
                        End Using

                        RunScript("window.open('../../../Temp/" + fileName + "','_blank');")
                    Catch ex As Exception
                    Finally
                        If (memStream IsNot Nothing) Then
                            memStream.Close()
                            memStream.Dispose()
                            memStream = Nothing
                        End If
                    End Try
            End Select
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Protected Sub grdVoucherItemList_BeforePerformDataSelect(sender As Object, e As EventArgs)
        ReceiptNo = (TryCast(sender, ASPxGridView)).GetMasterRowFieldValues("DocumentCode")
    End Sub

    Protected Sub odsVoucherTransactionDetail_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs)
        e.InputParameters("receiptNo") = ReceiptNo
    End Sub
End Class