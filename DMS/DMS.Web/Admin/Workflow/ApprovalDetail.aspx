﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ApprovalDetail.aspx.vb" Inherits="DMS.Web.ApprovalDetail" %>

<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.3.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="scriptPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cssPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <div class="w3-padding">

                <div class="w3-row w3-padding w3-white" id="divApproval" runat="server">
                    <div class="w3-half">
                        
                        <%If Not IsMulti Then %>
                        <div>
                            <asp:Label class="w3-row w3-padding w3-large bold" ID="lblStageTitle" runat="server"></asp:Label>
                        </div>
                        <%End If %>
                        <div class="w3-row">
                            <div class="w3-row w3-padding multiline_limiter">
                                <div class="w3-row w3-padding-4 inlineblock">
                                    <div class="inlineblock">
                                        Your comment: <span class="w3-tiny">(Max. 500 characters i.e. approx 80
                                                    words)</span>
                                    </div>
                                    <div class="inlineblock w3-padding-left w3-right">
                                        characters left: <span class="w3-tiny displayLabel">500</span>&nbsp;
                                    </div>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtApprovalComments" runat="server" CssClass="w3-border"
                                        TextMode="MultiLine" Height="80px" Width="400px"></asp:TextBox>
                                </div>
                            </div>
                            <div class="w3-row w3-padding-left">

                                <dx:ASPxButton ID="btnApprove" ClientInstanceName="btnApprove" AutoPostBack="false" BackColor="Green" runat="server" Text="APPROVE" EnableTheming="true" Theme="Office2010Blue">
                                    <ClientSideEvents Click="function (s, e) {pcApproveConfirmation.Show(); return false;}" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnReject" ClientInstanceName="btnReject" AutoPostBack="false" BackColor="Red" runat="server" Text="REJECT" EnableTheming="true" Theme="Office2010Blue">
                                    <ClientSideEvents Click="function (s, e) {pcRejection.Show(); return false; }" />
                                </dx:ASPxButton>
                                <dx:ASPxPopupControl ID="pcApproveConfirmation" runat="server" Width="320" CloseAction="CloseButton" CloseOnEscape="true" Modal="True"
                                    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="pcApproveConfirmation"
                                    HeaderText="Login" AllowDragging="True" PopupAnimationType="None" EnableViewState="False" AutoUpdatePosition="true">
                                    <ContentCollection>
                                        <dx:PopupControlContentControl>
                                            <div class="w3-text-green w3-padding w3-large">Are you sure you want to approve?</div>
                                            <div class="w3-padding">
                                                <dx:ASPxButton ID="btnApproveOK" ClientInstanceName="btnApproveOK" BackColor="Green" runat="server" Text="OK" EnableTheming="true" Theme="Office2010Blue">
                                                    <ClientSideEvents Click="function (s, e) {pcApproveConfirmation.Hide();}" />
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnApproveCancel" AutoPostBack="false" ClientInstanceName="btnApproveCancel" BackColor="Red" runat="server" Text="CANCEL" EnableTheming="true" Theme="Office2010Blue">
                                                    <ClientSideEvents Click="function (s, e) {pcApproveConfirmation.Hide();}" />
                                                </dx:ASPxButton>
                                            </div>
                                        </dx:PopupControlContentControl>
                                    </ContentCollection>
                                </dx:ASPxPopupControl>
                                <dx:ASPxPopupControl ID="pcRejection" runat="server" Width="320" CloseAction="CloseButton" CloseOnEscape="true" Modal="True"
                                    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="pcRejection"
                                    HeaderText="Login" AllowDragging="True" PopupAnimationType="None" EnableViewState="False" AutoUpdatePosition="true">
                                    <ContentCollection>
                                        <dx:PopupControlContentControl>
                                            <div class="w3-text-red w3-padding w3-large">Are you sure you want to reject?</div>
                                            <div class="w3-padding">
                                                <dx:ASPxButton ID="btnRejectOK" ClientInstanceName="btnRejectOK" BackColor="Green" runat="server" Text="OK" EnableTheming="true" Theme="Office2010Blue">
                                                    <ClientSideEvents Click="function (s, e) {pcRejection.Hide();}" />
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnRejectCancel" AutoPostBack="false" ClientInstanceName="btnRejectCancel" BackColor="Red" runat="server" Text="CANCEL" EnableTheming="true" Theme="Office2010Blue">
                                                    <ClientSideEvents Click="function (s, e) {pcRejection.Hide();}" />
                                                </dx:ASPxButton>
                                            </div>
                                        </dx:PopupControlContentControl>
                                    </ContentCollection>
                                </dx:ASPxPopupControl>
                            </div>
                        </div>
                    </div>
                    <%If Not IsMulti Then %>
                    <div class="w3-half w3-right-align">
                        <div class="w3-row w3-padding">
                            Waiting Since:
                                                <asp:Label ID="lblEntryDate" runat="server"></asp:Label>
                        </div>
                        <div class="w3-row w3-padding w3-text-black">
                            Days Passed:
                        </div>
                        <div class="w3-row w3-padding w3-small bold">
                            <asp:Label CssClass="w3-xxxlarge w3-padding-hor-32 w3-padding-ver-32" ID="lblDaysPassed" runat="server"></asp:Label>
                        </div>
                    </div>
                    <%End If %>
                </div>
                <%If Not IsMulti Then %>
                <div class="w3-row w3-padding-4">
                    &nbsp;
                </div>
                <div class="w3-row w3-bottombar w3-padding-4 w3-border-red">
                    Approval History
                </div>
                <div class="w3-row w3-padding-8">
                    <dx:ASPxGridView ID="grdApprovalHistory" ClientInstanceName="grdApprovalHistory" runat="server"
                        Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsApprovalHistory" EnableTheming="True" Theme="Office2010Blue">
                        <Columns>
                            <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entered Date" VisibleIndex="0" Width="10%">
                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn FieldName="LevelNo" Caption="Level No." VisibleIndex="1" Width="5%" />
                            <dx:GridViewDataTextColumn FieldName="LevelTitle" Caption="Level" VisibleIndex="2" Width="20%"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="User" Caption="User" VisibleIndex="3" Width="20%"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="StatusDescription" Caption="Status" VisibleIndex="4" Width="10%"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataDateColumn FieldName="ActionDate" Caption="Decision Date" VisibleIndex="5" Width="10%">
                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn FieldName="Comments" Settings-AllowAutoFilter="False" Caption="Comments" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                        </Columns>
                        <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                        <Settings ShowFilterRow="false" />
                        <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                    </dx:ASPxGridView>
                    <asp:ObjectDataSource ID="odsApprovalHistory" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                        SelectMethod="GetApprovalHistory" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow">
                        <SelectParameters>
                            <asp:Parameter ConvertEmptyStringToNull="true" Name="DocumentCode" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
                <%End If %>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
