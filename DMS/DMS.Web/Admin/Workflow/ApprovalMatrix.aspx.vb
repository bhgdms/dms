﻿Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Data
Imports DMS.DataContext
Imports DMS.Models.DTO
Imports DMS.Models

Public Class ApprovalMatrix
    Inherits BasePage

#Region " Private Properties .... "
    Dim caller As DataAccessHelper.ApprovalWorkflow = Nothing
    Dim caller2 As DataAccessHelper.SystemAdmin = Nothing
    Private ReadOnly Property MySessionId As String
        Get
            If ViewState("MY_SESSION_ID") Is Nothing Then
                ViewState("MY_SESSION_ID") = Session.SessionID + "-" + DateTime.Now.ToString("ddMMyyyyhhmmsss")
            End If
            Return ViewState("MY_SESSION_ID")
        End Get
    End Property
#End Region

#Region " Private Methods .... "
    Private Sub Initialize()
        Try
            divLevelContainer.ClientVisible(False)
            Dim lst As List(Of WorkflowHeaderDTO) = caller.FillModule()
            If lst IsNot Nothing AndAlso lst.Count() <> 0 Then
                ddlModule.DataSource = lst
                ddlModule.DataTextField = "ModuleName"
                ddlModule.DataValueField = "WorkflowId"
                ddlModule.DataBind()
                ddlModule.Items.Insert(0, New ListItem("-- Select -- ", ""))
            End If
        Catch
            Throw
        End Try
    End Sub

    Private Sub LoadLevel()
        Dim lst As List(Of TempWorkflowLevelDTO) = caller.LoadLevel(ddlProcess.SelectedValue, MySessionId)
        If lst IsNot Nothing AndAlso lst.Count() <> 0 Then
            rptLevel.DataSource = lst
            rptLevel.DataBind()
        End If
    End Sub
    Private Sub LoadUser(ddlUser As DropDownList)
        ddlUser.Items.Clear()
        Dim user As List(Of UserDTO) = New List(Of UserDTO)
        user = caller2.GetAllUsersBySite(UserProfile.SiteID)

        user = user.Where(Function(s) s.UserId <> "admin").ToList()
        If user.Count > 0 Then
            ddlUser.DataSource = user
            ddlUser.DataValueField = "UserId"
            ddlUser.DataTextField = "UserId"
            ddlUser.DataBind()

            ddlUser.Items.Insert(0, New ListItem("-- Select -- ", ""))
        End If
    End Sub
    Private Sub LoadLevelUser(gvList As GridView, levelId$)
        gvList.DataSource = Nothing
        Dim levels As List(Of TempWorkflowActionDTO) = New List(Of TempWorkflowActionDTO)
        levels = caller.LoadLevelUserDetail(levelId, MySessionId)
        If levels IsNot Nothing Then
            gvList.DataSource = levels
            gvList.DataBind()
        End If
    End Sub
    Private Sub SaveLevelUser(LevelId$, levelTitle$, gvList As GridView)
        Try
            Dim actionData As String = String.Empty
            For i As Integer = 0 To gvList.Rows.Count - 1
                actionData += gvList.DataKeys(i)("ActionId").ToString() + "^#"
            Next

            If Not String.IsNullOrEmpty(actionData) Then
                caller.SaveLevelData(LevelId, levelTitle, actionData)
            End If

        Catch
            Throw
        End Try
    End Sub
    Private Function SaveLevel(Optional levelToExclude As String = "") As Boolean
        For i As Integer = 0 To rptLevel.Items.Count - 1
            With rptLevel.Items(i)
                Dim ddlUser As DropDownList = DirectCast(.FindControl("ddlUser"), DropDownList)
                Dim txtLevelTitle As TextBox = DirectCast(.FindControl("txtLevelTitle"), TextBox)
                Dim gvList As GridView = DirectCast(.FindControl("gvList"), GridView)
                Dim lnkRemoveTitle As LinkButton = DirectCast(.FindControl("lnkRemoveTitle"), LinkButton)

                If lnkRemoveTitle.CommandArgument <> levelToExclude Then
                    If txtLevelTitle.Text.Trim = String.Empty Then
                        PageMessage = "Kindly enter the level title in LEVEL no. " + (i + 1).ToString
                        Return False
                    End If

                    SaveLevelUser(lnkRemoveTitle.CommandArgument, txtLevelTitle.Text.Trim, gvList)
                End If
            End With
        Next
        Return True
    End Function
#End Region

#Region " Page Events .... "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            caller = New DataAccessHelper.ApprovalWorkflow
            caller2 = New DataAccessHelper.SystemAdmin
            If Not IsPostBack Then Initialize()
        Catch ex As Exception
            'GoToApplicationErrorPage(ex)
        End Try
    End Sub

    Private Sub ddlModule_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlModule.SelectedIndexChanged
        Try
            ddlProcess.ClearSelection()
            ddlProcess.Items.Clear()
            If ddlModule.SelectedIndex = 0 Then
                ddlProcess.Enabled = False
            Else
                Dim lst As List(Of WorkflowStageDTO) = caller.FillStages(ddlModule.SelectedValue)
                If lst IsNot Nothing AndAlso lst.Count() <> 0 Then
                    ddlProcess.DataSource = lst
                    ddlProcess.DataTextField = "StageTitle"
                    ddlProcess.DataValueField = "StageId"
                    ddlProcess.DataBind()

                    ddlProcess.Items.Insert(0, New ListItem("-- Select -- ", ""))
                End If
            End If
        Catch ex As Exception
            ' GoToApplicationErrorPage(ex)
        End Try
    End Sub

    Private Sub lnkShowApproval_Click(sender As Object, e As EventArgs) Handles lnkShowApproval.Click
        Try
            rptLevel.DataSource = Nothing
            rptLevel.DataBind()

            divLevelContainer.ClientVisible(ddlProcess.SelectedIndex > 0)
            If ddlProcess.SelectedIndex > 0 Then
                Dim lst As List(Of TempWorkflowLevelDTO) = caller.LoadStage(ddlProcess.SelectedValue, MySessionId)
                If lst IsNot Nothing AndAlso lst.Count() <> 0 Then
                    rptLevel.DataSource = lst
                    rptLevel.DataBind()
                End If

            End If
        Catch ex As Exception
            'GoToApplicationErrorPage()
        End Try
    End Sub

    Private Sub rptLevel_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptLevel.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or
                    e.Item.ItemType = ListItemType.Item Then
                Dim ddlUser As DropDownList = DirectCast(e.Item.FindControl("ddlUser"), DropDownList)
                LoadUser(ddlUser)

                Dim gvList As GridView = DirectCast(e.Item.FindControl("gvList"), GridView)
                LoadLevelUser(gvList, DirectCast(e.Item.DataItem, TempWorkflowLevelDTO).LevelId)

            End If
        Catch ex As Exception
            'GoToApplicationErrorPage(ex)
        End Try
    End Sub

    Protected Sub lnkAddUser_Command(sender As Object, e As CommandEventArgs)
        Try
            Dim rpt As RepeaterItem = DirectCast(DirectCast(sender, LinkButton).NamingContainer, RepeaterItem)
            Dim ddlUser As DropDownList = DirectCast(rpt.FindControl("ddlUser"), DropDownList)
            Dim txtLevelTitle As TextBox = DirectCast(rpt.FindControl("txtLevelTitle"), TextBox)
            Dim gvList As GridView = DirectCast(rpt.FindControl("gvList"), GridView)

            If ddlUser.SelectedIndex = 0 Then
                PageMessage = "Kindly select the user !!!"
                Return
            End If

            For i As Integer = 0 To gvList.Rows.Count - 1
                If gvList.DataKeys(i)("UserId") = ddlUser.SelectedValue Then
                    PageMessage = "This user is already added in the approval list"
                    Return
                End If
            Next

            If txtLevelTitle.Text.Trim = String.Empty Then
                PageMessage = "Kindly enter the level title !!!"
                Return
            End If

            SaveLevelUser(e.CommandArgument, txtLevelTitle.Text.Trim, gvList)

            Dim result As Integer = caller.AddNewUser(e.CommandArgument, MySessionId, ddlUser.SelectedValue)
            If result = 0 Then
                LoadLevelUser(gvList, e.CommandArgument)
            End If

            ddlUser.ClearSelection()

        Catch ex As Exception
            'GoToApplicationErrorPage(ex)
        End Try
    End Sub

    Protected Sub lnkRemoveTitle_Command(sender As Object, e As CommandEventArgs)
        Try
            If Not SaveLevel(e.CommandArgument) Then Return
            caller.RemoveLevel(e.CommandArgument)
            LoadLevel()
        Catch
            Throw
        End Try
    End Sub

    Protected Sub lnkRemoveUser_Command(sender As Object, e As CommandEventArgs)
        caller.RemoveUser(e.CommandArgument)

        Dim lnkRemove As LinkButton = DirectCast(sender, LinkButton)
        Dim gridviewRow As GridViewRow = DirectCast(lnkRemove.NamingContainer, GridViewRow)
        Dim gvList As GridView = DirectCast(gridviewRow.NamingContainer, GridView)
        Dim levelId As String = gvList.DataKeys(gridviewRow.RowIndex)("LevelId").ToString
        Dim rpt As RepeaterItem = DirectCast(gvList.NamingContainer, RepeaterItem)
        Dim txtLevelTitle As TextBox = DirectCast(rpt.FindControl("txtLevelTitle"), TextBox)

        SaveLevelUser(e.CommandArgument, txtLevelTitle.Text.Trim, gvList)

        LoadLevelUser(gvList, levelId)
    End Sub

    Private Sub lnkAddLevel_Click(sender As Object, e As EventArgs) Handles lnkAddLevel.Click
        Try
            If Not SaveLevel() Then Return
            If caller.AddLevel(ddlProcess.SelectedValue, MySessionId) = 0 Then
                LoadLevel()
            End If
        Catch ex As Exception
            'GoToApplicationErrorPage(ex)
        End Try
    End Sub

    Private Sub lnkResetApproval_Click(sender As Object, e As EventArgs) Handles lnkResetApproval.Click
        lnkShowApproval_Click(Nothing, Nothing)
    End Sub

    Private Sub lnkUpdateApproval_Click(sender As Object, e As EventArgs) Handles lnkUpdateApproval.Click
        Try

            If Not SaveLevel() Then Return
            If caller.SaveWorkflow(ddlProcess.SelectedValue, MySessionId) = 0 Then
                ddlProcess.ClearSelection()
                PageMessage = "Approval Matrix updated in the system !!!"
                lnkShowApproval_Click(Nothing, Nothing)
            End If
        Catch ex As Exception
            'GoToApplicationErrorPage(ex)
        End Try
    End Sub

    Public Function GetApprovalStatus(status As Object) As Boolean
        If (status IsNot Nothing AndAlso Not IsDBNull(status)) Then
            Return CBool(status)
        Else
            Return False
        End If

    End Function


#End Region


End Class