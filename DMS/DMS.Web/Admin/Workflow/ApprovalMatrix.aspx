﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.master" CodeBehind="ApprovalMatrix.aspx.vb" Inherits="DMS.Web.ApprovalMatrix" %>

<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.3.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="scriptPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cssPlaceHolder" runat="server">
    <link href="../../../css/w3.css" rel="stylesheet" type="text/css" />
</asp:Content> 
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <div class="pageHeader">
                <div class="w3-row">
                    <div class="w3-padding inlineblock moveMiddle" style="width: 40%">
                        <div class="Title">
                            Approval Matrix Defining
                        </div>
                        <div class="TitleDescription">
                            Defining the approval matrix at different level of process
                        </div>
                    </div>
                </div>
            </div>
            <div class="w3-row w3-padding-ver-4 w3-padding-hor-2">
                <asp:HiddenField ID="hdnScriptToRun" runat="server" />
                <asp:HiddenField ID="hdnTabID" runat="server" Value="0" />

                <div class="w3-row w3-padding">
                    <div class="inlineblock" style="width: 120px">
                        Select Module:
                    </div>
                    <div class="inlineblock" style="width: 180px">
                        <asp:DropDownList ID="ddlModule" Width="95%" runat="server" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="inlineblock" style="width: 120px">
                        Select Process:
                    </div>
                    <div class="inlineblock" style="width: 240px">
                        <asp:DropDownList ID="ddlProcess" Width="95%" runat="server"></asp:DropDownList>
                    </div>
                    <div class="inlineblock" style="width: 220px">
                        <asp:LinkButton ID="lnkShowApproval" runat="server" CssClass="w3-btn w3-blue"><span class="adminuicon adminuicon-list82 w3-text-white moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Show</span></asp:LinkButton>
                        <%If rptLevel.Items.Count > 0 Then %>
                        <asp:LinkButton ID="lnkResetApproval" runat="server" CssClass="w3-btn w3-red"><span class="adminuicon adminuicon-circle100 w3-text-white moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Reset</span></asp:LinkButton>
                        <%End If %>
                    </div>
                    <%If rptlevel.Items.Count > 0 Then %>
                    <div class="inlineblock" style="width: 120px">
                        <asp:LinkButton ID="lnkUpdateApproval" runat="server" CssClass="w3-btn w3-green confirm_action"><span class="adminuicon adminuicon-verify8 w3-text-white moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Update Matrix</span></asp:LinkButton>
                    </div>
                    <%End If %>
                </div>

                <div id="divLevelContainer" runat="server">
                    <div class="w3-row w3-padding">
                        <div id="divTab">
                            <ul>
                                <li runat="server" id="liApprovalSetup"><a href='#<%=tabApprovalSetup.ClientID %>'>Approval Setup</a></li> 
                            </ul>
                            <div id="tabApprovalSetup" runat="server">
                                <div class="w3-row">
                                    <div class="w3-row w3-padding">
                                        <div class="inlineblock" style="width: 120px">
                                            <asp:LinkButton ID="lnkAddLevel" runat="server" CssClass="w3-btn w3-purple"><span class="adminuicon adminuicon-add176 w3-text-white moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Add Level</span></asp:LinkButton>
                                        </div>
                                        <%If rptLevel.Items.Count = 0 Then %>
                                        <div class="inlineblock w3-padding-ver-128 w3-padding-2">
                                            <div class="w3-card w3-label w3-khaki w3-padding-8 w3-padding-ver-32">No approval level has been defined in the system.</div>
                                        </div>
                                        <%End If %>
                                    </div>

                                    <div class="w3-white w3-padding" style="width: 90%">
                                        <asp:Repeater ID="rptLevel" runat="server">
                                            <ItemTemplate>
                                                <div class="w3-row w3-padding">
                                                    <div class="w3-col l3 m3 s3">
                                                        <div class="w3-card w3-padding" style="min-height: 100px; border-right-style: none;">
                                                            <div class="w3-padding-6"><span>Level No.:</span><span class="w3-padding-ver-24"><%#Eval("LevelNo") - 1 %></spa></div>
                                                            <div>
                                                                <label class="w3-label bold">Level Title</label>
                                                                <asp:TextBox ID="txtLevelTitle" runat="server" Width="95%" MaxLength="200" value='<%#Eval("LevelTitle") %>'></asp:TextBox>
                                                            </div>
                                                            <div class="w3-right-align">
                                                                <asp:LinkButton ID="lnkRemoveTitle" CssClass="delete_action" runat="server" OnCommand="lnkRemoveTitle_Command" CommandName="DELETE_LEVEL" CommandArgument='<%#Eval("LevelId") %>'>Delete Level</asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="w3-col l9 m9 s9">
                                                        <div class="w3-card w3-padding" style="min-height: 100px;">
                                                            <div>
                                                                <div class="inlineblock" style="width: 120px">Select User:</div>
                                                                <div class="inlineblock" style="width: 320px">
                                                                    <asp:DropDownList ID="ddlUser" DataTextField="name" DataValueField="UserId" Width="95%" runat="server"></asp:DropDownList>
                                                                </div>
                                                                <div class="inlineblock" style="width: 120px">
                                                                    <asp:LinkButton ID="lnkAddUser" CommandArgument='<%#Eval("LevelId") %>' CommandName="ADD_USER" OnCommand="lnkAddUser_Command" runat="server" CssClass="w3-btn w3-purple"><span class="adminuicon adminuicon-add176 w3-text-white moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Add User</span></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                            <div class="w3-padding-8">
                                                                <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="False" DataKeyNames="UserId,ActionId,LevelId"
                                                                    CellPadding="2">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemStyle Width="5px" HorizontalAlign="Right" BackColor="#eeeeee" />
                                                                            <ItemTemplate>
                                                                                <%# DirectCast(Container, GridViewRow).RowIndex + 1%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="UserName" SortExpression="UserName" HeaderText="User"
                                                                            ItemStyle-Width="240px" />
                                                                        <asp:BoundField DataField="Designation" SortExpression="Designation" HeaderText="Designation"
                                                                            ItemStyle-Width="180px" /> 
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkRemoveUser" CommandArgument='<%#Eval("ActionId") %>' OnCommand="lnkRemoveUser_Command" runat="server" CssClass="delete_action w3-btn w3-red w3-padding-hor-4 w3-padding-ver-8"><span class="adminuicon adminuicon-recycle69 w3-text-white moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Remove</span></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="DataGridItem" />
                                                                    <HeaderStyle CssClass="DataGridHeader" />
                                                                    <AlternatingRowStyle CssClass="DataGridAlternateItem" />
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>
            <asp:LinkButton ID="lnkDummy" runat="server" CssClass="hide"></asp:LinkButton>
            <script type="text/javascript">
            function onToolbarClick(s, e) {
                e.processOnServer = true; e.usePostBack = true;
            }
            function onBeginCallBack(s, e) {
                delete (s.cp_scriptToRun);
            }
            function onEndCallBack(s, e) {
                if (s.cp_scriptToRun) {
                    var fn = new Function(s.cp_scriptToRun);
                    fn();
                    delete (s.cp_scriptToRun);
                }
            }
            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {

                $('#divTab').tabs({
                    activate: function () {
                        var newIndex = $(this).tabs('option', 'active');
                        $('#<%=hdnTabID.ClientID %>').val(newIndex);

                            // grid.refresh();
                            window.location = $('#<%=lnkDummy.ClientID%>').prop('href');

                        },
                        active: $('#<%=hdnTabID.ClientID %>').val() != "" ? parseInt($('#<%=hdnTabID.ClientID %>').val()) : 0
                    });
                });
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(function (sender, args) {
                $('#<%=hdnScriptToRun.ClientID%>').val('');
                });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function (sender, args) {
                var script_to_run = $('#<%=hdnScriptToRun.ClientID%>').val();
                    if (script_to_run != '') {
                        var fn = new Function(script_to_run);
                        fn();
                    }
                });

            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
