﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.master" CodeBehind="ApprovalWorkflow.aspx.vb" Inherits="DMS.Web.ApprovalWorkflow" %>

<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.3.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="scriptPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cssPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="server">

    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <div class="pageHeader">
                <div class="w3-row">
                    <div class="inlineblock w3-left w3-padding-12 w3-padding-left" style="width: 300px">
                        <div class="Title">
                            Approval
                        </div>
                        <div class="TitleDescription">
                            List of entities waiting for approval
                        </div>
                    </div>
                    <div class="inlineblock hide" style="width: 500px">
                        <asp:HiddenField ID="hdnSystemCode" runat="server" Value="" />
                        <asp:HiddenField ID="hdnStatus" runat="server" Value="" />
                        <asp:HiddenField ID="hdnApprovalStatus" runat="server" Value="N" />
                        <asp:LinkButton ID="lnkSearch" runat="server" CssClass="hide"></asp:LinkButton>
                        <div class="w3-col s2 m2 l2 w3-padding-ver-8 tooltip" title="LEASING">
                            <div class="w3-padding-top w3-center ">
                                <div data-status="L" class="legend inlineblock w3-btn-floating w3-grey w3-micro moveMiddle bold">
                                    <div class="inlineblock w3-small moveMiddle w3-text-white statuscounter bold" runat="server"
                                        id="divL">
                                        00
                                    </div>
                                </div>
                            </div>
                            <div class="w3-micro w3-center w3-text-grey w3-padding-2 bold">
                                LEASING
                            </div>
                        </div>
                        <div class="w3-col s2 m2 l2 w3-padding-ver-8 tooltip" title="FINANCE">
                            <div class="w3-padding-top w3-center ">
                                <div data-status="F" class="legend inlineblock w3-btn-floating w3-purple w3-micro moveMiddle bold">
                                    <div class="inlineblock w3-small moveMiddle statuscounter" runat="server"
                                        id="divF">
                                        00
                                    </div>
                                </div>
                            </div>
                            <div class="w3-micro w3-center w3-text-grey w3-padding-2 bold">
                                FINANCE
                            </div>
                        </div>
                        <div class="w3-col s2 m2 l2 w3-padding-ver-8 tooltip" title="Maintenance">
                            <div class="w3-padding-top w3-center ">
                                <div data-status="M" class="legend inlineblock w3-btn-floating w3-teal w3-micro moveMiddle bold">

                                    <div class="inlineblock w3-small moveMiddle statuscounter" runat="server"
                                        id="divM">
                                        00
                                    </div>
                                </div>
                            </div>
                            <div class="w3-micro w3-center w3-text-grey w3-padding-2 bold">
                                MAINTENANCE
                            </div>
                        </div>
                        <div class="w3-col s2 m2 l2 w3-padding-ver-8 tooltip" title="PURCHASE">
                            <div class="w3-padding-top w3-center ">
                                <div data-status="P" class="legend inlineblock w3-btn-floating w3-red w3-micro moveMiddle bold">

                                    <div class="inlineblock w3-small moveMiddle statuscounter" runat="server"
                                        id="divP">
                                        00
                                    </div>
                                </div>
                            </div>
                            <div class="w3-micro w3-center w3-text-grey w3-padding-2 bold">
                                PURCHASE
                            </div>
                        </div>
                        <div class="w3-col s2 m2 l2 w3-padding-ver-8 tooltip" title="INVENTORY">
                            <div class="w3-padding-top w3-center ">
                                <div data-status="I" class="legend inlineblock w3-btn-floating w3-red w3-micro moveMiddle bold">

                                    <div class="inlineblock w3-small moveMiddle statuscounter" runat="server"
                                        id="divI">
                                        00
                                    </div>
                                </div>
                            </div>
                            <div class="w3-micro w3-center w3-text-grey w3-padding-2 bold">
                                INVENTORY
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w3-row w3-padding hide">
                <asp:Repeater ID="rptWorkflowHeader" runat="server">
                    <ItemTemplate>
                        <div class="w3-row w3-border-bottom w3-padding-12 w3-border-red w3-large bold">
                            <%#Eval("ModuleName") %>
                        </div>
                        <div class="w3-row w3-padding">
                            <asp:DataList ID="dlModule" OnItemDataBound="dlModule_ItemDataBound" runat="server" RepeatColumns="4" RepeatDirection="Horizontal">
                                <ItemTemplate>
                                    <div class="w3-padding-ver-64 w3-padding-hor-16 w3-medium"><%#Eval("StageTitle").ToString().ToUpper() %>&nbsp;<asp:LinkButton ID="lnkStageNo" runat="server" CssClass="w3-medium bold">( <%#Eval("Total") %> )</asp:LinkButton></div>

                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="w3-row w3-padding">
                <dx:ASPxPageControl ID="approvalTabs" EnableTheming="True" Theme="Office2010Blue" Width="100%" runat="server" CssClass="dxtcFixed" ActiveTabIndex="0" EnableHierarchyRecreation="True">
                    <TabPages>
                        <dx:TabPage Text="LEASING" Name="LA">
                            <ContentCollection>
                                <dx:ContentControl>
                                    <dx:ASPxPageControl ID="LATab" EnableTheming="True" Theme="Office2010Blue" Width="100%" runat="server" CssClass="dxtcFixed" ActiveTabIndex="0" EnableHierarchyRecreation="True">
                                        <TabPages>
                                            <dx:TabPage Text="RESERVATION" Name="RS">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdReservationList" ClientInstanceName="grdReservationList" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsReservationApprovalList" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="5%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openReservationDetail(this);' title="View Contract Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentNo" Caption="#" VisibleIndex="0" Width="10%" />
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="20%" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="2" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="TenantName" Caption="Tenant" VisibleIndex="3" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="BookingDate" Caption="Booking Date" VisibleIndex="4" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ExpectedStartDate" Caption="Contract Start Date" VisibleIndex="5" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="AdvancePayment" Caption="Advance" VisibleIndex="6" Width="10%">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents RowClick="function(s, e) { OnReservedGridRowClick(e); }" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="LevelTitle" Expression="[Status] = 'C'" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsReservationApprovalList" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetReservationList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="STANDARD PROPOSAL" Name="PL">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdNewProposal" ClientInstanceName="grdNewProposal" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsNewProposalApprovalList" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="1%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openProposalDetail(this);' class="proposal_detail" title="View Proposal Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentNo" Caption="#" VisibleIndex="0" Width="10%" />
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" CellStyle-CssClass="longTextStyler" Caption="Landlord" VisibleIndex="0" Width="20%" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="2" Width="5%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="TenantName" CellStyle-CssClass="longTextStyler" Caption="Tenant" VisibleIndex="3" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ContractStartDate" Caption="Start Date" VisibleIndex="4" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ContractEndDate" Caption="End Date" VisibleIndex="5" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="FinalRent" Caption="Rent" VisibleIndex="6" Width="10%">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" CellStyle-Wrap="True" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" CellStyle-CssClass="longTextStyler" />
                                                            </Columns>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents RowClick="function(s, e) { OnNewContractRowClick(e); }" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="LevelTitle" Expression="[Status] = 'C'" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsNewProposalApprovalList" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetNewProposalList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow">
                                                            <SelectParameters>
                                                                <asp:Parameter Name="StageCode" DefaultValue="LAPL" />
                                                            </SelectParameters>
                                                        </asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="DISCOUNTED PROPOSAL" Name="DS">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdContractDiscount" ClientInstanceName="grdContractDiscount" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsContractDiscount" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="5%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openProposalDetail(this);' class="proposal_detail" title="View Discount Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentNo" Caption="#" VisibleIndex="0" Width="10%" />
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="20%" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="2" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="TenantName" Caption="Tenant" VisibleIndex="3" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ContractStartDate" Caption="Start Date" VisibleIndex="4" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ContractEndDate" Caption="End Date" VisibleIndex="5" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="FinalRent" Caption="Rent" VisibleIndex="6" Width="10%">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents RowClick="function(s, e) { OnNewContractRowClick(e); }" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="LevelTitle" Expression="[Status] = 'C'" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsContractDiscount" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetNewProposalList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow">
                                                            <SelectParameters>
                                                                <asp:Parameter Name="StageCode" DefaultValue="LADS" />
                                                            </SelectParameters>
                                                        </asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="NEW CONTRACT" Name="NC">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdNewContract" ClientInstanceName="grdNewContract" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsNewContractApprovalList" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="5%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openContractDetail(this);' class="contract_detail" title="View Contract Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentNo" Caption="#" VisibleIndex="0" Width="10%" />
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="20%" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="2" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="TenantName" Caption="Tenant" VisibleIndex="3" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ContractStartDate" Caption="Start Date" VisibleIndex="4" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ContractEndDate" Caption="End Date" VisibleIndex="5" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="FinalRent" Caption="Rent" VisibleIndex="6" Width="10%">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents RowClick="function(s, e) { OnNewContractRowClick(e); }" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsNewContractApprovalList" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetNewContractList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="CONTRACT DISCARD" Name="CD">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdContractDiscard" ClientInstanceName="grdContractDiscard" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsContractDiscardApprovalList" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="5%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openContractDetail(this);' class="contract_detail" title="View Contract Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentNo" Caption="#" VisibleIndex="0" Width="10%" />
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="20%" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="2" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="TenantName" Caption="Tenant" VisibleIndex="3" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ContractStartDate" Caption="Start Date" VisibleIndex="4" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ContractEndDate" Caption="End Date" VisibleIndex="5" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="FinalRent" Caption="Rent" VisibleIndex="6" Width="10%">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents RowClick="function(s, e) { OnNewContractRowClick(e); }" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsContractDiscardApprovalList" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetContractDiscardList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="CONTRACT RENEWAL" Name="CR">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdContractRenewal" ClientInstanceName="grdContractRenewal" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsRenewContractApprovalList" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="5%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openContractDetail(this);' class="contract_detail" title="View Contract Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentNo" Caption="#" VisibleIndex="0" Width="10%" />
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="20%" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="2" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="TenantName" Caption="Tenant" VisibleIndex="3" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ContractStartDate" Caption="Start Date" VisibleIndex="4" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ContractEndDate" Caption="End Date" VisibleIndex="5" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="FinalRent" Caption="Rent" VisibleIndex="6" Width="10%">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents RowClick="function(s, e) { OnRenewalRowClick(e); }" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsRenewContractApprovalList" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetRenewContractList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="TERMINATION REQUEST" Name="TR">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdTerminationRequest" ClientInstanceName="grdTerminationRequest" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsTR" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="5%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openContractDetail(this);' class="contract_detail" title="View Contract Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentNo" Caption="#" VisibleIndex="0" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" CellStyle-Wrap="False"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="2" CellStyle-Wrap="False"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="TenantName" Caption="Tenant" VisibleIndex="3" CellStyle-Wrap="False"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ConfirmedDate" Caption="Notice Date" VisibleIndex="4" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="TerminationDate" Caption="Termination Date" VisibleIndex="5" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="FinalRent" Caption="Rent" VisibleIndex="6" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" />
                                                            </Columns>
                                                            <Styles Cell-CssClass="makeUpper" Cell-Wrap="False"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsTR" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetTerminationRequestList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="NOC TO VACATE" Name="TC">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdTerminationConfirmation" ClientInstanceName="grdTerminationConfirmation" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsTC" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="5%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openContractDetail(this);' class="contract_detail" title="View Contract Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentNo" Caption="#" VisibleIndex="0" Width="10%" />
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="20%" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="2" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="TenantName" Caption="Tenant" VisibleIndex="3" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ConfirmedDate" Caption="Notice Date" VisibleIndex="4" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="TerminationDate" Caption="Termination Date" VisibleIndex="5" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="KeyHandoverDate" Caption="Key hand over Date" VisibleIndex="6" Width="10%" Visible="false">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ChargeDate" Caption="Charge Date" VisibleIndex="7" Width="10%" Visible="false">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="FinalRent" Caption="Rent" VisibleIndex="8" Width="10%">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="9" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsTC" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetTerminationApprovalList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="PRE-SETTLEMENT" Name="CS">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdSettlement" ClientInstanceName="grdSettlement" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsCS" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="5%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openContractDetail(this);' class="contract_detail" title="View Contract Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentNo" Caption="#" VisibleIndex="0" Width="10%" />
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="20%" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="2" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="TenantName" Caption="Tenant" VisibleIndex="3" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="KeyHandoverDate" Caption="Key hand over Date" VisibleIndex="4" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ChargeDate" Caption="Charge Date" VisibleIndex="5" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="FinalRent" Caption="Rent" VisibleIndex="6" Width="10%">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsCS" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetSettlementApprovalList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow">
                                                            <SelectParameters>
                                                                <asp:Parameter Name="status" DefaultValue="T" />
                                                            </SelectParameters>
                                                        </asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="PDC ON HOLD" Name="PD">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdPDConHold" ClientInstanceName="grdPDConHold" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsPDConHoldList" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="5%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openPDConHoldDetail(this);' title="View Request Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="#" VisibleIndex="0" Width="10%" />
                                                                <dx:GridViewDataTextColumn FieldName="Landlord" Caption="Landlord" VisibleIndex="0" Width="20%" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Unit" Caption="Unit" VisibleIndex="2" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="TenantName" Caption="Tenant" VisibleIndex="3" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="ChequeNo" Caption="Chq. No" VisibleIndex="5" Width="10%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ChequeDate" Caption="Chq. Date" VisibleIndex="6" Width="5%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Amount" Caption="Amount" VisibleIndex="6" Width="5%">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="HoldDate" Caption="Hold Date" VisibleIndex="6" Width="5%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsPDConHoldList" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetPDConHoldList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow">
                                                            <SelectParameters>
                                                                <asp:Parameter Name="status" DefaultValue="T" />
                                                            </SelectParameters>
                                                        </asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="CONTRACT EXPENSE" Name="CE">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdContractExpense" ClientInstanceName="grdContractExpense" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsContractExpense" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Admin/Portfolio/TenancyContract/ContractExpenseDetail.aspx");' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DocumentDate" Caption="Document Date" VisibleIndex="2" Width="50px" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Document Code" VisibleIndex="3" Width="100px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="chargedescription" Caption="Charge Description" VisibleIndex="4" Width="300px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="chargeamount" Caption="Amount" VisibleIndex="5" Width="100px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="vatamount" Caption="VAT" VisibleIndex="6" Width="100px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="netamount" Caption="Net" VisibleIndex="7" Width="100px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="unitno" Caption="Unit" VisibleIndex="8" Width="100px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="propertyname" Caption="Property" VisibleIndex="9" Width="200px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="ContractCode" CellStyle-Wrap="False" Caption="Contract Code" VisibleIndex="10" Width="150px">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="tenant" Caption="Tenant" VisibleIndex="11" Width="300px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="12" Width="10%" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="13" Width="5%" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="14" Width="5%" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="15" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsContractExpense" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetContractExpenseList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow">
                                                            <SelectParameters>
                                                                <asp:Parameter Name="moduleCode" DefaultValue="LA" />
                                                            </SelectParameters>
                                                        </asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                        </TabPages>
                                    </dx:ASPxPageControl>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="FINANCE" Name="FA">
                            <ContentCollection>
                                <dx:ContentControl>
                                    <dx:ASPxPageControl ID="FATab" EnableTheming="True" Theme="Office2010Blue" Width="100%" runat="server" CssClass="dxtcFixed" ActiveTabIndex="0" EnableHierarchyRecreation="True">
                                        <TabPages>
                                            <dx:TabPage Text="SETTLEMENT" Name="TS">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdList" ClientInstanceName="grdList" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsTS" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="5%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Accounts/Operation/Payment/SettlementView.aspx");' class="contract_detail" title="View Contract Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentNo" Caption="#" VisibleIndex="0" Width="10%" />
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="20%" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="2" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="TenantName" Caption="Tenant" VisibleIndex="3" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="KeyHandoverDate" Caption="Key hand over Date" VisibleIndex="4" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ChargeDate" Caption="Charge Date" VisibleIndex="5" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="FinalRent" Caption="Rent" VisibleIndex="6" Width="10%">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsTS" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetSettlementApprovalList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow">
                                                            <SelectParameters>
                                                                <asp:Parameter Name="status" DefaultValue="S" />
                                                            </SelectParameters>
                                                        </asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="VOUCHER" Name="VH">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <div class="w3-padding-hor-2">
                                                            <asp:LinkButton ID="lnkApprovalVoucher" runat="server" CssClass="w3-btn w3-teal w3-ver-padding-8"><span class="adminuicon adminuicon-verify8 w3-text-white moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Approve</span></asp:LinkButton>
                                                        </div>
                                                        <dx:ASPxGridView ID="grdVoucherList" ClientInstanceName="grdVoucherList" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsVoucherTransaction" EnableTheming="True" Theme="Office2010Blue">
                                                            <Toolbars>
                                                                <dx:GridViewToolbar EnableAdaptivity="true">
                                                                    <Items>
                                                                        <dx:GridViewToolbarItem Command="ExportToPdf" />
                                                                        <dx:GridViewToolbarItem Command="ExportToXlsx" />
                                                                        <dx:GridViewToolbarItem Command="ExportToCsv" />
                                                                    </Items>
                                                                </dx:GridViewToolbar>
                                                            </Toolbars>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="1%">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewCommandColumn ShowSelectCheckbox="true" SelectAllCheckboxMode="Page" ShowClearFilterButton="true" Caption="#" VisibleIndex="0" Width="1%" />
                                                                <dx:GridViewDataTextColumn Width="1%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openVoucherDetail(this);' title="View Detail" data-keyfield='<%#"VNO=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="ReceiptDate" Caption="Date" VisibleIndex="1" Width="5%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Txn. No." VisibleIndex="2" Width="20%" />
                                                                <dx:GridViewDataTextColumn FieldName="VoucherTypeDescription" Caption="Type" VisibleIndex="4" CellStyle-Wrap="False" Width="15%" />
                                                                <dx:GridViewDataTextColumn FieldName="Amount" Caption="Amount" VisibleIndex="6" Width="5%">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="5%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="SubmittedBy" Settings-AllowAutoFilter="False" Caption="Submitted By" VisibleIndex="11" CellStyle-Wrap="False" Width="15%" />
                                                            </Columns>
                                                            <Templates>
                                                                <DetailRow>
                                                                    <dx:ASPxGridView ID="grdVoucherItemList" ClientInstanceName="grdVoucherItemList" runat="server" OnBeforePerformDataSelect="grdVoucherItemList_BeforePerformDataSelect"
                                                                        Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsVoucherTransactionDetail" EnableTheming="True" Theme="Office2010Blue">
                                                                        <Columns>
                                                                            <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="1%">
                                                                                <DataItemTemplate>
                                                                                    <%#Container.ItemIndex + 1 %>
                                                                                </DataItemTemplate>
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn FieldName="Ledger" Caption="Main Acct." Width="15%" CellStyle-Wrap="False" />
                                                                            <dx:GridViewDataTextColumn FieldName="SubLedger" Caption="Sub Acct." Width="15%" CellStyle-Wrap="False" />
                                                                            <dx:GridViewDataTextColumn FieldName="Landlord" Caption="Landlord" Width="5%" CellStyle-Wrap="False" />
                                                                            <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" Width="5%" CellStyle-Wrap="False" />
                                                                            <dx:GridViewDataTextColumn FieldName="Unit" Caption="Unit" Width="5%" CellStyle-Wrap="False" />
                                                                            <dx:GridViewDataTextColumn FieldName="ContractCode" Caption="Contract#" Width="5%" CellStyle-Wrap="False" />
                                                                            <dx:GridViewDataTextColumn FieldName="Activity1" Caption="Act. #1" Width="5%" CellStyle-Wrap="False" />
                                                                            <dx:GridViewDataTextColumn FieldName="Activity2" Caption="Act. #2" Width="5%" CellStyle-Wrap="False" />
                                                                            <dx:GridViewDataTextColumn FieldName="Debit" Caption="Debit" Width="5%" CellStyle-Wrap="False" />
                                                                            <dx:GridViewDataTextColumn FieldName="Credit" Caption="Credit" Width="5%" CellStyle-Wrap="False" />
                                                                            <dx:GridViewDataTextColumn FieldName="Narration" Caption="Narration" Width="30%" CellStyle-Wrap="False" />
                                                                            <dx:GridViewDataTextColumn FieldName="ChequeNo" Caption="Chq./Ref. No" Width="10%"></dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataDateColumn FieldName="ChequeDate" Caption="Chq./Xfr Date" Width="5%">
                                                                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                                <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                                <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                            </dx:GridViewDataDateColumn>
                                                                        </Columns>
                                                                        <Settings ShowFilterRow="false" ShowFooter="true" />
                                                                        <Styles>
                                                                            <Cell CssClass="w3-tiny"></Cell>
                                                                        </Styles>
                                                                        <TotalSummary>
                                                                            <dx:ASPxSummaryItem FieldName="Debit" SummaryType="Sum" DisplayFormat="{0:#,##0.00}" />
                                                                            <dx:ASPxSummaryItem FieldName="Credit" SummaryType="Sum" DisplayFormat="{0:#,##0.00}" />
                                                                        </TotalSummary>

                                                                        <SettingsPager PageSize="10">
                                                                            <PageSizeItemSettings Visible="true" Items="10, 20, 50, 100" />
                                                                        </SettingsPager>
                                                                        <SettingsSearchPanel Visible="True" />
                                                                        <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                                    </dx:ASPxGridView>
                                                                </DetailRow>
                                                            </Templates>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <SettingsDetail ShowDetailRow="true" AllowOnlyOneMasterRowExpanded="true" ShowDetailButtons="true" />
                                                            <Settings ShowFilterBar="Visible" ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="true" ShowGroupFooter="VisibleIfExpanded" ShowPreview="true" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsPopup>
                                                                <HeaderFilter Width="235" Height="280">
                                                                </HeaderFilter>
                                                            </SettingsPopup>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior />
                                                            <ClientSideEvents />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <SettingsExport EnableClientSideExportAPI="true" Landscape="true" LeftMargin="0" RightMargin="0" TopMargin="0" BottomMargin="0" ExcelExportMode="WYSIWYG" />
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsVoucherTransaction" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetVoucherList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                        <asp:ObjectDataSource ID="odsVoucherTransactionDetail" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetVoucherItemList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow" OnSelecting="odsVoucherTransactionDetail_Selecting">
                                                            <SelectParameters>
                                                                <asp:Parameter Name="receiptNo" DefaultValue="" />
                                                            </SelectParameters>
                                                        </asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="PREPAID EXPENSE" Name="PE">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdPrepaidExpense" ClientInstanceName="grdPrepaidExpense" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsPrepaidExpenseTransaction" EnableTheming="True" Theme="Office2010Blue">
                                                            <Toolbars>
                                                                <dx:GridViewToolbar EnableAdaptivity="true">
                                                                    <Items>
                                                                        <dx:GridViewToolbarItem Command="ExportToPdf" />
                                                                        <dx:GridViewToolbarItem Command="ExportToXlsx" />
                                                                        <dx:GridViewToolbarItem Command="ExportToCsv" />
                                                                    </Items>
                                                                </dx:GridViewToolbar>
                                                            </Toolbars>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="1%">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="1%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openPrepaidExpenseDetail(this);' title="View Detail" data-keyfield='<%#"SC=" & Eval("ExpenseCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Document No" VisibleIndex="0" Width="100px" />
                                                                <dx:GridViewDataTextColumn FieldName="ExpenseLedger" Caption="Expense Acct." VisibleIndex="3" Width="250px"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DateFrom" Caption="Date From" VisibleIndex="4" Width="80px">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DateTo" Caption="Date To" VisibleIndex="5" Width="80px">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="NoOfMonths" Caption="No. of Months" VisibleIndex="6" Width="80px" />
                                                                <dx:GridViewDataTextColumn FieldName="Amount" Caption="Amount" VisibleIndex="7" Width="100px">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="PerMonthAmount" Caption="Per Month" VisibleIndex="8" Width="100px">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Landlord" Caption="Landlord" VisibleIndex="9" Width="250px"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="10" Width="200px" />
                                                                <dx:GridViewDataTextColumn FieldName="Unit" Caption="Unit" VisibleIndex="11" Width="100px" />

                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterBar="Visible" ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="true" ShowGroupFooter="VisibleIfExpanded" ShowPreview="true" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsPopup>
                                                                <HeaderFilter Width="235" Height="280">
                                                                </HeaderFilter>
                                                            </SettingsPopup>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior />
                                                            <ClientSideEvents />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <SettingsExport EnableClientSideExportAPI="true" Landscape="true" LeftMargin="0" RightMargin="0" TopMargin="0" BottomMargin="0" ExcelExportMode="WYSIWYG" />
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsPrepaidExpenseTransaction" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetPrepaidExpenseList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="PETTY CASH ACCOUNT" Name="PC">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdPettyCash" ClientInstanceName="grdPettyCash" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsPettyCashTransaction" EnableTheming="True" Theme="Office2010Blue">
                                                            <Toolbars>
                                                                <dx:GridViewToolbar EnableAdaptivity="true">
                                                                    <Items>
                                                                        <dx:GridViewToolbarItem Command="ExportToPdf" />
                                                                        <dx:GridViewToolbarItem Command="ExportToXlsx" />
                                                                        <dx:GridViewToolbarItem Command="ExportToCsv" />
                                                                    </Items>
                                                                </dx:GridViewToolbar>
                                                            </Toolbars>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="1%">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="1%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openPettyCashDetail(this);' title="View Detail" data-keyfield='<%#Eval("Id") %>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                        <a href='#' onclick='return openWorkflowDetail(this);' class="workflow_approve" title="Approval" data-keyfield='<%#Eval("DataId") %>'>
                                                                            <img src="../../../images/greenflag.gif" alt="Approve" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="AccountTitle" Caption="Account Name" VisibleIndex="1" Width="15%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LedgerTitle" Caption="Ledger" VisibleIndex="2" Width="30%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Staff" Caption="Staff" VisibleIndex="3" Width="10%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="MaxLimit" Caption="Limit" VisibleIndex="4" Width="2%">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="CurrentBalance" Caption="Balance" VisibleIndex="5" Width="2%">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>

                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="3%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterBar="Visible" ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="true" ShowGroupFooter="VisibleIfExpanded" ShowPreview="true" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsPopup>
                                                                <HeaderFilter Width="235" Height="280">
                                                                </HeaderFilter>
                                                            </SettingsPopup>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior />
                                                            <ClientSideEvents />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <SettingsExport EnableClientSideExportAPI="true" Landscape="true" LeftMargin="0" RightMargin="0" TopMargin="0" BottomMargin="0" ExcelExportMode="WYSIWYG" />
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsPettyCashTransaction" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetPettyCashList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="GOOD RECEIPT" Name="GR">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdGoodReceipt" ClientInstanceName="grdGoodReceipt" runat="server"
                                                            KeyFieldName="DocumentCode" AutoGenerateColumns="False" DataSourceID="odsGoodReceipt" EnableTheming="True" Theme="Office2010Blue">
                                                            <Toolbars>
                                                                <dx:GridViewToolbar EnableAdaptivity="true">
                                                                    <Items>
                                                                        <dx:GridViewToolbarItem Name="CustomExportToPDF" Text="Export to PDF" Image-IconID="export_exporttopdf_16x16" />
                                                                        <dx:GridViewToolbarItem Name="CustomExportToXLSX" Text="Export to XLSX" Image-IconID="export_exporttoxlsx_16x16office2013">
                                                                        </dx:GridViewToolbarItem>
                                                                    </Items>
                                                                </dx:GridViewToolbar>
                                                            </Toolbars>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Purchase/Operation/Order/PurchaseGoodReceiptDetail.aspx");' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DocumentDate" Caption="Request Date" VisibleIndex="0" Width="80px" CellStyle-Wrap="false">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Requisition No." VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="300px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="0" Width="200px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="ReferenceNo" Caption="PO#" VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="RequestBy" Caption="Request By" VisibleIndex="1" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Item" Caption="Total Item" VisibleIndex="2" Width="50px" CellStyle-Wrap="false"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Total Quantity" VisibleIndex="2" Width="50" CellStyle-Wrap="false"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterBar="Visible" ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="true" ShowGroupFooter="VisibleIfExpanded" ShowPreview="true" />
                                                            <SettingsPager PageSize="20">
                                                                <PageSizeItemSettings Visible="true" Items="20, 50, 70, 100" />
                                                            </SettingsPager>
                                                            <SettingsPopup>
                                                                <HeaderFilter Width="235" Height="280">
                                                                </HeaderFilter>
                                                            </SettingsPopup>
                                                            <ClientSideEvents />
                                                            <TotalSummary>
                                                                <dx:ASPxSummaryItem FieldName="Quantity" SummaryType="Sum" DisplayFormat="{0:#,##0}" />
                                                            </TotalSummary>
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <SettingsExport EnableClientSideExportAPI="true" Landscape="true" LeftMargin="0" RightMargin="0" TopMargin="0" BottomMargin="0" ExcelExportMode="WYSIWYG" />
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsGoodReceipt" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetGoodReceiptList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="GOOD RECEIPT COSTING" Name="GC">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdGoodReceiptCosting" ClientInstanceName="grdGoodReceiptCosting" runat="server"
                                                            KeyFieldName="DocumentCode" AutoGenerateColumns="False" DataSourceID="odsGoodReceiptCosting" EnableTheming="True" Theme="Office2010Blue">
                                                            <Toolbars>
                                                                <dx:GridViewToolbar EnableAdaptivity="true">
                                                                    <Items>
                                                                        <dx:GridViewToolbarItem Name="CustomExportToPDF" Text="Export to PDF" Image-IconID="export_exporttopdf_16x16" />
                                                                        <dx:GridViewToolbarItem Name="CustomExportToXLSX" Text="Export to XLSX" Image-IconID="export_exporttoxlsx_16x16office2013">
                                                                        </dx:GridViewToolbarItem>
                                                                    </Items>
                                                                </dx:GridViewToolbar>
                                                            </Toolbars>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Accounts/Operation/Transaction/GoodReceiptDetail.aspx");' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DocumentDate" Caption="Request Date" VisibleIndex="0" Width="80px" CellStyle-Wrap="false">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Requisition No." VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="300px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="0" Width="200px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="ReferenceNo" Caption="PO#" VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="RequestBy" Caption="Request By" VisibleIndex="1" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Item" Caption="Total Item" VisibleIndex="2" Width="50px" CellStyle-Wrap="false"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Total Quantity" VisibleIndex="2" Width="50" CellStyle-Wrap="false"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterBar="Visible" ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="true" ShowGroupFooter="VisibleIfExpanded" ShowPreview="true" />
                                                            <SettingsPager PageSize="20">
                                                                <PageSizeItemSettings Visible="true" Items="20, 50, 70, 100" />
                                                            </SettingsPager>
                                                            <SettingsPopup>
                                                                <HeaderFilter Width="235" Height="280">
                                                                </HeaderFilter>
                                                            </SettingsPopup>
                                                            <ClientSideEvents />
                                                            <TotalSummary>
                                                                <dx:ASPxSummaryItem FieldName="Quantity" SummaryType="Sum" DisplayFormat="{0:#,##0}" />
                                                            </TotalSummary>
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <SettingsExport EnableClientSideExportAPI="true" Landscape="true" LeftMargin="0" RightMargin="0" TopMargin="0" BottomMargin="0" ExcelExportMode="WYSIWYG" />
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsGoodReceiptCosting" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetGoodReceiptCostingList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="FIXED ASSET" Name="AD">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdFixedAsset" ClientInstanceName="grdFixedAsset" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsFixedAsset" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Accounts/Operation/Asset/AssetDepreciationDetail.aspx");' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DocumentDate" Caption="Document Date" VisibleIndex="2" Width="50px" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Document Code" VisibleIndex="3" Width="100px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="AssetCode" Caption="Asset Code" VisibleIndex="4" Width="100px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="ShortName" Caption="Asset Name" VisibleIndex="5" Width="180px" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LandlordCode" CellStyle-CssClass="tooltip" Caption="Landlord" VisibleIndex="6" Width="50px" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="PropertyCode" Caption="Property" VisibleIndex="7" Width="50px" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Unit" Caption="Unit" VisibleIndex="8" Width="50px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="ItemCode" Caption="Item Code" VisibleIndex="9" Width="70px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="PurchasePrice" Caption="Price" VisibleIndex="10" Width="50px" CellStyle-Wrap="False"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewBandColumn Caption="Depreciation" VisibleIndex="11">
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn FieldName="DepreciationPeriod" Caption="(Yrs)" VisibleIndex="1" Width="30px"></dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn FieldName="DepreciationStartDate" Caption="Start" VisibleIndex="2" Width="50px">
                                                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy"></PropertiesTextEdit>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn FieldName="DepreciationEndDate" Caption="End" VisibleIndex="3" Width="50px">
                                                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy"></PropertiesTextEdit>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn FieldName="PerMonth" Caption="P/M" VisibleIndex="4" Width="30px"></dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                </dx:GridViewBandColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="12" Width="10%" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="13" Width="5%" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="14" Width="30%" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="15" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsFixedAsset" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetAssetDepreciationList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="SUPPLIER INVOICE" Name="SI">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdSupplierInvoice" ClientInstanceName="grdSupplierInvoice" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsSupplierInvoiceList" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Accounts/Operation/Supplier/SupplierInvoiceDetail.aspx");' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DocumentDate" Caption="Document Date" VisibleIndex="2" Width="80px">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Document No." VisibleIndex="3" Width="120px">
                                                                    <Settings AllowHeaderFilter="True" />
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="SupplierName" Caption="Supplier" VisibleIndex="5" Width="450px">
                                                                    <Settings AllowHeaderFilter="True" />
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="ReferenceNo" Caption="Reference#" VisibleIndex="7" Width="100px">
                                                                    <Settings AllowHeaderFilter="True" />
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Amount" Caption="Invoice Amount" VisibleIndex="8" Width="50px"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DueDate" Caption="Due Date" VisibleIndex="10" Width="100px">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="12" Width="10%" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="13" Width="5%" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="14" Width="5%" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="15" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsSupplierInvoiceList" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetSupplierInvoiceList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="PROFORMA INVOICE" Name="IS">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdProformaInvoice" ClientInstanceName="grdProformaInvoice" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsProformaInvoiceList" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Accounts/Operation/Payment/InvoiceDetail.aspx");' title="View Detail" data-keyfield='<%#"VNO=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FixedStyle="Left" FieldName="DocumentDate" Caption="Document Date" VisibleIndex="2" Width="80px">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Document No." VisibleIndex="3" Width="120px">
                                                                    <Settings AllowHeaderFilter="True" />
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="ReferenceNo" Caption="Ref. No." VisibleIndex="3" Width="120px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="VATType" Caption="Type" VisibleIndex="4" Width="80px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="TAXAmount" Caption="TAX Amount" VisibleIndex="5" Width="90px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00" NullDisplayText="0"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="VATAmount" Caption="VAT" VisibleIndex="6" Width="80px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="NetAmount" Caption="Net Amount" VisibleIndex="7" Width="90px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="8" Width="80px" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="9" Width="50px" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="10" Width="70px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="11" CellStyle-Wrap="False" Width="300px" />
                                                                <dx:GridViewDataTextColumn FieldName="ReferenceName" Caption="Tenant/Supplier" VisibleIndex="12" Width="280px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="Landlord" Caption="Landlord" VisibleIndex="13" Width="280px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Property" FieldName="Property" VisibleIndex="14" Width="150px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Unit" FieldName="Unit" VisibleIndex="15" Width="100px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <Styles FixedColumn-BackColor="Linen" Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" HorizontalScrollBarMode="Visible" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsProformaInvoiceList" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetProformaInvoiceList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="TAX INVOICE" Name="IR">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdTAXInvoice" ClientInstanceName="grdTAXInvoice" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsTAXInvoiceList" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Accounts/Operation/Invoice/InvoiceDetail.aspx");' title="View Detail" data-keyfield='<%#"VNO=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FixedStyle="Left" FieldName="DocumentDate" Caption="Document Date" VisibleIndex="2" Width="80px">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Document No." VisibleIndex="3" Width="120px">
                                                                    <Settings AllowHeaderFilter="True" />
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="ReferenceNo" Caption="Ref. No." VisibleIndex="3" Width="120px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="VATType" Caption="Type" VisibleIndex="4" Width="80px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="TAXAmount" Caption="TAX Amount" VisibleIndex="5" Width="90px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00" NullDisplayText="0"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="VATAmount" Caption="VAT" VisibleIndex="6" Width="80px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="NetAmount" Caption="Net Amount" VisibleIndex="7" Width="90px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="8" Width="80px" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="9" Width="50px" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="10" Width="70px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="11" CellStyle-Wrap="False" Width="300px" />
                                                                <dx:GridViewDataTextColumn FieldName="ReferenceName" Caption="Tenant/Supplier" VisibleIndex="12" Width="280px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="Landlord" Caption="Landlord" VisibleIndex="13" Width="280px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Property" FieldName="Property" VisibleIndex="14" Width="150px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Unit" FieldName="Unit" VisibleIndex="15" Width="100px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <Styles FixedColumn-BackColor="Linen" Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" HorizontalScrollBarMode="Visible" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsTAXInvoiceList" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetTAXInvoiceList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="SUB METER INVOICE" Name="SM">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdSubMeterInvoice" ClientInstanceName="grdSubMeterInvoice" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsSubMeterInvoiceList" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Accounts/Operation/Invoice/SubMeterInvoiceDetail.aspx");' title="View Detail" data-keyfield='<%#"VNO=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FixedStyle="Left" FieldName="DocumentDate" Caption="Document Date" VisibleIndex="2" Width="80px">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Document No." VisibleIndex="3" Width="120px">
                                                                    <Settings AllowHeaderFilter="True" />
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="ReferenceNo" Caption="Ref. No." VisibleIndex="3" Width="120px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="Property" Caption="Property" VisibleIndex="4" Width="80px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="TAXAmount" Caption="TAX Amount" VisibleIndex="5" Width="90px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00" NullDisplayText="0"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="VATAmount" Caption="VAT" VisibleIndex="6" Width="80px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="NetAmount" Caption="Net Amount" VisibleIndex="7" Width="90px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="8" Width="80px" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="9" Width="50px" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="10" Width="70px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="11" CellStyle-Wrap="False" Width="300px" />
                                                            </Columns>
                                                            <Styles FixedColumn-BackColor="Linen" Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" HorizontalScrollBarMode="Visible" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsSubMeterInvoiceList" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetSubMeterInvoiceList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="UTILITY EXPENSE" Name="UE">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdMeterInvoice" ClientInstanceName="grdMeterInvoice" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsMeterInvoiceList" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Accounts/Operation/Invoice/MeterInvoiceDetail.aspx");' title="View Detail" data-keyfield='<%#"VNO=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FixedStyle="Left" FieldName="DocumentDate" Caption="Document Date" VisibleIndex="2" Width="80px">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Document No." VisibleIndex="3" Width="120px">
                                                                    <Settings AllowHeaderFilter="True" />
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="ReferenceNo" Caption="Ref. No." VisibleIndex="3" Width="120px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="Landlord" Caption="Landlord" VisibleIndex="4" Width="80px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="TAXAmount" Caption="TAX Amount" VisibleIndex="5" Width="90px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00" NullDisplayText="0"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="VATAmount" Caption="VAT" VisibleIndex="6" Width="80px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FixedStyle="Left" FieldName="NetAmount" Caption="Net Amount" VisibleIndex="7" Width="90px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="8" Width="80px" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="9" Width="50px" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="10" Width="70px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="11" CellStyle-Wrap="False" Width="300px" />
                                                            </Columns>
                                                            <Styles FixedColumn-BackColor="Linen" Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" HorizontalScrollBarMode="Visible" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsMeterInvoiceList" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetMeterInvoiceList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="CONTRACT EXPENSE" Name="CE">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdFAContractExpense" ClientInstanceName="grdFAContractExpense" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsFAContractExpense" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Admin/Portfolio/TenancyContract/ContractExpenseDetail.aspx");' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DocumentDate" Caption="Document Date" VisibleIndex="2" Width="50px" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Document Code" VisibleIndex="3" Width="100px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="chargedescription" Caption="Charge Description" VisibleIndex="4" Width="300px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="chargeamount" Caption="Amount" VisibleIndex="5" Width="100px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="vatamount" Caption="VAT" VisibleIndex="6" Width="100px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="netamount" Caption="Net" VisibleIndex="7" Width="100px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="unitno" Caption="Unit" VisibleIndex="8" Width="100px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="propertyname" Caption="Property" VisibleIndex="9" Width="200px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="ContractCode" CellStyle-Wrap="False" Caption="Contract Code" VisibleIndex="10" Width="150px">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="tenant" Caption="Tenant" VisibleIndex="11" Width="300px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="12" Width="10%" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="13" Width="5%" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="14" Width="5%" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="15" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsFAContractExpense" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetContractExpenseList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow">
                                                            <SelectParameters>
                                                                <asp:Parameter Name="moduleCode" DefaultValue="FA" />
                                                            </SelectParameters>
                                                        </asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="BUILDING EXPENSE" Name="BE">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdFAPropertyExpense" ClientInstanceName="grdFAContractExpense" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsFAPropertyExpense" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Accounts/Operation/Transaction/PropertyExpenseDetail.aspx");' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DocumentDate" Caption="Document Date" VisibleIndex="2" Width="50px" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Document Code" VisibleIndex="3" Width="100px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="chargedescription" Caption="Charge Description" VisibleIndex="4" Width="300px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="supplier" Caption="Supplier" VisibleIndex="4" Width="300px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="amount" Caption="Amount" VisibleIndex="5" Width="100px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="vatamount" Caption="VAT" VisibleIndex="6" Width="100px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="netamount" Caption="Net" VisibleIndex="7" Width="100px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="property" Caption="Property" VisibleIndex="9" Width="200px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="12" Width="10%" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="13" Width="5%" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="14" Width="5%" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="15" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsFAPropertyExpense" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetPropertyExpenseList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow">
                                                        </asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="DEBIT CREDIT NOTE" Name="DC">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdDebitCreditNote" ClientInstanceName="grdDebitCreditNote" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsDebitCreditNote" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Accounts/Operation/Transaction/NoteDetail.aspx");' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DocumentDate" Caption="Document Date" VisibleIndex="2" Width="50px" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Document Code" VisibleIndex="3" Width="100px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataDateColumn FieldName="TransactionDate" Caption="Transaction Date" VisibleIndex="2" Width="50px" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Narration" Caption="Narration" VisibleIndex="4" Width="300px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="NoteTo" Caption="NoteTo" VisibleIndex="4" Width="300px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Amount" Caption="Amount" VisibleIndex="5" Width="100px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn> 
                                                                <dx:GridViewDataTextColumn FieldName="StatusDescription" Caption="Status Description" VisibleIndex="9" Width="200px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="NoteTypeDescription" Caption="NoteType Description" VisibleIndex="9" Width="200px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="12" Width="10%" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="13" Width="5%" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="14" Width="5%" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="15" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsDebitCreditNote" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetDebitCreditNoteList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow">
                                                        </asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                        </TabPages>
                                    </dx:ASPxPageControl>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="MAINTENANCE" Name="MA">
                            <ContentCollection>
                                <dx:ContentControl>
                                    <dx:ASPxPageControl ID="MATab" EnableTheming="True" Theme="Office2010Blue" Width="100%" runat="server" CssClass="dxtcFixed" ActiveTabIndex="0" EnableHierarchyRecreation="True">
                                        <TabPages>
                                            <dx:TabPage Text="CONTRACT EXPENSE" Name="CE">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdMAContractExpense" ClientInstanceName="grdMAContractExpense" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsMAContractExpense" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Admin/Portfolio/TenancyContract/ContractExpenseDetail.aspx");' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DocumentDate" Caption="Document Date" VisibleIndex="2" Width="50px" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Document Code" VisibleIndex="3" Width="100px" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="chargedescription" Caption="Charge Description" VisibleIndex="4" Width="300px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="chargeamount" Caption="Amount" VisibleIndex="5" Width="100px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="vatamount" Caption="VAT" VisibleIndex="6" Width="100px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="netamount" Caption="Net" VisibleIndex="7" Width="100px" CellStyle-Wrap="False">
                                                                    <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="unitno" Caption="Unit" VisibleIndex="8" Width="100px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="propertyname" Caption="Property" VisibleIndex="9" Width="200px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="ContractCode" CellStyle-Wrap="False" Caption="Contract Code" VisibleIndex="10" Width="150px">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="tenant" Caption="Tenant" VisibleIndex="11" Width="300px" CellStyle-Wrap="False">
                                                                    <SettingsHeaderFilter Mode="CheckedList"></SettingsHeaderFilter>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="12" Width="10%" CellStyle-Wrap="False">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="13" Width="5%" CellStyle-Wrap="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="14" Width="5%" CellStyle-Wrap="False" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="15" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsMAContractExpense" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetContractExpenseList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow">
                                                            <SelectParameters>
                                                                <asp:Parameter Name="moduleCode" DefaultValue="MA" />
                                                            </SelectParameters>
                                                        </asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                        </TabPages>
                                    </dx:ASPxPageControl>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="PURCHASE" Name="PA">
                            <ContentCollection>
                                <dx:ContentControl>
                                    <dx:ASPxPageControl ID="PATab" EnableTheming="True" Theme="Office2010Blue" Width="100%" runat="server" CssClass="dxtcFixed" ActiveTabIndex="0" EnableHierarchyRecreation="True">
                                        <TabPages>
                                            <dx:TabPage Text="PURCHASE REQUISITION" Name="PR">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdPurchaseRequisitionList" ClientInstanceName="grdPurchaseRequisitionList" runat="server"
                                                            KeyFieldName="DocumentCode" AutoGenerateColumns="False" DataSourceID="odsPurchaseRequisitionRequest" EnableTheming="True" Theme="Office2010Blue">
                                                            <Toolbars>
                                                                <dx:GridViewToolbar EnableAdaptivity="true">
                                                                    <Items>
                                                                        <dx:GridViewToolbarItem Name="CustomExportToPDF" Text="Export to PDF" Image-IconID="export_exporttopdf_16x16" />
                                                                        <dx:GridViewToolbarItem Name="CustomExportToXLSX" Text="Export to XLSX" Image-IconID="export_exporttoxlsx_16x16office2013">
                                                                        </dx:GridViewToolbarItem>
                                                                    </Items>
                                                                </dx:GridViewToolbar>
                                                            </Toolbars>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openPurchaseRequisitionRequestDetail(this);' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DocumentDate" Caption="Request Date" VisibleIndex="0" Width="80px" CellStyle-Wrap="false">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Requisition No." VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="300px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="0" Width="200px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="ReferenceNo" Caption="Reference#" VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="RequestBy" Caption="Request By" VisibleIndex="1" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Item" Caption="Total Item" VisibleIndex="2" Width="50px" CellStyle-Wrap="false"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Total Quantity" VisibleIndex="2" Width="50" CellStyle-Wrap="false"></dx:GridViewDataTextColumn>

                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Settings ShowFilterBar="Visible" ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="true" ShowGroupFooter="VisibleIfExpanded" ShowPreview="true" />
                                                            <SettingsPager PageSize="20">
                                                                <PageSizeItemSettings Visible="true" Items="20, 50, 70, 100" />
                                                            </SettingsPager>
                                                            <SettingsPopup>
                                                                <HeaderFilter Width="235" Height="280">
                                                                </HeaderFilter>
                                                            </SettingsPopup>
                                                            <ClientSideEvents />
                                                            <TotalSummary>
                                                                <dx:ASPxSummaryItem FieldName="Quantity" SummaryType="Sum" DisplayFormat="{0:#,##0}" />
                                                            </TotalSummary>
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents ToolbarItemClick="function (s, e) { onToolbarClick(s, e);}" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <SettingsExport EnableClientSideExportAPI="true" Landscape="true" LeftMargin="0" RightMargin="0" TopMargin="0" BottomMargin="0" ExcelExportMode="WYSIWYG" />
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsPurchaseRequisitionRequest" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetPurchaseRequisitionRequestList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="PURCHASE INQUIRY" Name="IQ">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdPurchaseInquiry" ClientInstanceName="grdPurchaseInquiry" runat="server"
                                                            KeyFieldName="DocumentCode" AutoGenerateColumns="False" DataSourceID="odsPurchaseInquiry" EnableTheming="True" Theme="Office2010Blue">
                                                            <Toolbars>
                                                                <dx:GridViewToolbar EnableAdaptivity="true">
                                                                    <Items>
                                                                        <dx:GridViewToolbarItem Name="CustomExportToPDF" Text="Export to PDF" Image-IconID="export_exporttopdf_16x16" />
                                                                        <dx:GridViewToolbarItem Name="CustomExportToXLSX" Text="Export to XLSX" Image-IconID="export_exporttoxlsx_16x16office2013">
                                                                        </dx:GridViewToolbarItem>
                                                                    </Items>
                                                                </dx:GridViewToolbar>
                                                            </Toolbars>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openPurchaseInquiryDetail(this);' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DocumentDate" Caption="Request Date" VisibleIndex="0" Width="80px" CellStyle-Wrap="false">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Requisition No." VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="300px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="0" Width="200px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="ReferenceNo" Caption="Reference#" VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="RequestBy" Caption="Request By" VisibleIndex="1" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Item" Caption="Total Item" VisibleIndex="2" Width="50px" CellStyle-Wrap="false"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Total Quantity" VisibleIndex="2" Width="50" CellStyle-Wrap="false"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Settings ShowFilterBar="Visible" ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="true" ShowGroupFooter="VisibleIfExpanded" ShowPreview="true" />
                                                            <SettingsPager PageSize="20">
                                                                <PageSizeItemSettings Visible="true" Items="20, 50, 70, 100" />
                                                            </SettingsPager>
                                                            <SettingsPopup>
                                                                <HeaderFilter Width="235" Height="280">
                                                                </HeaderFilter>
                                                            </SettingsPopup>
                                                            <ClientSideEvents ToolbarItemClick="function (s, e) { onToolbarClick(s, e);}" />
                                                            <TotalSummary>
                                                                <dx:ASPxSummaryItem FieldName="Quantity" SummaryType="Sum" DisplayFormat="{0:#,##0}" />
                                                            </TotalSummary>
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <SettingsExport EnableClientSideExportAPI="true" Landscape="true" LeftMargin="0" RightMargin="0" TopMargin="0" BottomMargin="0" ExcelExportMode="WYSIWYG" />
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsPurchaseInquiry" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetPurchaseInquiryList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="PURCHASE ORDER" Name="PO">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdPurchaseOrder" ClientInstanceName="grdPurchaseOrder" runat="server"
                                                            KeyFieldName="DocumentCode" AutoGenerateColumns="False" DataSourceID="odsPurchaseOrder" EnableTheming="True" Theme="Office2010Blue">
                                                            <Toolbars>
                                                                <dx:GridViewToolbar EnableAdaptivity="true">
                                                                    <Items>
                                                                        <dx:GridViewToolbarItem Name="CustomExportToPDF" Text="Export to PDF" Image-IconID="export_exporttopdf_16x16" />
                                                                        <dx:GridViewToolbarItem Name="CustomExportToXLSX" Text="Export to XLSX" Image-IconID="export_exporttoxlsx_16x16office2013">
                                                                        </dx:GridViewToolbarItem>
                                                                    </Items>
                                                                </dx:GridViewToolbar>
                                                            </Toolbars>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Sr." VisibleIndex="0" Width="30px" CellStyle-Wrap="false">
                                                                    <DataItemTemplate>
                                                                        <%#Container.ItemIndex + 1 %>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Width="25px" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openPurchaseOrderDetail(this);' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="DocumentDate" Caption="Request Date" VisibleIndex="0" Width="80px" CellStyle-Wrap="false">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                    <SettingsHeaderFilter Mode="DateRangePicker" DateRangeCalendarSettings-EnableMultiSelect="true"></SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" ShowFilterRowMenu="False" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentCode" Caption="Requisition No." VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="300px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="0" Width="200px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="ReferenceNo" Caption="Reference#" VisibleIndex="0" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="RequestBy" Caption="Request By" VisibleIndex="1" Width="100px" CellStyle-Wrap="false">
                                                                    <SettingsHeaderFilter Mode="CheckedList">
                                                                    </SettingsHeaderFilter>
                                                                    <Settings AllowHeaderFilter="True" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Item" Caption="Total Item" VisibleIndex="2" Width="50px" CellStyle-Wrap="false"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Total Quantity" VisibleIndex="2" Width="50" CellStyle-Wrap="false"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Settings ShowFilterBar="Visible" ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="true" ShowGroupFooter="VisibleIfExpanded" ShowPreview="true" />
                                                            <SettingsPager PageSize="20">
                                                                <PageSizeItemSettings Visible="true" Items="20, 50, 70, 100" />
                                                            </SettingsPager>
                                                            <SettingsPopup>
                                                                <HeaderFilter Width="235" Height="280">
                                                                </HeaderFilter>
                                                            </SettingsPopup>
                                                            <ClientSideEvents />
                                                            <TotalSummary>
                                                                <dx:ASPxSummaryItem FieldName="Quantity" SummaryType="Sum" DisplayFormat="{0:#,##0}" />
                                                            </TotalSummary>
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <SettingsExport EnableClientSideExportAPI="true" Landscape="true" LeftMargin="0" RightMargin="0" TopMargin="0" BottomMargin="0" ExcelExportMode="WYSIWYG" />
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsPurchaseOrder" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetPurchaseOrderList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                        </TabPages>
                                    </dx:ASPxPageControl>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="INVENTORY" Name="IA">
                            <ContentCollection>
                                <dx:ContentControl>
                                    <dx:ASPxPageControl ID="IATab" EnableTheming="True" Theme="Office2010Blue" Width="100%" runat="server" CssClass="dxtcFixed" ActiveTabIndex="0" EnableHierarchyRecreation="True">
                                        <TabPages>
                                            <dx:TabPage Text="REQUISITION REQUEST" Name="MR">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdInventoryRequisitionList" ClientInstanceName="grdInventoryRequisitionList" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsInventoryRequisitionRequest" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="5%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Inventory/Operation/Requisition/Material_Requisition_Detail.aspx");' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentNo" Caption="#" VisibleIndex="0" Width="100px" />
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="200px" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" Width="150px"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="2" Width="100px"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Item" Caption="Item" VisibleIndex="3" Width="50px"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Quantity" VisibleIndex="3" Width="50px"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="90px">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="50px">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30px" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="150px" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents RowClick="function(s, e) { OnReservedGridRowClick(e); }" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsInventoryRequisitionRequest" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetInventoryRequisitionRequestList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="MATERIAL ISSUE" Name="MA">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdInventoryRequisitionConfirmationList" ClientInstanceName="grdInventoryRequisitionConfirmationList" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsInventoryRequisitionConfirmationRequest" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="5%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openInventoryRequisitionRequestDetail(this);' title="View Requisition Detail" data-keyfield='<%#Eval("DocumentCode") %>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                        <a href='#' onclick='return openWorkflowDetail(this);' class="workflow_approve" title="Approval" data-keyfield='<%#Eval("DataId") %>'>
                                                                            <img src="../../../images/greenflag.gif" alt="Approve" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentNo" Caption="#" VisibleIndex="0" Width="10%" />
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="20%" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="2" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Item" Caption="Item" VisibleIndex="3" Width="5%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Quantity" VisibleIndex="3" Width="5%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents RowClick="function(s, e) { OnReservedGridRowClick(e); }" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsInventoryRequisitionConfirmationRequest" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetInventoryRequisitionConfirmationRequestList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                            <dx:TabPage Text="MATERIAL RETURN" Name="MR">
                                                <ContentCollection>
                                                    <dx:ContentControl>
                                                        <dx:ASPxGridView ID="grdInventoryReturnConfirmationList" ClientInstanceName="grdInventoryReturnConfirmationList" runat="server"
                                                            Width="100%" KeyFieldName="DataId" AutoGenerateColumns="False" DataSourceID="odsInventoryReturnConfirmationRequest" EnableTheming="True" Theme="Office2010Blue">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Width="5%" CellStyle-Wrap="false" VisibleIndex="0">
                                                                    <DataItemTemplate>
                                                                        <a href='#' onclick='return openApprovalDetailPage(this, "../../../Inventory/Operation/Stock/ItemReturnHistoryDetail.aspx");' title="View Detail" data-keyfield='<%#"SC=" & Eval("DocumentCode") & "&DID=" & Eval("DataId")%>'>
                                                                            <img src="../../../images/edit.png" alt="View Detail" /></a>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DocumentNo" Caption="#" VisibleIndex="0" Width="10%" />
                                                                <dx:GridViewDataTextColumn FieldName="LandlordName" Caption="Landlord" VisibleIndex="0" Width="20%" />
                                                                <dx:GridViewDataTextColumn FieldName="Property" Caption="Property" VisibleIndex="1" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="UnitNo" Caption="Unit" VisibleIndex="2" Width="20%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Item" Caption="Item" VisibleIndex="3" Width="5%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Quantity" VisibleIndex="3" Width="5%"></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn FieldName="EnteredDate" Caption="Entry Date" VisibleIndex="7" Width="10%">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Over_Day" Settings-AllowAutoFilter="False" Caption="Over Days" VisibleIndex="8" Width="5%">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="LevelNo" Settings-AllowAutoFilter="False" Caption="Level" VisibleIndex="9" Width="30%" />
                                                                <dx:GridViewDataTextColumn FieldName="LevelTitle" Settings-AllowAutoFilter="False" Caption="Status" VisibleIndex="10" CellStyle-Wrap="False" Width="30%" />
                                                            </Columns>
                                                            <Styles Cell-Font-Size="8pt" Header-Font-Size="8pt"></Styles>
                                                            <Settings ShowFilterRow="false" />
                                                            <SettingsPager>
                                                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                                            </SettingsPager>
                                                            <SettingsSearchPanel Visible="True" />
                                                            <SettingsBehavior AllowFocusedRow="true" AllowSelectByRowClick="true" />
                                                            <ClientSideEvents RowClick="function(s, e) { OnReservedGridRowClick(e); }" />
                                                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                                                            <FormatConditions>
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 1" Format="Custom" CellStyle-BackColor="Yellow" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 2" Format="Custom" CellStyle-BackColor="#ffcc66" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] = 3" Format="Custom" CellStyle-BackColor="Orange" />
                                                                <dx:GridViewFormatConditionHighlight FieldName="Over_Day" Expression="[Over_Day] >= 7" Format="Custom" CellStyle-BackColor="Red" CellStyle-ForeColor="White" />
                                                            </FormatConditions>
                                                        </dx:ASPxGridView>
                                                        <asp:ObjectDataSource ID="odsInventoryReturnConfirmationRequest" runat="server" DataObjectTypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"
                                                            SelectMethod="GetInventoryReturnConfirmationRequestList" TypeName="PropertyManagement.DataAccessLayer.ApprovalWorkflow"></asp:ObjectDataSource>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                        </TabPages>
                                    </dx:ASPxPageControl>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>

                <dx:ASPxPopupControl ID="apcWorkflowDetail" runat="server" AllowDragging="True" AllowResize="True"
                    CloseAction="CloseButton" ContentUrl="ApprovalDetail.aspx" AutoUpdatePosition="true" Modal="true"
                    EnableViewState="False" PopupElementID="popupArea" PopupHorizontalAlign="WindowCenter"
                    PopupVerticalAlign="WindowCenter" ShowFooter="True" ShowOnPageLoad="False"
                    Width="1000px" Height="600px" MinWidth="310px" MinHeight="280px"
                    FooterText="Try to resize the control using the resize grip or the control's edges"
                    HeaderText="Approval Detail" ClientInstanceName="apcWorkflowDetail" EnableHierarchyRecreation="True" FooterStyle-Wrap="True">
                    <ContentStyle Paddings-Padding="0" />
                    <ClientSideEvents />
                </dx:ASPxPopupControl>

            </div>

            <script type="text/javascript">
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
                    function OnReservedGridRowClick(e) {
                        grdReservationList.GetRowValues(e.visibleIndex, 'DataId', function (values) {
                            apcWorkflowDetail.SetContentUrl('../Reservation/ReservationDetail.aspx?DataId=' + values);
                            apcWorkflowDetail.Show();
                        });
                    }
                    // Value array contains "EmployeeID" and "Notes" field values returned from the server 
                    function OnGetRowValues(values) {
                        alert(values);
                    }

                    $('.reservation_detail').on('click', function () {
                        var systemcode = $(this).data('keyfield');
                        apcWorkflowDetail.SetContentUrl('../Reservation/ReservationDetail.aspx?Popup=1&SC=' + systemcode);
                        apcWorkflowDetail.Show();
                        return false;
                    })
                    $('.contract_detail').on('click', function () {
                        return openContractDetail(this);
                    })
                    $('.workflow_approve').on('click', function () {
                        return openWorkflowDetail(this);
                    })


                    $('#<%=lnkApprovalVoucher.ClientID%>').on('click', function () {
                        grdVoucherList.GetSelectedFieldValues('DataId', OnGetSelectedVoucherFieldValues);
                        return false;
                    });

                });

                function onToolbarClick(s, e) {
                    e.processOnServer = true; e.usePostBack = true;
                }

                function OnGetSelectedVoucherFieldValues(selectedValues) {
                    var dataIds = '';
                    if (selectedValues.length == 0) return;
                    openWorkflowForMultiSelection(selectedValues.join('^'));
                    return false;
                }

                function openApprovalDetailPage(a, url) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl(url + "?" + systemcode);
                    apcWorkflowDetail.SetWidth(screen.availWidth - 100);
                    apcWorkflowDetail.SetHeight(screen.availHeight - 90);
                    apcWorkflowDetail.Show();
                    return false;
                }

                function openProposalDetail(a) {
                    return openApprovalDetailPage(a, '../Proposal/ProposalDetail.aspx');
                }

                function openContractDetail(a) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('../TenancyContract/ContractDetailAdvance.aspx?' + systemcode);
                    apcWorkflowDetail.SetWidth(screen.availWidth - 100);
                    apcWorkflowDetail.SetHeight(screen.availHeight - 90);
                    apcWorkflowDetail.Show();
                    return false;
                }

                function openBasicContractDetail(a) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('../TenancyContract/ContractDetail.aspx?' + systemcode);
                    apcWorkflowDetail.SetWidth(screen.availWidth - 100);
                    apcWorkflowDetail.SetHeight(screen.availHeight - 90);
                    apcWorkflowDetail.Show();
                    return false;
                }

                function openReservationDetail(a) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('../Reservation/ReservationDetail.aspx?Popup=1&' + systemcode);
                    apcWorkflowDetail.Show();
                    return false;
                }

                function openTransactionDetail(a) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('../../../Accounts/Operation/Payment/TransactionDetail.aspx?id=' + systemcode);
                    apcWorkflowDetail.SetWidth(screen.availWidth - 100);
                    apcWorkflowDetail.SetHeight(screen.availHeight - 90);
                    apcWorkflowDetail.Show();
                    return false;
                }
                function openVoucherDetail(a) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('../../../Admin/Transactions/Payment/VoucherDetail.aspx?' + systemcode);
                    apcWorkflowDetail.SetWidth(screen.availWidth - 100);
                    apcWorkflowDetail.SetHeight(screen.availHeight - 90);
                    apcWorkflowDetail.Show();
                    return false;
                }
                function openInventoryRequisitionRequestDetail(a) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('../../../Inventory/Operation/Requisition/material_requisition_detail.aspx?MID=' + systemcode);
                    apcWorkflowDetail.SetWidth(screen.availWidth - 100);
                    apcWorkflowDetail.SetHeight(screen.availHeight - 90);
                    apcWorkflowDetail.Show();
                    return false;
                }
                function openPurchaseRequisitionRequestDetail(a) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('../../../Purchase/Operation/Requisition/Purchase_Requisition_Request_Detail.aspx?' + systemcode);
                    apcWorkflowDetail.SetWidth(screen.availWidth - 100);
                    apcWorkflowDetail.SetHeight(screen.availHeight - 90);
                    apcWorkflowDetail.Show();
                    return false;
                }
                function openPurchaseOrderDetail(a) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('../../../Purchase/Operation/Order/PurchaseOrderDetail.aspx?' + systemcode);
                    apcWorkflowDetail.SetWidth(screen.availWidth - 100);
                    apcWorkflowDetail.SetHeight(screen.availHeight - 90);
                    apcWorkflowDetail.Show();
                    return false;
                }
                function openGoodReceiptDetail(a) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('../../../Purchase/Operation/Order/PurchaseGoodReceiptDetail.aspx?' + systemcode);
                    apcWorkflowDetail.SetWidth(screen.availWidth - 100);
                    apcWorkflowDetail.SetHeight(screen.availHeight - 90);
                    apcWorkflowDetail.Show();
                    return false;
                }
                function openPurchaseInquiryDetail(a) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('../../../Purchase/Operation/Order/Purchase_Requisition_Detail.aspx?' + systemcode);
                    apcWorkflowDetail.SetWidth(screen.availWidth - 100);
                    apcWorkflowDetail.SetHeight(screen.availHeight - 90);
                    apcWorkflowDetail.Show();
                    return false;
                }
                function openPrepaidExpenseDetail(a) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('../../../Accounts/Operation/Transaction/PrepaidExpenseDetail.aspx?' + systemcode);
                    apcWorkflowDetail.SetWidth(screen.availWidth - 100);
                    apcWorkflowDetail.SetHeight(screen.availHeight - 90);
                    apcWorkflowDetail.Show();
                    return false;
                }
                function openPDConHoldDetail(a) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('../../../Admin/PortFolio/TenancyContract/PDCHoldDetail.aspx?' + systemcode);
                    apcWorkflowDetail.SetWidth(screen.availWidth - 100);
                    apcWorkflowDetail.SetHeight(screen.availHeight - 90);
                    apcWorkflowDetail.Show();
                    return false;
                }
                function openPettyCashDetail(a) {
                    var systemcode = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('../../../Accounts/Operation/Transaction/PettyCashAccount.aspx?PID=' + systemcode);
                    apcWorkflowDetail.SetWidth(screen.availWidth - 100);
                    apcWorkflowDetail.SetHeight(screen.availHeight - 90);
                    apcWorkflowDetail.Show();
                    return false;
                }
                function openWorkflowDetail(a) {
                    var dataid = $(a).data('keyfield');
                    apcWorkflowDetail.SetContentUrl('ApprovalDetail.aspx?Popup=1&DataId=' + dataid);
                    apcWorkflowDetail.Show();
                    return false;
                }
                function openWorkflowForMultiSelection(dataId) {
                    apcWorkflowDetail.SetContentUrl('ApprovalDetail.aspx?Popup=1&MultiDataId=' + dataId);
                    apcWorkflowDetail.Show();
                    return false;
                }

                function hideWorkflowDetail(stageCode) {
                    apcWorkflowDetail.Hide();
                    if (stageCode != null && stageCode != '0') {
                        showProgressOnForce();
                        window.location = "/Admin/Portfolio/Workflow/ApprovalWorkflow.aspx?" + stageCode;
                    }
                }

            </script>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
