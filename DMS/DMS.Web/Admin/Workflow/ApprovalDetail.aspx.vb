﻿Imports DMS.DataContext.DataAccessHelper

Public Class ApprovalDetail
    Inherits BasePage
    Private caller As ApprovalWorkflow = Nothing
    Protected ReadOnly Property IsMulti As Boolean
        Get
            Return Request.QueryString("MultiDataId") IsNot Nothing
        End Get
    End Property

    Private Property DataId As String
        Get
            Return ViewState("DATA_ID")
        End Get
        Set(value As String)
            ViewState("DATA_ID") = value
        End Set
    End Property
    Private Property DocumentCode As String
        Get
            Return ViewState("DOCUMENT_CODE")
        End Get
        Set(value As String)
            ViewState("DOCUMENT_CODE") = value
        End Set
    End Property
    Private Property StageCode As String
        Get
            Return ViewState("STAGE_CODE")
        End Get
        Set(value As String)
            ViewState("STAGE_CODE") = value
        End Set
    End Property
    Private ReadOnly Property StageCodeQueryParam As String
        Get
            Dim queryParam As New QueryString("PARAM")
            queryParam.Add("SC", StageCode)
            Return queryParam.Generate
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'caller = New DMS.DataAccessLayer.ApprovalWorkflow
            If Not IsPostBack Then Initialize()
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub btnApproveOK_Click(sender As Object, e As EventArgs) Handles btnApproveOK.Click
        Try
            Dim ids As List(Of String) = DataId.Split("^").ToList()
            For Each did In ids
                If IsMulti Then LoadWorkflowDetail(did)
                'If Not caller.UpdateApprovalStatus(did, StageCode, True, False, txtApprovalComments.Text.Trim) Then
                '    Throw New ArgumentException("Error while approving the document !!!")
                'End If
            Next
            RunScript(String.Format("window.parent.hideWorkflowDetail('{0}'); showProgressOnForce();", StageCodeQueryParam))
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub btnRejectOK_Click(sender As Object, e As EventArgs) Handles btnRejectOK.Click
        Try
            Dim ids As List(Of String) = DataId.Split("^").ToList()
            For Each did In ids
                If IsMulti Then LoadWorkflowDetail(did)
                'If Not caller.UpdateApprovalStatus(did, StageCode, False, False, txtApprovalComments.Text.Trim) Then
                '    Throw New ArgumentException("Error while rejecting the document !!!")
                'End If
            Next
            RunScript(String.Format("window.parent.hideWorkflowDetail('{0}'); showProgressOnForce();", StageCodeQueryParam))
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub Initialize()
        Try
            If Request.QueryString("DataId") IsNot Nothing Then
                DataId = Request.QueryString("DataId")
                LoadWorkflowDetail(DataId)
            ElseIf Request.QueryString("MultiDataId") IsNot Nothing Then
                DataId = Request.QueryString("MultiDataId")
            Else
                RunScript("window.parent.hideWorkflowDetail('0');")
            End If
        Catch
            Throw
        End Try
    End Sub

    Private Sub LoadWorkflowDetail(DataId%)
        Try
            Using local As DataTable = New DataTable() ' (New DMS.DataAccessLayer.ApprovalWorkflow()).GetWorkflowDetail(DataId)
                If local IsNot Nothing AndAlso local.Rows.Count > 0 Then
                    With local.Rows(0)
                        DocumentCode = .Item("DOCUMENTCODE")
                        StageCode = .Item("STAGECODE")
                        lblStageTitle.Text = .Item("LEVELTITLE")
                        lblEntryDate.Text = CDate(.Item("ENTEREDDATE")).ToString("dd/MM/yyyy")
                        Dim days As Integer = DateDiff(DateInterval.Day, CDate(.Item("ENTEREDDATE")), DateTime.Now)
                        lblDaysPassed.Text = days.ToString
                        If days = 1 Then
                            lblDaysPassed.BackColor = System.Drawing.Color.Yellow
                            lblDaysPassed.ForeColor = Drawing.Color.Black
                        End If
                        If days.IsBetween(2, 4) Then
                            lblDaysPassed.BackColor = System.Drawing.Color.Orange
                            lblDaysPassed.ForeColor = Drawing.Color.Black
                        End If
                        If days > 4 Then
                            lblDaysPassed.BackColor = System.Drawing.Color.Red
                            lblDaysPassed.ForeColor = Drawing.Color.White
                        End If
                    End With
                Else
                    RunScript("alert('System is not able to locate the workflow data. Kindly try again'); window.parent.hideWorkflowDetail('0');")
                End If
            End Using
        Catch
            Throw
        End Try
    End Sub

    Private Sub odsApprovalHistory_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs) Handles odsApprovalHistory.Selecting
        e.InputParameters("DocumentCode") = DocumentCode
    End Sub
End Class