﻿Imports System.Web
Imports System.Web.Services
Imports System.IO

Public Class dremoval
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Try
            context.Response.ContentType = "text/plain"
            Dim savedpath As String = context.Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings("TEMP_FOLDER"))
            If context.Request.Form("myId") IsNot Nothing Then
                savedpath += "\" + context.Request.Form("myId")
            End If
            If context.Request.Form("fileName") IsNot Nothing Then
                savedpath += "\" + context.Request.Form("fileName")
            End If
            If File.Exists(savedpath) Then File.Delete(savedpath)
            context.Response.Write("SUCCESS")
        Catch
            context.Response.Write("FAILED")
        End Try

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class