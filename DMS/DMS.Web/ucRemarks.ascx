﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucRemarks.ascx.vb" Inherits="DMS.Web.ucRemarks" %>
<asp:UpdatePanel ID="up" runat="server">
    <ContentTemplate> 

        <%If Not IsReadOnly Then %>
        <div>
            <asp:TextBox ID="txtRemarks" runat="server" Width="450px" MaxLength="500" placeholder="Enter Remarks"
                CssClass="w3-border w3-border-grey w3-padding remarksText">
            </asp:TextBox>&nbsp;
            <asp:LinkButton ID="lnkAddRemarks" runat="server" ToolTip="Add Remarks" CssClass="w3-text-blue addRemarks">
                <i class="moveMiddle basicon basicon-conversation8"></i>
            </asp:LinkButton>
        </div>
        <%End If %>
        
        <div class="w3-row w3-padding-4">

        </div>
        <asp:GridView ID="gvRemarks" runat="server" AutoGenerateColumns="False" Width="100%">
            <RowStyle CssClass="DataGridItem" />
            <HeaderStyle CssClass="DataGridHeader" />
            <AlternatingRowStyle CssClass="DataGridAlternateItem" />
            <EmptyDataTemplate>
                <div class="w3-center w3-card w3-padding w3-pale-yellow emptyRemarksList">
                    <div class="w3-padding-4">
                        <<< No Remarks Added >>>
                    </div>
                </div>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="No." ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <div style="width: 40px;"><%#Container.DataItemIndex + 1 %>.</div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div style="width: 500px">
                            <%# DirectCast(Container.DataItem, DMS.Models.DTO.RemarksDTO).Remarks%>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Submitted By"  ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div style="width: 120px">
                            <%#DirectCast(Container.DataItem, DMS.Models.DTO.RemarksDTO).DoneBy%>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Submitted On" ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div class="inlineblock moveTop" style="width: 150px">
                            <%#CDate(DirectCast(Container.DataItem, DMS.Models.DTO.RemarksDTO).RemarksOn).ToString("dd-MM-yyyy hh:mm ttt")%>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField> 
            </Columns>
        </asp:GridView>
        <asp:HiddenField ID="hdnCount" runat="server" value="0" />
        <%If False Then%>
        <script type="text/javascript" src="../../../include/common.js">
        </script>
        <script type="text/javascript" src="../../../include/jquery-vsdoc.js">
        </script>
        <%End If%>
        <script type="text/javascript">
            
            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
                <%=Me.ClientID%>_init_page_events_and_controls();
            });
             

            function <%=Me.ClientID%>_init_page_events_and_controls() {

                $('.remarksText').bind('keypress', function () {
                    var zone = $(this).closest('div');
                    var e = event || window.event;
                    if (e.keyCode == 13) {
                        if ($.trim($(this).val()).length == 0) {
                            $.showMessage('No remarks added !!!');
                        }
                        else {                            
                            window.location = $('.addRemarks', zone).prop('href');
                        }
                        return false;
                    }
                });

            }
        </script>

    </ContentTemplate>
</asp:UpdatePanel>

