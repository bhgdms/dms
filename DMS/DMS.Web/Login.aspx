﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="DMS.Web.Login" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--[if IE 6]>
<html id="ie6" lang="en-US">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=1048, user-scalable=yes" />
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0,Transition=0)" />
    <link rel="stylesheet" href="Content/css/default.css" type="text/css">
    <link rel="stylesheet" href="Content/english.css" type="text/css">
    <link rel="stylesheet" href="Content/css/fonts/basicon/flaticon.css" type="text/css">
    <link rel="stylesheet" href="Content/css/fonts/usericon/flaticon.css" type="text/css">
    <link rel="stylesheet" href="Content/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href='Content/css/select2.min.css' type="text/css" />
    <link rel="stylesheet" href="Content/css/w3.css" type="text/css">
    <script language="javascript" src="Scripts/jQuery-3.3.1.js"></script>
    <script language="javascript" src="Scripts/Common.js"></script>
    <script src="Scripts/select2.min.js" type="text/javascript"></script> 
    <title>DOCUMENT MANAGEMENT SYSTEM</title>
    <style type="text/css">
        html, body, form {
            margin: 0;
            padding: 0;
            height: 100%;
            width: 100%;
        } 

        #login-table, #up, #loginDiv {
            /*width: 100%;
            height: 100%;*/
            padding: 0 0 0 0;
        }

        #divWelcome {
            color: darkblue;
            padding: 20px 0px 20px 0px;
            left: 50%;
            text-align: center;
            font-size: 34pt;
            letter-spacing: 4pt;
        }

        #divProductType
        {
		padding:0;
            /*width: 100%;*/
            text-align: center;
            font-size: 42pt;
            color: white;
            text-transform: uppercase;
            letter-spacing: 3pt;
        } 

        #messageDiv {
            position: absolute;
            top: 30%;
            left: 25%;
            margin-top: -100px;
            margin-left: -200px;
            width: 610px;
            height: 550px;
            border-right: solid 1px #ffc600;
            border-top: solid 0px #ffc600;
        }

        .midDiv {
            background-color: bisque;
            opacity: .78;
            position: absolute;
            top: 45%;
            left: 50%;
            margin-top: -100px;
            margin-left: -200px;
            width: 400px;
            height: 300px;
            border: solid 1px black;
            border-top: solid 5px #ffc600;
        }

        .logo {
            display: inline-block;
        }

        .midDiv div.title {
            padding: 12px 16px 6px 11px;
            color: black;
            height: 48px;
            font-weight: bold;
            font-size: 20pt;
            background-color: aliceblue;
            text-align: center;
        }

        .midDiv div.loginTable {
            position: absolute;
            top: 80px;
            padding: 12px 2px 6px 2px;
        }

        #footer_login {
            text-align:center;
            position: absolute;
            top: 95%;
            left: 50%;
            margin-left: -165px;
            /*padding: 12px 16px 6px 11px;*/
        }

        .loginTitle {
            color: #aaa;
            font-size: 12pt;
            font-weight: bold;
            width: 120px;
            padding: 0px 10px 0px 10px;
            text-align: right;
            opacity: .78;
        }

        .loginTextBoxZone {
            color: black;
        }

        .loginTextBox {
            padding: 0px 10px 0px 10px;
            width: 350px;
            color: black;
            margin-left: 10px;
        }

            .loginTextBox input {
                /*box-shadow: inset 0 0 30px rgba(0, 0, 0, 0.1);
                color: #f0f0f0;
                border: 1px solid rgba(255, 255, 255, 0.2);
                -webkit-transition: all 200ms ease-in-out;
                -moz-transition: all 200ms ease-in-out;
                -ms-transition: all 200ms ease-in-out;
                -o-transition: all 200ms ease-in-out;
                transition: all 200ms ease-in-out color: #aaa;*/
                height: 30px;
                background-color: aliceblue;
            }

        input[type="text"]:hover, input[type="password"]:hover, input[type="email"]:hover, textarea:hover {
            border: 1px solid rgba(255, 255, 255, 0.4);
        }

        input[type="text"]:focus, input[type="password"]:focus, input[type="email"]:focus, textarea:focus {
            border: 1px solid rgba(255, 255, 255, 0.4);
        }

        .loginTextBox input[type=submit], input[type=button] {
        }

        .main-container {
            height: 100%;
        }

        .sub-container {
            min-height: 100%;
        }

        .select option {
            color: #000;
        }
    </style>
</head>
<body onfocus="document.getElementById('txtUserId').focus();">
    <div id="divMaster" class="main-container">
        <form id="form1" runat="server">
            <asp:ScriptManager ID="smLogin" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="up" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlLogin" runat="server">
                        <div id="divProductType">
				            <img src='images/BHCLOGO.PNG' alt='Logo' width="30%" />
                        </div>
                        <div id="loginDiv">
                            <div id="divWelcome">
                                DOCUMENT MANAGEMENT SYSTEM
                            </div> 
                            <div class="midDiv" id="loginSection">
                                <div class="title">
                                    LOGIN
                                </div>
                                <div class="loginTable">
                                    <div class="loginTextBox"> 
                                        <div class="w3-padding-4">
                                            Enter Username
                                        </div>
                                        <div class="w3-padding-4">
                                            <asp:TextBox ID="txtUserId" runat="server" Width="100%" MaxLength="20"></asp:TextBox>
                                        </div>
                                        <div class="w3-padding-4">
                                            <div class="inlineblock">Enter Password&nbsp;</div>
                                            <div class="inlineblock capswarning w3-card w3-red w3-padding-ver-4">CAPS LOCK IS ON</div>
                                        </div>
                                        <div class="w3-padding-4">
                                            <asp:TextBox ID="txtPassword" runat="server" Width="100%" MaxLength="20" TextMode="Password"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="w3-padding-4 w3-center actionZone">
                                        <button id="btnSignIn" class="default-button">
                                            <i class="fa fa-sign-in"></i>Sign In</button>
                                    </div>
                                    <div class="w3-padding-8 w3-right-align w3-text-black">
                                        If you dont remember the credential, <a href="#" id="aForgotPassword" class="w3-text-blue">click here</a>.
                                    </div>
                                    <div class="hide">
                                        <div class="inlineblock loginTitle">
                                            &nbsp;
                                        </div>
                                        <div class="inlineblock loginTextBox">

                                            <asp:Button ID="btnSubmit" runat="server" Visible="false" CssClass="default-button btn-cyan"
                                                Text="LOGIN" Width="170px"></asp:Button>
                                        </div>
                                    </div>
                                    <div style="color: #fff">
                                    </div>
                                </div>
                            </div>
                            <div class="midDiv hide" id="forgotPasswordSection">
                                <div class="title">
                                    FORGOT PASSWORD
                                </div>
                                <div class="loginTable w3-center">
                                    <div class="loginTextBoxZone">
                                        <div class="smalltext">
                                            Enter your email address registered in the system
                                        </div>
                                        <div class="smallgap">
                                        </div>
                                        <div class="inlineblock loginTextBox">
                                            <asp:TextBox ID="txtUserIDForgot" runat="server" Width="250px" MaxLength="200"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="smallgap">
                                    </div>
                                    <div class="actionZone">
                                        <div class="inlineblock loginTitle">
                                            &nbsp;
                                        </div>
                                        <div class="inlineblock loginTextBox">
                                            <asp:Button ID="btnForgotPassword" Visible="false" runat="server" CssClass="default-button btn-green"
                                                Width="170px" Text="Email Credential" />&nbsp;<asp:Button ID="btnBacktoLogin" runat="server"
                                                    CssClass="default-button btn-red" Text="Back to Login" Visible="false" />
                                            <button id="btnEmail" class="default-button btn-black">
                                                <i class="fa fa-envelope-o"></i>&nbsp;Email Credential</button>
                                            <button id="btnBack" class="default-button btn-black backToLogin">
                                                <i class="fa fa-reply"></i>&nbsp;Back to Login</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="midDiv hide w3-center" id="errorLoginTable">
                                <div class="title">
                                    ALERT !!!
                                </div>
                                <div class="w3-padding-hor-32 w3-padding-ver-64 w3-center">
                                    <div class="w3-padding w3-red errorMessage">
                                        Invalid User Id or Password !!!
                                    </div>
                                    <div class="w3-padding-32">
                                        <button id="btnBackToLogin" class="default-button btn-red backToLogin">
                                            <i class="fa fa-reply"></i>&nbsp;Back to Login</button>
                                    </div>
                                </div>
                            </div>
                            <div class="midDiv hide w3-center" id="errorFPTable">
                                <div class="title">
                                    ALERT !!!
                                </div>
                                <div class="w3-padding-hor-32 w3-padding-ver-64 w3-center">
                                    <div class="w3-padding w3-red errorMessage">
                                        Invalid User Id or Password !!!
                                    </div>
                                    <div class="w3-padding-32">
                                        <button id="btnBackToFPLogin" class="default-button btn-red backToForgotPassword">
                                            <i class="fa fa-reply"></i>&nbsp;Go Back</button>
                                    </div>
                                </div>
                            </div>
                            <div class="midDiv hide w3-center" id="emailSent">
                                <div class="title">
                                    EMAIL SENT !!!
                                </div>
                                <div class="w3-padding-hor-32 w3-padding-ver-64 w3-center">
                                    <div class="w3-padding w3-green errorMessage">
                                        Password sent to mentioned email ...
                                    </div>
                                    <div class="w3-padding-32">
                                        <button id="btnBackFP" class="default-button btn-red backToForgotPassword">
                                            <i class="fa fa-reply"></i>&nbsp;Go Back</button>
                                    </div>
                                </div>
                            </div>
                            <div class="midDiv hide w3-center" id="progressSection">
                                <div class="title">
                                    PLEASE WAIT ...
                                </div>
                                <div class="w3-padding-hor-128">
                                    <div class="w3-padding-ver-64 w3-center">
                                        <div class="w3-padding-4 bold w3-text-yellow">
                                            Checking your credential !!!
                                        </div>
                                    </div>
                                    <div class=" w3-padding-ver-48">
                                        <img src='images/prg.gif' alt="login" align="middle" />
                                    </div>
                                </div>
                            </div> 
                            <div id="footer_login">
                                Copyright (c)
                            <%=DateTime.Now.Year()%>, Bu Haleeba Holding Group.
                            </div>
                        </div>
                        <asp:HiddenField ID="hdnLoginStatus" runat="server" />
                        <asp:HiddenField ID="hdnLoginURL" runat="server" />
                        <%If False Then%>
                        <script language="javascript" type="text/javascript" src="include/jquery-vsdoc.js"></script>
                        <%End If%>
                        <script language="javascript" type="text/javascript">


                            var page_level_message = '', warning_off = false;
                            $('.capswarning').hide();
                            document.getElementById('<%=txtPassword.CLientID %>').addEventListener('keyup', function (event) {
                                if (event.getModifierState("CapsLock")) {
                                    $('.capswarning').show();
                                } else {
                                    $('.capswarning').hide();
                                }
                            });

                            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
                                init_page_events_and_controls(); 
                            });


                            function init_page_events_and_controls() {
                                var jcontrols = ['#<%=txtUserId.CLientID %>',
                                                '#<%=txtPassword.ClientID %>',
                                                        '#btnSignIn',
                                                '#<%=txtUserIDForgot.ClientID %>',
                                                        '#btnEmail'
                                                ]

                                $(jcontrols[0]).focus();
                                 
                                $(jcontrols[2]).bind('click', function () {
                                    var errorMessage = ''; 
                                    if ($.trim($(jcontrols[0]).val()) == '') {
                                        errorMessage = 'Enter User Id';
                                    }
                                    if ($.trim($(jcontrols[1]).val()) == '') {
                                        errorMessage += ((errorMessage.length > 0) ? ('\r\n') : ('')) + 'Enter Password';
                                    }
                                    if (errorMessage.length > 0) { alert('Kindly fulfill the following requirement:\r\n' + errorMessage); return false; }

                                    var parameter = {};
                                    parameter.userName = $.trim($(jcontrols[0]).val());
                                    parameter.password = $.trim($(jcontrols[1]).val());  

                                    $('#loginSection').hide();
                                    $('#progressSection').show();

                                    $.ajax({
                                        type: "POST",
                                        url: "webService/Authentication.asmx/VerifyLogin",
                                        data: JSON.stringify(parameter),
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        async: false,
                                        success: function (response) {
                                            var result = response.d.split('#');
                                            if (result[0] == 'SUCCESS') {
                                                redirectToRTN('Home.aspx');
                                            }
                                            else {
                                                $('.errorMessage').html(result[1]);
                                                $('#progressSection').hide();
                                                $('#errorLoginTable').show();
                                            }
                                            return false;
                                        },
                                        failure: function (response) {
                                            $('.errorMessage').html(response.d);
                                            $('#progressSection').hide();
                                            $('#errorLoginTable').show();
                                        }
                                    });
                                    return false;
                                });

                                $(jcontrols[4]).on('click', function () {
                                    Validation.init();
                                  <%--  Validation.addRequired('<%=txtUserIDForgot.ClientId%>', 'Enter email address');
                                    Validation.addTypeValidation('<%=txtUserIDForgot.ClientId%>', CompareType.Email, 'Enter email address');--%>
                                    if (!Validation.execute()) return false;

                                    var parameter = {};
                                    parameter.emailId = $.trim($(jcontrols[3]).val());

                                    $('#forgotPasswordSection').hide();
                                    $('#progressSection').show();

                                    var isCancelled = false;

                                    setTimeout(cancelEmailSending, 3000);

                                    $.ajax({
                                        type: "POST",
                                        url: "webService/Authentication.asmx/EmailPassword",
                                        data: JSON.stringify(parameter),
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        async: false,
                                        success: function (response) {
                                            var result = response.d.split('#');
                                            if (!isCancelled) {
                                                isCancelled = true;
                                                if (result[0] == 'SUCCESS') {
                                                    $('#progressSection').hide();
                                                    $('#emailSent').show();
                                                }
                                                else {
                                                    $('.errorMessage').html(result[1]);
                                                    $('#progressSection').hide();
                                                    $('#errorFPTable').show();
                                                }
                                            }
                                            return false;
                                        },
                                        failure: function (response) {
                                            $('.errorMessage').html(response.d);
                                            $('#progressSection').hide();
                                            $('#errorFPTable').show();
                                        }
                                    });

                                    return false;
                                });

                                if (page_level_message.length > 0) {
                                    eval(page_level_message);
                                    page_level_message = '';

                                }

                                $('#aForgotPassword').bind('click', function () {
                                    $('#forgotPasswordSection').show();
                                    $('#loginSection').hide();
                                    $(jcontrols[3]).val('');
                                    return false;
                                });

                                $('.backToLogin').bind('click', function () {
                                    $('#forgotPasswordSection').hide();
                                    $('#errorLoginTable').hide();
                                    $('#loginSection').show();
                                    $(jcontrols[0]).val(''); $(jcontrols[1]).val('');
                                    return false;
                                });

                                $('.backToForgotPassword').bind('click', function () {
                                    $('#forgotPasswordSection').show();
                                    $('#emailSent').hide();
                                    $('#errorFPTable').hide();
                                    $(jcontrols[3]).val('');
                                    return false;
                                });

                                $('select[multiple!="multiple"]').not('.no_search,.hide').select2({
                                    theme: "classic",
                                    width: 'resolve'
                                });

                                $('select[multiple!="multiple"]').filter('.no_search').not('.hide').select2({
                                    theme: "classic",
                                    width: 'resolve',
                                    minimumResultsForSearch: -1
                                });

                                function cancelEmailSending() {
                                    if (!isCancelled) {
                                        $('.errorMessage').html('The email server is not available right now. Kindly try later.');
                                        $('#progressSection').hide();
                                        $('#errorFPTable').show();
                                        isCancelled = true;
                                    }
                                }
                            }

                            var postBackControl;
                            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(function (sender, args) {
                                postBackControl = args.get_postBackElement().name;
                            });

                            function openWindow(url) {
                                window.location.href = url;
                                //window.open(url, 'winLogin', 'scrollbars=yes, titlebar=no, resizable=yes, menubar=no, toolbar=no, status=no, left=0, top=0, height=' + screen.height + ', width=' + screen.width);
                            }

                        </script>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </form>
    </div>
</body>
</html>
