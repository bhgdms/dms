﻿
Imports DMS.DataContext.DataAccessHelper
Imports DMS.Models.DTO

Public Class ucRemarks
    Inherits BaseUserControl

    Dim caller As Documents

    Public Property DocId As Integer
        Get
            Return ViewState("DOCUMENT_ID")
        End Get
        Set(value As Integer)
            ViewState("DOCUMENT_ID") = value
        End Set
    End Property

    Public Property OldRowCount As Integer
        Get
            Return ViewState("ROW_COUNT")
        End Get
        Set(value As Integer)
            ViewState("ROW_COUNT") = value
        End Set
    End Property

    Public Property REMARKS_TABLE() As List(Of RemarksDTO)
        Get
            If ViewState("REMARKS_TABLE") Is Nothing Then
                Dim remarkObj As List(Of RemarksDTO) = New List(Of RemarksDTO)
                ViewState("REMARKS_TABLE") = remarkObj
            End If
            Return ViewState("REMARKS_TABLE")
        End Get
        Set(value As List(Of RemarksDTO))
            ViewState("REMARKS_TABLE") = value
        End Set
    End Property

    Public Property SaveToDatabase As Boolean
        Get
            Return If(ViewState("SAVE_TO_DATABASE") Is Nothing, False, CBool(ViewState("SAVE_TO_DATABASE")))
        End Get
        Set(value As Boolean)
            ViewState("SAVE_TO_DATABASE") = value
        End Set
    End Property
    Public Property IsReadOnly As Boolean
        Get
            Return If(ViewState("IS_READONLY") Is Nothing, False, CBool(ViewState("IS_READONLY")))
        End Get
        Set(value As Boolean)
            ViewState("IS_READONLY") = value
        End Set
    End Property
#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            caller = New Documents
            If Not IsPostBack Then
                OldRowCount = REMARKS_TABLE.Count()
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region
    Public Function SubmitToDatabase() As Boolean
        Try
            caller = New Documents
            Dim remarks As String = String.Empty

            If REMARKS_TABLE.Count > 0 Then
                For i = 0 To REMARKS_TABLE.Count - 1
                    REMARKS_TABLE(i).DocumentId = DocId
                Next
                caller.CreateRemarks(REMARKS_TABLE)
            End If
            Return True
        Catch ex As Exception

        End Try
    End Function
    Public Sub LoadRemarks()
        caller = New Documents
        txtRemarks.Text = String.Empty
        REMARKS_TABLE = caller.GetDocumentRemarks(DocId)
        BindRemarks()
    End Sub
    Private Sub BindRemarks()
        gvRemarks.DataSource = REMARKS_TABLE
        gvRemarks.DataBind()

        hdnCount.Value = REMARKS_TABLE.Count
    End Sub
    Public Sub AddRemarks()
        Try
            If SaveToDatabase Then
                Dim remarkLst As List(Of RemarksDTO) = New List(Of RemarksDTO)
                Dim remarkObj As RemarksDTO = New RemarksDTO()

                remarkObj.Id = 0
                remarkObj.DocumentId = DocId
                remarkObj.Remarks = txtRemarks.Text.Trim()
                remarkObj.RemarksOn = DateTime.Now
                remarkObj.DoneBy = UserProfile.UserID

                remarkLst.Add(remarkObj)

                caller.CreateRemarks(remarkLst)
                LoadRemarks()
            Else
                Dim remarkObj As RemarksDTO = New RemarksDTO()
                remarkObj.Id = 0
                remarkObj.DocumentId = DocId
                remarkObj.Remarks = txtRemarks.Text.Trim()
                remarkObj.RemarksOn = DateTime.Now
                remarkObj.DoneBy = UserProfile.UserID

                REMARKS_TABLE.Add(remarkObj)
                BindRemarks()
            End If
            txtRemarks.Text = String.Empty
        Catch
            Throw
        End Try
    End Sub
    Private Sub lnkAddRemarks_Click(sender As Object, e As System.EventArgs) Handles lnkAddRemarks.Click
        Try
            If Not String.IsNullOrEmpty(txtRemarks.Text.Trim()) Then
                AddRemarks()
            Else
                ShowMessage("Please add some comment in the field, before add remarks", "Error.!!")
            End If

        Catch ex As Exception

        End Try
    End Sub

End Class