﻿Imports DMS.DataContext
'Imports SevenHeavenFramework.Web.Tools
Imports System.Web.UI
Imports DevExpress.Web

Public Class SiteMaster
    Inherits MasterPage

#Region " private properties "
    Private ReadOnly Property MenuList As String
        Get
            Session("MENU_HTML") = String.Format("<ul id=""app_menu""><li><a href=""{0}""><img src=""{1}"" style=""padding-top:3px""></a></li>{2}</ul>", SiteMap.RootNode.Url, GetPageVirtualPath(SiteMap.RootNode("Image")), GetSiteMapSubMenu(SiteMap.RootNode.ChildNodes))
            Return Session("MENU_HTML").ToString
        End Get
    End Property

    Protected ReadOnly Property GetRootPath As String
        Get
            Dim applicationPath As String = Request.ApplicationPath
            Dim str As StringBuilder = New StringBuilder(applicationPath)
            If Not applicationPath.EndsWith("/") Then
                str.Append("/")
            End If
            Return str.ToString()
        End Get
    End Property
#End Region

#Region " Protected Properties .... "
    Protected Function GetPageVirtualPath(ByVal pageName As String) As String
        Return GetRootPath + pageName
    End Function
    Protected ReadOnly Property LoggedInTime As String
        Get
            If ViewState("LOGGED_IN_TIME") Is Nothing Then ViewState("LOGGED_IN_TIME") = DateTime.Now.ToString("hh:mm ttt")
            Return ViewState("LOGGED_IN_TIME").ToString
        End Get
    End Property
    Protected Property LastLoginTime As String
        Get
            Return Session("LAST_LOGIN_TIME")
        End Get
        Private Set(value As String)
            Session("LAST_LOGIN_TIME") = value
        End Set
    End Property
    Protected ReadOnly Property DisplayName As String
        Get
            Return UserProfile.UserName
        End Get
    End Property
    Protected ReadOnly Property UserRole As String
        Get
            Return UserProfile.RoleDescription
        End Get
    End Property
    Protected ReadOnly Property UserID As String
        Get
            Return UserProfile.UserID
        End Get
    End Property
#End Region

#Region " Page Load "

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If Not UserProfile.IsSessionExpired Then
        '    Session("UserName") = UserProfile.UserName
        'Else
        '    Dim page As String = Request.Url.PathAndQuery.Replace("&", ",,,")
        '    Response.Redirect(GetRootPath + "Login.aspx?RTN=" + page, False)
        '    Exit Sub
        'End If

        'If Not String.IsNullOrEmpty(UserProfile.UserName) AndAlso Session("UserName") IsNot Nothing Then
        '    UserProfile.IsSessionExpired = False

        '    FormsAuthentication.Initialize()
        '    Dim expires As DateTime = DateTime.Now.AddDays(10)
        '    Dim ticket As FormsAuthenticationTicket = New FormsAuthenticationTicket(1, UserProfile.UserName, DateTime.Now, expires, False, String.Empty, FormsAuthentication.FormsCookiePath)
        '    Dim encryptedTicket As String = FormsAuthentication.Encrypt(ticket)
        '    Dim authCookie As HttpCookie = New HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)
        '    authCookie.Expires = expires
        '    Session("UserName") = UserProfile.UserName
        'Else
        '    UserProfile.IsSessionExpired = True
        'End If

    End Sub

    ''' <summary>
    ''' Loads the Page
    ''' </summary>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            hdnMessage.Value = String.Empty
            ' Validate Login
            If UserProfile.IsSessionExpired Then
                Dim page As String = Request.Url.PathAndQuery.Replace("&", ",,,")
                Response.Redirect(GetRootPath + "Login.aspx?RTN=" + page, False)
                Exit Sub
            End If
            'If _loadingFailed Then Exit Sub

            'If Not IsPostBack Then

            '    MenuPlaceHolder.Text = MenuList

            'End If

            ' If an error occured while performing above steps
            ' handle the error.
        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region " Functions "
    Private Function GetSiteMapSubMenu(node As SiteMapNodeCollection) As String
        Try
            If node.Count = 0 Then Return String.Empty
            Dim siteMapString As New StringBuilder()
            Dim visibleNode As List(Of SiteMapNode) = node.Cast(Of SiteMapNode).Where(Function(n) n("visible") Is Nothing OrElse n("visible").ToString().ToUpper() = "TRUE").ToList
            For Each childNode As SiteMapNode In visibleNode
                Dim imageURL$ = String.Empty, added As Boolean = False
                If Not childNode.ParentNode.Equals(SiteMap.RootNode) Then
                    imageURL = String.Format("<img src=""{0}"">", GetPageVirtualPath(If(childNode("image") Is Nothing, "Images/spacer.gif", childNode("image"))))
                End If
                If childNode.HasChildNodes Then
                    If (childNode("id") Is Nothing) Then
                        Dim menuString$ = GetSiteMapSubMenu(childNode.ChildNodes)
                        If menuString.Length > 0 And visibleNode.Count > 1 Then
                            menuString = "<ul>" + menuString + "</ul>"
                        End If
                        If Not String.IsNullOrEmpty(menuString) Then
                            If visibleNode.Count > 0 Then
                                Dim url$ = If(childNode.Url Is Nothing OrElse childNode.Url = String.Empty, "#", childNode.Url)
                                Dim title$ = childNode.Title
                                If childNode("bold") IsNot Nothing Then title = String.Format("<{0}>{1}</{0}>", "B", title)
                                If childNode("underline") IsNot Nothing Then title = String.Format("<{0}>{1}</{0}>", "U", title)
                                If childNode("italic") IsNot Nothing Then title = String.Format("<{0}>{1}</{0}>", "I", title)
                                If childNode("color") IsNot Nothing Then title = String.Format("<font color='{0}'>{1}</font>", childNode("color"), title)
                                siteMapString.AppendFormat("<li><a href=""{0}"">{1}{2}</a>{3}</li>", url, imageURL, title, menuString)
                            Else
                                siteMapString.Append(menuString)
                            End If
                            added = True

                        End If

                    End If
                ElseIf childNode("iscategory") Is Nothing AndAlso (childNode("id") Is Nothing) Then
                    Dim title$ = childNode.Title
                    If childNode("bold") IsNot Nothing Then title = String.Format("<{0}>{1}</{0}>", "B", title)
                    If childNode("underline") IsNot Nothing Then title = String.Format("<{0}>{1}</{0}>", "U", title)
                    If childNode("italic") IsNot Nothing Then title = String.Format("<{0}>{1}</{0}>", "I", title)
                    If childNode("color") IsNot Nothing Then title = String.Format("<font color='{0}'>{1}</font>", childNode("color"), title)
                    siteMapString.AppendFormat("<li><a href=""{0}"">{1}{2}</a></li>", childNode.Url, imageURL, title)
                    added = True
                ElseIf Not childNode("iscategory") Is Nothing Then
                    siteMapString.AppendFormat("<li class=""category""><a>{0}</a></li>", childNode.Title)
                End If
                If added AndAlso (childNode("hasSeperator") IsNot Nothing AndAlso childNode("hasSeperator").ToUpper() = "TRUE") Then
                    siteMapString.AppendFormat("<li class=""menusep""></li>")
                End If

            Next

            Return siteMapString.ToString()
        Catch
            Throw
        End Try
    End Function

    Private Function IsChildVisible(item As MenuItem) As Boolean
        Dim isVisible As Boolean = item.Visible
        If isVisible AndAlso item.HasChildren Then
            isVisible = False
            For Each lineitem As MenuItem In item.Items
                If IsChildVisible(lineitem) Then
                    isVisible = True
                End If
            Next
            item.Visible = isVisible
        End If
        Return isVisible
    End Function
#End Region

#Region " Events "
    ''' <summary>
    ''' Logout from the site
    ''' </summary>
    Protected Sub lnkLogout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ancLogout.ServerClick
        Try
            Session.Abandon()
            Session.Clear()
            Response.Cookies.Clear()

            'UserProfile.UserName = String.Empty
            FormsAuthentication.SignOut()

            Response.Redirect("~/Login.aspx", False)
        Catch ex As Exception
            ' do nothing
        End Try
    End Sub

    'Private Sub btnChangePassword_Click(sender As Object, e As System.EventArgs) Handles btnChangePassword.Click
    '    Try
    '        If txtOldPassword.Text.Trim <> UserProfile.Password Then
    '            ShowMessage("Incorrect Old Password")
    '            Exit Sub
    '        End If

    '        Dim userDal As New SystemAdmin
    '        userDal.ChangePassword(UserProfile.UserID, SevenHeavenFramework.Security.Encryptor.Encrypt(txtNewPassword.Text.Trim))
    '        ShowMessageAndRedirect("Password Changed !!!", GetRootPath + "Login.aspx")

    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub mainmenu_ItemDataBound(source As Object, e As MenuItemEventArgs) Handles mainmenu.ItemDataBound
        Try
            Dim node As SiteMapNode = DirectCast(e.Item.DataItem, SiteMapNode)
            If node IsNot Nothing Then
                If node("Visible") IsNot Nothing Then
                    e.Item.Visible = node("Visible").ToString().ToUpper = "TRUE"
                End If

                If e.Item.Visible AndAlso node("id") IsNot Nothing AndAlso Not UserProfile.HasPrivilege(node("id")) Then
                    e.Item.Visible = False
                End If

                If e.Item.Visible Then
                    If node("image") IsNot Nothing Then
                        e.Item.Image.Url = "/" + node("Image")
                    End If
                    If node("hasSeperator") IsNot Nothing AndAlso node("hasSeperator").ToString().ToUpper() = "TRUE" Then
                        e.Item.BeginGroup = True
                    End If
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub mainmenu_DataBound(sender As Object, e As EventArgs) Handles mainmenu.DataBound
        Try
            For i As Integer = 0 To mainmenu.Items.Count - 1
                With mainmenu
                    IsChildVisible(.Items(i))
                End With
            Next
        Catch ex As Exception

        End Try
    End Sub
#End Region

End Class