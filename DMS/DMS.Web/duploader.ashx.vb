﻿Imports System.Web
Imports System.Web.Services
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Net

Public Class duploader
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Try

            If context.Request.Files.Count > 0 Then

                'Fetch the Uploaded File.
                Dim postedFile As HttpPostedFile = context.Request.Files(0)

                'Set the Folder Path.
                Dim folderPath As String = context.Server.MapPath("~/data/")

                'Set the File Name.
                Dim fileName As String = Path.GetFileName(postedFile.FileName)

                'Save the File in Folder.
                postedFile.SaveAs(folderPath + fileName)

                'Send File details in a JSON Response.
                Dim json As String = New JavaScriptSerializer().Serialize(New With {
                    .name = fileName
                })
                context.Response.StatusCode = CInt(HttpStatusCode.OK)
                context.Response.ContentType = "text/json"
                context.Response.Write(json)
                context.Response.End()
            End If
        Catch
            'context.Response.Write("FAILED#")
        End Try

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class