/// <reference path="jquery-vsdoc.js" />
var lastSelectedClass;
var contentPlaceHolderId, jhead, juhead;
var queryObj = {};
var elementToDisable;
var doCloseWindow, doClosePopup;
var lastValue = {};
var dialogResult = { result: true, data: '' };
var progress_text = null;

function getRootUrl() {
    //return window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
    return rootProject;
}

jQuery.extend({
    isFieldBlank: function (strFieldValue) {
        return ($.trim(strFieldValue).length == 0);
    },
    isValidInteger: function (strInput) {
        strInput = $.trim(strInput);
        if (strInput.length == 0)
            return false;
        var MinIncrementPattern = /^(\d{0,8})$/;
        var matchArray = strInput.match(MinIncrementPattern);
        return (matchArray != null);
    },
    isValidNumeric: function (strInput) {
        strInput = $.trim(strInput);
        if (strInput.length == 0)
            return false;
        var MinIncrementPattern = /^(\d{0,15})(\.{0,1})(\d{0,6})$/;
        var matchArray = strInput.match(MinIncrementPattern);
        return (matchArray != null);
    },
    isValidEmail: function (strInput) {
        strInput = $.trim(strInput);
        if (strInput.length == 0)
            return false;
        return strInput.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/);
    },
    rnd: function () {
        today = new Date();
        jran = today.getTime();

        ia = 9301;
        ic = 49297;
        im = 233280;
        jran = (jran * ia + ic) % im;

        return jran / (im * 1.0);
    },
    rand: function (number) {
        return Math.ceil($.rnd() * number);
    },
    showDialog: function (url, width, height, id, title) {

        var myRand = $.rand(50000);
        if (url.indexOf("?") > -1)
            url += "&";
        else
            url += "?";
        url += "rand=" + myRand;

        if ($("#" + id).length > 0)
            $("#" + id).remove();

        {
            var titleDiv = $('<div/>')
                .addClass('title')
                    .append($('<div />').css('float', 'left').html(title))
                    .append($('<div />').css('float', 'right')
                                        .append($('<a />').attr({ 'href': '#', 'id': 'aClose' })
                                                        .append($('<img />').attr({ 'src': getRootUrl() + 'images/b_close.jpg', 'border': '0' })).bind('click', function () { $.hideDialog(id); })));
            var detailDiv = $('<div/>')
                .addClass('detail')
                    .append($('<div />').addClass('detailMessage').css({ 'height': height }, { 'overflow': 'auto' })
                                        .append($('<iframe />').css({ 'margin': '0px 0px 0px 0px', 'padding': '0px 0px 0px 0px' }).attr({ 'src': url, 'width': width - 10, 'height': height - 10, 'allowtransparency': true, 'frameborder': '0', 'scrolling': 'auto' })));
            $('body').append($('<div />').attr('id', id).addClass('popup_block').append($('<div />').addClass('redpanel').append(titleDiv).append(detailDiv)));
        }

        $("#" + id).addClass("ui-widget-content").draggable().css({ 'width': Number(width) });

        var dh = ($('#divMaster').height() > screen.availHeight) ? (screen.availHeight) : ($('#divMaster').height());

        var popMargTop = $(window).scrollTop() + (dh / 2) - (($('#' + id).height()) / 2);
        var popMargLeft = (screen.availWidth / 2) - (($('#' + id).width()) / 2);
        $('#' + id).css({
            'top': popMargTop,
            'left': popMargLeft
        }).fadeIn();

        if ($("#fade").length == 0) {
            $('body').append($('<div />').attr('id', 'fade').css({
                'top': 0,
                'left': 0,
                'width': screen.availWidth,
                'height': dh,
                'background-color': '#aaa',
                'position': 'fixed',
                'opacity': 0.8
            }));
            $('#fade').fadeIn(0);
        }

        return false;
    },
    hideDialog: function (id) {
        $('#fade').remove();
        $('#' + id).remove();
    },
    hideDialogOfParent: function (id) {
        window.parent.$('#fade').remove();
        window.parent.$('#' + id).remove();
        return false;
    },
    //    confirmMessage: function (message, callbackObject, title, okButtonText, cancelButtonText) {

    //        okButtonText = okButtonText || 'OK';
    //        cancelButtonText = cancelButtonText || 'Cancel';
    //        title = title || 'Message From System';
    //        return $.showMessage(message, title, okButtonText, true, cancelButtonText, callbackObject);
    //    },
    //    showMessage: function (message, title, okButtonText, isConfirm, cancelButtonText, callbackObject) {
    //        okButtonText = okButtonText || 'OK';
    //        cancelButtonText = cancelButtonText || 'Cancel';
    //        isConfirm = isConfirm || false;
    //        title = title || 'Message From System';
    //        var id = 'messageBox_DIV';
    //        if ($("#" + id).length == 0) {
    //            var titleDiv = $('<div/>')
    //                .addClass('title')
    //                    .append($('<div />').css('float', 'left').html(title + '&nbsp;&nbsp;&nbsp;'))
    //                    .append($('<div />').css('float', 'right')
    //                                        .append($('<a />').attr({ 'href': '#', 'id': 'aClose' })
    //                                                        .append($('<img />').attr({ 'src': getRootUrl() + 'images/b_close.jpg', 'border': '0' })).on('click', function () { $.hideMessage(id); })));
    //            $btn1 = $('<input />').prop({ 'value': okButtonText, 'width': '75px', 'type': 'button' })
    //                                .addClass('w3-btn w3-round-xxlarge w3-tiny w3-green w3-padding-right')
    //                                .on('click', function () { if ($(callbackObject).prop('href')) window.location = $(callbackObject).prop('href'); else $(callbackObject).click(); $.hideMessage(id); });
    //            $btn2 = $('<input />').prop({ 'value': (isConfirm) ? (cancelButtonText) : (okButtonText), 'width': '75px', 'type': 'button' })
    //                                .addClass('w3-btn w3-round-xxlarge w3-tiny w3-red')
    //                                .on('click', function () { $.hideMessage(id); });
    //            $btndiv = $('<div />').attr('align', 'center').append('<br />');
    //            if (isConfirm)
    //                $btndiv.append($btn1).append("&nbsp;&nbsp;");
    //            $btndiv.append($btn2);
    //            var detailDiv = $('<div/>')
    //                .addClass('detail').css('background-color', '#ffffff')
    //                    .append($('<div />').addClass('detailMessage').html(message))
    //                    .append($btndiv);
    //            $('body').append($('<div />').attr('id', id).css({ 'min-width': '400px' }, { 'max-width': '800px' }).addClass('popup_block').append($('<div />').addClass('redpanel').append(titleDiv).append(detailDiv)));
    //        }

    //        $('#' + id).addClass("ui-widget-content").draggable().show();
    //        var dh = ($('#divMaster').height() > screen.availHeight) ? (screen.availHeight) : ($('#divMaster').height());
    //        var dw = ($('#divMaster').width() > screen.availWidth) ? (screen.availWidth) : ($('#divMaster').width());
    //        var popMargTop = $('body').scrollTop() + (dh / 2) - (($('#' + id).height()) / 2);
    //        var popMargLeft = $('body').scrollLeft() + (dw / 2) - (($('#' + id).width()) / 2);
    //        $('#' + id).css({
    //            'top': popMargTop,
    //            'left': popMargLeft
    //        });

    //        if ($("#fade_message").length == 0) {
    //            $('body').append($('<div />').attr('id', 'fade_message').css({
    //                'top': 0,
    //                'left': 0,
    //                'width': screen.availWidth,
    //                'height': screen.availHeight,
    //                'background-color': '#aaa',
    //                'position': 'fixed',
    //                'opacity': 0.8
    //            }));
    //            $('#fade_message').fadeIn(0);
    //        }

    //        return false;
    //    },
    confirmMessage: function (message, callbackObject, title, okButtonText, cancelButtonText) {
        //var id = 'messageBox_DIV';

        //okButtonText = okButtonText || 'OK';
        //cancelButtonText = cancelButtonText || 'Cancel';
        //title = title || 'Message From System';

        //$btn1 = $('<input />').prop({ 'value': okButtonText, 'width': '75px', 'type': 'button' })
        //                        .addClass('w3-btn w3-round-xxlarge w3-tiny w3-green w3-padding-right')
        //                        .on('click', function () { $(callbackObject).data('DIALOGRESULT', 'SUCCESS'); if ($(callbackObject).prop('href')) window.location = $(callbackObject).prop('href'); else $(callbackObject).click(); $.hideMessage(id); });
        //$btn2 = $('<input />').prop({ 'value': cancelButtonText, 'width': '75px', 'type': 'button' })
        //                        .addClass('w3-btn w3-round-xxlarge w3-tiny w3-red')
        //                        .on('click', function () { $(callbackObject).data('DIALOGRESULT', 'FAILURE'); $.hideMessage(id); });
        //$btndiv = $('<div />').attr('align', 'center').append('<br />');
        //$btndiv.append($btn1).append("&nbsp;&nbsp;");
        //$btndiv.append($btn2);

        //return $.message_window(id, message, title, $btndiv);
        var div = document.createElement("div");
        div.innerHTML = message;
        swal(
            {
                title: "Need your confirmation?",
                content: div,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((confirmed) => {
                if (confirmed) { $(callbackObject).data('DIALOGRESULT', 'SUCCESS'); if ($(callbackObject).prop('href')) window.location = $(callbackObject).prop('href'); else $(callbackObject).click(); }
                else $(callbackObject).data('DIALOGRESULT', 'FAILURE');
            });
        return false;
    },
    showMessageCallBack: function (message, title, callbackObject, okButtonText) {
        var id = 'messageBox_DIV';

        okButtonText = okButtonText || 'OK';
        $btn1 = $('<input />').prop({ 'value': okButtonText, 'width': '75px', 'type': 'button' })
                                .addClass('w3-btn w3-round-xxlarge w3-tiny w3-green w3-padding-right')
                                .on('click', function () { if ($(callbackObject).prop('href')) window.location = $(callbackObject).prop('href'); else $(callbackObject).click(); $.hideMessage(id); });
        $btndiv = $('<div />').attr('align', 'center').append('<br />');
        $btndiv.append($btn1);
        $.message_window(id, message, title, $btndiv);
    },
    showMessage: function (message, title, icon) {
        var div = document.createElement("div");
        icon = icon || 'info';
        div.className = 'w3-medium';
        div.innerHTML = message;
        swal({ title: title, content: div, icon: icon });
        return false;
    },
    showMessageRequirement: function (message, description) {
        var div = document.createElement("div");
        div.style = 'text-align:left;padding-left:12px;'
        div.innerHTML = description;
        swal({ title: message, content: div, icon: 'info' });
        return false;
    },
    showMessageAttention: function (title, message) {
        $.showMessage(title, message, 'info');
        return false;
    },
    showMessageFailure: function (title, message) {
        $.showMessage(title, message, 'warning');
        return false;
    },
    showMessageError: function (title, message) {
        $.showMessage(title, message, 'error');
        return false;
    },
    showMessageSystemError: function () {
        return $.showMessageError('SYSTEM ERROR !!!', 'There seems to be some issue connecting to job data. Our team has been notified. While our team is fixing the issue, you can continue different task.');
    },
    showMessageSuccess: function (title, message) {
        $.showMessage(title, message, 'success');
        return false;
    },
    showMessageAndRedirect: function (message, title, redirectURL, okButtonText) {
        var div = document.createElement("div");
        div.className = 'w3-medium';
        div.innerHTML = message;
        swal({ title: title, content: div, icon: 'info' }).then((value) => { window.location = redirectURL });
        return false;
    },
    showMessageAndRedirectToParent: function (message, redirectURL, title, okButtonText) {
        var div = document.createElement("div");
        div.className = 'w3-medium';
        div.innerHTML = message;
        swal({ title: title, content: div, icon: 'info' }).then((value) => { browsePapasPage(redirectURL) });
        return false;
    },
    message_window: function (window_id, message, title, btnObject) {
        title = title || 'Message From System';
        var id = window_id || 'messageBox_DIV';
        if ($("#" + id).length == 0) {
            var titleDiv = $('<div/>')
                .addClass('title')
                    .append($('<div />').css('float', 'left').html(title + '&nbsp;&nbsp;&nbsp;'))
                    .append($('<div />').css('float', 'right')
                                        .append($('<a />').attr({ 'href': '#', 'id': 'aClose' })
                                                        .append($('<img />').attr({ 'src': getRootUrl() + 'images/b_close.jpg', 'border': '0' })).on('click', function () { $.hideMessage(id); })));
            var detailDiv = $('<div/>')
                .addClass('detail').css('background-color', '#ffffff')
                    .append($('<div />').addClass('detailMessage').html(message))
                    .append(btnObject);
            $('body').append($('<div />').prop('id', id).css({ 'max-width': '600px' }).addClass('popup_block').append($('<div />').addClass('redpanel').append(titleDiv).append(detailDiv)));
        }

        $('#' + id).addClass("ui-widget-content").draggable().show();
        var dh = ($('#divMaster').height() > screen.availHeight) ? (screen.availHeight) : ($('#divMaster').height());
        var dw = ($('#divMaster').width() > screen.availWidth) ? (screen.availWidth) : ($('#divMaster').width());
        var popMargTop = $('body').scrollTop() + (dh / 2) - (($('#' + id).height()) / 2);
        var popMargLeft = $('body').scrollLeft() + (dw / 2) - (($('#' + id).width()) / 2);
        $('#' + id).css({
            'top': popMargTop,
            'left': popMargLeft
        });

        if ($("#fade_message").length == 0) {
            $('body').append($('<div />').attr('id', 'fade_message').css({
                'top': 0,
                'left': 0,
                'width': screen.availWidth,
                'height': screen.availHeight,
                'background-color': '#aaa',
                'position': 'fixed',
                'opacity': 0.8
            }));
            $('#fade_message').fadeIn(0);
        }

        return false;
    },
    hideMessage: function (id) {
        $('#fade_message').remove();
        $('#' + id).remove();
    },
    hideShutdown: function () {
        $('#fade').remove();
        $('#progress_shutdown').remove();
    },
    shutdown: function () {
        progressText = '<div class="w3-text-white w3-xxlarge sessionMessage">Your session will be timed out in</div><div class="w3-padding-4 w3-text-white w3-xxxlarge bold sessionTimer"></div>';
        var id = 'progress_shutdown';
        if ($("#" + id).length == 0) {
            var detailDiv = $('<div/>')
                .addClass('w3-indigo')
                    .append($('<div />').attr('align', 'center').addClass('w3-padding-24').html(progressText))
                    .append($('<div />').attr('align', 'center').addClass('w3-padding-24')
                    .append($('<a />').css('width', '95px').addClass('w3-large w3-padding w3-text-white w3-border w3-round-xlarge timerLogout').attr({ 'href': getRootUrl() + 'login.aspx?RTN=' + window.location.pathname, 'border': '0' }).text('Logout'))
                    .append($('<span />').addClass('w3-padding-ver-4'))
                        .append($('<a href="#" id="refreshSession" />').css('width', '95px').addClass('w3-large w3-padding w3-text-white w3-border w3-round-xlarge w3-padding-ver-16 timerContinue').text('Renew Session').on('click', function () { renewSession(); }))
                    .append($('<span />').addClass('w3-padding-ver-4'))
                        .append($('<a href="#" id="continueSession" />').css('width', '95px').addClass('w3-large w3-padding w3-text-white w3-border w3-round-xlarge w3-padding-ver-16 timerContinue').text('Continue').on('click', function () { continueSession(); })));
            $('body').append($('<div />').attr('id', id).addClass('popup_block').append(detailDiv));
        }

        $('#' + id).show();
        var dh = ($('#divMaster').height() > screen.availHeight) ? (screen.availHeight) : ($('#divMaster').height());
        var dw = ($('#divMaster').width() > screen.availWidth) ? (screen.availWidth) : ($('#divMaster').width());
        var popMargTop = $('body').scrollTop() + (dh / 2) - (($('#' + id).height()) / 2);
        var popMargLeft = $('body').scrollLeft() + (dw / 2) - (($('#' + id).width()) / 2);
        $('#' + id).css({
            'top': popMargTop,
            'left': 0,
            'width': '100%',
            'padding': '0'

        });

        if ($("#fade").length == 0) {
            $('body').append($('<div />').attr('id', 'fade').css({
                'top': 0,
                'left': 0,
                'width': screen.availWidth,
                'height': $('#divMaster').height(),
                'background-color': '#444',
                'position': 'fixed',
                'opacity': 0.8
            }));
            $('#fade').fadeIn(0);
        }

        return false;
    },
    showProgress: function () {
        progress_text = progress_text || 'Your request is under process. Please Wait ...';
        var id = 'progress_DIV';
        if ($("#" + id).length == 0) {
            var detailDiv = $('<div/>')
                .addClass('w3-indigo')
                    .append($('<div id="divProgressText" />').attr('align', 'center').addClass('w3-padding-24 w3-text-white w3-xxlarge').html(progress_text))
                    .append($('<div />').attr('align', 'center').addClass('w3-padding-24').append($('<img />').attr({ 'src': getRootUrl() + 'images/prg.gif', 'border': '0' })));
            $('body').append($('<div />').attr('id', id).addClass('popup_block').append(detailDiv));
        }
        else {
            $('#divProgressText').html(progress_text);
        }

        $('#' + id).css('z-index', '99999').show();
        var dh = ($('#divMaster').height() > screen.availHeight) ? (screen.availHeight) : ($('#divMaster').height());
        var dw = ($('#divMaster').width() > screen.availWidth) ? (screen.availWidth) : ($('#divMaster').width());
        var popMargTop = $('body').scrollTop() + (dh / 2) - (($('#' + id).height()) / 2);
        var popMargLeft = $('body').scrollLeft() + (dw / 2) - (($('#' + id).width()) / 2);
        $('#' + id).css({
            'top': popMargTop,
            'left': 0,
            'width': '100%',
            'padding': '0'

        });

        if ($("#fade_P").length == 0) {
            $('body').append($('<div />').attr('id', 'fade_P').css({
                'top': 0,
                'left': 0,
                'width': screen.availWidth,
                'height': $('#divMaster').height(),
                'background-color': '#444',
                'position': 'fixed',
                'opacity': 0.8,
                'z-index': 99990
            }));
            $('#fade_P').fadeIn(0);
        }

        return false;
    },
    hideProgress: function () {
        $('#fade_P').remove();
        $('#progress_DIV').hide();
        return false;
    },
    ShowPopup: function (divId, width, title, hdnPopupID, isDialog) {
        if ($("#" + divId).length == 0) {
            return false;
        }
        if (title == null || typeof (title) == 'undefined')
            title = 'Property Management';

        if (!(hdnPopupID == null || typeof (hdnPopupID) == 'undefined'))
            $('#' + hdnPopupID).val('ON');

        isDialog = isDialog | false;

        if ($("#" + divId + ' > div.redpanel').length == 0) {

            var titleDiv = $('<div/>')
            .addClass('title')
                .append($('<div />').addClass('dialogTitle').css('float', 'left').html(title + '&nbsp;&nbsp;&nbsp;'));
            if (!isDialog) {
                $(titleDiv).append($('<div />').css('float', 'right')
                                    .append($('<a />').attr({ 'href': '#', 'id': 'aClose' })
                                                    .append($('<img />').attr({ 'src': getRootUrl() + 'images/b_close.jpg', 'border': '0' })).bind('click', function () { return $.HidePopup(divId, hdnPopupID); })));
            }
            $("#" + divId).prepend(($('<div />').addClass('redpanel').append(titleDiv)));
            $("#" + divId).addClass("w3-card w3-white w3-border-black").draggable().css({ 'width': Number(width) });
            var dh = ($('#divMaster').height() > screen.availHeight) ? (screen.availHeight) : ($('#divMaster').height());
            var dw = ($('#divMaster').width() > screen.availWidth) ? (screen.availWidth) : ($('#divMaster').width());
            var popMargTop = $(window).scrollTop() + (dh / 2) - (($('#' + divId).height()) / 2);
            var popMargLeft = (dw / 2) - (($('#' + divId).width()) / 2);
            $("#" + divId).css({
                'top': popMargTop,
                'left': popMargLeft
            });
        }
        $("#" + divId).show();
        $('.dialogTitle').html(title + '&nbsp;&nbsp;&nbsp;');
        if ($("#fade").length == 0) {
            $('body').append($('<div />').attr('id', 'fade').css({
                'top': 0,
                'left': 0,
                'width': screen.availWidth,
                'height': screen.availHeight,
                'background-color': '#aaa',
                'position': 'fixed',
                'opacity': 0.8,
                'z-index': 9
            }));
            $('#fade').fadeIn(0);
        }

        return false;
    },
    HidePopup: function (divId, hdnPopupID) {
        if ($('#' + divId).length > 0) $('#' + divId).hide();
        if ($('#fade').length > 0) $('#fade').remove();
        if (!(hdnPopupID == null || typeof (hdnPopupID) == 'undefined')) {
            $('#' + hdnPopupID).val('OFF');
        }
        return false;
    },
    HidePopupFromParent: function (divId) {
        window.parent.$('#fade').remove();
        window.parent.$('#' + divId).hide();
        return false;
    },
    isBrowserMSIE: function () {
        return (navigator.appName.toUpperCase() == 'MICROSOFT INTERNET EXPLORER');
    }
});

function HideAllBlackBackGround() {
    if ($('#fade').length > 0) $('#fade').remove();
    if ($('#fade_P').length > 0) $('#fade_P').remove();
    if ($('#fade_message').length > 0) $('#fade_message').remove();
}
Number.prototype.padZero = function (length) {
    var s = String(this);

    var ss = s.split('.');

    var s = ss[ss.length - 1];

    while (s.length < (length || 2)) { s = "0" + s; }

    ss[ss.length - 1] = s;

    return ss.join('.');
}

String.prototype.toDate = function ()
{
    var from = this.split("/");
    return new Date(from[2], from[1] - 1, from[0]);
}

Date.prototype.getDifference = function(targetDate)
{
    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = this.getTime();
    var date2_ms = targetDate.getTime()

    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(date1_ms - date2_ms);

    // Convert back to days and return
    return Math.round(difference_ms/ONE_DAY);
}
Date.prototype.getMonthDifference = function (targetDate) {

    var a = this;
    var b = targetDate;

    if (b < a) {
        a = targetDate;
        b = this;
    }

    // Months between years.
    var months = (b.getFullYear() - a.getFullYear()) * 12;

    // Months between... months.
    months += (b.getMonth() - a.getMonth());

    // Subtract one month if b's date is less that a's.
    if (a.getDate() < b.getDate()) {
        months++;
    }

    return months;
}

var CompareType = {
    Integer: "Integer",
    Double: "Double",
    String: "String",
    Email: "Email",
    MultiEmail: "Email",
    Website: "Website",
    AlphaNumeric: "AlphaNumeric",
    FullName: "FullName",
    SingleName: "SingleName",
    PositiveNumber: "PositiveNumber",
    NegativeNumber: "NegativeNumber",
    Number: "Number",
    SystemDate: "SystemDate"
}

var CheckCompareType = {
    AtLeast: 'Atleast',
    AtMost: 'Atmost',
    Exact: 'Exact'
}

var RegularExpression = {
    Email: /^[\S]+(\.[\S]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i,
    MultiEmail: /^(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*;\s*|\s*$))*$/i,
    Website: /^((?:http(?:s)?\:\/\/)?[a-zA-Z0-9_-]+(?:.[a-zA-Z0-9_-]+)*.[a-zA-Z]{2,4}(?:\/[a-zA-Z0-9_]+)*(?:\/[a-zA-Z0-9_]+.[a-zA-Z]{2,4}(?:\?[a-zA-Z0-9_]+\=[a-zA-Z0-9_]+)?)?(?:\&[a-zA-Z0-9_]+\=[a-zA-Z0-9_]+)*)$/,
    AlphaNumeric: /^[a-zA-Z0-9_]+$/,
    FullName: /[\s\S]+/,
    SingleName: /[\S]+/,
    UserId: /[a-zA-Z0-9_]{6,15}/,
    PositiveNumber: /\d+(,\d{3})*(\.\d+(e\d+)?)?/,
    NegativeNumber: /-\d+(,\d{3})*(\.\d+(e\d+)?)?/,
    Number: /^-?\d+(,\d{3})*(\.\d+(e\d+)?)?$/,
    SystemDate: /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/
}

var validationArray = {};

var Validation = {
    init: function () {
        validationArray = {};
    },
    header: '',
    okButtonText: '',
    tabId: '',
    addRequired: function (validationControl, validationMessage, tabToFocus) {
        if (validationArray[validationControl] == null) validationArray[validationControl] = {};
        validationArray[validationControl]['R'] = { validationMessage: validationMessage, tabToFocus: tabToFocus };
    },
    addRequiredFromMultiple: function (validationControlIds, noOfChecks, validationMessage, tabToFocus) {
        if (validationArray[validationControlIds] == null) validationArray[validationControlIds] = {};
        validationArray[validationControlIds]['M'] = { noOfChecks: noOfChecks, validationMessage: validationMessage, tabToFocus: tabToFocus };
    },
    addExpressionForMultipleEmail: function (validationControl, validationMessage, tabToFocus) {
        if (validationArray[validationControl] == null) validationArray[validationControl] = {};
        validationArray[validationControl]['A'] = { validationMessage: validationMessage, tabToFocus: tabToFocus };
    },
    addExpression: function (validationControl, validationMessage, regularExpression, tabToFocus) {
        if (validationArray[validationControl] == null) validationArray[validationControl] = {};
        regularExpression = typeof regularExpression !== 'undefined' ? regularExpression : '';
        validationArray[validationControl]['E'] = { validationMessage: validationMessage, regularExpression: regularExpression, tabToFocus: tabToFocus };
    },
    addCountValidation: function (parentControl, childControlToLook, validationMessage, tabToFocus) {
        if (validationArray[validationControl] == null) validationArray[validationControl] = {};
        validationArray[parentControl]['C'] = { subControlToCount: childControlToLook, validationMessage: validationMessage, tabToFocus: tabToFocus };
    },
    addCheckValidation: function (validationControl, noOfChecks, checkCompareType, validationMessage, tabToFocus) {
        if (validationArray[validationControl] == null) validationArray[validationControl] = {};
        validationArray[validationControl]['S'] = { noOfChecks: noOfChecks, checkCompareType: checkCompareType, validationMessage: validationMessage, tabToFocus: tabToFocus };
    },
    addRangeValidation: function (validationControl, fromRange, toRange, compareType, validationMessage, tabToFocus) {
        if (validationArray[validationControl] == null) validationArray[validationControl] = {};
        validationArray[validationControl]['G'] = { fromRange: fromRange, toRange: toRange, compareType: compareType, validationMessage: validationMessage, tabToFocus: tabToFocus };
    },
    addLengthValidation: function (validationControl, minimumLength, maximumLength, validationMessage, tabToFocus) {
        if (validationArray[validationControl] == null) validationArray[validationControl] = {};
        validationArray[validationControl]['L'] = { minimumLength: minimumLength, maximumLength: maximumLength, validationMessage: validationMessage, tabToFocus: tabToFocus };
    },
    addTypeValidation: function (validationControl, compareType, validationMessage, tabToFocus) {
        if (validationArray[validationControl] == null) validationArray[validationControl] = {};
        validationArray[validationControl]['T'] = { compareType: compareType, validationMessage: validationMessage, tabToFocus: tabToFocus };
    },
    execute: function () {
        var alertMessage = '', tabIndex = -1, firstObjectToFocus = '';
        $.each(validationArray, function (controlToValidate, validationData) {
            $.each(validationData, function (validationType, validationParameter) {
                var obj = $.extend({
                    regularExpression: '',
                    subControlToCount: '',
                    validationMessage: '',
                    fromRange: 0,
                    toRange: 0,
                    compareType: '',
                    checkCompareType: '',
                    tabToFocus: -1,
                    noOfChecks: 0,
                    minimumLength: 0,
                    maximumLength: 0
                }, validationParameter);
                var addMessage = false;
                switch (validationType) {
                    case "R":
                        if ($('#' + controlToValidate).val() == null || $.trim($('#' + controlToValidate).val()) == '') addMessage = true;
                        if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                        break;
                    case "M":
                        var count = 0;
                        $.each(controlToValidate.split('^'), function (index, value) {
                            if ($('#' + value).val() != null && $.trim($('#' + value).val()) != '') count++;
                        });
                        if (count != obj.noOfChecks) addMessage = true;
                        if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                        break;
                    case "A":
                        var val = $.trim($('#' + controlToValidate).val());
                        if (val != null && val != '')
                        {
                            val = val.replace(';', ',');
                            var emails = val.split(',');
                            $(emails).each(function (index, value) {
                                value = $.trim(value);
                                if (value != null && value != '' && !value.match(RegularExpression.Email)) {
                                    addMessage = true;
                                    return false;
                                }
                            })
                        } 
                        if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                        break;
                    case "E":
                        var val = $.trim($('#' + controlToValidate).val());
                        if (val != null && val != '' && !val.match(obj.regularExpression)) addMessage = true;
                        if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                        break;
                    case "C":
                        if ($('#' + controlToValidate + ' > ' + obj.subControlToCount).length == 0) addMessage = true;
                        if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                        break;
                    case "G":
                        var val = $.trim($('#' + controlToValidate).val());
                        if (val != null && val != '') {
                            switch (obj.compareType) {
                                case CompareType.Integer:
                                    if (!(parseInt(val) >= parseInt(obj.fromRange) && parseInt(val) <= parseInt(obj.toRange))) addMessage = true;
                                    if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                                    break;
                                case CompareType.Double:
                                    if (!(parseFloat(val) >= parseFloat(obj.fromRange) && parseFloat(val) <= parseFloat(obj.toRange))) addMessage = true;
                                    if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                                    break;
                                case CompareType.String:
                                    if (!(val.toString() >= obj.fromRange.toString() && val.toString() <= obj.toRange.toString())) addMessage = true;
                                    if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                                    break;
                            }
                        }
                        break;
                    case "L":
                        var val = $.trim($('#' + controlToValidate).val());
                        if (val != null && val != '' && (val.length < obj.minimumLength || val.length > obj.maximumLength)) addMessage = true;
                        if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                        break;
                    case "S":
                        switch (obj.checkCompareType) {
                            case CheckCompareType.AtLeast:
                                if ($('input[id*="' + controlToValidate + '"]:checked').length < obj.noOfChecks) addMessage = true;
                                break;
                            case CheckCompareType.AtMost:
                                if ($('input[id*="' + controlToValidate + '"]:checked').length == 0 ||
                                    $('input[id*="' + controlToValidate + '"]:checked').length > obj.noOfChecks) addMessage = true;
                                break;
                            case CheckCompareType.Exact:
                                if ($('input[id*="' + controlToValidate + '"]:checked').length != obj.noOfChecks) addMessage = true;
                                break;
                        }
                        if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                        break;
                    case "T":
                        var val = $.trim($('#' + controlToValidate).val());
                        if (val != null && val != '') {
                            switch (obj.compareType) {
                                case CompareType.Integer:
                                    if (!$.isValidInteger(val)) addMessage = true;
                                    if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                                    break;
                                case CompareType.Double:
                                    if (!$.isValidNumeric(val)) addMessage = true;
                                    if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                                    break;
                                case CompareType.Email:
                                    if (!$.isValidEmail(val)) addMessage = true;
                                    if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                                    break;
                                default:
                                    var tester = new RegExp(RegularExpression[obj.compareType]);
                                    if (!tester.test(val)) addMessage = true;
                                    if (addMessage && obj.tabToFocus != -1 && tabIndex == -1) { tabIndex = obj.tabToFocus; firstObjectToFocus = controlToValidate }
                                    break;
                            }
                        }
                        break;
                }
                if (addMessage) {
                    alertMessage += "<div class='w3-small w3-padding-4 w3-border-bottom w3-text-red' style='text-align:left;'> <img src='../../../images/news_bullet.gif' align='absmiddle'>&nbsp;" + obj.validationMessage + "</div>";
                    return false;
                }
            });
        });
        if (alertMessage != '') {
            if (this.header == '') this.header = 'Message From System';
            if (this.okButtonText == '') this.okButtonText = 'OK';
            if (this.tabId != '' && tabIndex != -1) $('#' + this.tabId).tabs('select', tabIndex);
            if (firstObjectToFocus != '') $('#' + firstObjectToFocus).focus();
            $.showMessage(alertMessage, this.header, this.okButtonText);
            //alert(alertMessage);
            return false;
        }
        return true;

    }
}

function validate(validationArray, headerText) {
    var alertMessage = '';
    $(validationArray).each(function () {
        var obj = $.extend({
            isRegular: false,
            regularExpression: '',
            controlToValidate: '',
            validateMessage: ''
        }, this);
        if (obj.isRegular) {
            var val = $.trim($('#' + this).val());
            if (!val.match(obj.regularExpression))
                alertMessage += "-- " + obj.validateMessage + "\r\n";
        }
        else
            if ($.trim($('#' + this).val()) == '')
                alertMessage += "-- " + obj.validateMessage + "\r\n";
    });
    if (alertMessage != '') {
        alertMessage = headerText + "\r\n\r\n" + alertMessage;
        alert(alertMessage);
        return false;
    }
    return true;
}

function PostBackParentFromPopup(eventTarget, eventArgument) {
    var parentWin = window.opener;
    if (parentWin == null) parentWin = window.parent;
    if (parentWin.__doPostBack != null) {
        parentWin.__doPostBack(eventTarget, eventArgument);
    }
    else {
        var theform = parentWin.document.Form1;
        theform.__EVENTTARGET.value = eventTarget.split("$").join(":");
        theform.__EVENTARGUMENT.value = eventArgument;
        theform.submit();
    }
}

function ReloadParent() {
    var parentWin = window.opener;
    if (parentWin == null) parentWin = window.parent;
    if (parentWin != null) {
        parentWin.location.reload();
    }
}

function CallParentClickFunction(eventTarget, eventArgument, dResult) {
    var parentWin = window.opener;
    if (parentWin == null) parentWin = window.parent;
    if (parentWin != null) {
        parentWin.dialogResult.result = dResult;
        parentWin.dialogResult.data = eventArgument;
        parentWin.document.getElementById(eventTarget).click();
    }
}

function AJAXPostBackParentFromPopup(eventTarget, eventArgument) {
    var parentWin = window.opener;
    if (parentWin == null) parentWin = window.parent;
    if (parentWin != null) {
        var prm = parentWin.Sys.WebForms.PageRequestManager.getInstance();
        prm._doPostBack(eventTarget, eventArgument);
    }
    else {
        var theform = parentWin.document.Form1;
        theform.__EVENTTARGET.value = eventTarget.split("$").join(":");
        theform.__EVENTARGUMENT.value = eventArgument;
        theform.submit();
    }
}

function TextboxCounter(ctrl, MaxLength) {
    var obj = document.getElementById(ctrl);
    if (obj != null) {
        if (parseInt(obj.value.length) > parseInt(MaxLength)) {
            alert('Maximum allow character ' + MaxLength + '.');
            obj.focus();
            return false;
        }
    }
}

function redirectToRTN(urlIfNotRTN) {
    var querystring = location.search.split('?');
    if (querystring.length > 1) {
        var prefix = querystring[1].substr(0, 5);
        if (prefix.toUpperCase() == 'RTN=/')
            urlIfNotRTN = querystring[1].substr(5);
        if (prefix.toUpperCase() == 'ASPXE')
            urlIfNotRTN = querystring[1].substr(15);
    }
    for (var i = 2; i < querystring.length; i++) {
        urlIfNotRTN += '?' + querystring[i];
    }
    window.location.href = urlIfNotRTN;
}
function loadQueryStringValue() {
    var querystring = location.search.replace('?', '').split('&');
    for (var i = 0; i < querystring.length; i++) {
        var name = querystring[i].split('=')[0];
        var value = querystring[i].split('=')[1];
        queryObj[name] = value;
    }
}

loadQueryStringValue();

function openModalWindow(url, queryString, width, height) {
    if (typeof (queryString) != 'undefined' && queryString != null)
        url += '?' + queryString;

    if (typeof (ModalPopup) != 'undefined') {
        ModalPopup.SetContentUrl(url);
        ModalPopup.SetHeaderText('Property Management Enterprise');
        ModalPopup.Show();
        ModalPopup.SetSize(width, height);
        ModalPopup.UpdatePosition();
    }
}

function addNewDocumentUploader(masterDivId) {
    var length = $('#div[id*="divUpload_"]', '#' + masterDivId).length;
    if (length == null) length = 0; length++;

    var subDivId = 'divUpload_' + length;

    // prepare html for file upload 
    var fileHtml = '<div id="' + subDivId + '"><table class="tableex"><tr><td><input type="file" width="120px"></td><td>Remove</td></tr></table></div>';

    $('#' + masterDivId).append(fileHtml);
}

function enableDisableControl(controlId, isEnabled) {
    if (isEnabled) $('#' + controlId).removeAttr('disabled');
    else $('#' + controlId).attr('disabled', 'true');
}
function enableDisable(control, isEnabled) {
    if (isEnabled) $(control).removeAttr('disabled');
    else $(control).prop('disabled', 'true');

    if ($(control).children().length > 0) {
        $(control).children().each(function () {
            enableDisable(this, isEnabled);
        });
    }
}
function browsePapasPage(url) {
    var parentWin = window.opener;
    if (parentWin == null) parentWin = window.parent;
    if (parentWin != null) {
        parentWin.open(url, '_self');
    }
}
function refreshPapasPage() {
    var parentWin = window.opener;
    if (parentWin == null) parentWin = window.parent;
    if (parentWin != null) {
        parentWin.open(parentWin.location.href, '_self');
    }
}
function setDate() {
    if ($(".datepicker:enabled").length > 0) {
        $(".datepicker:enabled").datepicker({
            showOn: "button",
            buttonImage: getRootUrl() + "images/calendar.png",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy'
        });
    }
    if ($(".datepickerfocus:enabled").length > 0) {
        $(".datepickerfocus:enabled").datepicker({
            showOn: "focus",
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy'
        });
    }
    if ($(".maxDatePicker:enabled").length > 0) {
        $(".maxDatePicker:enabled").datepicker({
            showOn: "button",
            buttonImage: getRootUrl() + "images/calendar.png",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            maxDate: ($(this).data('maxdate') != null) ? ($(this).data('maxdate')) : (0),
            dateFormat: 'dd/mm/yy'
        });
    }
    if ($(".maxDatePickerfocus:enabled").length > 0) {
        $(".maxDatePickerfocus:enabled").datepicker({
            showOn: "focus",
            changeMonth: true,
            changeYear: true,
            maxDate: ($(this).data('maxdate') != null) ? ($(this).data('maxdate')) : (0),
            dateFormat: 'dd/mm/yy'
        });
    }
    if ($(".minDatePicker:enabled").length > 0) {
        
        $(".minDatePicker:enabled").each(function () {
            $(this).datepicker({
                showOn: "button",
                buttonImage: getRootUrl() + "images/calendar.png",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                minDate: ($(this).data('mindate') != null) ? ($(this).data('mindate')) : (0),
                dateFormat: 'dd/mm/yy'
            });
        });
    }
    if ($(".minDatePickerfocus:enabled").length > 0) {
        $(".minDatePickerfocus:enabled").datepicker({
            showOn: "focus",
            changeMonth: true,
            changeYear: true,
            minDate: ($(this).data('mindate') != null) ? ($(this).data('mindate')) : (0),
            dateFormat: 'dd/mm/yy'
        });
    }
    if ($(".rangeDatePickerfocus:enabled").length > 0) {
        $(".rangeDatePickerfocus:enabled").datepicker({
            showOn: "focus",
            changeMonth: true,
            changeYear: true,
            minDate: ($(this).data('mindate') != null) ? ($(this).data('mindate')) : (0),
            maxDate: ($(this).data('maxdate') != null) ? ($(this).data('maxdate')) : (0),
            dateFormat: 'dd/mm/yy'
        });
    }
    if ($('.monthpicker:enabled').length > 0) {
        $('.monthpicker:enabled').monthpicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'MM yy',

            onClose: function () {
                var iMonth = $("#ui-monthpicker-div .ui-monthpicker-month :selected").val();
                var iYear = $("#ui-monthpicker-div .ui-monthpicker-year :selected").val();
                $(this).monthpicker('setDate', new Date(iYear, iMonth, 1));
            },

            beforeShow: function () {
                if ((selDate = $(this).val()).length > 0) {
                    iYear = selDate.substring(selDate.length - 4, selDate.length);
                    iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5),
$(this).datepicker('option', 'monthNames'));
                    $(this).monthpicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
                    $(this).monthpicker('setDate', new Date(iYear, iMonth, 1));
                }
            }
        });
    }
}

function changePriority(lstBoxId, hdnId, dir) {
    var listBox = document.getElementById(lstBoxId);
    var hdn = document.getElementById(hdnId);
    var oldindex, newindex;
    if (listBox.options.length == 0) return false;
    oldindex = listBox.selectedIndex;
    if (dir == '0') {
        newindex = oldindex - 1;
        if (newindex < 0) newindex = 0;
    }
    else {
        newindex = oldindex + 1;
        if (newindex == listBox.options.length) newindex -= 1;
    }

    if (newindex != oldindex) {
        var option = listBox.options[newindex];
        listBox.options[newindex] = new Option(listBox.options[oldindex].text, listBox.options[oldindex].value);
        listBox.options[oldindex] = new Option(option.text, option.value);
        listBox.options[newindex].selected = true;
    }

    hdn.value = '';
    for (var i = 0; i < listBox.options.length; i++) {
        hdn.value += listBox.options[i].value + '^';
    }
}

// usage : <input type="file" name="F1" size="20" onkeypress="return false;">
function onkeyPress(e) {
    try {
        //var key = window.event ? e.keyCode : e.which;
        //if (key == 13)
        //StartClick();
        e.cancelBubble = true;
        e.returnValue = false;
        return false;
    } catch (e) { }
}
function switchList(from) {
    try {
        var ddl1 = document.getElementById((from) ? ('ctl00_MainContent_lstFrom') : ('ctl00_MainContent_lstTo'));
        var ddl2 = document.getElementById((from) ? ('ctl00_MainContent_lstTo') : ('ctl00_MainContent_lstFrom'));
        var hdn = document.getElementById('ctl00_MainContent_hdnIds');

        for (var i = 0; i < ddl1.options.length; i++)
            if (ddl1.options[i].selected) {
                var option = ddl1.options[i];
                ddl2.options[ddl2.options.length] = new Option(option.text, option.value);
            }
        for (var i = 0; i < ddl1.options.length; i++)
            if (ddl1.options[i].selected) {
                ddl1.options[i] = null; i = -1;
            }

        hdn.value = '';
        ddl2 = document.getElementById('ctl00_MainContent_lstTo');
        for (var i = 0; i < ddl2.options.length; i++) {
            hdn.value += ddl2.options[i].value + '^';
        }
    }
    catch (e) {
    }
}
function swapUList(from) {
    var ddl1 = document.getElementById('lstRole');
    var hdn = document.getElementById('hdnIds');
    var Brady = new Array(ddl1.options.length)

    if (from) { if (ddl1.selectedIndex <= 0) return; }
    else
        if (ddl1.selectedIndex < 0) return;

    if (from) {
        if (ddl1.selectedIndex == 0) return;
        var value = ddl1.value;
        var text = ddl1.options[ddl1.selectedIndex].text;
        ddl1.options[ddl1.selectedIndex].text = ddl1.options[ddl1.selectedIndex - 1].text;
        ddl1.options[ddl1.selectedIndex].value = ddl1.options[ddl1.selectedIndex - 1].value;
        ddl1.options[ddl1.selectedIndex - 1].text = text;
        ddl1.options[ddl1.selectedIndex - 1].value = value;
        ddl1.selectedIndex -= 1;
    }
    else {
        if (ddl1.selectedIndex == ddl1.options.length - 1) return;
        var value = ddl1.value;
        var text = ddl1.options[ddl1.selectedIndex].text;
        ddl1.options[ddl1.selectedIndex].text = ddl1.options[ddl1.selectedIndex + 1].text;
        ddl1.options[ddl1.selectedIndex].value = ddl1.options[ddl1.selectedIndex + 1].value;
        ddl1.options[ddl1.selectedIndex + 1].text = text;
        ddl1.options[ddl1.selectedIndex + 1].value = value;
        ddl1.selectedIndex += 1;
    }

    hdn.value = '';
    for (var i = 0; i < ddl1.options.length; i++) {
        hdn.value += ddl1.options[i].value + '^';
    }
}

function openPageWithTitle(pagename, heading) {
    window.parent.document.getElementById("lnkTitle").innerText = heading;
    window.location.href = pagename;
}
function openPage(pagename) {
    window.location.href = pagename;
}
function setPageTitle(heading) {
    window.parent.document.getElementById("lnkTitle").innerText = heading;
}



//Check if the fiel is blank
function isFieldBlank(strFieldValue) {

    //If the field is blank:		return value is 1
    //If the field is not blank:	return value is 0

    strFieldValue = LTrim(RTrim(strFieldValue))
    if (strFieldValue == '') {
        return 1
    }
    else {
        return 0
    }
}


//This function removes the spaces on the RHS
function RTrim(strField) {
    var strTemp = new String();
    var isEmpty = 'Yes';
    var len;
    strTemp = strField;
    len = strTemp.length;

    while (isEmpty == 'Yes') {
        if (strTemp.substring(len - 1, len) == ' ') {
            isEmpty = 'Yes';
        }
        else {
            isEmpty = 'No';
            break;
        }
        strTemp = strTemp.substring(0, len - 1);
        len = strTemp.length;
    }
    return strTemp;
}


//This function removes the spaces on the LHS
function LTrim(strField) {
    var strTemp = new String();
    var isEmpty = 'Yes';
    var len;
    strTemp = strField;
    len = strTemp.length;

    while (isEmpty == 'Yes') {
        if (strTemp.substring(0, 1) == ' ') {
            isEmpty = 'Yes';
        }
        else {
            isEmpty = 'No';
            break;
        }
        strTemp = strTemp.substring(1, len);
        len = strTemp.length;
    }
    return strTemp;
}

function Trim(strField) {
    return RTrim(LTrim(strField));
}

//function to check for an integer
function ValidateInteger(strInput) {
    // Function that checks if the value passed to it is an integer
    if (LTrim(RTrim(strInput)) == '') {
        return 0
    }

    //var MinIncrementPattern = /^(\d{0,8})(\.{0,1})(\d{0,2})$/;
    strInput = LTrim(RTrim(strInput))
    var MinIncrementPattern = /^(\d{0,8})$/;
    var matchArray = strInput.match(MinIncrementPattern);

    if (matchArray != null) {
        return 1
    }
    else {
        return -1
    }
}

//function to validate a numeric number
function ValidateNumeric(strInput) {

    if (LTrim(RTrim(strInput)) == '') {
        return 0
    }
    strInput = LTrim(RTrim(strInput))
    var ValuePattern = /^(\d{0,8})(\.{0,1})(\d{0,2})$/;
    var matchArray = strInput.match(ValuePattern);

    if (matchArray != null) {
        return 1
    }
    else {
        return -1
    }
}

//function to check the status of the parent
function checkStatusOfParent(frmName) {
    var blnFound = false;
    for (i = 0; i <= window.opener.document.forms.length - 1; i++) {
        if (window.opener.document.forms[i].name == frmName) {
            blnFound = true;
            break;
        }
    }
    if (blnFound) {
        return 1
    }
    else {
        alert('You have moved away from the window from which this PopUp has been opened. Any action done with the presently open pop-up may result in an error. The pop-up will now be closed.');
        window.close();
    }
}



//Purpose:  Validates fields for precision	
//			This function can also be used to validate a decimal value			
function validateDecimalPrecision(objElement, intMaxDigitsBeforeDecimal, intMaxDigitsAfterDecimal) {
    //Local variable declaration
    var strDecimalPattern, regExpDecimal, strElementValue;

    //Build the regular expression dynamically based on parameters
    strDecimalPattern = "^\\d{1," + intMaxDigitsBeforeDecimal + "}(.\\d{1," + intMaxDigitsAfterDecimal + "})$";

    //Convert the string to RegExp Object
    regExpDecimal = new RegExp(strDecimalPattern);

    //Input string to validate
    strElementValue = objElement.value;
    strElementValue = LTrim(RTrim(strElementValue));

    //Evaluate the reqular exp
    if (regExpDecimal.test(strElementValue)) {
        return true; //Input string is as per reg expression.
    }
    else {
        return false;
    }
} //end function validateDecimalPrecision



//Purpose:  Returns the count of no. of digits after decimal point
function NoOfDigitsAfterDecimalPoint(strDecimalString) {
    strDecimalString = LTrim(RTrim(strDecimalString));

    var intIndexOfDecimal, strAfterDecimal;
    intDecimalPosition = strDecimalString.indexOf('.');
    strAfterDecimal = strDecimalString.substring(intDecimalPosition + 1, strDecimalString.length)
    return strAfterDecimal.length;
}


//Purpose:  Removes the preceeding zeros
function RemovePreceedingZeros(strInputNumber) {
    while (strInputNumber.length > 0) {
        if (strInputNumber.substring(0, 1) == '0') {
            strInputNumber = strInputNumber.substring(1, strInputNumber.length)
        }
        else {
            return strInputNumber
        }
    }
    return strInputNumber;
}


//Purpose:  This function retrieves the index for a particular "Display Text" in the drop down.
//			Input to this function is Options collection.
//			If input string is not present in the drop down list, then index '0' is returned.
function getIndexFromDisplayText(myOption, strSelectText) {
    //var myOption;
    //myOption = window.document.frmCommSlab.cboCusCountry.options;
    var i;

    for (i = 0; i < myOption.length; ++i) {
        if (myOption[i].value == LTrim(RTrim(strSelectText))) {
            return i;
        }
    }
}
function round_decimals(original_number, decimals) {
    var result1 = original_number * Math.pow(10, decimals)
    var result2 = Math.round(result1)
    var result3 = result2 / Math.pow(10, decimals)
    return pad_with_zeros(result3, decimals)

}
function pad_with_zeros(rounded_value, decimal_places) {
    var value_string = rounded_value.toString();
    var decimal_location = value_string.indexOf(".");

    // Is there a decimal point?
    if (decimal_location == -1) {
        // If no, then all decimal places will be padded with 0s
        decimal_part_length = 0;

        // If decimal_places is greater than zero, tack on a decimal point
        value_string += decimal_places > 0 ? "." : "";
    }
    else {
        // If yes, then only the extra decimal places will be padded with 0s
        decimal_part_length = value_string.length - decimal_location - 1;
    }

    // Calculate the number of decimal places that need to be padded with 0s
    var pad_total = decimal_places - decimal_part_length;

    if (pad_total > 0) {
        // Pad the string with 0s
        for (var counter = 1; counter <= pad_total; counter++)
            value_string += "0";
    }
    return value_string;
}
function PopUpWin(theUrl, WinName, WinFeatures) {
    var childWin = window.open(theUrl, WinName, WinFeatures);
    childWin.focus();

}
function IsDecimal(sText) {
    var validChars = "0123456789.";
    var isNumber = true;
    var chr;
    var pos = 0;
    for (i = 0; i < sText.length && isNumber == true; i++) {
        chr = sText.charAt(i);
        if (validChars.indexOf(chr) == -1) {
            isNumber = false;
        }
        if (chr == ".") {
            pos = pos + 1;
        }
        if (pos > 1) {
            isNumber = false;
        }
    }
    return isNumber;
}

function SwapChoice(tablename, itemName) {
    var row = igtbl_getRowById(itemName);
    var col = igtbl_getColumnById(itemName);
    var cell = igtbl_getCellById(itemName);
    if (col.Key == "Accept")
        if (cell.getValue() == true)
            row.getCellFromKey("Reject").setValue(false);

    if (col.Key == "Reject")
        if (cell.getValue() == true)
            row.getCellFromKey("Accept").setValue(false);
}

function SwapChoiceSR(tablename, itemName) {
    var row = igtbl_getRowById(itemName);
    var col = igtbl_getColumnById(itemName);
    var cell = igtbl_getCellById(itemName);
    //if (col.Key == "Accept")
    //if (cell.getValue() == true)
    //row.getCellFromKey("Reject").setValue(false);

    //if (col.Key == "Reject")
    //if (cell.getValue() == true)
    //row.getCellFromKey("Accept").setValue(false);

    if (col.Key == "Accept") {
        if (cell.getValue() == 'true') {
            row.getCellFromKey("Reject").setValue(false);
            row.setExpanded(false);
            row.FirstChildRow.getCellFromKey('REASON').setValue('');
        }
    }
    if (col.Key == "Reject") {
        if (cell.getValue() == 'true') {
            row.getCellFromKey("Accept").setValue(false);
            row.setExpanded(cell.getValue());
        }
        else {
            row.setExpanded(false);
            row.FirstChildRow.getCellFromKey('REASON').setValue('');
        }
        if (cell.getValue())
            if (row.FirstChildRow.getCellFromKey('REASON').getValue() == null)
                document.all.hdnOldValue.value = '';
            else
                document.all.hdnOldValue.value = row.FirstChildRow.getCellFromKey('REASON').getValue();
        else
            row.FirstChildRow.getCellFromKey('REASON').setValue(document.all.hdnOldValue.value);

    }
}
function IsAlphabetic(sText) {
    var regExpression = "^[a-zA-Z. ]+$";
    if (sText.match(regExpression)) {
        return true;
    } else {
        return false;
    }
}

function cp_txtOnChange(value) {
    var smallChar = 0, bigChar = 0, numeric = 0, special = 0;

    var WeakLetters = '';
    var tdWeak = document.getElementById('tdWeak');
    var tdStrong = document.getElementById('tdStrong');
    var tdMedium = document.getElementById('tdMedium');
    var tdTitle = document.getElementById('tdTitle');

    tdWeak.style.backgroundColor = "Gainsboro";
    tdMedium.style.backgroundColor = "Gainsboro";
    tdStrong.style.backgroundColor = "Gainsboro";
    tdTitle.innerText = " Undetermined";

    if (value.length < 6) return;

    smallChar = 0; bigChar = 0; numeric = 0; special = 0;

    for (var i = 0; i < value.length; i++) {
        if (value.substr(i, 1) <= 'z' && value.substr(i, 1) >= 'a') {
            if (smallChar == 0) smallChar = 1;
        }
        else {
            if (value.substr(i, 1) <= '9' && value.substr(i, 1) >= '0') {
                if (numeric == 0) numeric = 1;
            }
            else {
                if (value.substr(i, 1) <= 'Z' && value.substr(i, 1) >= 'A') {
                    if (bigChar == 0) bigChar = 1;
                }
                else {
                    if (special == 0 && ('*$-+?_&=!%{}/').indexOf(value.substr(i, 1)) > -1) special = 1;
                }
            }
        }
    }

    if ((smallChar + numeric + bigChar + special) == 1) {
        tdWeak.style.backgroundColor = "RoyalBlue";
        tdTitle.innerText = " Weak";
    }
    if ((smallChar + numeric + bigChar + special) > 1) {
        tdTitle.innerText = " Medium";
        tdWeak.style.backgroundColor = "RoyalBlue";
        tdMedium.style.backgroundColor = "RoyalBlue";
    }
    if ((smallChar + numeric + bigChar + special) > 2) {
        tdTitle.innerText = " Strong";
        tdWeak.style.backgroundColor = "RoyalBlue";
        tdMedium.style.backgroundColor = "RoyalBlue";
        tdStrong.style.backgroundColor = "RoyalBlue";
    }
}
function MO(e) {
    if (!e)
        var e = window.event;
    var S = e.srcElement;
    while (S.tagName != "TD")
    { S = S.parentElement; }
    S.className = "btnSelect";
}
function MU(e) {
    if (!e)
        var e = window.event;
    var S = e.srcElement;
    while (S.tagName != "TD")
    { S = S.parentElement; }
    S.className = "btnNormal";
}
var lastmenu = null;
function clearMenu() {
    if (lastmenu != null)
        lastmenu.className = "menuNormal"
}
function MenuO(e) {
    try {
        if (!e)
            var e = window.event;
        var S = e.srcElement;
        while (S.tagName != "TABLE")
        { S = S.parentElement; }
        if (lastmenu == null)
            lastmenu = S;
        lastmenu.className = "menuNormal"
        S.className = "menuSelect";
        lastmenu = S;
    }
    catch (e) { }
}
function MenuU(e) {
    alert('hi');
    if (!e)
        var e = window.event;
    var S = e.srcElement;
    while (S.tagName != "TABLE")
    { S = S.parentElement; }
    S.className = "menuNormal";
}
function subMenuO(e) {
    //    alert('subMenuO');
    try {
        if (!e)
            var e = window.event;
        var S = e.srcElement;
        while (S.tagName != "TR")
        { S = S.parentElement; }
        var menu = new Array('sublmenuSelect', 'submmenuSelect', 'subrmenuSelect');
        for (var i = 0; i < 3; i++) {
            if (S.children[i] != null) {
                if (S.children[i].children[0] != null)
                { S.children[i].children[0].className = menu[i]; }
                else
                    continue;
            }
            else
                continue;
        }
        //    if (S.children[0].children[0] != null)
        //    S.children[0].children[0].className="sublmenuSelect";
        //    if (S.children[1].children[0] != null)
        //    S.children[1].children[0].className="submmenuSelect";
        //    if (S.children[2].children[0] != null)
        //    S.children[2].children[0].className="subrmenuSelect";
        lastmenu.className = "menuOnSubSelect"
    }
    catch (e) { }
}
function subMenuU(e) {
    //    alert('subMenuU');
    try {
        if (!e)
            var e = window.event;
        var S = e.srcElement;
        while (S.tagName != "TR")
        { S = S.parentElement; }
        var menu = new Array('sublmenuNormal', 'submmenuNormal', 'subrmenuNormal');
        for (var i = 0; i < 3; i++) {
            if (S.children[i] != null)
                if (S.children[i].children[0] != null)
                    S.children[i].children[0].className = menu[i];
                else
                    continue;
            else
                continue;
        }
        //    S.children[0].children[0].className="sublmenuNormal";
        //    S.children[1].children[0].className="submmenuNormal";
        //    S.children[2].children[0].className="subrmenuNormal";
        lastmenu.className = "menuNormal"
    }
    catch (e) { }
}

function valLog() {
    var browserName = navigator.appName;
    if (browserName != "Microsoft Internet Explorer") {
        alert('My Property works only in Microsoft Internet Explorer.\r\nPlease open My Property in Microsoft Internet Explorer.');
        window.opener = window.self;
        window.close();
        return;
    }

    var win = window.open('Login.aspx', '_blank', 'menubar=no,toolbar=no,status=yes,resizable=yes,top=0,left=0,width=' + (screen.availWidth - 10) + ',height=' + (screen.availHeight - 60));
    window.opener = window.self;
    window.close();
}
function HMO(mySelf) {
    var e = window.event;
    var S = e.srcElement;
    while (S != null && S.tagName != "DIV")
        S = S.parentElement;
    if (S == null) return;
    S.style.borderColor = 'DarkGray';
    S.style.backgroundColor = 'AliceBlue';
    mySelf.style.cursor = 'hand';
}
function HMU(mySelf) {
    var e = window.event;
    var S = e.srcElement;
    while (S != null && S.tagName != "DIV")
        S = S.parentElement;
    if (S == null) return;
    S.style.borderColor = 'White';
    S.style.backgroundColor = 'White';
    mySelf.style.cursor = 'default';
}
function HMClick() {
    try {
        var e = window.event;
        var S = e.srcElement;
        while (S != null && S.tagName != "DIV")
            S = S.parentElement;
        if (S == null) return;
        window.event.cancelBubble = true;
        S = S.children[0].children[0].children[0].children[1].children[0].children[0].children[0].children[0].children[0];
        WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(S.id.replace(/_/g, '$'), "", true, "", "", false, true));
    } catch (e) { }
}

function HMClickWithOutValidation() {
    var e = window.event;
    var S = e.srcElement;
    while (S != null && S.tagName != "DIV")
        S = S.parentElement;
    if (S == null) return;
    window.event.cancelBubble = true;
    S = S.children[0];
    __doPostBack(S.id.replace(/_/g, '$'), '')
}





function GetDecimalDelimiter(countryCode) {

    switch (countryCode) {
        case 3:
            return '#';
        case 2:
            return ',';
        default:
            return '.';
    }
}

function GetCommaDelimiter(countryCode) {

    switch (countryCode) {
        case 3:
            return '*';
        case 2:
            return ',';
        default:
            return ',';
    }

}

function FormatClean(num) {
    var sVal = '';
    var nVal = num.length;
    var sChar = '';

    try {
        for (c = 0; c < nVal; c++) {
            sChar = num.charAt(c);
            nChar = sChar.charCodeAt(0);
            if ((nChar >= 48) && (nChar <= 57)) { sVal += num.charAt(c); }
        }
    }
    catch (exception) { AlertError("Format Clean", exception); }
    return sVal;
}


function FormatNumber(num, countryCode, decimalPlaces) {

    var minus = '';
    var comma = '';
    var dec = '';
    var preDecimal = '';
    var postDecimal = '';
    try {

        decimalPlaces = parseInt(decimalPlaces);
        comma = GetCommaDelimiter(countryCode);
        dec = GetDecimalDelimiter(countryCode);

        if (decimalPlaces < 1) { dec = ''; }
        if (num.lastIndexOf("-") == 0) { minus = '-'; }

        preDecimal = FormatClean(num);

        // preDecimal doesn't contain a number at all.
        // Return formatted zero representation.

        if (preDecimal.length < 1) {
            return minus + FormatEmptyNumber(dec, decimalPlaces);
        }

        // preDecimal is 0 or a series of 0's.
        // Return formatted zero representation.

        if (parseInt(preDecimal) < 1) {
            return minus + FormatEmptyNumber(dec, decimalPlaces);
        }

        // predecimal has no numbers to the left.
        // Return formatted zero representation.

        if (preDecimal.length == decimalPlaces) {
            return minus + '0' + dec + preDecimal;
        }

        // predecimal has fewer characters than the
        // specified number of decimal places.
        // Return formatted leading zero representation.

        if (preDecimal.length < decimalPlaces) {
            if (decimalPlaces == 2) {
                return minus + FormatEmptyNumber(dec, decimalPlaces - 1) + preDecimal;
            }
            return minus + FormatEmptyNumber(dec, decimalPlaces - 2) + preDecimal;
        }

        // predecimal contains enough characters to
        // qualify to need decimal points rendered.
        // Parse out the pre and post decimal values
        // for future formatting.

        if (preDecimal.length > decimalPlaces) {
            postDecimal = dec + preDecimal.substring(preDecimal.length - decimalPlaces,
preDecimal.length);
            preDecimal = preDecimal.substring(0, preDecimal.length - decimalPlaces);
        }

        // Place comma oriented delimiter every 3 characters
        // against the numeric represenation of the "left" side
        // of the decimal representation.  When finished, return
        // both the left side comma formatted value together with
        // the right side decimal formatted value.

        var regex = new RegExp('(-?[0-9]+)([0-9]{3})');

        while (regex.test(preDecimal)) {
            preDecimal = preDecimal.replace(regex, '$1' + comma + '$2');
        }

    }
    catch (exception) { AlertError("Format Number", exception); }
    return minus + preDecimal + postDecimal;
}

function FormatEmptyNumber(decimalDelimiter, decimalPlaces) {
    var preDecimal = '0';
    var postDecimal = '';

    for (i = 0; i < decimalPlaces; i++) {
        if (i == 0) { postDecimal += decimalDelimiter; }
        postDecimal += '0';
    }
    return preDecimal + postDecimal;
}


function AlertError(methodName, e) {
    if (e.description == null) { alert(methodName + " Exception: " + e.message); }
    else { alert(methodName + " Exception: " + e.description); }
}

// Written by Aamir
function CurrencyFormatted(amount) {
    var i = parseFloat(amount);
    if (isNaN(i)) { i = 0.00; }
    var minus = '';
    if (i < 0) { minus = '-'; }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if (s.indexOf('.') < 0) { s += '.00'; }
    if (s.indexOf('.') == (s.length - 2)) { s += '0'; }
    s = minus + s;
    return s;
}
// end of function CurrencyFormatted()

function CommaFormatted(amount) {
    var delimiter = ","; // replace comma if desired
    var a = amount.split('.', 2)
    var d = a[1];
    var i = parseInt(a[0]);
    if (isNaN(i)) { return ''; }
    var minus = '';
    if (i < 0) { minus = '-'; }
    i = Math.abs(i);
    var n = new String(i);
    var a = [];
    while (n.length > 3) {
        var nn = n.substr(n.length - 3);
        a.unshift(nn);
        n = n.substr(0, n.length - 3);
    }
    if (n.length > 0) { a.unshift(n); }
    n = a.join(delimiter);
    if (d.length < 1) { amount = n; }
    else { amount = n + '.' + d; }
    amount = minus + amount;
    return amount;
}
// end of function CommaFormatted()

function ConvertToCurrency(amount) {
    if (amount.indexOf(',') > 0)
        amount = replaceAll(amount, ",", ""); //amount.replace(",","");
    // alert(amount);
    var result = CurrencyFormatted(amount);
    result = CommaFormatted(result);
    return result;

}
function replaceAll(text, strA, strB) {
    return text.replace(new RegExp(strA, "g"), strB);
}

var jControl = $$ = function (serverControlId) {
    return $('#' + clientControl(serverControlId));
}
var clientControl = c$ = function (serverControlId) {
    return contentPlaceHolderId + '_' + serverControlId;
}


$.fn.labelExtender = function (labelForControl, isRequired) {
    var controlLabel = labelForControl;
    var isReqd = isRequired | false;
    setMeUp(this);
    $(this).bind('change', function () {
        setMeUp(this);
    });

    $(this).bind('blur', function () {
        setMeUp(this).removeClass((isReqd) ? ('glowRequiredText') : ('glowNonRequiredText'));
    });

    $(this).bind('focus', function () {
        setMeUp(this).addClass((isReqd) ? ('glowRequiredText') : ('glowNonRequiredText'));
        if ($(this).val() == controlLabel.toUpperCase())
            $(this).val('').removeClass('labelText');
    });

    function setMeUp(mySelf) {
        if ($(mySelf).val() == controlLabel.toUpperCase() && !$(mySelf).hasClass('labelText'))
            $(mySelf).addClass('labelText');
        if ($(mySelf).val().length == 0)
            $(mySelf).val(controlLabel.toUpperCase()).addClass('labelText');
        if ($(mySelf).val() != controlLabel.toUpperCase())
            $(mySelf).removeClass('labelText');
        return $(mySelf);
    }
}

function clearLabelExtender() {
    $('.labelText').each(function () {
        $(this).val('');
    });
}

$.fn.live = $.fn.on;



//$.widget("ui.timespinner", $.ui.spinner, {
//    options: {
//        // seconds
//        step: 60 * 1000,
//        // hours
//        page: 60
//    },

//    _parse: function (value) {
//        if (typeof value === "string") {
//            // already a timestamp
//            if (Number(value) == value) {
//                return Number(value);
//            }
//            return +Globalize.parseDate(value);
//        }
//        return value;
//    },

//    _format: function (value) {
//        return Globalize.format(new Date(value), "t");
//    }
//});


$.fn.drilldown = function () {
    $(this).find('.dmessage').css('display', 'none');

    //var closeTimer = null;
    var menuItem = null;
    var slideDirection = "SlideDown";
    /*
    function cancelTimer() {
    if (closeTimer) {
    window.clearTimeout(closeTimer);
    closeTimer = null;
    }
    }

    function close() {
    $(menuItem).slideUp(250);
    menuItem = null;
    }

    $(this).find('.dtitle > a').hover(function () {
    },
    function () {
    menuItem = $(this).parent().next();
    cancelTimer();
    closeTimer = window.setTimeout(close, 500);
    }
    );

    $(this).find('.dmessage').hover(function () {
    cancelTimer();
    menuitem = this;
    $(this).parent().next().slideDown(250);
    },
    function () {
    menuItem = this;
    cancelTimer();
    closeTimer = window.setTimeout(close, 500);
    }
    );
    */
    $('body').on('click', function () {
        if (menuItem != null) if (slideDirection == "SlideDown") menuItem.slideUp(250); else menuItem.slideDown(250);
    });
    $(this).find('.dtitle > a').click(function (e) {
        menuItem = $(this).parent().next();
        menuItem.slideDown(250);
        var pos = $(this).parent().position();
        if ((menuItem.offset().left + menuItem.outerWidth()) > screen.availWidth) {
            var right = ($(this).parent().offset().left + $(this).parent().outerWidth());
            menuItem.css('left', right - menuItem.outerWidth());
        }
        e.stopPropagation();
        return false;
    });
    $(this).find('.dmessage').click(function (e) {
        e.stopPropagation();
    });
}
$.fn.dropdown = function () {
    var dropDownZone = $(this).find('.drilldown_1');
    var dropDownBody = $(dropDownZone).find('.ledgerControl');
    $(dropDownBody).prependTo('body').hide();
    var slideDirection = "SlideDown";
    var downAnchor = $(dropDownZone).find('.down');
    var isopened = false, isHover = false;

    function close() {
        if ($(dropDownBody) != null) {
            if (slideDirection == "SlideDown")
                $(dropDownBody).hide('slide', { direction: 'up', duration: 250 });
            else
                $(dropDownBody).hide('slide', { direction: 'down', duration: 250 });

        }
    }

    $(document).mousedown(function () {
        if (!isHover) {
            isopened = false; close();
        }
    });

    $(dropDownZone).hover(function () { isHover = true; }, function () { isHover = false; });
    $(dropDownBody).hover(function () { isHover = true; }, function () { isHover = false; });

    $(downAnchor).click(function (e) {
        if (isopened) { close(); return; }
        isopened = true;
        var top = $(dropDownZone).offset().top - $(window).scrollTop();
        var screenH = $(window).height() + $(document).scrollTop();
        var screenH75 = screenH * 60 / 100;
        $(dropDownBody).css('left', $(dropDownZone).offset().left);

        if (top > screenH75) {
            $(dropDownBody).css('top', $(dropDownZone).offset().top - $(dropDownBody).outerHeight());
            slideDirection = "SlideUp";
        }
        else {
            slideDirection = "SlideDown";
            $(dropDownBody).css('top', $(dropDownZone).offset().top + $(dropDownZone).outerHeight());

        }

        if (slideDirection == "SlideDown")
            $(dropDownBody).show('slide', { direction: 'up', duration: 250 });
        else
            $(dropDownBody).show('slide', { direction: 'down', duration: 250 });

        e.stopPropagation();
        return false;
    });
    $(dropDownBody).click(function (e) {
        e.stopPropagation();
    });

    return this;

}
$.fn.dropdown_getValue = function () {
    var dropDownZone = $(this).find('.drilldown_1');
    var dtitle = $(dropDownZone).find('div.dtitle:first');
    var value = $(dtitle).find('input[type="hidden"]').val();
    if (value != null && value != '')
        return value.split(',');
    return value;
}
function dropdown_selectNode(ledgerId, nodeText) {

}
function setupMultiline_limiter(limit) {
    $('.multiline_limiter').each(function () {
        var limit = $(this).data('limit') || 500;
        $(this).data('limit', limit);
        $('textarea', $(this)).data('displayLabel', $('.displayLabel', $(this)));
        $('.displayLabel', $(this)).text(limit - $('textarea', $(this)).val().length);
    });

    $('textarea', '.multiline_limiter').bind('keyup', function () {
        var limit = $(this).closest('.multiline_limiter').data('limit') || 500;
        $(this).data('displayLabel').text(limit - $(this).val().length);
        if ($(this).val().length >= limit) return false;
    });
    $('textarea', '.multiline_limiter').bind('paste', function () {
        var limit = $(this).closest('.multiline_limiter').data('limit') || 500;
        var pastingdata = clipboardData.getData("text");
        if ((pastingdata.length + $(this).val().length) > limit) {
            var e = event || window.event;
            $.showMessage('The data entered in the text crossed the upper limit.');
            e.preventDefault();
            return false;
        }

        $(this).data('displayLabel').text(limit - $(this).val().length);
    });
}