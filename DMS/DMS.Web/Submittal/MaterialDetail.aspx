﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"  CodeBehind="MaterialDetail.aspx.vb" Inherits="DMS.Web.MaterialDetail" %>

<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.3.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Src="~/ucRemarks.ascx" TagPrefix="uc1" TagName="ucRemarks" %>
<%@ Register Src="~/ucDocUploader.ascx" TagPrefix="uc1" TagName="ucDocUploader" %>

<asp:Content ID="Content1" ContentPlaceHolderID="scriptPlaceHolder" runat="server">
    <link href="../Content/css/fonts/foldericon/flaticon.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cssPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pageHeader">
        <div style="width: 100%;">
            <div class="w3-padding inlineblock moveMiddle w3-left" style="width: 400px">
                <div class="Title">
                    Material Submittal Detail
                </div>
                <div class="TitleDescription">
                    Add/Modify Material Submittal details
                </div>
            </div>

        </div>
    </div>
    <div>
        <div class="w3-row">
            <div class="w3-row w3-padding">
                <div class="w3-card w3-padding w3-white">
                    <div class="w3-row w3-bottombar w3-padding-4 dialogTitle w3-medium bold">
                        Site Details
                    </div>
                    <div class="w3-row w3-padding-4"></div>

                    <div class="w3-row w3-padding-4 marginLeft100">
                        <div class="inlineblock moveMiddle" style="width: 170px">Site ID : </div>
                        <div class="inlineblock moveMiddle" style="width: 320px">
                            <asp:DropDownList ID="ddlSites" runat="server" Width="292px" AutoPostBack="True"></asp:DropDownList>
                        </div>

                        <div class="inlineblock moveMiddle" style="width: 170px">Site Name : </div>
                        <div class="inlineblock moveMiddle" style="width: 180px">
                            <asp:TextBox ID="txtSiteName" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                        </div>
                    </div>
                    <div class="w3-row w3-padding-4 marginLeft100" id="rowPassoword" runat="server">
                        <div class="inlineblock moveMiddle" style="width: 170px">Consultant : </div>
                        <div class="inlineblock moveMiddle" style="width: 320px">
                            <asp:TextBox ID="txtConsultant" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                        </div>

                        <div class="inlineblock moveMiddle" style="width: 170px">Contractor : </div>
                        <div class="inlineblock moveMiddle" style="width: 180px">
                            <asp:TextBox ID="txtContractor" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                        </div>
                    </div>

                    <div class="w3-row w3-padding-4 marginLeft100">
                        <div class="inlineblock moveMiddle" style="width: 170px">Client : </div>
                        <div class="inlineblock moveMiddle" style="width: 320px">
                            <asp:TextBox ID="txtClient" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                        </div>

                        <div class="inlineblock moveMiddle" style="width: 170px">Project Manager : </div>
                        <div class="inlineblock moveMiddle" style="width: 180px">
                            <asp:TextBox ID="txtProjectManager" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                        </div>
                    </div>

                    <div class="w3-row w3-padding-4 marginLeft100">
                        <div class="inlineblock moveMiddle" style="width: 170px">Site Location : </div>
                        <div class="inlineblock moveMiddle" style="width: 320px">
                            <asp:TextBox ID="txtSiteLocation" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                        </div>

                        <div class="inlineblock moveMiddle" style="width: 170px">Document Code : </div>
                        <div class="inlineblock moveMiddle" style="width: 300px">
                            <b>
                                <asp:Label ID="lblDocumentCode" Font-Size="X-Large" runat="server" Width="300px"></asp:Label></b>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
    <div class="w3-row">
        <div class="w3-row w3-padding">
            <div class="w3-card w3-padding w3-white">

                <div class="w3-row w3-bottombar w3-padding-4 dialogTitle w3-medium bold">
                    Attachments
                </div>
                <div class="w3-row w3-padding-4"></div>

                <div class="w3-row w3-padding-4 marginLeft100">
                    <uc1:ucDocUploader runat="server" ID="ucDocs" />
                </div>

            </div>
        </div>
    </div>
    <div class="w3-row">
        <div class="w3-row w3-padding">
            <div id="divDocDetails" class="w3-card w3-padding w3-white col-lg-12 col-sm-12">
                <div class="w3-row w3-bottombar w3-padding-4 dialogTitle w3-medium bold">
                    Material Submittal Details
                </div>
                <div class="w3-row w3-padding-4"></div>

                <div class="w3-row w3-padding-4 col-lg-12 col-sm-12 col-md-12">
                    <div class="col-lg-2 col-sm-2 col-md-2">Date : </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <dx:ASPxDateEdit DisplayFormatString="dd/MM/yyyy" EditFormatString="dd/MM/yyyy" ID="deDocumentDate" ClientInstanceName="deDocumentDate" runat="server" Width="100%" EnableTheming="True" Theme="Glass">
                            <CalendarProperties>
                            </CalendarProperties>
                        </dx:ASPxDateEdit>
                    </div>
                    <div class="col-lg-2 col-sm-2 col-md-2">Revision : </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                         <dx:ASPxSpinEdit ID="spIncRevision" runat="server" DisplayFormatString="#,#;#,#;" Width="100%" Number="0" />
                    </div>


                </div>

                <div class="w3-row w3-padding-4 col-lg-12 col-sm-12 col-md-12">
                    <div class="col-lg-2 col-sm-2 col-md-2">Type : </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <asp:DropDownList ID="ddlType" runat="server" Width="280px">
                            <asp:ListItem Text="MECH" Value="MECH" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="CIVIL" Value="CIVIL"></asp:ListItem>
                            <asp:ListItem Text="ELEC" Value="ELEC"></asp:ListItem>
                            <asp:ListItem Text="ARCH" Value="ARCH"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="w3-row w3-padding-4 col-lg-12 col-sm-12 col-md-12">
                    <div class="col-lg-2 col-sm-2 col-md-2">Manufacturer : </div>
                    <div class="col-lg-10 col-sm-10 col-md-10">
                        <asp:TextBox ID="txtManufacturer" runat="server" Width="80%" MaxLength="200"></asp:TextBox>
                    </div>
                </div>
                <div class="w3-row w3-padding-4 col-lg-12 col-sm-12 col-md-12">

                    <div class="col-lg-2 col-sm-2 col-md-2">Local Supplier : </div>
                    <div class="col-lg-10 col-sm-10 col-md-10">
                        <asp:TextBox ID="txtLocalSupplier" runat="server"  Width="80%" MaxLength="200"></asp:TextBox>
                    </div>
                     
                </div>
                <div class="w3-row w3-padding-4 col-lg-12 col-sm-12 col-md-12">
                    <div class="col-lg-2 col-sm-2 col-md-2">Description : </div>
                    <div class="col-lg-10 col-sm-10 col-md-10">
                        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Height="60px" Width="80%"  MaxLength="200"></asp:TextBox>
                    </div>
                </div>
                <div class="w3-row w3-padding-4 col-lg-12 col-sm-12 col-md-12">

                    <div class="col-lg-2 col-sm-2 col-md-2">Comments : </div>
                    <div class="col-lg-10 col-sm-10 col-md-10">
                        <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Height="60px" Width="80%" MaxLength="200"></asp:TextBox>
                    </div> 
                </div>
                <div class="w3-row w3-padding-4 col-lg-12 col-sm-12 col-md-12">
                    <div class="col-lg-2 col-sm-2 col-md-2">Status : </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <asp:DropDownList ID="ddlStaus" runat="server" Width="280px">
                            <asp:ListItem Text="Resolved the query" Value="A" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Does not Resolved the query" Value="B"></asp:ListItem>
                            <asp:ListItem Text="Required An Engineer's Instruction" Value="C"></asp:ListItem>
                            <asp:ListItem Text="Required Change Request" Value="D"></asp:ListItem>
                            <asp:ListItem Text="Rejected and Resubmit" Value="UR"></asp:ListItem>
                        </asp:DropDownList>                        
                    </div>

                    <div class="col-lg-2 col-sm-2 col-md-2">Reply Date : </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <dx:ASPxDateEdit DisplayFormatString="dd/MM/yyyy" EditFormatString="dd/MM/yyyy" ID="deReplyDate" ClientInstanceName="deReplyDate" runat="server" Width="320px" EnableTheming="True" Theme="Glass">
                            <CalendarProperties>
                            </CalendarProperties>
                        </dx:ASPxDateEdit>
                    </div> 
                </div> 
                <div class="w3-row w3-padding-4 col-lg-12 col-sm-12 col-md-12">

                    <div class="col-lg-2 col-sm-2 col-md-2">No. of Days With Consultant : </div>
                    <div class="col-lg-4 col-sm-4 col-md-4"> 
                         <dx:ASPxSpinEdit ID="spNoOfDays" runat="server" DisplayFormatString="#,#;#,#;Zero" Width="280px" Number="1" />
                    </div>
                    <div class="col-lg-2 col-sm-2 col-md-2">Samples : </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <asp:RadioButtonList ID="rdButton" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="No" Value="0" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>

                </div> 
                <div class="w3-row w3-padding-4 col-lg-12 col-sm-12 col-md-12">

                    <div class="col-lg-2 col-sm-2 col-md-2">Plan Date - Early : </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <dx:ASPxDateEdit DisplayFormatString="dd/MM/yyyy" EditFormatString="dd/MM/yyyy" ID="deEarlyPlanDate" ClientInstanceName="deEarlyPlanDate" runat="server" Width="280px" EnableTheming="True" Theme="Glass">
                            <CalendarProperties>
                            </CalendarProperties>
                        </dx:ASPxDateEdit>                        
                    </div>
                    <div class="col-lg-2 col-sm-2 col-md-2">Plan Date - Late : </div>
                    <div class="col-lg-4 col-sm-4 col-md-4"> 
                        <dx:ASPxDateEdit DisplayFormatString="dd/MM/yyyy" EditFormatString="dd/MM/yyyy" ID="deLatePlanDate" ClientInstanceName="deLatePlanDate" runat="server" Width="320px" EnableTheming="True" Theme="Glass">
                            <CalendarProperties>
                            </CalendarProperties>
                        </dx:ASPxDateEdit>
                    </div>

                </div> 
                <div class="w3-row w3-padding-4 col-lg-12 col-sm-12 col-md-12">
                     
                    <div class="col-lg-2 col-sm-2 col-md-2">Final Status : </div>
                    <div class="col-lg-4 col-sm-4 col-md-4">
                        <asp:DropDownList ID="ddlFinalStatus" runat="server" Width="280px">
                            <asp:ListItem Text="Resolved the query" Value="A" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Does not Resolved the query" Value="B"></asp:ListItem>
                            <asp:ListItem Text="Required An Engineer's Instruction" Value="C"></asp:ListItem>
                            <asp:ListItem Text="Required Change Request" Value="D"></asp:ListItem>
                            <asp:ListItem Text="Rejected and Resubmit" Value="UR"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div> 
            </div>
        </div>
    </div> 
    <div class="w3-row hide">
        <div class="w3-row w3-padding">

            <div class="w3-card w3-padding w3-white">
                <div class="w3-row w3-bottombar w3-padding-4 dialogTitle w3-medium bold">
                    Remarks
                </div>
                <div class="w3-row w3-padding-4">
                </div>
                <div class="w3-row w3-padding-4 marginLeft100">
                    <div class="inlineblock moveTop" style="width: 150px; margin-top: 5px;">Remarks : </div>
                    <div class="inlineblock moveMiddle" style="width: 700px">
                        <uc1:ucRemarks runat="server" ID="ucRemarks" />

                    </div>
                </div> 
            </div>
        </div>
    </div>

    <div class="w3-row w3-padding-4 w3-center">
        <div class="inlineblock w3-padding-12">
            <div>
                <asp:LinkButton ToolTip="Save and Go Back to List" runat="server" ID="lnkSave" CssClass="w3-btn w3-blue"><span class="adminuicon adminuicon-write19 moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Send</span></asp:LinkButton>
                <asp:LinkButton ToolTip="Approve" runat="server" ID="lnkApprove" CssClass="w3-btn w3-orange"><span class="adminuicon adminuicon-write19 moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Approve</span></asp:LinkButton>
                <asp:LinkButton ToolTip="Reject" runat="server" ID="lnkReject" CssClass="w3-btn w3-orange"><span class="adminuicon adminuicon-write19 moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Reject</span></asp:LinkButton>
                <asp:LinkButton ToolTip="Send" runat="server" ID="lnkSendForApproval" CssClass="w3-btn w3-orange"><span class="adminuicon adminuicon-write19 moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Send</span></asp:LinkButton>
                <asp:LinkButton ToolTip="Back to List" runat="server" ID="lnkBackToList" OnClientClick="window.location.href='Materials.aspx'; return false;" CssClass="w3-btn w3-grey"><span class="adminuicon adminuicon-verify8 moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Back to List</span></asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnStatus" runat="server" />
    <asp:HiddenField ID="hdnUserRole" runat="server" />
    <asp:HiddenField ID="hdnScriptToRun" runat="server" />
    <%If False Then%>
    <script language="javascript" type="text/javascript" src="/script/common.js">
    </script>
    <script language="javascript" type="text/javascript" src="/script/jquery-vsdoc.js">
    </script>
    <%End If%>
    <style>
        .disabledbutton {
            pointer-events: none;
        }
        .enableTextBox {
            pointer-events: visible !important;
        }
    </style>

    <script type="text/javascript"> 
        var status = $('#<%=hdnStatus.ClientID %>').val();
        var userRoleId = $('#<%=hdnUserRole.ClientID %>').val();

<%--        if ((userRoleId == '3' && status == "N") || (userRoleId == '4' && status == "W")) {
            $("#divDistribute").removeClass("disabledbutton");
        }
        else {
            $("#divDistribute").addClass("disabledbutton");
        }

        if ((status == "W") || (status == "A")) {
            $("#divDocDetails").addClass("disabledbutton"); 
            if (userRoleId == '3') {
                $('#<%=txtCrossReference.ClientID %>').addClass("enableTextBox");
                $('#<%=txtFwdRefNo.ClientID %>').addClass("enableTextBox");                
            }
        }
        else {
            $("#divDocDetails").removeClass("disabledbutton");
        }--%>

        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
            init_page_events_and_controls();
        });

        function init_page_events_and_controls() {

            $('#<%=lnkSave.ClientID%>').on('click', function () {
                Validation.init();

                if ($('.emptyList').length > 0) {
                    $.showMessage('Attachment is missing. Kindly add atleast one attachment.');
                    return false;
                }

                  
<%--
                Validation.addRequired('<%=ddlSites.ClientID%>', 'Select Site');
                Validation.addRequired('<%=txtSubjectTitle.ClientID%>', 'Enter Subject title');
                Validation.addRequired('<%=ddlSender.ClientID%>', 'Select Sender');
                Validation.addRequired('<%=ddlReceiver.ClientID%>', 'Select Receiver');--%>

                if (Validation.execute()) {
                    return $.confirmMessage('<div class="w3-center"><div class="w3-padding w3-text-red w3-xlarge">ATTENTION !!!</div><div class="w3-padding">Do you want to Save this detail?</div></div>', this);
                }
                else {
                    return false;
                }
            }); 
        }
    </script>
    <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
