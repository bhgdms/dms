﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ManageDocument

    '''<summary>
    '''ddlSites control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlSites As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtSiteName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSiteName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''rowPassoword control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rowPassoword As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''txtConsultant control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtConsultant As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtContractor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtContractor As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtClient control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtClient As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtProjectManager control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtProjectManager As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtSiteLocation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSiteLocation As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblDocumentCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDocumentCode As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ucDocs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucDocs As Global.DMS.Web.ucDocUploader

    '''<summary>
    '''txtRefNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRefNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlCategory control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCategory As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlDocumentPurpose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlDocumentPurpose As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlSender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlSender As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlReceiver control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlReceiver As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtOriginator control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOriginator As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtOrganizationAddressTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOrganizationAddressTo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''deDocumentDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents deDocumentDate As Global.DevExpress.Web.ASPxDateEdit

    '''<summary>
    '''deReceivedDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents deReceivedDate As Global.DevExpress.Web.ASPxDateEdit

    '''<summary>
    '''txtSubjectTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSubjectTitle As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFwdRefNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFwdRefNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtSubjectDesc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSubjectDesc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCrossReference control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCrossReference As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRemarks control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRemarks As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlPriority control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPriority As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''divActionRequiredBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divActionRequiredBy As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ddlActionRequiredBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlActionRequiredBy As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''gvDistributeTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvDistributeTo As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''ucRemarks control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucRemarks As Global.DMS.Web.ucRemarks

    '''<summary>
    '''divActionTakenBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divActionTakenBy As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblActionTakenBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblActionTakenBy As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblActionTakenOn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblActionTakenOn As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lnkSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkSave As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkApprove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkApprove As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkReject control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkReject As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkSendForApproval control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkSendForApproval As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkBackToList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkBackToList As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''hdnStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnStatus As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnUserRole control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnUserRole As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnScriptToRun control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnScriptToRun As Global.System.Web.UI.WebControls.HiddenField
End Class
