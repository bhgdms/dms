﻿Imports System.IO
Imports DMS.DataContext
Imports DMS.DataContext.DataAccessHelper
Imports DMS.Models.DTO
Imports DMS.Models.Common

Public Class ManageDocument
    Inherits BasePage

    Dim caller As Documents
    Dim caller2 As SystemAdmin
    Dim caller3 As Masters
    Dim fileDto As List(Of FileDTO)
    Dim distList As List(Of DocumentDistributeToDTO)

    Private Property DocId As Integer
        Get
            Return ViewState("DOCUMENT_ID")
        End Get
        Set(value As Integer)
            ViewState("DOCUMENT_ID") = value
        End Set
    End Property

    Private Property SiteId As Integer
        Get
            Return ViewState("SITEID")
        End Get
        Set(value As Integer)
            ViewState("SITEID") = value
        End Set
    End Property

    Private Property Status As String
        Get
            Return ViewState("STATUS")
        End Get
        Set(value As String)
            ViewState("STATUS") = value
        End Set
    End Property

    Protected ReadOnly Property SessionID As String
        Get
            If ViewState("SESSION_ID") Is Nothing Then
                ViewState("SESSION_ID") = Session.SessionID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
            End If
            Return ViewState("SESSION_ID").ToString
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            caller = New Documents()
            caller2 = New SystemAdmin()
            caller3 = New Masters()
            fileDto = New List(Of FileDTO)
            distList = New List(Of DocumentDistributeToDTO)
            hdnUserRole.Value = UserProfile.RoleID
            If Not IsPostBack Then
                Initialize()
            End If
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub Initialize()

        FillPurpose()

        If Request.QueryString("PARAM") IsNot Nothing Then
            Dim queryEnc As New QueryString("PARAM")
            Dim items As ListItemCollection = queryEnc.Decompose()

            If items.Count > 0 AndAlso items.FindByValue("DID") IsNot Nothing Then
                DocId = CInt(items.FindByValue("DID").Text)
                LoadDocument()
            End If
        Else
            DocId = 0
            Status = "N"
            hdnStatus.Value = "N"
            ddlSites.SelectedValue = UserProfile.SiteID
            SiteId = UserProfile.SiteID
            ddlSites_SelectedIndexChanged(Nothing, Nothing)
        End If

        FillDistributeTo()
        FillProjectSites()
        FillSenderReceiver()
        SetButtonVisibility()

        ucDocs.DocId = DocId
        ucDocs.Status = Status
        ucDocs.BindDocuments()
    End Sub

    Private Sub LoadDocument()
        Try
            Dim doc As DocumentsDTO = New DocumentsDTO()
            doc = caller.GetDocumentsById(DocId)

            If doc IsNot Nothing AndAlso doc.Id > 0 Then
                ddlSites.SelectedValue = doc.SiteID
                ddlCategory.SelectedValue = doc.CategoryId
                ddlSender.SelectedValue = doc.SenderId
                ddlReceiver.SelectedValue = doc.ReceiverId
                deDocumentDate.Value = doc.DateOfLetter
                deReceivedDate.Value = doc.DateReceived
                txtSubjectTitle.Text = doc.SubjectTitle
                txtSubjectDesc.Text = doc.SubjectDescription
                txtRefNo.Text = doc.ReferenceNo
                txtFwdRefNo.Text = doc.ReplyRefNo
                txtRemarks.Text = doc.Remarks
                txtCrossReference.Text = doc.CrossReferences
                ddlPriority.SelectedValue = doc.Priority
                txtOriginator.Text = doc.Originator
                txtOrganizationAddressTo.Text = doc.OrganizationAddressTo

                Status = doc.WorkflowStatus
                SiteId = doc.SiteID
                hdnStatus.Value = doc.WorkflowStatus
                lblActionTakenBy.Text = doc.ActionTakenBy
                If Not doc.ActionTakenOn Is Nothing Then
                    lblActionTakenOn.Text = doc.ActionTakenOn
                Else
                    lblActionTakenOn.Text = String.Empty
                End If

                lblDocumentCode.Text = doc.DocumentCode

                ucRemarks.DocId = DocId
                ucRemarks.LoadRemarks()

            End If

        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub FillProjectSites()
        Dim sites As List(Of SiteDTO) = New List(Of SiteDTO)
        sites = caller2.GetAllProjectSites()

        If sites.Count > 0 Then
            ddlSites.DataSource = sites
            ddlSites.DataValueField = "ID"
            ddlSites.DataTextField = "SiteID"
            ddlSites.DataBind()

            ddlSites.Items.Insert(0, New ListItem("-- Select -- ", ""))
            ddlSites_SelectedIndexChanged(Nothing, Nothing)
            ddlSites.Enabled = False

            txtSiteName.Enabled = False
            txtConsultant.Enabled = False
            txtContractor.Enabled = False
            txtClient.Enabled = False
            txtProjectManager.Enabled = False
            txtSiteLocation.Enabled = False
        End If

    End Sub

    Private Sub SetButtonVisibility()
        If UserProfile.RoleID.In(UserRoles.SITE_USER, UserRoles.SECRETARY) Then
            lnkApprove.Visible = False
            lnkReject.Visible = False
            'lnkSendForApproval.Visible = True AndAlso Status.In("N")
        ElseIf UserProfile.RoleID.In(UserRoles.PROJECT_MANAGER) Then
            lnkApprove.Visible = True AndAlso Status = "W"
            lnkReject.Visible = True AndAlso Status = "W"
            'lnkSendForApproval.Visible = False
        ElseIf UserProfile.RoleID = UserRoles.SUPER_ADMINISTRATION Then
            lnkApprove.Visible = True AndAlso Status.In("W")
            lnkReject.Visible = True AndAlso Status.In("W")
            'lnkSendForApproval.Visible = True AndAlso Status.In("N")
        Else
            lnkApprove.Visible = False
            lnkReject.Visible = False
            'lnkSendForApproval.Visible = False
        End If
        lnkSendForApproval.Visible = False

        'If UserProfile.RoleID = UserRoles.PROJECT_MANAGER OrElse UserProfile.RoleID = UserRoles.SUPER_ADMINISTRATION Then
        '    ddlActionRequiredBy.Enabled = True
        'Else
        '    ddlActionRequiredBy.Enabled = False
        'End If
    End Sub

    Private Sub ddlSites_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSites.SelectedIndexChanged
        Dim site As SiteDTO = New SiteDTO()
        If Not String.IsNullOrEmpty(ddlSites.SelectedValue) Then
            site = caller2.GetProjectSiteById(ddlSites.SelectedValue)
            If site IsNot Nothing Then
                txtSiteName.Text = site.SiteName
                txtConsultant.Text = site.Consultant
                txtContractor.Text = site.Contractor
                txtClient.Text = site.Client
                txtProjectManager.Text = site.ProjectManager
                txtSiteLocation.Text = site.AddressLine1 + " " + site.AddressLine2
            End If
        Else
            txtSiteName.Text = String.Empty
            txtConsultant.Text = String.Empty
            txtContractor.Text = String.Empty
            txtClient.Text = String.Empty
            txtProjectManager.Text = String.Empty
            txtSiteLocation.Text = String.Empty
        End If

    End Sub

    Private Sub FillSenderReceiver()
        Dim sender As List(Of DocumentMovementDTO) = New List(Of DocumentMovementDTO)
        sender = caller.GetSenderBySite(SiteId)

        If sender.Count > 0 Then
            ddlSender.DataSource = sender
            ddlSender.DataValueField = "MovementId"
            ddlSender.DataTextField = "Description"
            ddlSender.DataBind()

            ddlSender.Items.Insert(0, New ListItem("-- Select -- ", ""))

            ddlReceiver.DataSource = sender
            ddlReceiver.DataValueField = "MovementId"
            ddlReceiver.DataTextField = "Description"
            ddlReceiver.DataBind()

            ddlReceiver.Items.Insert(0, New ListItem("-- Select -- ", ""))

        End If
    End Sub

    Private Sub FillDistributeTo()
        Dim lst As List(Of DocumentDistributeToDTO) = New List(Of DocumentDistributeToDTO)
        lst = caller2.GetAllRolesForDistributeWithFlags(DocId)
        If lst IsNot Nothing AndAlso lst.Count > 0 Then
            gvDistributeTo.DataSource = lst
            gvDistributeTo.DataBind()
        End If

    End Sub

    Private Sub FillPurpose()
        Dim lst As List(Of PurposeDTO) = New List(Of PurposeDTO)
        lst = caller3.GetAllPurpose()

        If lst.Count > 0 Then
            ddlDocumentPurpose.DataSource = lst
            ddlDocumentPurpose.DataValueField = "DocumentPurposeId"
            ddlDocumentPurpose.DataTextField = "DocumentPurpose"
            ddlDocumentPurpose.DataBind()
        End If

    End Sub

    Private Function PrepareObject() As DocumentsDTO
        Dim obj As DocumentsDTO = New DocumentsDTO

        obj.SiteID = ddlSites.SelectedValue
        obj.Site = ddlSites.SelectedItem.Text
        obj.CategoryId = ddlCategory.SelectedValue
        obj.DocumentPurposeId = ddlDocumentPurpose.SelectedValue
        obj.SenderId = ddlSender.SelectedValue
        obj.ReceiverId = ddlReceiver.SelectedValue
        obj.DateOfLetter = deDocumentDate.Value
        obj.DateReceived = deReceivedDate.Value
        obj.SubjectTitle = txtSubjectTitle.Text
        obj.SubjectDescription = txtSubjectDesc.Text
        obj.ReferenceNo = txtRefNo.Text
        obj.ReplyRefNo = txtFwdRefNo.Text
        obj.Remarks = txtRemarks.Text
        obj.CrossReferences = txtCrossReference.Text
        obj.Priority = ddlPriority.SelectedValue
        obj.Originator = txtOriginator.Text
        obj.OrganizationAddressTo = txtOrganizationAddressTo.Text

        If Session("FileList") IsNot Nothing Then
            fileDto = DirectCast(Session("FileList"), List(Of FileDTO))
        Else
            fileDto = caller.GetDocumentAttachments(DocId)
        End If

        For i As Integer = 0 To gvDistributeTo.Rows.Count - 1
            Dim row As GridViewRow = gvDistributeTo.Rows(i)

            Dim distObj = New DocumentDistributeToDTO()
            distObj.DocumentId = DocId
            distObj.RoleId = CType(DirectCast(row.FindControl("lblRoleId"), Label).Text, Integer)
            distObj.IsForAction = DirectCast(row.FindControl("chkIsForAction"), CheckBox).Checked
            distObj.IsForInfo = DirectCast(row.FindControl("chkIsForInfo"), CheckBox).Checked

            distList.Add(distObj)
        Next

        Return obj
    End Function

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles lnkSave.Click, lnkSendForApproval.Click

        Try
            Dim isSendForApproval As Boolean = DirectCast(sender, LinkButton).ID = "lnkSendForApproval"
            Dim id As Integer = 0
            Dim obj As DocumentsDTO = PrepareObject()

            If isSendForApproval Then
                If DocId = 0 Then
                    obj.Id = 0
                    obj.CreatedBy = UserProfile.UserID
                    obj.ModifiedBy = UserProfile.UserID
                    obj.WorkflowStatus = "W"
                    obj.SubmittedBy = UserProfile.UserID
                    obj.SubmittedOn = DateTime.Now

                    id = caller.CreateDocumentData(obj, fileDto, distList)
                Else
                    obj.Id = DocId
                    obj.ModifiedBy = UserProfile.UserID
                    obj.WorkflowStatus = "W"
                    obj.SubmittedBy = UserProfile.UserID
                    obj.SubmittedOn = DateTime.Now

                    id = caller.UpdateDocumentData(obj, fileDto, distList)
                End If
            Else
                If DocId = 0 Then
                    obj.Id = 0
                    obj.CreatedBy = UserProfile.UserID
                    'obj.WorkflowStatus = "N"
                    obj.WorkflowStatus = "A"

                    id = caller.CreateDocumentData(obj, fileDto, distList)
                Else
                    obj.Id = DocId
                    obj.WorkflowStatus = "A"
                    obj.ModifiedBy = UserProfile.UserID

                    id = caller.UpdateDocumentData(obj, fileDto, distList)
                End If
            End If

            ucRemarks.DocId = id
            ucRemarks.SubmitToDatabase()

            Session("isDataUnchanged") = True

            If id > 0 Then
                DocId = id
                If isSendForApproval Then
                    ShowMessageAndRedirect("<div class=""w3-center""><div class=""w3-padding w3-text-red w3-xlarge"">" +
                                            "SENT SUCCESSFULLY !!!</div><div class=""w3-padding"">Document data has been send...</div></div>", "DocumentList.aspx")
                Else
                    PageMessage = "Document data has been saved succesfully ."
                End If

            End If


        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub btnApprove_Click(sender As Object, e As System.EventArgs) Handles lnkApprove.Click
        Try
            Dim id As Integer = 0
            Dim obj As DocumentsDTO = PrepareObject()

            obj.Id = DocId
            obj.WorkflowStatus = "A"
            obj.ApprovedBy = UserProfile.UserID
            obj.ApprovedOn = DateTime.Now
            obj.ActionTakenBy = UserProfile.UserID
            obj.ActionTakenOn = DateTime.Now

            id = caller.UpdateDocumentData(obj, fileDto, distList)

            ucRemarks.DocId = id
            ucRemarks.SubmitToDatabase()

            Session("isDataUnchanged") = True

            If id > 0 Then
                ShowMessageAndRedirect("<div class=""w3-center""><div class=""w3-padding w3-text-red w3-xlarge"">" +
                                        "SAVED !!!</div><div class=""w3-padding"">Document data has been approved...</div></div>", "DocumentList.aspx")
            End If
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub btnReject_Click(sender As Object, e As System.EventArgs) Handles lnkReject.Click
        Try
            Dim id As Integer = 0
            Dim obj As DocumentsDTO = PrepareObject()

            obj.Id = DocId
            obj.WorkflowStatus = "N"
            obj.SubmittedBy = Nothing
            obj.SubmittedOn = Nothing
            obj.ModifiedBy = UserProfile.UserID

            id = caller.UpdateDocumentData(obj, fileDto, distList)

            ucRemarks.DocId = id
            ucRemarks.SubmitToDatabase()

            Session("isDataUnchanged") = True

            If id > 0 Then
                ShowMessageAndRedirect("<div class=""w3-center""><div class=""w3-padding w3-text-red w3-xlarge"">" +
                                        "SAVED !!!</div><div class=""w3-padding"">Document data has been rejected...</div></div>", "DocumentList.aspx")
            End If
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

End Class
