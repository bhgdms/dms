﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ViewDocument.aspx.vb" MaintainScrollPositionOnPostback="true" Inherits="DMS.Web.ViewDocument" %>


<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.3.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Src="~/ucRemarks.ascx" TagPrefix="uc1" TagName="ucRemarks" %>
<%@ Register Src="~/ucDocUploader.ascx" TagPrefix="uc1" TagName="ucDocUploader" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="scriptPlaceHolder" runat="server">
    <link href="../Content/css/fonts/foldericon/flaticon.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cssPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%--<asp:UpdatePanel ID="up" runat="server" >
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload"  />
        </Triggers> 
        <ContentTemplate>--%>
    <div class="pageHeader">
        <div style="width: 100%;">
            <div class="w3-padding inlineblock moveMiddle w3-left" style="width: 400px">
                <div class="Title">
                    Document Detail
                </div>
                <div class="TitleDescription">
                    Add/Modify Document details
                </div>
            </div>

        </div>
    </div>
    <div>
        <div class="w3-row">
            <div class="w3-row w3-padding">
                <div class="w3-card w3-padding w3-white">
                    <div class="w3-row w3-bottombar w3-padding-4 dialogTitle w3-medium bold">
                        Site Details
                    </div>
                    <div class="w3-row w3-padding-4"></div>
                    
                    <div class="w3-row w3-padding-4 marginLeft100">
                        <div class="inlineblock moveMiddle" style="width: 170px">Site ID : </div>
                        <div class="inlineblock moveMiddle" style="width: 320px">
                            <asp:DropDownList ID="ddlSites" runat="server" Width="292px" AutoPostBack="True"></asp:DropDownList>
                        </div>

                        <div class="inlineblock moveMiddle" style="width: 170px">Site Name : </div>
                        <div class="inlineblock moveMiddle" style="width: 180px">
                            <asp:TextBox ID="txtSiteName" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                        </div>
                    </div>
                    <div class="w3-row w3-padding-4 marginLeft100" id="rowPassoword" runat="server">
                        <div class="inlineblock moveMiddle" style="width: 170px">Consultant : </div>
                        <div class="inlineblock moveMiddle" style="width: 320px">
                            <asp:TextBox ID="txtConsultant" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                        </div>

                        <div class="inlineblock moveMiddle" style="width: 170px">Contractor : </div>
                        <div class="inlineblock moveMiddle" style="width: 180px">
                            <asp:TextBox ID="txtContractor" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                        </div>
                    </div>
                    
                    <div class="w3-row w3-padding-4 marginLeft100">
                        <div class="inlineblock moveMiddle" style="width: 170px">Client : </div>
                        <div class="inlineblock moveMiddle" style="width: 320px">
                            <asp:TextBox ID="txtClient" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                        </div>

                        <div class="inlineblock moveMiddle" style="width: 170px">Project Manager : </div>
                        <div class="inlineblock moveMiddle" style="width: 180px">
                            <asp:TextBox ID="txtProjectManager" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                        </div>
                    </div>

                    <div class="w3-row w3-padding-4 marginLeft100">
                        <div class="inlineblock moveMiddle" style="width: 170px">Site Location : </div>
                        <div class="inlineblock moveMiddle" style="width: 320px">
                            <asp:TextBox ID="txtSiteLocation" runat="server" Width="280px" MaxLength="200"></asp:TextBox>
                        </div>

                        <div class="inlineblock moveMiddle" style="width: 170px">Document Code : </div>
                        <div class="inlineblock moveMiddle" style="width: 300px">
                            <b><asp:Label ID="lblDocumentCode" Font-Size="X-Large" runat="server" Width="300px"></asp:Label></b>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="w3-row">
        <div class="w3-row w3-padding">
            <div class="w3-card w3-padding w3-white"> 

                <div class="w3-row w3-bottombar w3-padding-4 dialogTitle w3-medium bold">
                    Attachments
                </div>
                <div class="w3-row w3-padding-4"></div>

                <div class="w3-row w3-padding-4 marginLeft100">                    
                    <uc1:ucDocUploader runat="server" id="ucDocs" /> 
                </div>

            </div>
        </div>
    </div>
    <div class="w3-row">
        <div class="w3-row w3-padding">            
            <div id="divDocDetails" class="w3-card w3-padding w3-white">
                <div class="w3-row w3-bottombar w3-padding-4 dialogTitle w3-medium bold">
                    Document Details
                </div>
                <div class="w3-row w3-padding-4"></div>
                
                <div class="w3-row w3-padding-4 marginLeft100">

                    <div class="inlineblock moveMiddle" style="width: 170px">Reference No : </div>
                    <div class="inlineblock moveMiddle" style="width: 320px">
                        <asp:TextBox ID="txtRefNo" runat="server" Width="268px" MaxLength="200"></asp:TextBox>
                    </div>
                    <div class="inlineblock moveMiddle" style="width: 170px">Category : </div>
                    <div class="inlineblock moveMiddle" style="width: 320px">
                        <asp:DropDownList ID="ddlCategory" runat="server" Width="280px">
                            <asp:ListItem Text="InComing" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="OutGoing" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>

                </div>
                <div class="w3-row w3-padding-4 marginLeft100 hide">
                    
                    <div class="inlineblock moveMiddle" style="width: 170px">Document Purpose : </div>
                    <div class="inlineblock moveMiddle" style="width: 180px">
                        <asp:DropDownList ID="ddlDocumentPurpose" runat="server" Width="280px"> 
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="w3-row w3-padding-4 marginLeft100">
                    <div class="inlineblock moveMiddle" style="width: 170px">Sender : </div>
                    <div class="inlineblock moveMiddle" style="width: 320px">
                        <asp:DropDownList ID="ddlSender" runat="server" Width="280px"></asp:DropDownList>
                    </div>

                    <div class="inlineblock moveMiddle" style="width: 170px">Receiver : </div>
                    <div class="inlineblock moveMiddle" style="width: 180px">
                        <asp:DropDownList ID="ddlReceiver" runat="server" Width="280px"></asp:DropDownList>
                    </div>
                </div>
                <div class="w3-row w3-padding-4 marginLeft100">
                    <div class="inlineblock moveMiddle" style="width: 170px">Originator : </div>
                    <div class="inlineblock moveMiddle" style="width: 320px">
                        <asp:TextBox ID="txtOriginator" runat="server" Width="268px" MaxLength="200"></asp:TextBox>
                    </div>

                    <div class="inlineblock moveMiddle" style="width: 170px">Addressed to : </div>
                    <div class="inlineblock moveMiddle" style="width: 180px">
                        <asp:TextBox ID="txtOrganizationAddressTo" runat="server" Width="268px" MaxLength="200"></asp:TextBox>
                    </div>
                </div>
                
                <div class="w3-row w3-padding-4 marginLeft100">
                    <div class="inlineblock moveMiddle" style="width: 170px">Date of Letter : </div>
                    <div class="inlineblock moveMiddle" style="width: 320px">
                        <dx:ASPxDateEdit DisplayFormatString="dd/MM/yyyy" EditFormatString="dd/MM/yyyy" ID="deDocumentDate" ClientInstanceName="deDocumentDate" runat="server" Width="280px" EnableTheming="True" Theme="Glass">
                            <CalendarProperties>
                            </CalendarProperties>
                        </dx:ASPxDateEdit>
                    </div>

                    <div class="inlineblock moveMiddle" style="width: 170px">Received Date : </div>
                    <div class="inlineblock moveMiddle" style="width: 320px">
                        <dx:ASPxDateEdit DisplayFormatString="dd/MM/yyyy" EditFormatString="dd/MM/yyyy" ID="deReceivedDate" ClientInstanceName="deReceivedDate" runat="server" Width="280px" EnableTheming="True" Theme="Glass">
                            <CalendarProperties>
                            </CalendarProperties>
                        </dx:ASPxDateEdit>
                    </div>
                </div>

                <div class="w3-row w3-padding-4 marginLeft100">

                    <div class="inlineblock moveTop" style="width: 170px">Subject : </div>
                    <div class="inlineblock moveMiddle" style="width: 320px">
                        <asp:TextBox ID="txtSubjectTitle" runat="server" TextMode="MultiLine" Height="60px" Width="274px" MaxLength="200"></asp:TextBox>
                    </div>

                    <div class="inlineblock moveTop" style="width: 170px">Reply / Forwarding Reference No : </div>
                    <div class="inlineblock moveTop" style="width: 320px">
                        <asp:TextBox ID="txtFwdRefNo" runat="server" TextMode="MultiLine" Height="60px" Width="274px" MaxLength="200"></asp:TextBox>
                    </div>
                </div>
                <div class="w3-row w3-padding-4 marginLeft100">

                    <div class="inlineblock moveTop" style="width: 170px">Important Subject : </div>
                    <div class="inlineblock moveMiddle" style="width: 320px">
                        <asp:TextBox ID="txtSubjectDesc" runat="server" TextMode="MultiLine" Height="60px" Width="274px" MaxLength="200"></asp:TextBox>
                    </div>
                    <div class="inlineblock moveTop" style="width: 170px;">Cross Reference : </div>
                    <div class="inlineblock moveMiddle" style="width: 320px">
                        <asp:TextBox ID="txtCrossReference" runat="server" TextMode="MultiLine" Height="60px" Width="274px" MaxLength="200"></asp:TextBox>
                    </div>
                </div>
                <div class="w3-row w3-padding-4 marginLeft100">

                    <div class="inlineblock moveTop" style="width: 170px">Secretary Notes : </div>
                    <div class="inlineblock moveMiddle" style="width: 320px">
                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Height="60px" Width="274px" MaxLength="200"></asp:TextBox>
                    </div>
                    <div class="inlineblock moveTop" style="width: 170px;">Priority : </div>
                    <div class="inlineblock moveTop" style="width: 320px">
                        <asp:DropDownList ID="ddlPriority" runat="server" Width="280px">
                            <asp:ListItem Text="-- Select --" Value=""></asp:ListItem>
                            <asp:ListItem Text="High" Value="High"></asp:ListItem>
                            <asp:ListItem Text="Normal" Value="Normal"></asp:ListItem>
                            <asp:ListItem Text="Low" Value="Low" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>

                </div>
                <div class="w3-row w3-padding-4 marginLeft100">
                </div> 
                <div class="w3-row w3-padding-4 marginLeft100 hide" id="divActionRequiredBy" runat="server">

                    <div class="inlineblock moveMiddle" style="width: 150px">Required Action By : </div>
                    <div class="inlineblock moveMiddle" style="width: 320px">
                        <asp:DropDownList ID="ddlActionRequiredBy" runat="server" Width="280px"></asp:DropDownList>
                    </div>
                     
                </div>
            </div>         
        </div>
    </div> 
    
    <div class="w3-row">        
        <div class="w3-row w3-padding">
            <div id="divDistribute" class="w3-card w3-padding w3-white">
                <div class="w3-row w3-bottombar w3-padding-4 dialogTitle w3-medium bold">
                    Document Distribute To
                </div>
                <div class="w3-row w3-padding-4"></div>
                <div class="w3-row w3-padding-4 marginLeft100">
                    <div class="inlineblock moveTop" style="width: 170px">Distribute To :</div>
                    <div class="inlineblock moveMiddle" style="width: 800px;">
                        <%--<asp:CheckBoxList ID="chkDistributeTo" Visible="false" runat="server" RepeatDirection="Vertical">
                        </asp:CheckBoxList>--%>
                        <asp:GridView ID="gvDistributeTo" runat="server" AutoGenerateColumns="False" Width="80%">
                            <RowStyle CssClass="DataGridItem" />
                            <HeaderStyle CssClass="DataGridHeader" />
                            <AlternatingRowStyle CssClass="DataGridAlternateItem" />
                            <EmptyDataTemplate>
                                <div class="w3-center w3-card w3-padding w3-pale-yellow emptyList">
                                    <div class="w3-padding-4">
                                        <<< NO distribute list found >>>
                                    </div>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="RoleId" ItemStyle-Width="5%" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoleId" runat="server" Width="200px" Text='<%# Bind("RoleId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Roles" ItemStyle-Width="55%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoleDesc" runat="server" Width="200px" Text='<%# Bind("RoleDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="For Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkIsForAction" runat="server" Width="15px" Checked='<%# Bind("IsForAction") %>'></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="For Info" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkIsForInfo" runat="server" Width="15px" Checked='<%# Bind("IsForInfo") %>'></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="w3-row">
        <div class="w3-row w3-padding">
            <div class="w3-card w3-padding w3-white">
                <div class="w3-row w3-bottombar w3-padding-4 dialogTitle w3-medium bold">
                    Remarks
                </div> 
                <div class="w3-row w3-padding-4">

                </div>
                <div class="w3-row w3-padding-4 marginLeft100">
                    <div class="inlineblock moveTop" style="width: 150px; margin-top:5px;">Remarks : </div>
                    <div class="inlineblock moveMiddle" style="width: 700px">
                        <uc1:ucRemarks runat="server" id="ucRemarks" />
                        
                    </div>
                </div> 
                <div class="w3-row w3-padding-4 marginLeft100 hide" id="divActionTakenBy" runat="server">

                    <div class="inlineblock moveMiddle" style="width: 150px">Last Action By : </div>
                    <div class="inlineblock moveMiddle" style="width: 320px">
                        <asp:Label ID="lblActionTakenBy" runat="server" Width="300px"></asp:Label> 
                    </div>

                    <div class="inlineblock moveMiddle" style="width: 140px">Last Action On: </div>
                    <div class="inlineblock moveMiddle" style="width: 320px">
                        <asp:Label ID="lblActionTakenOn" runat="server" Width="300px"></asp:Label> 
                    </div>                     
                </div> 
            </div>
        </div>
    </div>

    <div class="w3-row w3-padding-4 w3-center">
        <div class="inlineblock w3-padding-12">            
            <div>
                <asp:LinkButton ToolTip="Save and Go Back to List" runat="server" ID="lnkSave" CssClass="w3-btn w3-blue"><span class="adminuicon adminuicon-write19 moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Send</span></asp:LinkButton>
                <asp:LinkButton ToolTip="Approve" runat="server" ID="lnkApprove" CssClass="w3-btn w3-orange"><span class="adminuicon adminuicon-write19 moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Approve</span></asp:LinkButton>
                <asp:LinkButton ToolTip="Reject" runat="server" ID="lnkReject" CssClass="w3-btn w3-orange"><span class="adminuicon adminuicon-write19 moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Reject</span></asp:LinkButton>
                <asp:LinkButton ToolTip="Send for Approval" runat="server" ID="lnkSendForApproval" CssClass="w3-btn w3-orange"><span class="adminuicon adminuicon-write19 moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Send For Approval</span></asp:LinkButton>
                <asp:LinkButton ToolTip="Back to List" runat="server" ID="lnkBackToList" OnClientClick="window.location.href='/Home.aspx'; return false;" CssClass="w3-btn w3-grey"><span class="adminuicon adminuicon-verify8 moveMiddle"></span><span class="w3-padding-ver-8 moveMiddle">Back to List</span></asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnStatus" runat="server" />
    <asp:HiddenField ID="hdnUserRole" runat="server" />
    <%If False Then%>
    <script language="javascript" type="text/javascript" src="/script/common.js">
    </script>
    <script language="javascript" type="text/javascript" src="/script/jquery-vsdoc.js">
    </script>
    <%End If%>

    <style>
        .disabledbutton {
            pointer-events: none; 
        }
    </style>
    <script type="text/javascript">  
        var status = $('#<%=hdnStatus.ClientID %>').val();
        var userRoleId = $('#<%=hdnUserRole.ClientID %>').val();

        if ((userRoleId == '3' && status == "N") || (userRoleId == '4' && status == "W")) {
            $("#divDistribute").removeClass("disabledbutton");
        }
        else {
            $("#divDistribute").addClass("disabledbutton");
        }

        if ((status == "W") || (status == "A")) {
            $("#divDocDetails").addClass("disabledbutton");
        }
        else {
            $("#divDocDetails").removeClass("disabledbutton");
        }

          
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
            init_page_events_and_controls();
        });

        function init_page_events_and_controls() {
            $('#<%=lnkSave.ClientID%>').on('click', function () {
                Validation.init();

                if ($('.emptyList').length > 0) {
                    $.showMessage('Attachment is missing. Kindly add atleast one attachment.');
                    return false;
                }

                var chkActionlength = $("table[id*=<%= gvDistributeTo.ClientID %>] input[type=checkbox][id*=chkIsForAction]:checked").length;
                var chkInfolength = $("table[id*=<%= gvDistributeTo.ClientID %>] input[type=checkbox][id*=IsForInfo]:checked").length;
                if (chkActionlength == 0 && chkInfolength == 0) {
                    $.showMessage('No selection made in Distribute to, Kindly select atleast one.');
                    return false;
                } 

                <%--if ($('[id$=<%=chkDistributeTo.ClientID%>] input:checked').length == 0) {
                    $.showMessage('No selection made in Distribute to, Kindly select atleast one.');
                    return false;
                }--%>

                if (status == "A" && '<%= ucRemarks.OldRowCount %>' == $('#<%=ucRemarks.FindControl("hdnCount").ClientID() %>').val()) {
                    $.showMessage('Please add your remarks before save');
                    return false;
                }  

                Validation.addRequired('<%=ddlSites.ClientID%>', 'Select Site');
                Validation.addRequired('<%=txtSubjectTitle.ClientID%>', 'Enter Subject title');
                Validation.addRequired('<%=ddlSender.ClientID%>', 'Select Sender');
                Validation.addRequired('<%=ddlReceiver.ClientID%>', 'Select Receiver');

                if (Validation.execute()) {
                    return $.confirmMessage('<div class="w3-center"><div class="w3-padding w3-text-red w3-xlarge">ATTENTION !!!</div><div class="w3-padding">Do you want to Save this detail?</div></div>', this);
                }
                else {
                    return false;
                }
            });
            $('#<%=lnkSendForApproval.ClientID%>').on('click', function () {
                Validation.init();

                if ($('.emptyList').length > 0) {
                    $.showMessage('Attachment is missing. Kindly add atleast one attachment.');
                    return false;
                }

                var chkActionlength = $("table[id*=<%= gvDistributeTo.ClientID %>] input[type=checkbox][id*=chkIsForAction]:checked").length;
                var chkInfolength = $("table[id*=<%= gvDistributeTo.ClientID %>] input[type=checkbox][id*=IsForInfo]:checked").length;
                if (chkActionlength == 0 && chkInfolength == 0) {
                    $.showMessage('No selection made in Distribute to, Kindly select atleast one.');
                    return false;
                } 

                if ('<%= ucRemarks.OldRowCount %>' == $('#<%=ucRemarks.FindControl("hdnCount").ClientID() %>').val()) {
                    $.showMessage('Please add your remarks before send for approval');
                    return false;
                }   

                Validation.addRequired('<%=ddlSites.ClientID%>', 'Select Site');
                Validation.addRequired('<%=txtSubjectTitle.ClientID%>', 'Enter Subject title');
                Validation.addRequired('<%=ddlSender.ClientID%>', 'Select Sender');
                Validation.addRequired('<%=ddlReceiver.ClientID%>', 'Select Receiver');

                if (Validation.execute()) {
                    return $.confirmMessage('<div class="w3-center"><div class="w3-padding w3-text-red w3-xlarge">ATTENTION !!!</div><div class="w3-padding">Do you want to Save this detail?</div></div>', this);
                }
                else {
                    return false;
                }
            });

            $('#<%=lnkApprove.ClientID%>').on('click', function () {
                Validation.init();

                if ($('.emptyList').length > 0) {
                    $.showMessage('Attachment is missing. Kindly add atleast one attachment.');
                    return false;
                }

                var chkActionlength = $("table[id*=<%= gvDistributeTo.ClientID %>] input[type=checkbox][id*=chkIsForAction]:checked").length;
                var chkInfolength = $("table[id*=<%= gvDistributeTo.ClientID %>] input[type=checkbox][id*=IsForInfo]:checked").length;
                if (chkActionlength == 0 && chkInfolength == 0) {
                    $.showMessage('No selection made in Distribute to, Kindly select atleast one.');
                    return false;
                } 

                if ('<%= ucRemarks.OldRowCount %>' == $('#<%=ucRemarks.FindControl("hdnCount").ClientID() %>').val()) {
                    $.showMessage('Please add your remarks before save');
                    return false;
                } 

                if (Validation.execute()) {
                    return $.confirmMessage('<div class="w3-center"><div class="w3-padding w3-text-red w3-xlarge">ATTENTION !!!</div><div class="w3-padding">Do you want to Approve this detail?</div></div>', this);
                }
                else {
                    return false;
                }
            });
        }
    </script>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
