﻿Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Data
Imports DMS.DataContext.DataAccessHelper
Imports DMS.Models.Common

Public Class DocumentList
    Inherits BasePage

    Dim caller As Documents
    Protected ReadOnly Property GetDetailUrl(DID$) As String
        Get
            Dim query As New QueryString("PARAM")
            query.Add("DID", DID)
            Return query.Generate
            'hg
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            caller = New Documents()
            If Not IsPostBack Then
                If UserProfile.RoleID = UserRoles.SITE_USER OrElse UserProfile.RoleID = UserRoles.SECRETARY OrElse UserProfile.RoleID = UserRoles.SUPER_ADMINISTRATION Then
                    gridDocuments.Toolbars(0).Items(2).Visible = True
                Else
                    gridDocuments.Toolbars(0).Items(2).Visible = False
                End If
            End If
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Protected Sub grd_ToolbarItemClick(source As Object, e As ASPxGridViewToolbarItemClickEventArgs) Handles gridDocuments.ToolbarItemClick
        Try
            Dim grid As ASPxGridView = DirectCast(source, ASPxGridView)
            Dim tempFolder As String = PHYSICAL_TEMP_FOLDER + "\TEMP" + "\" + Session.SessionID
            If Not System.IO.Directory.Exists(tempFolder) Then System.IO.Directory.CreateDirectory(tempFolder)

            Select Case e.Item.Name
                Case "CustomExportToXLSX"
                    e.Handled = True
                    Dim fileName As String = "Documents" + ".xlsx"
                    Dim memStream As New MemoryStream
                    Try
                        grid.ExportToXlsx(memStream, New DevExpress.XtraPrinting.XlsxExportOptionsEx() With {.ExportType = .ExportType.WYSIWYG})
                        If Not Directory.Exists(Server.MapPath("~/TEMP/" + Session.SessionID)) Then
                            Directory.CreateDirectory(Server.MapPath("~/TEMP/" + Session.SessionID))
                        End If

                        Using fs As New FileStream(Server.MapPath("~/TEMP/" + Session.SessionID + "/" + fileName), FileMode.Create)
                            memStream.WriteTo(fs)
                            fs.Close()
                        End Using

                        RunScript("window.open('../../../Temp/" + Session.SessionID + "/" + fileName + "','_blank');")
                    Catch ex As Exception
                    Finally
                        If (memStream IsNot Nothing) Then
                            memStream.Close()
                            memStream.Dispose()
                            memStream = Nothing
                        End If
                    End Try

                Case "CustomExportToPDF"
                    e.Handled = True
                    Dim fileName As String = "Documents" + ".pdf"
                    Dim memStream As New MemoryStream
                    Try
                        grid.ExportToPdf(memStream)

                        Using fs As New FileStream(Server.MapPath("~/TEMP/" + Session.SessionID + "/" + fileName), FileMode.Create)
                            memStream.WriteTo(fs)
                            fs.Close()
                        End Using

                        RunScript("window.open('../../../Temp/" + Session.SessionID + "/" + fileName + "','_blank');")
                    Catch ex As Exception
                    Finally
                        If (memStream IsNot Nothing) Then
                            memStream.Close()
                            memStream.Dispose()
                            memStream = Nothing
                        End If
                    End Try

            End Select
        Catch ex As Exception
            Me.LogException(ex)
        End Try
    End Sub

    Private Sub odsDocuments_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs) Handles odsDocuments.Selecting
        e.InputParameters("roleId") = UserProfile.RoleID
        e.InputParameters("siteId") = UserProfile.SiteID
    End Sub

    Private Sub gridDocuments_DataBound(sender As Object, e As EventArgs) Handles gridDocuments.DataBound
        If UserProfile.RoleID = UserRoles.SECRETARY OrElse UserProfile.RoleID = UserRoles.SITE_USER OrElse UserProfile.RoleID = UserRoles.SUPER_ADMINISTRATION Then
            gridDocuments.Columns("IsReponded").Visible = True
        Else
            gridDocuments.Columns("IsReponded").Visible = False
        End If


    End Sub
End Class