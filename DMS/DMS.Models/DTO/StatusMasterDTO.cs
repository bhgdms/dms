﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    public class StatusMasterDTO
    {
        public int StatusId { get; set; }
        public string StatusCode { get; set; }
        public string ModuleCode { get; set; }
        public string StatusDescription { get; set; }
        public string ForeColor { get; set; }
        public string BackColor { get; set; }
        public string ColorClass { get; set; }
    }
}
