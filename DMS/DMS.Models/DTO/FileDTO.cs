﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    public class FileDTO
    {        
        public int FileId { get; set; }
        public int DocumentId { get; set; }
        public int TemporaryFileId { get; set; }
        public string FileName { get; set; }
        public string TemporaryFileName { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public string FilePath { get; set; }
        public DateTime? UploadedOn { get; set; }
        public string UploadedBy { get; set; }
    }
}
