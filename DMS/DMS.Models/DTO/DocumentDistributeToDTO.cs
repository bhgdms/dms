﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    public class DocumentDistributeToDTO
    {
        public int DocumentId { get; set; }
        public int RoleId { get; set; }
        public string RoleDescription { get; set; }
        public bool IsSelected { get; set; }
        public bool IsForAction { get; set; }
        public bool IsForInfo { get; set; }
    }
}
