﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    public class RoleDTO
    {
        public short RoleId { get; set; }
        public string Description { get; set; }
        public string Active { get; set; }
    }
}
