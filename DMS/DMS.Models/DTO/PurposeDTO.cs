﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    public class PurposeDTO
    {
        public int DocumentPurposeId { get; set; }
        public string DocumentPurpose { get; set; }
        public Nullable<bool> Active { get; set; }
    }
}
