﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    public class WorkflowHeaderDTO
    {
        public int WorkflowId { get; set; }
        public string ModuleName { get; set; }
        public string ModuleCode { get; set; }
    }
}
