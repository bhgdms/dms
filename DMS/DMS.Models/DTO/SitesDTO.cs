﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    public class SiteDTO
    {
        public int ID { get; set; }
        public string SiteID { get; set; }
        public string SiteName { get; set; }
        public Nullable<bool> IsHeadOffice { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string POBoxNo { get; set; }
        public string Consultant { get; set; }
        public string Contractor { get; set; }
        public string Client { get; set; }
        public string ProjectManager { get; set; }
        public Nullable<bool> Active { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}
