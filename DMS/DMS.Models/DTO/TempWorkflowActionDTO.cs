﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    public class TempWorkflowActionDTO
    {
        public int ActionId { get; set; }
        public string SessionId { get; set; }
        public string ModuleCode { get; set; }
        public Nullable<int> StageId { get; set; }
        public Nullable<int> LevelId { get; set; }
        public Nullable<int> RoleId { get; set; }
        public string UserId { get; set; }
        public Nullable<bool> IsCreate { get; set; }
        public Nullable<bool> IsEdit { get; set; }
        public Nullable<bool> IsApprove { get; set; }
        public Nullable<bool> IsPost { get; set; }
        public Nullable<int> LowerLimit { get; set; }
        public Nullable<int> UpperLimit { get; set; }
        public string ActionStatus { get; set; } 
        public string Designation { get; set; }
        public string UserName { get; set; }
    }
}
