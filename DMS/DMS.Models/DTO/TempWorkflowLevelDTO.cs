﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    public class TempWorkflowLevelDTO
    {
        public int LevelId { get; set; }
        public string SessionId { get; set; }
        public Nullable<int> StageId { get; set; }
        public Nullable<int> LevelNo { get; set; }
        public string LevelTitle { get; set; }
        public string LevelStatus { get; set; }
        public Nullable<int> MapLevelId { get; set; }
    }
}
