﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    public class DocumentsDTO
    {
        public int Id { get; set; }
        public Nullable<int> SiteID { get; set; }
        public string Site { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public string Category { get; set; }
        public Nullable<int> SenderId { get; set; }
        public string Sender { get; set; }
        public Nullable<int> ReceiverId { get; set; }
        public string Receiver { get; set; }
        public string ReferenceNo { get; set; }
        public Nullable<System.DateTime> DateOfLetter { get; set; }
        public Nullable<System.DateTime> DateReceived { get; set; }
        public Nullable<int> DocumentPurposeId { get; set; }
        public string DocumentPurpose { get; set; }
        public string WorkflowStatus { get; set; }
        public string WorkflowStatusDescription { get; set; }
        public Nullable<int> StatusId { get; set; }
        public string StatusDesc { get; set; }
        public string SubjectTitle { get; set; }
        public string SubjectDescription { get; set; }
        public Nullable<int> RequiredActionId { get; set; }
        public string RequiredActionDesc { get; set; }
        public string ActionTakenBy { get; set; }
        public string ActionTakenDesc { get; set; }
        public Nullable<System.DateTime> ActionTakenOn { get; set; }
        public string ReplyRefNo { get; set; }
        public string Remarks { get; set; }
        public string CrossReferences { get; set; }
        public string OperationalRemarks { get; set; } 
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string SubmittedTo { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string DocumentCode { get; set; }
        public bool? IsHeadOfficeSite { get; set; }
        public string Priority { get; set; }
        public string Originator { get; set; }
        public string OrganizationAddressTo { get; set; }
        public bool IsReponded { get; set; }
    }
}
