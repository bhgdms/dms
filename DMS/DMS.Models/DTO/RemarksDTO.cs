﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    [Serializable]
    public class RemarksDTO
    {
        public int Id { get; set; }
        public Nullable<int> DocumentId { get; set; }
        public string Remarks { get; set; }
        public Nullable<System.DateTime> RemarksOn { get; set; }
        public string DoneBy { get; set; }
    }
}
