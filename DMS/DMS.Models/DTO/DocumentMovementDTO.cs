﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    public class DocumentMovementDTO
    {
        public int MovementId { get; set; }
        public string MovementCode { get; set; }
        public string MovementName { get; set; }
        public string Description { get; set; }
        public Nullable<bool> Active { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> SiteID { get; set; }
    }
}
