﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    public class WorkflowStageDTO
    {
        public int StageId { get; set; }
        public Nullable<int> WorkflowId { get; set; }
        public string StageCode { get; set; }
        public string StageTitle { get; set; }
        public Nullable<int> OrderLevel { get; set; }
        public string StageStatus { get; set; }
        public string StageStatusDescription { get; set; }
        public Nullable<bool> MailConfirmation { get; set; }
        public string MailTemplateLevel { get; set; }
        public string MailTemplateApprove { get; set; }
        public string MailTemplateReject { get; set; }
        public string MailTemplateExceptional { get; set; }
    }
}
