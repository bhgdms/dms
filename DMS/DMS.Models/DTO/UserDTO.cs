﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.DTO
{
    public class UserDTO
    {
        public int UID { get; set; }
        public int SiteID { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public short? RoleId { get; set; }
        public string Role { get; set; } 
        public string SiteName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public bool? Blocked { get; set; }
        public bool? Active { get; set; }
        public DateTime? CurrentAccessed { get; set; }
        public DateTime? LastAccessed { get; set; }
        public int? NoOfAttempt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public DateTime? LastPasswordChanged { get; set; }
    }
}
