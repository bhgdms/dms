﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Models.Common
{ 
    public enum UserStatus
    {
        Active = 0,
        BLOCKED = 1,
        USER_INCORRECT = 2,
        NOT_EXIST = 3,
    }

    public enum UserRoles
    {
        SUPER_ADMINISTRATION = 1,
        SITE_USER = 2,
        SECRETARY = 3,
        PROJECT_MANAGER = 4,
        OPERATIONAL_MANAGER = 5,
        QS_MANAGER = 6,
        PLANING_MANAGER = 7,
        COMMERCIAL_MANAGER = 8,
        SAFETY_MANAGER = 9,
        COST_CONTROL_MANAGER = 10,
        FINANCE_MANAGER = 11
    }

    public static class ModuleCodes
    {
        public static string DOC_STATUS = "DOC_STATUS";
        public static string DOC_ACTION = "DOC_ACTION";
    }

    public class EmailMessages
    {
        public const string PASSWORDRESET = "Your password has been reset.<br><br>Your temporary password is <b>{0}</b><br><br>It is recommended that you should change your password after login.";
        public const string FORGOT_PASSWORD = "Your credential to access the portal is as follow:<br><br>Your password is <b>{1}</b><br><br>It is recommended that you should change your password after login.";
        public const string PORTAL_ACCESS = "Your credential to access the portal is as follow:<br><br>Your login Id is <b>{0}</b><br><br>Your password is <b>{1}</b><br><br>It is recommended that you should change your password after login.";
        public const string ACCOUNTUNBLOCKED = "Your account has been unblocked.";
        public const string ACCOUNTBLOCKED = "Your account has been blocked.<br>For more detail, please contact administrator.";
        public const string ACCOUNTACTIVATED = "Your account is activated now.";
        public const string ACCOUNTDEACTIVATED = "Your account is deactived.<br>For more detail, please contact administrator.";
        public const string ACCOUNTCREATED = "Dear {0},<br>Your account has been created.<br><br>Your login details are as follow:<br><b>User Id:</b> {1}<br><b>Password:</b> {2}<br><br>Account created by: {3}<br>Date &amp; Time: {4}<br><br>Click the following url for logging in to PROMAN<br><a href='{5}'>{5}</a>";
        public const string ACCOUNTMODIFIED = "Dear {0},<br>Your account information has been modified.<br><br>Account modified by: {1}<br>Date &amp; Time: {2}<br><br>For further detail, please contact administrator.";

    }
}
