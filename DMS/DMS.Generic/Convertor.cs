﻿using System;

namespace DMS.Generic
{
    public class Convertor
    {
        public Convertor()
        {
        }

        public static object GetDBNullIfNullOrEmpty(string stringValue)
        {
            object value;
            try
            {
                if (stringValue != null)
                {
                    stringValue = stringValue.Trim();
                    if (stringValue != string.Empty)
                    {
                        value = stringValue;
                        return value;
                    }
                }
                value = DBNull.Value;
            }
            catch
            {
                value = null;
            }
            return value;
        }

        public static string GetNothingIfEmpty(string stringValue)
        {
            string str;
            try
            {
                if (stringValue != null)
                {
                    stringValue = stringValue.Trim();
                    if (stringValue != string.Empty)
                    {
                        str = stringValue;
                        return str;
                    }
                }
                str = null;
            }
            catch
            {
                str = null;
            }
            return str;
        }

        public static string GetNothingIfZero(string stringValue)
        {
            string str;
            string str1;
            try
            {
                if (stringValue == "0")
                {
                    str1 = null;
                }
                else
                {
                    str1 = stringValue;
                }
                str = str1;
            }
            catch
            {
                throw;
            }
            return str;
        }

        public static string GetYesNo(bool BooleanValue)
        {
            string str;
            try
            {
                str = ((BooleanValue ? "Y" : "N")).ToString();
            }
            catch
            {
                throw;
            }
            return str;
        }
    }
}