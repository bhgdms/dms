﻿using System;
using System.Web;
using System.Web.SessionState;

namespace DMS.Generic
{
    public class UserProfile
    {        
        public static bool IsSessionExpired
        {
            get
            {
                return HttpContext.Current.Session["USER_PROFILE_DATA"] == null;
            }
        } 

        public static string Password
        {
            get
            {
                if (UserProfile.IsSessionExpired)
                {
                    return string.Empty;
                }
                return ((UserProfile.UserProfileData)HttpContext.Current.Session["USER_PROFILE_DATA"]).Password;
            }
        } 

        public static string RoleDescription
        {
            get
            {
                if (UserProfile.IsSessionExpired)
                {
                    return string.Empty;
                }
                return ((UserProfile.UserProfileData)HttpContext.Current.Session["USER_PROFILE_DATA"]).RoleDescription;
            }
        }

        public static int? RoleID
        {
            get
            {
                if (UserProfile.IsSessionExpired)
                {
                    return null;
                }
                return new int?(((UserProfile.UserProfileData)HttpContext.Current.Session["USER_PROFILE_DATA"]).RoleID);
            }
        }

        public static int? SiteID
        {
            get
            {
                if (UserProfile.IsSessionExpired)
                {
                    return null;
                }
                return new int?(((UserProfile.UserProfileData)HttpContext.Current.Session["USER_PROFILE_DATA"]).SiteID);
            }
        }

        public static string UserID
        {
            get
            {
                if (UserProfile.IsSessionExpired)
                {
                    return string.Empty;
                }
                return ((UserProfile.UserProfileData)HttpContext.Current.Session["USER_PROFILE_DATA"]).UserID;
            }
        }

        public static string UserName
        {
            get
            {
                if (UserProfile.IsSessionExpired)
                {
                    return string.Empty;
                }
                return ((UserProfile.UserProfileData)HttpContext.Current.Session["USER_PROFILE_DATA"]).UserName;
            }
        }

        public static bool Blocked
        {
            get
            {
                if (UserProfile.IsSessionExpired)
                {
                    return false;
                }
                return ((UserProfile.UserProfileData)HttpContext.Current.Session["USER_PROFILE_DATA"]).Blocked;
            }
        }
        
        public UserProfile()
        {
        }         

        public static void Reset()
        {
            HttpContext.Current.Session["USER_PROFILE_DATA"] = null;
        }

        public void SetProfile(string userID, string userName, string password, int roleID, string roleDescription, bool blocked, int siteId)
        {
            try
            {
                if (HttpContext.Current.Session["USER_PROFILE_DATA"] == null)
                {
                    HttpContext.Current.Session["USER_PROFILE_DATA"] = new UserProfile.UserProfileData();
                }
                UserProfile.UserProfileData item = (UserProfile.UserProfileData)HttpContext.Current.Session["USER_PROFILE_DATA"];                
                item.UserID = userID;
                item.UserName = userName;
                item.Password = password;
                item.RoleID = roleID;
                item.RoleDescription = roleDescription;
                item.Blocked = blocked;
                item.SiteID = siteId;
                HttpContext.Current.Session["USER_PROFILE_DATA"] = item;
            }
            catch
            {
                throw;
            }
        }

        private class UserProfileData
        {
            public string UserID;

            public string UserName;

            public int RoleID;

            public int SiteID;

            public string RoleDescription;

            public string Password;

            public bool Blocked;

            public UserProfileData()
            {
            }
        }
    }
}
